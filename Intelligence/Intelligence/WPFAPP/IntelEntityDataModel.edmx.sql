




---- -----------------------------------------------------------
---- Entity Designer DDL Script for MySQL Server 4.1 and higher
---- -----------------------------------------------------------
---- Date Created: 06/24/2015 15:03:33
---- Generated from EDMX file: C:\Users\lewis.crawford\Documents\intelligence\WorkingMantisWPFAPP\WPFAPP\WPFAPP\IntelEntityDataModel.edmx
---- Target version: 3.0.0.0
---- --------------------------------------------------


---- --------------------------------------------------
---- Dropping existing FOREIGN KEY constraints
---- NOTE: if the constraint does not exist, an ignorable error will be reported.
---- --------------------------------------------------

---- --------------------------------------------------
---- Dropping existing tables
---- --------------------------------------------------
--SET foreign_key_checks = 0;
--SET foreign_key_checks = 1;

---- --------------------------------------------------
---- Creating all tables
---- --------------------------------------------------

--CREATE TABLE `Platforms`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` varchar (30) NOT NULL, 
--	`active` bool NOT NULL);

--ALTER TABLE `Platforms` ADD PRIMARY KEY (id);




--CREATE TABLE `Products`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` varchar (30) NOT NULL, 
--	`active` bool NOT NULL);

--ALTER TABLE `Products` ADD PRIMARY KEY (id);




--CREATE TABLE `TaskInstances`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`timeCreated` datetime NOT NULL, 
--	`timeAssigned` datetime, 
--	`timeStarted` datetime, 
--	`timeFinished` datetime, 
--	`timeDeadline` datetime, 
--	`comments` varchar (250), 
--	`platform_id` int NOT NULL, 
--	`assignee_id` int, 
--	`assigner_id` int, 
--	`version_id` int NOT NULL, 
--	`taskTemplate_id` int NOT NULL, 
--	`resolution_id` int NOT NULL, 
--	`priority_id` int NOT NULL);

--ALTER TABLE `TaskInstances` ADD PRIMARY KEY (id);




--CREATE TABLE `IntelligenceUsers`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`username` varchar (30) NOT NULL, 
--	`email` varchar (30) NOT NULL, 
--	`accessLevel_id` int NOT NULL);

--ALTER TABLE `IntelligenceUsers` ADD PRIMARY KEY (id);




--CREATE TABLE `Versions`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` varchar (30), 
--	`timeCreated` datetime NOT NULL, 
--	`timeClosed` longtext, 
--	`comments` longtext NOT NULL, 
--	`product_id` int NOT NULL, 
--	`creator_id` int NOT NULL, 
--	`resolution_id` int NOT NULL);

--ALTER TABLE `Versions` ADD PRIMARY KEY (id);




--CREATE TABLE `TaskTemplates`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` varchar (30) NOT NULL, 
--	`description` varchar (30) NOT NULL, 
--	`active` bool NOT NULL, 
--	`product_id` int NOT NULL, 
--	`parentTask_id` int);

--ALTER TABLE `TaskTemplates` ADD PRIMARY KEY (id);




--CREATE TABLE `Bugs`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`mantisID` int NOT NULL, 
--	`taskInstance_id` int NOT NULL);

--ALTER TABLE `Bugs` ADD PRIMARY KEY (id);




--CREATE TABLE `AccessLevels`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` longtext NOT NULL, 
--	`description` longtext NOT NULL, 
--	`active` longtext NOT NULL);

--ALTER TABLE `AccessLevels` ADD PRIMARY KEY (id);




--CREATE TABLE `Resolutions`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` longtext NOT NULL, 
--	`description` longtext NOT NULL, 
--	`active` longtext NOT NULL);

--ALTER TABLE `Resolutions` ADD PRIMARY KEY (id);




--CREATE TABLE `Priorities`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` longtext NOT NULL, 
--	`description` longtext NOT NULL, 
--	`active` longtext NOT NULL);

--ALTER TABLE `Priorities` ADD PRIMARY KEY (id);




--CREATE TABLE `VersionResolutions`(
--	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
--	`name` longtext NOT NULL, 
--	`description` longtext NOT NULL, 
--	`active` longtext NOT NULL);

--ALTER TABLE `VersionResolutions` ADD PRIMARY KEY (id);




--CREATE TABLE `task_templates_platforms`(
--	`taskTemplates_id` int NOT NULL, 
--	`platforms_id` int NOT NULL);

--ALTER TABLE `task_templates_platforms` ADD PRIMARY KEY (taskTemplates_id, platforms_id);






---- --------------------------------------------------
---- Creating all FOREIGN KEY constraints
---- --------------------------------------------------

---- Creating foreign key on `platform_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `fk_instances_platforms_platformid`
--    FOREIGN KEY (`platform_id`)
--    REFERENCES `Platforms`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_instances_platforms_platformid'

--CREATE INDEX `IX_fk_instances_platforms_platformid` 
--    ON `TaskInstances`
--    (`platform_id`);

---- Creating foreign key on `product_id` in table 'Versions'

--ALTER TABLE `Versions`
--ADD CONSTRAINT `fk_versions_products_productid`
--    FOREIGN KEY (`product_id`)
--    REFERENCES `Products`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_versions_products_productid'

--CREATE INDEX `IX_fk_versions_products_productid` 
--    ON `Versions`
--    (`product_id`);

---- Creating foreign key on `assignee_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `fk_instances_assigneeid_users_userid`
--    FOREIGN KEY (`assignee_id`)
--    REFERENCES `IntelligenceUsers`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_instances_assigneeid_users_userid'

--CREATE INDEX `IX_fk_instances_assigneeid_users_userid` 
--    ON `TaskInstances`
--    (`assignee_id`);

---- Creating foreign key on `assigner_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `fk_instances_assignerid_users_userid`
--    FOREIGN KEY (`assigner_id`)
--    REFERENCES `IntelligenceUsers`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_instances_assignerid_users_userid'

--CREATE INDEX `IX_fk_instances_assignerid_users_userid` 
--    ON `TaskInstances`
--    (`assigner_id`);

---- Creating foreign key on `version_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `fk_instances_versions_versionid`
--    FOREIGN KEY (`version_id`)
--    REFERENCES `Versions`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_instances_versions_versionid'

--CREATE INDEX `IX_fk_instances_versions_versionid` 
--    ON `TaskInstances`
--    (`version_id`);

---- Creating foreign key on `creator_id` in table 'Versions'

--ALTER TABLE `Versions`
--ADD CONSTRAINT `fk_versions_user_userid`
--    FOREIGN KEY (`creator_id`)
--    REFERENCES `IntelligenceUsers`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_versions_user_userid'

--CREATE INDEX `IX_fk_versions_user_userid` 
--    ON `Versions`
--    (`creator_id`);

---- Creating foreign key on `product_id` in table 'TaskTemplates'

--ALTER TABLE `TaskTemplates`
--ADD CONSTRAINT `fk_templates_products_productid`
--    FOREIGN KEY (`product_id`)
--    REFERENCES `Products`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_templates_products_productid'

--CREATE INDEX `IX_fk_templates_products_productid` 
--    ON `TaskTemplates`
--    (`product_id`);

---- Creating foreign key on `taskTemplate_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `fk_instances_templates_templateid`
--    FOREIGN KEY (`taskTemplate_id`)
--    REFERENCES `TaskTemplates`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_instances_templates_templateid'

--CREATE INDEX `IX_fk_instances_templates_templateid` 
--    ON `TaskInstances`
--    (`taskTemplate_id`);

---- Creating foreign key on `parentTask_id` in table 'TaskTemplates'

--ALTER TABLE `TaskTemplates`
--ADD CONSTRAINT `fk_templates_templates_templateid`
--    FOREIGN KEY (`parentTask_id`)
--    REFERENCES `TaskTemplates`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'fk_templates_templates_templateid'

--CREATE INDEX `IX_fk_templates_templates_templateid` 
--    ON `TaskTemplates`
--    (`parentTask_id`);

---- Creating foreign key on `taskTemplates_id` in table 'task_templates_platforms'

--ALTER TABLE `task_templates_platforms`
--ADD CONSTRAINT `FK_task_templates_platforms_task_templates`
--    FOREIGN KEY (`taskTemplates_id`)
--    REFERENCES `TaskTemplates`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating foreign key on `platforms_id` in table 'task_templates_platforms'

--ALTER TABLE `task_templates_platforms`
--ADD CONSTRAINT `FK_task_templates_platforms_platform`
--    FOREIGN KEY (`platforms_id`)
--    REFERENCES `Platforms`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_task_templates_platforms_platform'

--CREATE INDEX `IX_FK_task_templates_platforms_platform` 
--    ON `task_templates_platforms`
--    (`platforms_id`);

---- Creating foreign key on `resolution_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `FK_ResolutionTaskInstance`
--    FOREIGN KEY (`resolution_id`)
--    REFERENCES `Resolutions`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_ResolutionTaskInstance'

--CREATE INDEX `IX_FK_ResolutionTaskInstance` 
--    ON `TaskInstances`
--    (`resolution_id`);

---- Creating foreign key on `taskInstance_id` in table 'Bugs'

--ALTER TABLE `Bugs`
--ADD CONSTRAINT `FK_BugTaskInstance`
--    FOREIGN KEY (`taskInstance_id`)
--    REFERENCES `TaskInstances`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_BugTaskInstance'

--CREATE INDEX `IX_FK_BugTaskInstance` 
--    ON `Bugs`
--    (`taskInstance_id`);

---- Creating foreign key on `accessLevel_id` in table 'IntelligenceUsers'

--ALTER TABLE `IntelligenceUsers`
--ADD CONSTRAINT `FK_AccessLevelIntelligenceUser`
--    FOREIGN KEY (`accessLevel_id`)
--    REFERENCES `AccessLevels`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_AccessLevelIntelligenceUser'

--CREATE INDEX `IX_FK_AccessLevelIntelligenceUser` 
--    ON `IntelligenceUsers`
--    (`accessLevel_id`);

---- Creating foreign key on `priority_id` in table 'TaskInstances'

--ALTER TABLE `TaskInstances`
--ADD CONSTRAINT `FK_PriorityTaskInstance`
--    FOREIGN KEY (`priority_id`)
--    REFERENCES `Priorities`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_PriorityTaskInstance'

--CREATE INDEX `IX_FK_PriorityTaskInstance` 
--    ON `TaskInstances`
--    (`priority_id`);

---- Creating foreign key on `resolution_id` in table 'Versions'

--ALTER TABLE `Versions`
--ADD CONSTRAINT `FK_VersionResolutionVersion`
--    FOREIGN KEY (`resolution_id`)
--    REFERENCES `VersionResolutions`
--        (`id`)
--    ON DELETE NO ACTION ON UPDATE NO ACTION;

---- Creating non-clustered index for FOREIGN KEY 'FK_VersionResolutionVersion'

--CREATE INDEX `IX_FK_VersionResolutionVersion` 
--    ON `Versions`
--    (`resolution_id`);

---- --------------------------------------------------
---- Script has ended
---- --------------------------------------------------
