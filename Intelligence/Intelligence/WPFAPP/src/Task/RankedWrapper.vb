﻿Public MustInherit Class RankedWrapper(Of T As {Class, IHasID, New, IRanked})
	Inherits Wrapper(Of T)

	Public Sub New(itemToWrap As T, Optional db As IntelligenceDB = Nothing, Optional shouldCallInitializeFields As Boolean = True)
		MyBase.New(itemToWrap, db, shouldCallInitializeFields)
	End Sub


	''' <summary>
	''' Determines the position this wrapper occurs in a list.
	''' Ranks are not guaranteed to be strictly sequential, but
	''' will always maintain the correct ordering.
	''' </summary>
	Public ReadOnly Property rank As Integer
		Get
			Return getWrappedItem().rank
		End Get
	End Property

	''' <summary>
	''' Sets the rank for this product (which determines its position in the list).
	''' The change is put on a stack of all changes that are saved to the database in a batch
	''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
	''' </summary>
	''' <param name="newRank">
	''' The new value of the product's rank (which determines its position in the list).
	''' </param>
	''' <param name="db">The open database context to make the changes in.</param>
	Public Sub setRank(newRank As Integer, db As IntelligenceDB)
		setFieldAndMarkModified(newRank, Me.rank, Sub(v) getWrappedItem.rank = v, Function() Me.rank, db)
	End Sub
End Class
