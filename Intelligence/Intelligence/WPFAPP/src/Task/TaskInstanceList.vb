﻿Imports System.ComponentModel

''' <summary>
''' A list containing <see cref="TaskInstanceWrapper"/>s from a given version and platform.
''' This list can represent all top-level root tasks, or a list of
''' instances that are subtasks of an existing task.
''' </summary>
Public Class TaskInstanceList
    Inherits WrapperList(Of TaskInstance)

    ''' <summary>The internally stored wrapper around the version the tasks are for.</summary>
    Private _version As VersionWrapper
    ''' <summary>A wrapper around the version the tasks are for.</summary>
    Public ReadOnly Property version As VersionWrapper
        Get
            Return _version
        End Get
    End Property

    ''' <summary>The internally stored wrapper around the platform the tasks are for.</summary>
    Private _platform As Platform
    ''' <summary>A wrapper around the platform the tasks are for.</summary>
    Public ReadOnly Property platform As Platform
        Get
            Return _platform
        End Get
    End Property

    ''' <summary>The instance that the tasks in this list are subtasks of.</summary>
    Private _parent As TaskInstanceWrapper
    ''' <summary>
    ''' The instance that the tasks in this list are subtasks of.
    ''' If this is <c>Nothing</c> then the tasks in this list are top-level root tasks.
    ''' </summary>
    Public ReadOnly Property parent As TaskInstanceWrapper
        Get
            Return _parent
        End Get
    End Property

    '''<summary>Constructor for creating list of all top level tasks for a the given version and platform.</summary>
    '''<param name="version">The version the tasks are for.</param>
    '''<param name="platform">The platform the tasks are for.</param>
    '''<param name="inDB">The open database context the tasks will be loaded from initially.</param>
    Public Sub New(version As VersionWrapper, platform As Platform, Optional inDB As IntelligenceDB = Nothing)
        Me.New(version, platform, Nothing,
               Function(db As IntelligenceDB)
                   Return From task In db.TaskInstances
                          Where task.version.id = version.id _
                          AndAlso task.platform.id = platform.id _
                          AndAlso task.taskTemplate.parentTask Is Nothing
                          Order By task.taskTemplate.rank Ascending, task.taskTemplate.id Ascending
                          Select task
               End Function, inDB)
        AddHandler version.PropertyChanged, AddressOf onVersionPropertyChanged
    End Sub

    ''' <summary>
    ''' Constructor for creating list of subtasks for the given parent task.
    ''' The <paramref name="parent"/> must not be <c>Nothing</c>.
    ''' </summary>
    '''<param name="version">The version the tasks are for.</param>
    '''<param name="platform">The platform the tasks are for.</param>
    '''<param name="parent">
    ''' The instance that the tasks in this list are subtasks of.
    ''' Must not be <c>Nothing</c>. 
    '''</param>
    '''<param name="inDB">The open database context the tasks will be loaded from initially.</param>
    Public Sub New(version As VersionWrapper, platform As Platform, parent As TaskInstanceWrapper, inDB As IntelligenceDB)
        Me.New(version, platform, parent,
               Function(db As IntelligenceDB)
                   Return From subTask As TaskInstance In db.TaskInstances
                          Where version.id = subTask.version.id _
                          AndAlso subTask.platform.id = platform.id _
                          AndAlso subTask.taskTemplate.parentTask IsNot Nothing _
                          AndAlso subTask.taskTemplate.parentTask.id = parent.templateID
                          Order By subTask.taskTemplate.rank Ascending, subTask.taskTemplate.id Ascending
                          Select subTask
               End Function, inDB)
    End Sub

    ''' <summary>The internal constructor used by both top-level task lists, and subtask lists.</summary>
    '''<param name="version">The version the tasks are for.</param>
    '''<param name="platform">The platform the tasks are for.</param>
    ''' <param name="parent">
    ''' The instance that the tasks in this list are subtasks of.
    ''' If this is <c>Nothing</c> then the tasks in this list are top-level root tasks.
    ''' </param>
    ''' <param name="getContents">
    ''' A function for loading the contents from the database.
    ''' This function will be different depending on whether the list
    ''' is for top-level root tasks, or subtasks.
    ''' </param>
    Private Sub New(version As VersionWrapper, platform As Platform, parent As TaskInstanceWrapper, getContents As System.Func(Of IntelligenceDB, IEnumerable(Of TaskInstance)), inDB As IntelligenceDB)
        MyBase.New(getContents)
        Me._version = version
        Me._platform = platform
        Me._parent = parent
        Me.wrap = AddressOf wrapInstance
        reloadContents(inDB)
    End Sub

    ''' <summary>
    ''' Create a new <see cref="TaskInstanceWrapper"/> around the given task instance.
    ''' Will set up its parent wrapper, version field, and leaf-node listeners as well.
    ''' </summary>
    ''' <param name="t">The instance to wrap.</param>
    ''' <param name="db">The database context the instance retreived from.</param>
    ''' <returns>A newly created wrapper around the given instance.</returns>
    Private Function wrapInstance(t As TaskInstance, db As IntelligenceDB) As TaskInstanceWrapper
        Dim child = New TaskInstanceWrapper(t, parent, version, db)
        If parent IsNot Nothing Then
            parent.AddHandlerOnLeafNodesChanged(child)
        End If
        Return child
    End Function

    ''' <summary>
    ''' A global flag to disable reloading of all <see cref="TaskInstanceList"/>s.
    ''' This is to stop the realoading of a <see cref="VersionWrapper"/> unnecessarily reloading
    ''' any lists that will shortly be destroyed, or replaced in the UI. This flag is not
    ''' an ideal solution, but it cuts down on refresh times.
    ''' </summary>
    Public Shared allowReload As Boolean = True

    ''' <summary>Reload the list if the version's resolution changes.</summary>
    ''' <param name="Sender">The <see cref="VersionWrapper"/> that changed resolutions.</param>
    ''' <param name="e">Used to determine which of the version's property changes.</param>
    Public Sub onVersionPropertyChanged(Sender As Object, e As PropertyChangedEventArgs)
        If allowReload Then
            If NotifyPropertyChangedBase.isEventForProperty(e, Function() version.resolution) Then
                reloadContents()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Stop this list from listening to property changed events fired by
    ''' <see cref="version"/>. Used when lists are destroyed or removed from the UI.
    ''' </summary>
    Public Sub stopListeningToVersion()
        RemoveHandler version.PropertyChanged, AddressOf onVersionPropertyChanged
    End Sub

End Class
