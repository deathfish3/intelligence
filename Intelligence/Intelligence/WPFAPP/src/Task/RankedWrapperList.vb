﻿Public MustInherit Class RankedWrapperList(Of T As {Class, IHasID, New, IRanked})
	Inherits WrapperList(Of T)

	Public Sub New(contentsLoader As System.Func(Of IntelligenceDB, IEnumerable(Of T)), wrap As System.Func(Of T, IntelligenceDB, Wrapper(Of T)))
		MyBase.New(contentsLoader, wrap)
	End Sub

	Protected Sub New(contentsLoader As System.Func(Of IntelligenceDB, IEnumerable(Of T)))
		MyBase.New(contentsLoader)
	End Sub

	''' <summary>
	''' Move the given template to a position beside another template (either
	''' above or below). If the templates have different parents, the given wrapper
	''' will be copied to the new parent, and the old version will be deleted.
	''' </summary>
	''' <param name="wrapperToMove">The wrapper whose position will change.</param>
	''' <param name="wrapperBeside">The wrapper to move relative to.</param>
	''' <param name="moveAbove">
	''' If true, <paramref name="wrapperToMove"/> will be moved above
	''' <paramref name="wrapperBeside"/>. Otherwise, it will move below it.
	''' </param>
	''' <param name="db">The database context to move the tempalte in.</param>
	''' <returns>
	''' A wrapper around the newly moved template 
	''' (because this function may destroy the original wrapper, and replace it).
	''' </returns>
	Public Function moveWrapperBeside(wrapperToMove As RankedWrapper(Of T), wrapperBeside As RankedWrapper(Of T), moveAbove As Boolean, db As IntelligenceDB) As Wrapper(Of T)
		If wrapperToMove IsNot Nothing AndAlso wrapperBeside IsNot Nothing AndAlso Not wrapperToMove.Equals(wrapperBeside) Then
			If Not Me.Contains(wrapperToMove) Then
				wrapperToMove = getCopyToMoveIntoList(wrapperToMove, db)
			End If
			Dim newRank As Integer = If(moveAbove, wrapperBeside.rank, wrapperBeside.rank + 1)

			For Each wrapper As RankedWrapper(Of T) In Me.Where(Function(t As RankedWrapper(Of T)) t.rank >= newRank)
				wrapper.setRank(wrapper.rank + 1, db)
			Next

			If Not wrapperToMove Is Nothing Then
				wrapperToMove.setRank(newRank, db)
				wrapperToMove.saveChanges(db)
				Me.Remove(wrapperToMove)
				Dim indexBeside As Integer = Me.IndexOf(wrapperBeside)
				Dim indexToInsert As Integer = If(moveAbove, indexBeside, indexBeside + 1)
				Me.Insert(indexToInsert, wrapperToMove)
			End If
		End If
		Return wrapperToMove
	End Function

	Overridable Function getCopyToMoveIntoList(wrapperToMove As RankedWrapper(Of T), db As IntelligenceDB) As RankedWrapper(Of T)
		Return Nothing
	End Function
End Class
