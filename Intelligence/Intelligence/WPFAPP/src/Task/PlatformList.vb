﻿''' <summary>A list containing all active <see cref="PlatformWrapper"/>s.</summary>
Public Class PlatformList
	Inherits RankedWrapperList(Of Platform)

    ''' <summary>Create a new list of all active platforms.</summary>
    Public Sub New()
		MyBase.New(
			Function(db As IntelligenceDB) (
				   From plat As Platform In db.Platforms
				   Where plat.active = True
				   Order By plat.rank Ascending, plat.id Ascending
				   Select plat
			),
			Function(p, db) New PlatformWrapper(p)
		)
	End Sub

    ''' <summary>Initialise a newly created platform with default values.</summary>
    ''' <param name="newItem">The newly created platform.</param>
    ''' <param name="db">Unused.</param>
    Protected Overrides Sub initialiseNewItem(newItem As Platform, db As IntelligenceDB)
		newItem.active = True
		newItem.rank = If(Not Me.Any, 0, Me.Max(Function(w) w.getWrappedItem.rank)) + 1
		newItem.name = ""
    End Sub

    ''' <summary>
    ''' Delete the given platform from the database and save the changes.
    ''' If any templates are compatible with this platform, it will be removed
    ''' from their compatibility lists to stop any new tasks being generated for this platform.
    ''' If it had any instances attached to it, it will have its
    ''' <see cref="Platform.active"/> field set to <c>False</c> rather
    ''' than being permanantly deleted (to keep it around for stats and historical purposes).
    ''' </summary>
    ''' <param name="db">The open database context to delete the platform from.</param>
    ''' <param name="toDelete">A wrapper around the platform to delete.</param>
    ''' <returns><c>True</c> if successfully deleted.</returns>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of Platform)) As Boolean
        Return DBManager.deletePlatform(db, toDelete.getWrappedItem())
    End Function
End Class
