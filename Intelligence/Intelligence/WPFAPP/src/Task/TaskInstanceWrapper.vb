﻿Imports System.ComponentModel
Imports WPFAPP.TaskInstanceUtils
Imports System.Text.RegularExpressions
Imports System.Text

''' <summary>
''' A wrapper around a <see cref="TaskInstance"/> that can be
''' bound to the UI, and will update the database when changes are made.
''' Many of the fields in a <see cref="TaskInstance"/> will affect the
''' whole tree of instances for that version, so the setter fields
''' also take care of making sure the whole tree in the database is updated
''' to reflect these changes.
''' </summary>
''' <remarks>
''' The tree propagation relies heavily on methods in  <see cref="TaskInstanceUtils"/>.
''' </remarks>
Public Class TaskInstanceWrapper
    Inherits Wrapper(Of TaskInstance)
    Implements IComparable(Of TaskInstanceWrapper)

    ''' <summary>The maximum number of tasks a user may have started at once.</summary>
    Public Shared ReadOnly MAX_IN_PROGRESS_TASKS = 5

#Region "Initialization"
    '##########################################################################
    '#                             Initialization                             #
    '##########################################################################

    ''' <summary>
    ''' Wrap an instance from the given version for the given platform.
    ''' Will also load and wrap its subtasks.
    ''' </summary>
    ''' <param name="taskInstance">The instance to wrap.</param>
    ''' <param name="version">
    ''' A wrapper around the <see cref="Version"/> it came from.
    ''' This wrapper will be notified of any changes to the task that
    ''' will affect the overall status of the version (e.g resolution indicators).
    ''' </param>
    ''' <param name="db">The database context the instance came from.</param>
    Public Sub New(taskInstance As TaskInstance, version As VersionWrapper, db As IntelligenceDB)
        Me.New(taskInstance, Nothing, version, db)
    End Sub

    ''' <summary>
    ''' Wrap an instance from the given version for the given platform.
    ''' Will also load and wrap its subtasks.
    ''' </summary>
    ''' <param name="taskInstance">The instance to wrap.</param>
    ''' <param name="parent">A pre-existing wrapper around this instance's parent task.</param>
    ''' <param name="version">A wrapper around the <see cref="Version"/> it came from.</param>
    ''' <param name="db">The database context the instance came from.</param>
    Public Sub New(taskInstance As TaskInstance, parent As TaskInstanceWrapper, version As VersionWrapper, db As IntelligenceDB)
        MyBase.New(taskInstance, Nothing, False)
        Me._version = version
        Me._platform = platform
        Me._parent = parent
        initializeFields(taskInstance, db)
    End Sub

    ''' <summary>Load in all connected foreign key items, and all subtasks for this instance.</summary>
    ''' <param name="newItem">The <see cref="TaskInstance"/> to attached to an open database context.</param>
    ''' <param name="db">The database context this instance came from. Must not be <c>Nothing</c>.</param>
    Protected Overrides Sub initializeFields(newItem As TaskInstance, Optional db As IntelligenceDB = Nothing)
        MyBase.initializeFields(newItem, db)

        _platform = newItem.platform
        _priority = newItem.priority
        _resolution = newItem.resolution
        _template = newItem.taskTemplate
        _assignee = newItem.assignee
        _assigner = newItem.assigner
        _subTasks = Nothing
        _subTasks = New TaskInstanceList(version, platform, Me, db)
        Me.loadPathString()
    End Sub

#End Region

#Region "Read Only Properties"
    '##########################################################################
    '#                          Read Only Properties                          #
    '##########################################################################

    ''' <summary>
    ''' The internally stored "parent" for this instance. 
    ''' May be <c>Nothing</c>, even if this instance is not a root task.
    ''' </summary>
    Private _parent As TaskInstanceWrapper

    ''' <summary>
    ''' A wrapper around the "parent" for this instance. 
    ''' 
    ''' Instances do no have a parent field, A is a parent of B if their version and 
    ''' platform ids are the same, and A's task template is a parent of B's task template.
    ''' 
    ''' If the tree of Wrappers does not start from a root task, this will be <c>Nothing</c>
    ''' even if this is not a root node, so <see cref="TaskInstanceUtils.getParentInstance"/> 
    ''' should be used if an unwrapped parent is still required.
    ''' </summary>
    Public Property parent As TaskInstanceWrapper
        Get
            Return _parent
        End Get
        Set(value As TaskInstanceWrapper)
            _parent = value
        End Set
    End Property

    ''' <summary>The name of the <see cref="TaskTemplate"/> this instance is based on.</summary>
    Public Overrides Property name As String
        Get
            Return _template.name
        End Get
        Set(value As String)
            Throw New Exception("Cannot set task instance name directly. Must use edit the template instead.")
        End Set
    End Property

    ''' <summary>The description from the <see cref="TaskTemplate"/> this instance is based on.</summary>
    Public ReadOnly Property description As String
        Get
            Return _template.description
        End Get
    End Property

    ''' <summary>Whether this task's <see cref="resolution"/> means it is open or in-progress.</summary>
    Public ReadOnly Property isOpen As Boolean
        Get
            Return ResolutionUtils.isOpen(_resolution)
        End Get
    End Property

    ''' <summary>The internal representation of the full path from the top-most parent (not including this node).</summary>
    Private _path As New List(Of TaskTemplate)

    ''' <summary>The full path of a task from the top-most parent (not including this node).</summary>
    Public ReadOnly Property path As List(Of TaskTemplate)
        Get
            Return _path
        End Get
    End Property

    ''' <summary>The internally stored string representation of the full path from the top-most parent</summary>
    Private _pathString As String = ""

    ''' <summary>The string representation full path of a task from the top-most parent (not including this node).</summary>
    Public ReadOnly Property pathString As String
        Get
            Return _pathString
        End Get
    End Property

    ''' <summary>Load in the full path of this task instance, and its string representation.</summary>
    Private Sub loadPathString()
        Dim sb As New StringBuilder
        appendToPathFromWrapper(Me, sb)
        _pathString = sb.ToString
    End Sub

    ''' <summary>Starting from the parent of the given wrapper, build up the full path to this node.</summary>
    ''' <param name="wrapper">The wrapper who's parent to start from.</param>
    ''' <param name="sb">The stringbuilder to append the path string to.</param>
    Private Sub appendToPathFromWrapper(wrapper As TaskInstanceWrapper, sb As StringBuilder)
        If wrapper.parent IsNot Nothing Then
            path.AddRange(wrapper.parent.path)
            path.Add(wrapper.template)

            sb.Append(parent.pathString)
            sb.Append("/")
            sb.Append(parent.name)
        Else
            appendToPathFromInstance(wrapper.getWrappedItem, sb)
        End If
    End Sub

    ''' <summary>Starting from the parent of the given instance, build up the path recursively towards the root.</summary>
    ''' <param name="instance"></param>
    ''' <param name="sb"></param>
    Private Sub appendToPathFromInstance(instance As TaskInstance, sb As StringBuilder)
        Dim parent As TaskInstance = TaskInstanceUtils.getParentInstance(instance)
        If parent IsNot Nothing Then
            path.Insert(0, parent.taskTemplate)

            sb.Insert(0, parent.taskTemplate.name & "/")
            appendToPathFromInstance(parent, sb)
        End If
    End Sub

    ''' <summary>The name and description, separated by a blank line (newline is omitted if description is empty).</summary>
    Public ReadOnly Property nameAndDescription As String
        Get
            If description Is Nothing OrElse description.Length = 0 Then
                Return name
            Else
                Return name & Environment.NewLine & Environment.NewLine & description
            End If
        End Get
    End Property

    ''' <summary>A wrapper around the <see cref="Product"/> this instance came from.</summary>
    Public ReadOnly Property product As ProductWrapper
        Get
            Return version.product
        End Get
    End Property

    ''' <summary>The internally stored version that this instance came from.</summary>
    Private _version As VersionWrapper
    ''' <summary>A wrapper around the version this instace came from.</summary>
    Public ReadOnly Property version As VersionWrapper
        Get
            If _version Is Nothing Then
                Using db As New IntelligenceDB
                    Dim meFromDB As TaskInstance = db.TaskInstances.Find(Me.id)
                    _version = New VersionWrapper(meFromDB.version, db)
                End Using
            End If
            Return _version
        End Get
    End Property

    ''' <summary>The internally stored platform this task instance is for.</summary>
    Private _platform As Platform
    ''' <summary>The platform this task instance is for.</summary>
    Public ReadOnly Property platform As Platform
        Get
            Return _platform
        End Get
    End Property

    ''' <summary>The internally stored template this task instance is from.</summary>
    Private _template As TaskTemplate
    ''' <summary>The template this task instance is from.</summary>
    Public ReadOnly Property template As TaskTemplate
        Get
            Return _template
        End Get
    End Property

    ''' <summary>The id number of the template this task instance is from.</summary>
    Public ReadOnly Property templateID As Integer
        Get
            Return _template.id
        End Get
    End Property

    ''' <summary>The date and time this instance was started (or <c>Nothing</c> is it has not been started yet).</summary>
    Public ReadOnly Property timeStarted As DateTime?
		Get
			Return getWrappedItem().timeStarted
		End Get
	End Property

	Public ReadOnly Property durationInProgress As TimeSpan?
		Get
			If timeStarted Is Nothing OrElse timeFinished IsNot Nothing Then
				Return Nothing
			Else
				Dim now As DateTime = DateTime.Now()
				Dim durationOpen As TimeSpan = now - timeStarted
				Return durationOpen
			End If
		End Get
	End Property

    ''' <summary>The date and time this instance was completed (or <c>Nothing</c> is it has not been finished yet).</summary>
    Public ReadOnly Property timeFinished As DateTime?
        Get
            Return getWrappedItem().timeFinished
        End Get
    End Property

    ''' <summary>The internally stored list of all bugs found while performing this task.</summary>
    Private _bugs As BugList
    ''' <summary>The list of all bugs found while performing this task. Is generated from the text written in the comments section.</summary>
    Public ReadOnly Property bugs As BugList
        Get
            If _bugs Is Nothing Then
                _bugs = New BugList(Me)
            End If
            Return _bugs
        End Get
    End Property

    ''' <summary>
    ''' The total number of unassigned leaf nodes that are descendants of this node.
    ''' If this is a lead node, this will return 1 if it is unassigned, or 0 if it has been assigned.
    ''' </summary>
    Public ReadOnly Property unassignedtasks As Integer
        Get
            If isLeafNode Then
                Return If(unassigned, 1, 0)
            Else
                Dim sum = 0
                For Each child As TaskInstanceWrapper In subTasks.Cast(Of TaskInstanceWrapper)()
                    sum += child.unassignedtasks
                Next
                Return sum
            End If
        End Get
    End Property

    ''' <summary>If this is not a leaf node, and also has more than one unassigned leaf-node as a descendant.</summary>
    Public ReadOnly Property isParentWithUnassignedLeafNodes As Boolean
        Get
            Return Not isLeafNode AndAlso unassignedtasks > 0
        End Get
    End Property

    ''' <summary>If this task is a leaf node, and has also not been assigned.</summary>
    Public ReadOnly Property unassigned As Boolean
        Get
            Return isLeafNode AndAlso assignee Is Nothing
        End Get
    End Property

#End Region

#Region "Collections of Possible Values"
    '##########################################################################
    '#                   Collections of Possible Values                       #
    '##########################################################################

    ''' <summary>The list of all users this node can be assigned to (including a blank entry for unassigning.</summary>
    Public ReadOnly Property assignableUsers As IEnumerable(Of IntelligenceUser)
        Get
            Using db As New IntelligenceDB()
                Dim userList As List(Of IntelligenceUser) = db.IntelligenceUsers.ToList()
                userList.Add(New IntelligenceUser With {.username = "Unassigned"})
                Return userList
            End Using
        End Get
    End Property

    ''' <summary>
    ''' All resolutions that this instance could have 
    ''' (will be a restricted subset of all possible resolutions if the node is a leaf node).
    ''' </summary>
    Public ReadOnly Property allResolutions As IEnumerable(Of Resolution)
        Get
            If userCanSetResolution() Then
                Return ResolutionUtils.userSelectableResolutions
            Else
                Return ResolutionUtils.allResolutions
            End If
        End Get
    End Property

    ''' <summary>All priorities that could be assigned to this instance.</summary>
    Public ReadOnly Property allPriorities As IEnumerable(Of Priority)
        Get
            Return PriorityUtils.allPriorities
        End Get
    End Property

#End Region

#Region "Properties That Affect Child And Parent Tasks"
    '##########################################################################
    '#             Properties That Affect Child And Parent Tasks              #
    '##########################################################################

    ''' <summary>The internally cached resolution for this instance.</summary>
    Private _resolution As Resolution
    ''' <summary>
    ''' The resolution for this instance. 
    ''' Setting this resolution will cascade up the tree, setting the parent nodes
    ''' to the relevant resolutions, and saving all the changes to the database in a batch.
    ''' This code relies heavily on <see cref="TaskInstanceUtils"/> for most of its tree
    ''' travesal methods, as it must deal with both wrapped and unwrapped instances.
    ''' Will also fire the necessary UI notifications for all the wrapped instances that change.
    ''' Will also trigger a full refresh of the connected <see cref="VersionWrapper"/>, which
    ''' in turn will refresh its platforms, and the tree of task instances.
    ''' </summary>
    Public Property resolution As Resolution
        Get
            Return _resolution
        End Get
        Set(value As Resolution)
			If userCanSetResolution() Then
				If value Is Nothing Then Return
				Using db As New IntelligenceDB()
					setResolutionWithAlteringTree(value, db)
					If saveChanges(db) Then
						PlatformsForTasksListView.shouldRespondToSelectionChanging = False
						version.onTaskResolutionChanged()
						PlatformsForTasksListView.shouldRespondToSelectionChanging = True
					End If
				End Using
			End If
		End Set
    End Property

    ''' <summary>
    ''' Set the resolution for this instance, and cascade up the tree, setting the parent nodes
    ''' to the relevant resolutions, and saving all the changes to the database in a batch.
    ''' This code relies heavily on <see cref="TaskInstanceUtils"/> for most of its tree
    ''' travesal methods, as it must deal with both wrapped and unwrapped instances.
    ''' Will also fire the necessary UI notifications for all the wrapped instances that change.
    ''' </summary>
    ''' <param name="resolution">The new resolution to set.</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Public Sub setResolutionWithAlteringTree(resolution As Resolution, db As IntelligenceDB)
        setResolutionForInstance(Me, resolution, DateTime.Now, db)
    End Sub

    ''' <summary>
    ''' Sets the resolution for this instance, but does not cascade up the tree of instances.
    ''' The change is put on a stack of all changes that are saved to the database in a batch
    ''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
    ''' Will also add any UI notifications necessary to the stack so that they can be applied
    ''' in a batch if the changes are saved successfully.
    ''' </summary>
    ''' <param name="resolution">The new resolution to set.</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Public Sub setResolutionWithoutAlteringTree(resolution As Resolution, db As IntelligenceDB)
        setFieldAndMarkModified(resolution, _resolution, Sub(v)
                                                             If VersionResolutionUtils.isVersionOpen(getWrappedItem.version.resolution) Then
                                                                 _resolution = v
                                                                 getWrappedItem().resolution = v
                                                             End If
                                                         End Sub, Function() Me.resolution, db)
        addDelayedPropertyChange(Function() Me.numberOfLeavesByResolution, Me)
    End Sub

    ''' <summary>
    ''' Determines whether the user can change the resolution of this node on the UI.
    ''' Will be true if the task has been started, the task's version is open, and it is a leaf node.
    ''' </summary>
    Public ReadOnly Property userCanSetResolutionProperty As Boolean
        Get
            Return userCanSetResolution()
        End Get
    End Property

    ''' <summary>
    ''' Determines whether the user can change the resolution of this node on the UI.
    ''' Will be true if the task has been started, the task's version is open, and it is a leaf node.
    ''' </summary>
    ''' <returns>True if the task has been started, the task's version is open, and it is a leaf node.</returns>
    Public Function userCanSetResolution() As Boolean
        Using db As New IntelligenceDB
			Return getWrappedItem().timeStarted IsNot Nothing _
			AndAlso VersionResolutionUtils.isVersionOpen(version.resolution) _
			AndAlso assignee IsNot Nothing AndAlso assignee.id = UserInfo.getCurrentIntelligenceUser().id _
			AndAlso isLeafNode
		End Using
    End Function

    ''' <summary>
    ''' Set the finished time of this instance to the given date and time.
    ''' May set it to <c>Nothing</c> to indicate the task is being restarted.
    ''' </summary>
    ''' <param name="value">The new finishing time to set for this instance (or <c>Nothing</c> if unfinished).</param>
    ''' <param name="db">The database context to make changes in.</param>
    ''' <returns>True if the field was successfully modified.</returns>
    Public Function setFinishedTime(value As DateTime?, db As IntelligenceDB) As Boolean
		Dim success As Boolean = setFieldAndMarkModified(value, getWrappedItem.timeFinished, Sub(v) getWrappedItem.timeFinished = v, Function() Me.timeFinished, db)
		addDelayedPropertyChange(Function() Me.durationInProgress, Me)
		Return success
	End Function

    ''' <summary>The internally cached assignee for this task.</summary>
    Private _assignee As IntelligenceUser
    ''' <summary>
    ''' The assignee for this instance (will be <c>Nothing</c> if unassigned).
    ''' Setting this assignee will cascade up the tree, setting the parent nodes
    ''' to the relevant assignee, and saving all the changes to the database in a batch.
    ''' Parent tasks will be assigned to a single user if all their children are assigned
    ''' to the same user. Otherwise, they will be unassigned.
    ''' This code relies heavily on <see cref="TaskInstanceUtils"/> for most of its tree
    ''' travesal methods, as it must deal with both wrapped and unwrapped instances.
    ''' Will also fire the necessary UI notifications for all the wrapped instances that change.
    ''' Will also trigger a full refresh of the connected <see cref="VersionWrapper"/>, which
    ''' in turn will refresh its platforms, and the tree of task instances.
    ''' </summary>
    Public Property assignee As IntelligenceUser
        Get
            Return _assignee
        End Get
        Set(assignee As IntelligenceUser)
            Using db As New IntelligenceDB()
				Dim oldAssignee = _assignee
				assignInstance(Me, assignee, UserInfo.getCurrentIntelligenceUser().getWrappedItem(), DateTime.Now, db)

				If Not (
					(oldAssignee Is Nothing AndAlso assignee Is Nothing) _
					OrElse (oldAssignee IsNot Nothing AndAlso oldAssignee.id = assignee.id)
					) Then

					Dim openResolution As Resolution = ResolutionUtils.openResolution
					setResolutionWithAlteringTree(openResolution, db)
					setStartTimeForInstance(Me, Nothing, db)
					If saveChanges(db) Then
						PlatformsForTasksListView.shouldRespondToSelectionChanging = False
						version.onTaskResolutionChanged()
						PlatformsForTasksListView.shouldRespondToSelectionChanging = True
					End If
				End If

				saveChanges(db)
            End Using
            version.onTaskAssignmentChanged()
        End Set
    End Property

    ''' <summary>The internally cached assigner for this instance (will be <c>Nothing</c> if unassigned).</summary>
    Private _assigner As IntelligenceUser
    ''' <summary>The assigner for this instance (will be <c>Nothing</c> if unassigned).</summary>
    Public ReadOnly Property assigner As IntelligenceUser
        Get
            Return _assigner
        End Get
    End Property

    ''' <summary>The date and time when this instance was assigned (will be <c>Nothing</c> if this task is unassigned).</summary>
    Public ReadOnly Property timeAssigned As DateTime?
        Get
            Return getWrappedItem().timeAssigned
        End Get
    End Property

    ''' <summary>
    ''' Sets the assignee, assigner, and timeAssigned for this instance, but does not cascade up the tree of instances.
    ''' The change is put on a stack of all changes that are saved to the database in a batch
    ''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
    ''' Will also add any UI notifications necessary to the stack so that they can be applied
    ''' in a batch if the changes are saved successfully.
    ''' </summary>
    ''' <param name="assignee">The new assignee to set. May be <c>Nothing</c> if unassigning.</param>
    ''' <param name="assigner">The assigner setting this new assignee. Must be <c>Nothing</c> if unassigning.</param>
    ''' <param name="timeAssigned">The time when this assignee to set. Must be <c>Nothing</c> if unassigning.</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Public Sub setAssigneeWithoutAlteringTree(assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        setFieldAndMarkModified(assignee, _assignee, Sub(v)
                                                         Dim unused = getWrappedItem.assignee
                                                         getWrappedItem.assignee = v
                                                         _assignee = assignee
                                                     End Sub, Function() Me.assignee, db)

        setFieldAndMarkModified(assigner, _assigner, Sub(v)
                                                         Dim unused = getWrappedItem.assigner
                                                         _assigner = v
                                                         getWrappedItem.assigner = v
                                                     End Sub, Function() Me.assigner, db)
        setFieldAndMarkModified(timeAssigned, Me.timeAssigned, Sub(v)
                                                                   Dim unused = getWrappedItem.timeAssigned
                                                                   getWrappedItem.timeAssigned = v
                                                               End Sub, Function() Me.timeAssigned, db)
    End Sub

    ''' <summary>The internally cached priority for this instance.</summary>
    Private _priority As Priority
    ''' <summary>
    ''' The priority for this instance.
    ''' Setting this priority will cascade up the tree, setting the parent nodes
    ''' to the relevant priorities, and saving all the changes to the database in a batch.
    ''' This code relies heavily on <see cref="TaskInstanceUtils"/> for most of its tree
    ''' travesal methods, as it must deal with both wrapped and unwrapped instances.
    ''' Will also fire the necessary UI notifications for all the wrapped instances that change.
    ''' </summary>
    Public Property priority As Priority
        Get
            Return _priority
        End Get
        Set(value As Priority)
            Using db As New IntelligenceDB
                setPriorityForInstance(Me, value, db)
                saveChanges(db)
            End Using
        End Set
    End Property

    ''' <summary>
    ''' Sets the priority for this instance, but does not cascade up the tree of instances.
    ''' The change is put on a stack of all changes that are saved to the database in a batch
    ''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
    ''' Will also add any UI notifications necessary to the stack so that they can be applied
    ''' in a batch if the changes are saved successfully.
    ''' </summary>
    ''' <param name="priority">The new priority to set.</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Public Sub setPriorityWithoutAlteringTree(priority As Priority, db As IntelligenceDB)
        setFieldAndMarkModified(priority, _priority, Sub(v)
                                                         _priority = v
                                                         getWrappedItem.priority = v
                                                     End Sub, Function() Me.priority, db)
    End Sub

    ''' <summary>
    ''' The deadline for this instance (or <c>Nothing</c> if there is no deadline).
    ''' Setting this deadline will cascade up the tree, setting the parent nodes
    ''' to the relevant deadlines, and saving all the changes to the database in a batch.
    ''' Parent nodes will be given the deadline of the earliest child deadline.
    ''' This code relies heavily on <see cref="TaskInstanceUtils"/> for most of its tree
    ''' travesal methods, as it must deal with both wrapped and unwrapped instances.
    ''' Will also fire the necessary UI notifications for all the wrapped instances that change.
    ''' </summary>
    Public Property deadline As DateTime?
        Get
            Return getWrappedItem().timeDeadline
        End Get
        Set(value As DateTime?)
            Using db As New IntelligenceDB
                setDeadlineForInstance(Me, value, db)
                saveChanges(db)
            End Using
        End Set
    End Property

    ''' <summary>The deadline in a more human readable format for displaying on the UI.</summary>
    Public ReadOnly Property formattedDeadline As String
        Get
            Return If(deadline Is Nothing, Nothing, deadline.Value.ToString("dd/MM"))
        End Get
    End Property

    ''' <summary>
    ''' Set the deadline to the given value, but do not alter the rest of the tree.
    ''' The change is put on a stack of all changes that are saved to the database in a batch
    ''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
    ''' Will also add any UI notifications necessary to the stack so that they can be applied
    ''' in a batch if the changes are saved successfully.
    ''' </summary>
    ''' <param name="deadline">The new deadline to set (may be <c>Nothing</c>).</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Public Sub setDeadlineWithoutAlteringTree(deadline As DateTime?, db As IntelligenceDB)
        setFieldAndMarkModified(deadline, getWrappedItem.timeDeadline, Sub(v) getWrappedItem.timeDeadline = v, Function() Me.deadline, db)
        addDelayedPropertyChange(Function() Me.formattedDeadline, Me)
    End Sub

    ''' <summary>
    ''' Start this task by setting its <see cref="timeStarted"/> to <see cref="DateTime.Now"/>
    ''' and its <see cref="resolution"/> to <see cref="ResolutionUtils.inProgressResolution"/>.
    ''' If the user has <see cref="MAX_IN_PROGRESS_TASKS"/> or more tasks started already,
    ''' a warning dialog will be shown prompting the user to finish some tasks, or set them
    ''' to "open" before starting any new ones.
    ''' </summary>
    Public Sub startTask()
        If userCanStartTask() Then
            If Not UserInfo.getCurrentIntelligenceUser.tasksInProgressForCurrentUserCount >= MAX_IN_PROGRESS_TASKS Then
				Using db As New IntelligenceDB
					Dim inProgressResolution = ResolutionUtils.inProgressResolution
					setResolutionWithAlteringTree(inProgressResolution, db)
					setStartTimeForInstance(Me, DateTime.Now, db)
					If saveChanges(db) Then
						PlatformsForTasksListView.shouldRespondToSelectionChanging = False
						version.onTaskResolutionChanged()
						PlatformsForTasksListView.shouldRespondToSelectionChanging = True
					End If
				End Using
			Else
                Dim dlg As New ErrorDialog("Task Start Error",
                   "Error starting task: " & "Maximum (" & MAX_IN_PROGRESS_TASKS & ") 'IN PROGRESS' tasks reached.",
                   "Complete 'IN PROGRESS' tasks or set them to 'OPEN'")
                dlg.ShowDialog()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Set the time started to the given value, but do not alter the rest of the tree.
    ''' The change is put on a stack of all changes that are saved to the database in a batch
    ''' when <see cref="saveChanges"/> is called (and reverted if the save fails).
    ''' Will also add any UI notifications necessary to the stack so that they can be applied
    ''' in a batch if the changes are saved successfully.
    ''' </summary>
    ''' <param name="startTime">The new starting time to set.</param>
    ''' <param name="db">The database context to make the changes in.</param>
    Sub setStartTimeWithoutModifyingTree(startTime As DateTime?, db As IntelligenceDB)
		If getWrappedItem() IsNot Nothing Then
			setFieldAndMarkModified(startTime, getWrappedItem.timeStarted, Sub(v) getWrappedItem.timeStarted = v, Function() Me.timeStarted, db)
			addDelayedPropertyChange(Function() Me.userCanStartTaskProperty, Me)
			addDelayedPropertyChange(Function() Me.allResolutions, Me)
			addDelayedPropertyChange(Function() Me.timeStarted, Me)
			addDelayedPropertyChange(Function() Me.durationInProgress, Me)
			addDelayedPropertyChange(Function() Me.userCanSetResolutionProperty, Me)
		End If
	End Sub

    ''' <summary>
    ''' Whether the "Start Now" button should be shown for this task.
    ''' This enforces that the task is not started, is from an open version,
    ''' and is a leaf node, but the maximum in-progress task limit is enforced
    ''' in <see cref="startTask"/> rather than here (to make sure the button
    ''' appears to give a meaningful warning, rather than simply graying
    ''' everything out).
    ''' </summary>
    Public Function userCanStartTask() As Boolean
        Using db As New IntelligenceDB
            Dim isNotStarted As Boolean = getWrappedItem().timeStarted Is Nothing
			Dim versionOpen As Boolean = VersionResolutionUtils.isVersionOpen(version.resolution)
			Dim isAssignedToMe As Boolean = assignee IsNot Nothing AndAlso assignee.id = UserInfo.getCurrentIntelligenceUser().id
			Return isNotStarted AndAlso versionOpen AndAlso isLeafNode AndAlso isAssignedToMe
		End Using
    End Function

    ''' <summary>
    ''' Whether the "Start Now" button should be shown for this task.
    ''' This enforces that the task is not started, is from an open version,
    ''' and is a leaf node, but the maximum in-progress task limit is enforced
    ''' in <see cref="startTask"/> rather than here (to make sure the button
    ''' appears to give a meaningful warning, rather than simply graying
    ''' everything out).
    ''' </summary>
    Public ReadOnly Property userCanStartTaskProperty As Boolean
        Get
            Return userCanStartTask()
        End Get
    End Property

#End Region

#Region "Comments and Bugs"
	'##########################################################################
	'#                           Comments and Bugs                            #
	'##########################################################################

	'''' <summary>
	'''' Comments about this task.
	'''' If the text contains a "#" symbol followed by a number
	'''' (e.g. #18160) this number will be treated as the ID number
	'''' of a Mantis bug, and automatically added to the list of bugs.
	'''' </summary>
	'Public Property comments As CommentList
	'	Get
	'		Return getWrappedItem().comments
	'	End Get
	'	Set(value As String)
	'		setFieldAndSave(value, getWrappedItem.comments, Sub(v) getWrappedItem.comments = v)
	'		scanCommentsForBugIds(value)
	'	End Set
	'End Property

	Public ReadOnly Property addComment As ICommand
		Get
			Return New GenericCommand(
				Sub()
					Debug.Print("Adding to the list.")
					Dim success As Boolean = True
					Using db As New IntelligenceDB
						comments.addNew(db)
						success = DBManager.saveChanges(db)
					End Using
					If Not success Then
						Dim errorDialog As New ErrorDialog("Database Error", "Unable to add comment", "Something went wrong with the database connection, so the new comment could not be added.")
						errorDialog.Show()
					End If
				End Sub
			)
		End Get
	End Property

	Public ReadOnly Property deleteComment As ICommand
		Get
			Return New DeleteCommand(Of Comment)(comments, Function() True)
		End Get
	End Property


	''' <summary>The internally stored list of all comments found while performing this task.</summary>
	Private _comments As CommentList
    ''' <summary>The list of all comments about task.</summary>
    Public ReadOnly Property comments As CommentList
		Get
			If _comments Is Nothing Then
				_comments = New CommentList(Me)
			End If
			Return _comments
		End Get
	End Property


    ''' <summary>
    ''' Use regular expression to search string for # then a series of numbers (e.g. #18160), and add these
    ''' numbers to the instance's bug list as mantis bug ids
    ''' </summary>
    ''' <param name="comments">The comment string to scan through.</param>
    Public Sub scanCommentsForBugIds(comments As String)
        Using db As New IntelligenceDB
            Dim expression As String = "\#[0-9]+"
            Dim regEx As New Regex(expression, RegexOptions.IgnoreCase)

            Dim matches As MatchCollection = regEx.Matches(comments)
            Dim numMatches As Integer = matches.Count

            While (bugs.Count > numMatches)
                bugs.delete(bugs.Count - 1, db)
            End While

            While (bugs.Count < numMatches)
                bugs.addNewAndSave()
            End While

            For i As Integer = 0 To numMatches - 1
                Dim match As Match = matches.Item(i)
                Dim mantisId As Integer = match.Value.Substring(1)
                Dim bug As BugWrapper = bugs.Item(i)
                bug.setMantisID(mantisId, db)
            Next
            saveChanges(db)
            If Me.bugs.Any() Then
                Me.bugs.First.saveChanges(db)
            End If
        End Using
    End Sub

#End Region

#Region "Attachments"
	'##########################################################################
	'#                              Attachments                               #
	'##########################################################################

	Private _instanceAttachments As AttachmentList(Of InstanceAttachment)

	Public ReadOnly Property instanceAttachments As AttachmentList(Of InstanceAttachment)
		Get
			If _instanceAttachments Is Nothing Then
				_instanceAttachments = AttachmentUtils.getAttachmentsForInstance(getWrappedItem, True)
			End If
			Return _instanceAttachments
		End Get
	End Property

	Private _templateAttachments As AttachmentList(Of TemplateAttachment)
	Public ReadOnly Property templateAttachments As AttachmentList(Of TemplateAttachment)
		Get
			If _templateAttachments Is Nothing Then
				_templateAttachments = AttachmentUtils.getAttachmentsForTemplate(_template, False)
			End If
			Return _templateAttachments
		End Get
	End Property

	Private _inheritedTemplateAttachments As AttachmentList(Of TemplateAttachment)
	Public ReadOnly Property inheritedTemplateAttachments As AttachmentList(Of TemplateAttachment)
		Get
			If _inheritedTemplateAttachments Is Nothing Then
				_inheritedTemplateAttachments = AttachmentUtils.getInheritedAttachmentsForTemplate(_template)
			End If
			Return _inheritedTemplateAttachments
		End Get
	End Property

#End Region

#Region "SubTasks and Leaf Tasks"
	'##########################################################################
	'#                       SubTasks and Leaf Tasks                          #
	'##########################################################################

	''' <summary>
	''' Whether this task is at the very end of the tree of instances.
	''' Tasks at this level are the ones that the user must actually complete (rather than simply groupings of tasks).
	''' </summary>
	Public ReadOnly Property isLeafNode As Boolean
        Get
            Return subTasks Is Nothing OrElse Not subTasks.Any
        End Get
    End Property

    ''' <summary>
    ''' Whether this task is not at the very end of the tree of instances.
    ''' Tasks at these levels are simply groupings of tasks, rather than ones that the user must actually complete.
    ''' </summary>
    Public ReadOnly Property isNotLeafNode As Boolean
        Get
            Return Not isLeafNode
        End Get
    End Property

    ''' <summary>Whether this task is at the very top of the instance tree (has no parent instance).</summary>
    Public ReadOnly Property isRootNode As Boolean
        Get
            Return Me.parent Is Nothing
        End Get
    End Property

    ''' <summary>The number of leaf nodes that are descendants of this node.</summary>
    Public ReadOnly Property numberOfLeafTasks As Integer
        Get
            Using db As New IntelligenceDB
                Return getLeafNodes(getWrappedItem(), db).Count()
            End Using
        End Get
    End Property

    ''' <summary>The internally cached sorted list of the number of descendant leaf nodes for each resoltion.</summary>
    Private _leavesByResolution As List(Of ResolutionCount)
    ''' <summary>
    ''' The numbers of leaves descending from this node for each resolution (sorted by resolution).
    ''' If this node is a leaf, then it will be a single <see cref="ResolutionCount"/>
    ''' for this task's <see cref="resolution"/>.
    ''' </summary>
    Public ReadOnly Property numberOfLeavesByResolution As List(Of ResolutionCount)
        Get
            'If _leavesByResolution Is Nothing Then
            _leavesByResolution = New List(Of ResolutionCount)()

            '_leavesByResolution.Add(New ResolutionCount() With {.resolution = resolution, .Count = 1})
            'Return _leavesByResolution

            Dim counts As Hashtable = getLeafResolutionCounts()
            For Each Key As Resolution In counts.Keys
                _leavesByResolution.Add(New ResolutionCount() With {.resolution = Key, .Count = counts.Item(Key)})
            Next
            _leavesByResolution.OrderBy(Function(rc) rc.resolution.id)
            'End If
            Return _leavesByResolution
        End Get
    End Property

    ''' <summary>The internally cached hashtable of the number of descendant leaf nodes for each resoltion.</summary>
    Private _resolutionCounts As Hashtable

    ''' <summary>
    ''' Returns a hashtable detailing the counts of all the total number of leaf nodes for each resolution.
    '''If this node is a leaf, then it will be a single <see cref="ResolutionCount"/>
    ''' for this task's <see cref="resolution"/>.
    ''' </summary>
    ''' <returns>A hashtable detailing the counts of all the total number of leaf nodes for each resolution.</returns>
    Protected Function getLeafResolutionCounts() As Hashtable
        'If _resolutionCounts Is Nothing Then
        _resolutionCounts = New Hashtable
        If isLeafNode Then
            _resolutionCounts.Add(resolution, 1)
        Else
            For Each subTask As TaskInstanceWrapper In subTasks
                addCounts(_resolutionCounts, subTask.getLeafResolutionCounts())
            Next
        End If
        'End If
        Return _resolutionCounts
    End Function

    ''' <summary>Add two hashtables together to get the total count for each resolution.</summary>
    ''' <param name="toTable">One hashtable to add.</param>
    ''' <param name="fromTable">The other hashtable to add.</param>
    Private Sub addCounts(toTable As Hashtable, fromTable As Hashtable)
        For Each key In fromTable.Keys
            If Not toTable.ContainsKey(key) Then
                toTable.Add(key, 0)
            End If
            toTable.Item(key) += fromTable.Item(key)
        Next
    End Sub

    ''' <summary>The internally cached list of all wrapped subtasks for this node.</summary>
    Private _subTasks As TaskInstanceList
    ''' <summary>The list of all wrapped subtasks for this node.</summary>
    Public ReadOnly Property subTasks As TaskInstanceList
        Get
            If _subTasks Is Nothing Then
                Using db As New IntelligenceDB
                    _subTasks = New TaskInstanceList(version, platform, Me, db)
                End Using
            End If
            Return _subTasks
        End Get
    End Property

    ''' <summary>Represents a count of all tasks with that resolution.</summary>
    Public Class ResolutionCount
        ''' <summary>The resolution the tasks have.</summary>
        Public Property resolution As Resolution
        ''' <summary>The total number of tasks with that resolution.</summary>
        Public Property Count As Integer
        ''' <summary>Whether the resolution the tasks share is open or in progress.</summary>
        Public ReadOnly Property isOpenOrInProgress As Boolean
            Get
                If Me.resolution.id = ResolutionUtils.openResolution.id Or Me.resolution.id = ResolutionUtils.inProgressResolution.id Then
                    Return True
                End If
                Return False
            End Get
        End Property
    End Class

    ''' <summary>The list of all subtasks that are open (unused).</summary>
    Public ReadOnly Property openSubTasks As TaskInstanceList
        Get
            Dim openTasks = subTasks '.Where(Function(task) isOpen(task.resolution))
            Return subTasks
        End Get
    End Property

    ''' <summary>Triggered when a leaf node changes its resolution. Used to update the resoltion indicators.</summary>
    ''' <param name="sender">The <see cref="TaskInstanceWrapper"/> whose leaf node descendant changed its resoltion.</param>
    ''' <param name="e">Used to tell which property changed.</param>
    Public Sub onLeafNodesChanged(sender As Object, e As PropertyChangedEventArgs)
        If isEventForProperty(e, Function() Me.numberOfLeavesByResolution) Then
            onPropertyChanged(Function() Me.numberOfLeavesByResolution)
        End If
    End Sub

    ''' <summary>Register this node as a listener to the property changed events of the given child.</summary>
    ''' <param name="child"></param>
    Public Sub AddHandlerOnLeafNodesChanged(child As TaskInstanceWrapper)
        AddHandler child.PropertyChanged, AddressOf onLeafNodesChanged
    End Sub

#End Region

	''' <summary>
	''' Compare two wrappers, to see which one should be appear higher in the list.
	''' The two are compared first by priority, then by earliest deadline, then
	''' by version, product, and platform, and then are sorted according to the ordering
	''' of their task templates if all previous comparisons are equal.
	''' </summary>
	''' <param name="other">The other <see cref="TaskInstanceWrapper"/> to compare this one with.</param>
	''' <returns>Returns 1 if this is &gt; the other, -1 if it is &lt; the other, and 0 if they are equal.</returns>
	Public Function CompareTo(other As TaskInstanceWrapper) As Integer Implements IComparable(Of TaskInstanceWrapper).CompareTo
        Dim comparison As Integer
        Dim x, y As TaskInstanceWrapper
        x = Me
        y = other

        comparison = -(x.priority.id.CompareTo(y.priority.id))
        If 0 = comparison Then
            comparison = compareDates(x.deadline, y.deadline)
            If 0 = comparison Then
				comparison = x.version.timeCreated.CompareTo(y.version.timeCreated)
				If 0 = comparison Then
					comparison = x.product.rank.CompareTo(y.product.rank)
					If 0 = comparison Then comparison = x.product.id.CompareTo(y.product.id)
					If 0 = comparison Then
						comparison = x.platform.rank.CompareTo(y.platform.rank)
						If 0 = comparison Then comparison = x.platform.id.CompareTo(y.platform.id)
						If 0 = comparison Then
							Dim minHeight = Math.Min(x.path.Count, y.path.Count)
							For i As Integer = 0 To minHeight - 1
								Dim xPathElement = x.path.Item(i)
								Dim yPathElement = y.path.Item(i)
								comparison = xPathElement.rank.CompareTo(yPathElement.rank)
								If 0 = comparison Then
									comparison = xPathElement.id.CompareTo(yPathElement.id)
								End If
								If comparison <> 0 Then Exit For
							Next
							If comparison = 0 Then
								comparison = x.getWrappedItem.taskTemplate.rank.CompareTo(y.getWrappedItem.taskTemplate.rank)
								If 0 = comparison Then comparison = x.templateID.CompareTo(y.templateID)
							End If
						End If
					End If
				End If
			End If
        End If
        Return comparison
    End Function

    ''' <summary>
    ''' Compare two dates that may or may not be <c>Nothing</c>.
    ''' Dates that are set are given higher priority than ones that are <c>Nothing</c>.
    ''' </summary>
    ''' <param name="x">One date to compare.</param>
    ''' <param name="y">Another date to compare.</param>
    ''' <returns>Returns 1 if x &gt; y, -1 if x &lt; y, and 0 if x = y.</returns>
    Private Function compareDates(x As DateTime?, y As DateTime?)
        If x Is Nothing Then
            Return If(y Is Nothing, 0, 1)
        Else
            Return If(y Is Nothing, -1, x.Value.CompareTo(y.Value))
        End If
    End Function

End Class