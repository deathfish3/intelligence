﻿''' <summary>
''' A wrapper around a <see cref="StatsQuery"/> from the database.
''' Used to store pre-made queries to the intelligence database for
''' doing graphs of various properties. The <see cref="StatsQuery"/>
''' class stores metadata about the query such as who created it,
''' what it is named etc. and the real query information is stored
''' within it as a string (<see cref="StatsQueryWrapper.queryString"/>).
''' </summary>
Public Class StatsQueryWrapper
    Inherits Wrapper(Of StatsQuery)

    ''' <summary>Create a new wrapper around the given query.</summary>
    ''' <param name="statsQuery">The query to wrap.</param>
    Sub New(statsQuery As StatsQuery)
        MyBase.New(statsQuery)
    End Sub

    ''' <summary>
    ''' Load in foregeign key fields to this wrapper such as its
    ''' creator and its type while the database context is still open.
    ''' </summary>
    ''' <param name="newItem">The newly downloaded stats query.</param>
    ''' <param name="db">Unused.</param>
    Protected Overrides Sub initializeFields(newItem As StatsQuery, Optional db As IntelligenceDB = Nothing)
        MyBase.initializeFields(newItem, db)
        Me._type = newItem.type
        Me._creator = newItem.creator
    End Sub

    ''' <summary>The internally stored type of the query. Used to differentiate different query string formats.</summary>
    Private _type As StatsQueryType
    ''' <summary>
    ''' The type of the query. Used to differentiate different query string formats, or queries to different databases.
    ''' Currently only <see cref="StatsQueryTypeUtils.intelligenceTaskStats"/> is in use.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Property type As StatsQueryType
        Get
            Return _type
        End Get
        Set(value As StatsQueryType)
            setFieldAndSave(value, _type, Sub(v)
                                              _type = v
                                              getWrappedItem.type = v
                                          End Sub)
        End Set
    End Property

    ''' <summary>
    ''' A human readable description about the query.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Property description As String
        Get
            Return getWrappedItem.description
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.description, Sub(v) getWrappedItem.description = v)
        End Set
    End Property

    ''' <summary>
    ''' Whether users other than the one who created the query can read and write to it.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Property isVisibleToOthers As Boolean
        Get
            Return getWrappedItem.is_visible_to_others
        End Get
        Set(value As Boolean)
            setFieldAndSave(value, getWrappedItem.is_visible_to_others, Sub(v) getWrappedItem.is_visible_to_others = v)
        End Set
    End Property

    ''' <summary>
    ''' The string representation of the actual query to be performed.
    ''' 
    ''' For queries of the type <see cref="StatsQueryTypeUtils.intelligenceTaskStats"/> this
    ''' is a custom-formatted string that can be split using various delimiters and read read in using
    ''' <see cref="QueryChunk.importQueriesFrom"/>.
    ''' 
    ''' This can be extended to hold any type of query that can be represented as a string, but currently
    ''' only <see cref="StatsQueryTypeUtils.intelligenceTaskStats"/> is in use.
    ''' 
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Property queryString As String
        Get
            Return getWrappedItem.queryString
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.queryString, Sub(v) getWrappedItem.queryString = v)
        End Set
    End Property

    ''' <summary>When this query was originally created.</summary>
    Public ReadOnly Property timeCreated As DateTime
        Get
            Return getWrappedItem.timeCreated
        End Get
    End Property

    ''' <summary>The internally stored user that created this query.</summary>
    Private _creator As IntelligenceUser
    ''' <summary>The user that created this query.</summary>
    Public ReadOnly Property creator As IntelligenceUser
        Get
            Return _creator
        End Get
    End Property

    ''' <summary>
    ''' The human readable name of this query.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem.name
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.name, Sub(v) getWrappedItem.name = v)
        End Set
    End Property
End Class
