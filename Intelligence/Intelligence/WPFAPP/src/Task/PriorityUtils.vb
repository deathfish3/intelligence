﻿'############################################################################
'PrioritiesUtils.vb
'
'Purpose: To act as an 'enum' class for Priorities
'
'Description: Contains each priority in Database (as of 16/07/15). 
'Will populate each priority with associated priority stored in database
'
'Modification: To create a new priority entry, copy...

'       Private Shared _dbYourPriority As Priority = GetPriorityByName("YourPriorityInDb")
'       Public Shared ReadOnly Property yourPriority As Priority
'           Get
'               Return _dbYourPriority
'           End Get
'       End Property
'
' ...into PriorityUtils class.
'############################################################################


Public Class PriorityUtils

    'Get _dbNone
    'Sync _dbNone with database on init. Always return _dbNone
    Private Shared _dbNone As Priority = GetPriorityByName("none")
    Public Shared ReadOnly Property nonePriority As Priority
        Get
            Return _dbNone
        End Get
    End Property

    'Get _dbVeryLow
    'Sync _dbVeryLow with database on init. Always return _dbVeryLow
    Private Shared _dbVeryLow As Priority = GetPriorityByName("very low")
    Public Shared ReadOnly Property veryLowPriority As Priority
        Get
            Return _dbVeryLow
        End Get
    End Property

    'Get _dbLow
    'Sync _dbLow with database on init. Always return _dbLow
    Private Shared _dbLow As Priority = GetPriorityByName("low")
    Public Shared ReadOnly Property lowPriority As Priority
        Get
            Return _dbLow
        End Get
    End Property

    'Get _dbMedium
    'Sync _dbMedium with database on init. Always return _dbMedium
    Private Shared _dbMedium As Priority = GetPriorityByName("medium")
    Public Shared ReadOnly Property mediumPriority As Priority
        Get
            Return _dbMedium
        End Get
    End Property

    'Get _dbHigh
    'Sync _dbHigh with database on init. Always return _dbHigh
    Private Shared _dbHigh As Priority = GetPriorityByName("high")
    Public Shared ReadOnly Property highPriority As Priority
        Get
            Return _dbHigh
        End Get
    End Property

    'Get _dbVeryHigh
    'Sync _dbVeryHigh with database on init. Always return _dbVeryHigh
    Private Shared _dbVeryHigh As Priority = GetPriorityByName("very high")
    Public Shared ReadOnly Property veryHighPriority As Priority
        Get
            Return _dbVeryHigh
        End Get
    End Property

    'Get max priority in subtasks of taskinstance
    Public Shared Function maxPriorityOfSubtasks(taskInstance As TaskInstance, db As IntelligenceDB) As Priority
        Dim maxPriorityId As Integer =
        (From child In TaskInstanceUtils.getSubtasks(taskInstance, db)
         Select child.priority.id).Max()
        Return db.Priorities.Find(maxPriorityId)
    End Function

    'OVERLOAD: Get max priority in subtasks of taskinstance
    Public Shared Function maxPriorityOfSubtasks(taskInstance As TaskInstanceWrapper, db As IntelligenceDB) As Priority
        Dim maxPriorityId As Integer =
            (From child In TaskInstanceUtils.getSubtasks(taskInstance.getWrappedItem, db)
             Select child.priority.id).Max()
        Return db.Priorities.Find(maxPriorityId)
    End Function

    'Takes in 'name' and collects database priority with name field matching name
    Private Shared Function GetPriorityByName(name As String)
        Using db As New IntelligenceDB
            Return (From priority In db.Priorities
                    Where priority.name.ToLower.Equals(name.ToLower)
                    Select priority).FirstOrDefault
        End Using
    End Function

    Private Shared _allPriorities As IEnumerable(Of Priority)
    Public Shared ReadOnly Property allPriorities As IEnumerable(Of Priority)
        Get
            If _allPriorities Is Nothing Then
                Using db As New IntelligenceDB
                    _allPriorities = db.Priorities.Where(Function(p) p.active).ToList()
                End Using
            End If
            Return _allPriorities
        End Get
    End Property
End Class
