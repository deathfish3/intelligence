﻿Imports WPFAPP.PlatformSelectionEditor

''' <summary>
''' A wrapper around a <see cref="TaskTemplate"/> that can be
''' bound to the UI, and will update the database when changes are made.
''' Has numerous methods for dealing with adding, deleting, and moving
''' task templates, as well as updating the compatible platforms for the whole tree.
''' </summary>
Public Class TaskTemplateWrapper
	Inherits RankedWrapper(Of TaskTemplate)
	Implements PlatformSelectionListener

    ''' <summary>
    ''' The different ways of moving task templates in the drag-drop window of
    ''' <see cref="TaskTemplateTreeEditor"/>
    ''' </summary>
    Public Enum MoveType
        NONE
        CHILD_OF
        BELOW
        ABOVE
    End Enum

    ''' <summary>Create a new wrapper around the given template.</summary>
    ''' <param name="taskTemplate">The template to wrap.</param>
    ''' <param name="parent">
    ''' The template this task is a subtask of. 
    ''' May be <c>Nothing</c> if this task is a top-level root task. 
    ''' </param>
    ''' <param name="product">The product this template is for.</param>
    ''' <param name="container">
    ''' The <see cref="TaskTemplateList"/> that contains this wrapper.
    ''' May be either a top-level list, or a list of subtasks.
    ''' </param>
    Public Sub New(taskTemplate As TaskTemplate, parent As TaskTemplateWrapper, product As ProductWrapper, container As TaskTemplateList)
        MyBase.New(taskTemplate)
        Me._parent = parent
        Me._product = product
		Me._container = container
	End Sub

    ''' <summary>The internally stored type of the current drag/drop movement being performed.</summary>
    Private _dragDropMoveType As MoveType = MoveType.NONE
    ''' <summary>
    ''' The current type of drag/drop movement being performed.
    ''' Setting this will notify the UI that it has changed.
    ''' </summary>
    Public Property dragDropMoveType As MoveType
        Get
            Return Me._dragDropMoveType
        End Get
        Set(value As MoveType)
            Me._dragDropMoveType = value
            onPropertyChanged(Function() Me.dragDropMoveType)
        End Set
    End Property

    ''' <summary>Initialize the wrapper's fields from the newly downloaded task template.</summary>
    ''' <param name="newItem">The newly downloaded task template.</param>
    ''' <param name="db">The open database context the template came from.</param>
    Protected Overrides Sub initializeFields(newItem As TaskTemplate, Optional db As IntelligenceDB = Nothing)
        MyBase.initializeFields(newItem, db)
        Me._platforms = newItem.platforms
    End Sub

    ''' <summary>The internally stored template this task is a subtask of.</summary>
    Private _parent As TaskTemplateWrapper
    ''' <summary>
    ''' The template this task is a subtask of.
    ''' If this is <c>Nothing</c>, this task is a top-level root task.
    ''' </summary>
    Public ReadOnly Property parent As TaskTemplateWrapper
        Get
            Return _parent
        End Get
    End Property

    ''' <summary>Whether this template is for a top-level root task.</summary>
    Public ReadOnly Property isRootNode As Boolean
		Get
			Return Me.parent Is Nothing
		End Get
	End Property

    ''' <summary>The internally stored list that this task is contained in.</summary>
    Private _container As TaskTemplateList
    ''' <summary>
    ''' The list that this task is contained in.
    ''' May be either a top-level list, or a list of subtasks.
    ''' </summary>
    Public ReadOnly Property container As TaskTemplateList
        Get
            Return _container
        End Get
    End Property

    ''' <summary>The internally stored product this task template is for.</summary>
    Private _product As ProductWrapper
    ''' <summary>The product this task template is for.</summary>
    Public ReadOnly Property product As ProductWrapper
        Get
            Return _product
        End Get
    End Property

    ''' <summary>The internally stored list of subtasks for this task template.</summary>
    Private _subTasks As TaskTemplateList
    ''' <summary>
    ''' The list of subtasks for this task template.
    ''' This list will never be <c>Nothing</c>, but for leaf nodes, it will be empty.
    ''' </summary>
    Public ReadOnly Property subTasks As TaskTemplateList
        Get
            If _subTasks Is Nothing Then
                _subTasks = New TaskTemplateList(Me.product, Me)
            End If
            Return _subTasks
        End Get
    End Property

    ''' <summary>The internally stored list of all platforms this task template is compatible with.</summary>
    Dim _platforms As ICollection(Of Platform)
    ''' <summary>The list of all platforms this task template is compatible with.</summary>
    ReadOnly Property platforms As ICollection(Of Platform)
		Get
			Return _platforms
		End Get
	End Property

	Private _attachments As AttachmentList(Of TemplateAttachment)
	ReadOnly Property attachments As AttachmentList(Of TemplateAttachment)
		Get
			If _attachments Is Nothing Then
				_attachments = AttachmentUtils.getAttachmentsForTemplate(getWrappedItem, True)
			End If
			Return _attachments
		End Get
	End Property

	Private _inheritedAttachments As AttachmentList(Of TemplateAttachment)
	ReadOnly Property inheritedAttachments As AttachmentList(Of TemplateAttachment)
		Get
			If _inheritedAttachments Is Nothing Then
				_inheritedAttachments = AttachmentUtils.getInheritedAttachmentsForTemplate(getWrappedItem)
			End If
			Return _inheritedAttachments
		End Get
	End Property

    ''' <summary>
    ''' Get a new list that is a copy of all the platforms this task template is compatible with.
    ''' Each platform will be attached to the given database context.
    ''' This is used when copying templates to avoid multiple wrappers sharing a reference to the same list.
    ''' </summary>
    ''' <param name="db">The open database context to search for platforms in.</param>
    ''' <returns>A new list that is a copy of all the platforms this task template is compatible with.</returns>
    Public Function getCopyOfPlatforms(db As IntelligenceDB) As IEnumerable(Of Platform)
        Dim platformList As New List(Of Platform)
        Dim meFromDB As TaskTemplate = db.TaskTemplates.Find(id)
        For Each p As Platform In meFromDB.platforms
            platformList.Add(p)
        Next
        Return platformList
    End Function

    ''' <summary>
    ''' The human readable name of this task template.
    ''' Setting this field will automatically update the database, and notify 
    ''' the UI of the necessary changes.
    ''' </summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem.name
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.name, Sub(v) getWrappedItem.name = v)
            onPropertyChanged(Function() Me.nameAndDescription)
        End Set
    End Property

    ''' <summary>The name and description, separated by a blank line (newline is omitted if description is empty).</summary>
    Public ReadOnly Property nameAndDescription As String
        Get
            If description Is Nothing OrElse description.Length = 0 Then
                Return name
            Else
                Return name & Environment.NewLine & Environment.NewLine & description
            End If
        End Get
    End Property

    ''' <summary>
    ''' Text describing this task in more detail.
    ''' Setting this field will automatically update the database, and notify 
    ''' the UI of the necessary changes.
    ''' </summary>
    Public Property description As String
        Get
            Return getWrappedItem.description
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.description, Sub(v) getWrappedItem.description = v)
            onPropertyChanged(Function() Me.nameAndDescription)
        End Set
    End Property


    '########################################################
    '                   Platform Compatibility
    '########################################################

    ''' <summary>
    ''' Add the given platform to this template's list of compatible platforms,
    ''' and make the change in the given database context too.
    ''' Does not update the rest of the tree's compatible platforms.
    ''' </summary>
    ''' <param name="platformToAdd">The new platform this template is compatible with.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    ''' <returns>
    ''' True if the platform was successfully added. 
    ''' False if the list already contained the platform and no change was necessary.
    ''' </returns>
    Private Function addPlatform(platformToAdd As Platform, db As IntelligenceDB) As Boolean
        If Not platforms.Contains(platformToAdd) Then
            platforms.Add(platformToAdd)

            Dim templateFromDB As TaskTemplate = db.TaskTemplates.Find(id)
            Dim platformFromDB As Platform = db.Platforms.Find(platformToAdd.id)
            templateFromDB.platforms.Add(platformFromDB)

            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Remove the given platform to this template's list of compatible platforms,
    ''' and make the change in the given database context too.
    ''' Does not update the rest of the tree's compatible platforms.
    ''' </summary>
    ''' <param name="platformToRemove">The platform this template is no longer compatible with.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    ''' <returns>
    ''' True if the platform was successfully removed. 
    ''' False if the list did not contain the platform and no change was necessary.
    ''' </returns>
    Private Function removePlatform(platformToRemove As Platform, db As IntelligenceDB) As Boolean
        If platforms.Contains(platformToRemove) Then
            platforms.Remove(platformToRemove)

            Dim templateFromDB As TaskTemplate = db.TaskTemplates.Find(id)
            Dim platformFromDB As Platform = db.Platforms.Find(platformToRemove.id)
            templateFromDB.platforms.Remove(platformFromDB)

            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Add the given platform to this template's list of compatible platforms,
    ''' and propagate the change to all its subtasks too.
    ''' </summary>
    ''' <param name="platformToAdd">The new platform this template and its subtasks are compatible with.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    ''' <returns>Always returns <c>True</c>.</returns>
    Private Function addPlatformToTaskAndChildren(platformToAdd As Platform, db As IntelligenceDB) As Boolean
        addPlatform(platformToAdd, db)
        For Each child As TaskTemplateWrapper In subTasks
            child.addPlatformToTaskAndChildren(platformToAdd, db)
        Next
        Return True
    End Function

    ''' <summary>
    ''' Remove the given platform to this template's list of compatible platforms,
    ''' and propagate the change to all its subtasks too.
    ''' </summary>
    ''' <param name="platformToRemove">The platform this template and its subtasks are no longer compatible with.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    ''' <returns>
    ''' Returns <c>True</c> if the platform was successfully removed from this node and its subtasks.
    ''' Returns <c>False</c> if this node was not compatible with the platform to begin with, 
    ''' and so its children did not need updated.
    ''' </returns>
    Private Function removePlatformFromTaskAndChildren(platformToRemove As Platform, db As IntelligenceDB) As Boolean
        If removePlatform(platformToRemove, db) Then
            For Each child As TaskTemplateWrapper In subTasks
                child.removePlatformFromTaskAndChildren(platformToRemove, db)
            Next
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' When the given platform is added to one of this task's subtasks,
    ''' check whether this node needs updated to contain that platform too.
    ''' This will and propagate the change to all its ancestor tasks too if necessary.
    ''' </summary>
    ''' <param name="platformAdded">The new platform one of the template's subtasks is now compatible with.</param>
    ''' <param name="db">The open database context to make this changes in.</param>
    Public Sub updateTaskWhenPlatformAddedToChild(platformAdded As Platform, db As IntelligenceDB)
        If addPlatform(platformAdded, db) AndAlso parent IsNot Nothing Then
            parent.updateTaskWhenPlatformAddedToChild(platformAdded, db)
        End If
    End Sub

    ''' <summary>
    ''' When the given platform is removed from one of this task's subtasks,
    ''' check whether this node needs updated to remove that platform too.
    ''' Platorms are only removed from parents if no child nodes are compatible with that platform any more.
    ''' This will and propagate the change to all its ancestor tasks too if necessary.
    ''' </summary>
    ''' <param name="platformRemoved">The platform one of this template's subtasks is no longer compatible with.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Sub updateTaskWhenPlatformRemovedFromChild(platformRemoved As Platform, db As IntelligenceDB)
        If Not hasChildContainingPlatform(platformRemoved) _
        AndAlso removePlatform(platformRemoved, db) _
        AndAlso parent IsNot Nothing Then
            parent.updateTaskWhenPlatformRemovedFromChild(platformRemoved, db)
        End If
    End Sub

    ''' <summary>Whether this template has any subtasks that are compatible with the given platform.</summary>
    ''' <param name="platfomToCheck">The platform to check subtasks for.</param>
    ''' <returns><c>True</c> if any child tasks are compatible with <paramref name="platfomToCheck"/>.</returns>
    Private Function hasChildContainingPlatform(platfomToCheck As Platform) As Boolean
        Return (From child As TaskTemplateWrapper In subTasks
                 Where child.platforms.Contains(platfomToCheck)
                 Select child).Any()
    End Function

    ''' <summary>
    ''' The list of this task's compatible platforms, wrapped up as
    ''' <see cref="PlatformSelectionEditor.PlatformSelection"/>s. This will
    ''' be a list of all platforms, with the compatible ones ticked, and the
    ''' incompatible ones unticked.
    ''' </summary>
    Public ReadOnly Property platformSelections As IEnumerable(Of PlatformSelection)
        Get
            Return getPlatformSelections()
        End Get
    End Property

    ''' <summary>
    ''' Respond to a platform being selected or deselected for this task by
    ''' updating the relevant databse fields, and propagating the changes up
    ''' and down the tree of task templates as necessary.
    ''' </summary>
    ''' <param name="platform">The platform that was selected or deselected.</param>
    ''' <param name="isSelected">Whether the platform was selected or deselected.</param>
    Public Sub onPlatformSelectionChanged(platform As Platform, isSelected As Boolean) Implements PlatformSelectionListener.onPlatformSelectionChanged
        Using db As New IntelligenceDB
            If isSelected Then
                If addPlatformToTaskAndChildren(platform, db) AndAlso parent IsNot Nothing Then
                    parent.updateTaskWhenPlatformAddedToChild(platform, db)
                End If
            Else
                If removePlatformFromTaskAndChildren(platform, db) AndAlso parent IsNot Nothing Then
                    parent.updateTaskWhenPlatformRemovedFromChild(platform, db)
                End If
            End If
            saveChanges(db)
        End Using
    End Sub

    ''' <summary>
    ''' Gets the list of this task's compatible platforms, wrapped up as
    ''' <see cref="PlatformSelectionEditor.PlatformSelection"/>s. This will
    ''' be a list of all platforms, with the compatible ones ticked, and the
    ''' incompatible ones unticked.
    ''' </summary>
    ''' <returns>The list of all platforms selected or unselected as necessary.</returns>
    Private Function getPlatformSelections() As IEnumerable(Of PlatformSelection)

        'Filter through db.Platforms for active tasks
        Dim allActivePlatforms As IEnumerable(Of Platform)
		Using db As New IntelligenceDB
			allActivePlatforms = (
				From plat As Platform In db.Platforms
				Where plat.active
				Order By plat.rank Ascending, plat.id Ascending
				Select plat
			).ToList()
		End Using

		Dim platformSelections As IEnumerable(Of PlatformSelection) = wrapAsPlatformSelection(allActivePlatforms, True)

        'Loop through above and check agaisnt my platforms for ones that match. Keep those matches selected, deselect all others.
        selectAllPlatformsAvailableForTask(platformSelections, Me)

        For Each selection In platformSelections
            selection.listener = Me
        Next

        Return platformSelections
    End Function

    ''' <summary>
    ''' Given the list of <see cref="PlatformSelectionEditor.PlatformSelection"/>s, tick
    ''' or untick them to correspond to the given template's compatible platforms.
    ''' </summary>
    ''' <param name="platformSelections">The list to platform selections to tick or untick.</param>
    ''' <param name="taskTemplate">The template whos compatible platform list to mirror.</param>
    Private Shared Sub selectAllPlatformsAvailableForTask(platformSelections As IEnumerable(Of PlatformSelection), taskTemplate As TaskTemplateWrapper)
        For Each platformSelection As PlatformSelection In platformSelections
            platformSelection.isSelected = taskTemplate.platforms.Contains(platformSelection.platform)
        Next
    End Sub

End Class