﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel

''' <summary>
''' A wrapper around <see cref="Version"/>  binding to the UI and editing 
''' fields in the database. Also contains numerous summary methods for the 
''' number of open/passed/failed/etc. tasks in this version on a 
''' total/per user/per platform basis. 
''' </summary>
Public Class VersionWrapper
    Inherits Wrapper(Of Version)

#Region "Initialization"
    '###################################################################
    '#                        Initialization                           #
    '###################################################################

    ''' <summary>
    ''' A flag indicating whether information number of open/passed/failed/etc. 
    ''' tasks in this version should be loaded. This is a time consuming operation,
    ''' so it can be disabled for more lightweight wrappers, and enabled
    ''' for when these indicators are needed in the admin panel.
    ''' </summary>
    Public shouldLoadLeafNodeIndicators As Boolean = True

    ''' <summary>
    ''' Create a lightweight wrapper around the version with only the
    ''' minimum amount of information loaded. For use outside of the
    ''' admin panel, where the heavy duty stats are not shown.
    ''' </summary>
    ''' <param name="version">The <see cref="Version"/> to wrap.</param>
    Sub New(version As Version)
        Me.New(version, New ProductWrapper(version.product), False)
    End Sub

    ''' <summary>
    ''' Create a lightweight wrapper around the version with only the
    ''' minimum amount of information loaded. For use outside of the
    ''' admin panel, where the heavy duty stats are not shown.
    ''' </summary>
    ''' <param name="version">The <see cref="Version"/> to wrap.</param>
    ''' <param name="db">The open database contect the version came from.</param>
    Sub New(version As Version, db As IntelligenceDB)
        Me.New(version, New ProductWrapper(version.product), db, False)
    End Sub

    ''' <summary>
    ''' Create a wrapper around the given version of the given product.
    ''' If <paramref name="shouldLoadLeafNodeIndicators"/> is <c>True</c>
    ''' then all the information about the number of open/passed/failed/etc. 
    ''' tasks in this version will also be loaded.
    ''' </summary>
    ''' <param name="version">The <see cref="Version"/> to wrap.</param>
    ''' <param name="product">A wrapper around the <see cref="Product"/> this version is for.</param>
    ''' <param name="shouldLoadLeafNodeIndicators">
    ''' If <c>True</c>
    ''' then all the information about the number of open/passed/failed/etc. 
    ''' tasks in this version will be loaded.
    ''' </param>
    Sub New(version As Version, product As ProductWrapper, shouldLoadLeafNodeIndicators As Boolean)
        Me.New(version, product, Nothing, shouldLoadLeafNodeIndicators)
    End Sub


    ''' <summary>
    ''' Create a wrapper around the given version of the given product.
    ''' If <paramref name="shouldLoadLeafNodeIndicators"/> is <c>True</c>
    ''' then all the information about the number of open/passed/failed/etc. 
    ''' tasks in this version will also be loaded.
    ''' </summary>
    ''' <param name="version">The <see cref="Version"/> to wrap.</param>
    ''' <param name="product">A wrapper around the <see cref="Product"/> this version is for.</param>
    ''' <param name="db">The open database contect the version came from.</param>
    ''' <param name="shouldLoadLeafNodeIndicators">
    ''' If <c>True</c>
    ''' then all the information about the number of open/passed/failed/etc. 
    ''' tasks in this version will be loaded.
    ''' </param>
    Sub New(version As Version, product As ProductWrapper, db As IntelligenceDB, shouldLoadLeafNodeIndicators As Boolean)
        MyBase.New(version, Nothing, False)
        Me._product = product
        Me.shouldLoadLeafNodeIndicators = shouldLoadLeafNodeIndicators
        initializeFields(version, db)
    End Sub

    ''' <summary>
    ''' Load and cache the <see cref="Version"/>'s <see cref="Resolution"/>.
    ''' If <see cref="loadLeafNodeIndicators"/> is <c>True</c>, then all the
    ''' all the information about the number of open/passed/failed/etc. 
    ''' tasks in this version will also be loaded.
    ''' </summary>
    ''' <param name="newItem">
    ''' The newly created <see cref="Version"/> which must still 
    ''' be connected to an open <see cref="IntelligenceDB"/>.
    ''' </param>
    ''' <param name="inDB">
    ''' The database context the new <see cref="Version"/> came from.
    ''' If this is <c>Nothing</c>, a new one will be created.
    ''' </param>
    Protected Overrides Sub initializeFields(newItem As Version, Optional inDB As IntelligenceDB = Nothing)
        MyBase.initializeFields(newItem, inDB)

        _resolution = newItem.resolution

        If shouldLoadLeafNodeIndicators Then
            Dim db As IntelligenceDB = If(inDB IsNot Nothing, inDB, New IntelligenceDB)

            loadLeafNodeIndicators(db)

            If inDB Is Nothing Then
                db.Dispose()
            End If
        End If
    End Sub

    ''' <summary>Update the all version's fields from the database whenever one of its tasks changes resolution.</summary>
    Public Sub onTaskResolutionChanged()
        reloadWrappedItem()
    End Sub

    ''' <summary>Update the all version's fields from the database whenever one of its tasks changes assignee.</summary>
    Public Sub onTaskAssignmentChanged()
        reloadWrappedItem()
    End Sub

    ''' <summary>Update the all version's fields from the database whenever its list of tasks changes.</summary>
    Public Sub onTasksUpdated()
        Me.reloadWrappedItem()
    End Sub

#End Region

#Region "Properties"
    '###################################################################
    '#                          Properties                             #
    '###################################################################

    ''' <summary>The <see cref="Version"/>'s human readable name.</summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem.name
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.name, Sub(v) getWrappedItem.name = v)
        End Set
    End Property

    ''' <summary>Any comments about this <see cref="Version"/>.</summary>
    Public Property comments As String
        Get
            Return getWrappedItem.comments
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem.comments, Sub(v) getWrappedItem.comments = v)
        End Set
    End Property

    ''' <summary>
    ''' When the <see cref="Version"/> was first opened. 
    ''' If it was closed and re-opened multiple times, this will be the first opening time.
    ''' </summary>
    Public ReadOnly Property timeCreated As DateTime
        Get
            Return getWrappedItem.timeCreated
        End Get
    End Property

    ''' <summary>
    ''' When the <see cref="Version"/> was closed
    ''' (or <c>Nothing</c> if it is still open).
    ''' If it was closed and re-opene multiple times, this will be the
    ''' most recent closing time.
    ''' </summary>
    Public ReadOnly Property timeclosed As DateTime
        Get
            If getWrappedItem.timeClosed IsNot Nothing Then
                Return getWrappedItem.timeClosed.ToString
            End If
            Return Nothing
        End Get
    End Property

    ''' <summary>The internally stored wrapper around the <see cref="Product"/> this <see cref="Version"/> comes from.</summary>
    Private _product As ProductWrapper
    ''' <summary>A wrapper around the <see cref="Product"/> this <see cref="Version"/> comes from.</summary>
    Public ReadOnly Property product As ProductWrapper
        Get
            Return _product
        End Get
    End Property

    ''' <summary>
    ''' A property used to indicate that the tasks in this <see cref="Version"/>
    ''' have changed. It does not return anything useful, but is used by event
    ''' handlers to listen in to property changed events.
    ''' </summary>
    Public ReadOnly Property tasksChangedFlag As Object
        Get
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' All possible resolutions for this version.
    ''' Used for binding to <see cref="ComboBox"/>es in the UI.
    ''' </summary>
    Public ReadOnly Property allVersionResolutions As IEnumerable(Of VersionResolution)
        Get
            Return VersionResolutionUtils.allResolutions
        End Get
    End Property

    ''' <summary>An internal cache of this <see cref="Version"/>'s <see cref="Resolution"/>.</summary>
    Private _resolution As VersionResolution
    ''' <summary>
    ''' Get or set this <see cref="Version"/>'s <see cref="Resolution"/>.
    ''' Setting it will handle closing or re-opening of the version and all
    ''' its unfinished subtasks, as well as saving the changes to the database.
    ''' </summary>
    Public Property resolution As VersionResolution
        Get
            Return _resolution
        End Get
        Set(value As VersionResolution)
            Dim changed As Boolean = False
            Using db As New IntelligenceDB
                Dim oldResolution As VersionResolution = _resolution
                setFieldAndMarkModified(value, oldResolution, Sub(v)
                                                                  _resolution = v
                                                                  getWrappedItem.resolution = v
                                                              End Sub, Nothing, db)

                If VersionResolutionUtils.isVersionOpen(oldResolution) Then
                    If Not VersionResolutionUtils.isVersionOpen(value) Then
                        DBManager.closeVersion(db, getWrappedItem)
                    End If
                Else
                    If VersionResolutionUtils.isVersionOpen(value) Then
                        DBManager.reopenVersion(db, getWrappedItem)
                    End If
                End If
                changed = saveChanges(db)
            End Using
            If changed Then
                onTaskResolutionChanged()
            End If
        End Set
    End Property

#End Region

#Region "Leaf Task Resolution Information For Admin Panel"
    '###################################################################
    '#       Leaf Task Resolution Information For Admin Panel          #
    '###################################################################


    ''' <summary>The internally stored total number of open/passed/failed/etc. tasks in this <see cref="Version"/>.</summary>
    Private _allTasks As List(Of ResolutionCount) = Nothing

    ''' <summary>The total number of open/passed/failed/etc. tasks in this <see cref="Version"/>.</summary>
    Public ReadOnly Property alltasks
        Get
            Return _allTasks
        End Get
    End Property

    ''' <summary>The internally stored total number of tasks in this <see cref="Version"/>.</summary>
    Dim _alltaskscount As Integer = 0

    ''' <summary>The total number of tasks in this <see cref="Version"/>.</summary>
    Public ReadOnly Property alltaskscount As Integer
        Get
            Return _alltaskscount
        End Get
    End Property

    ''' <summary>The internally stored number of unassigned open/passed/failed/etc. tasks in this <see cref="Version"/>.</summary>
    Private _unassignedtasks As List(Of ResolutionCount) = Nothing

    ''' <summary>The number of unassigned open/passed/failed/etc. tasks in this <see cref="Version"/>.</summary>
    Public ReadOnly Property unassignedtasks
        Get
            Return _unassignedtasks
        End Get
    End Property

    ''' <summary>The internally stored total number of unassigned tasks in this <see cref="Version"/>.</summary>
    Dim _unassignedtaskscount As Integer = 0

    ''' <summary>The total number of unassigned tasks in this <see cref="Version"/>.</summary>
    Public ReadOnly Property unassignedtaskscount As Integer
        Get
            Return _unassignedtaskscount
        End Get
    End Property

    ''' <summary>The internally stored list of open/passed/failed/etc. tasks in this <see cref="Version"/> assigned to each user.</summary>
    Private _usersinversion As List(Of UserTasks)

    ''' <summary>The list of open/passed/failed/etc. tasks in this <see cref="Version"/> assigned to each user.</summary>
    Public ReadOnly Property usersinversion
        Get
            Return _usersinversion
        End Get
    End Property

    ''' <summary>The internally stored list of open/passed/failed/etc. tasks in this <see cref="Version"/> for each platform.</summary>
    Private _platformsInVersion As New ObservableCollection(Of PlatformTasks)

    ''' <summary>The list of open/passed/failed/etc. tasks in this <see cref="Version"/> for each platform.</summary>
    Public ReadOnly Property platformsInVersion As ObservableCollection(Of PlatformTasks)
        Get
            Return _platformsInVersion
        End Get
    End Property

    ''' <summary>The internally stored the <see cref="Platform"/> the user currently has selected in the admin panel.</summary>
    Private _selectedPlatform As PlatformTasks

    ''' <summary>The <see cref="Platform"/> the user currently has selected in the admin panel.</summary>
    Public Property selectedPlatform As PlatformTasks
        Get
            Return _selectedPlatform
        End Get
        Set(value As PlatformTasks)
            If value IsNot _selectedPlatform OrElse (value IsNot Nothing AndAlso Not value.Equals(_selectedPlatform)) Then
                setField(value, _selectedPlatform, Sub(v) _selectedPlatform = v)
            End If
        End Set
    End Property

#End Region

#Region "Initializing Leaf Task Resolution Information For Admin Panel"
    '###################################################################
    '#  Initializing Leaf Task Resolution Information For Admin Panel  #
    '###################################################################

    ''' <summary>
    ''' Load in and cache the all information about the number of open/passed/failed/etc. 
    ''' tasks in this version from the database on a total/per user/per platform basis.
    ''' </summary>
    ''' <param name="db">The database context to load the information from.</param>
    ''' <remarks>
    ''' These are all loaded from the same database context to try and make
    ''' as much use of the Entity Framework's built in caching as possible.
    ''' </remarks>
    Private Sub loadLeafNodeIndicators(db As IntelligenceDB)
        Dim tasksInThisVersion As IQueryable(Of TaskInstance) = db.TaskInstances.Where(Function(t) t.version.id = Me.id)
        Dim leafNodes As IQueryable(Of TaskInstance) = TaskInstanceUtils.filterLeafNodes(tasksInThisVersion, db)

        Me._allTasks = groupByResolution(leafNodes)
        Me._alltaskscount = totalTasks(_allTasks)

        Me._unassignedtasks = groupByResolution(leafNodes.Where(Function(leaf) leaf.assignee Is Nothing))
        Me._unassignedtaskscount = totalTasks(_unassignedtasks)

        Me._usersinversion = groupByUserAndResoltuion(leafNodes)

        Me.setUpPlatformList(groupByPlatformAndResoltuion(leafNodes))
    End Sub

    ''' <summary>
    ''' Group the given tasks by platform, and then count occurrances of each resolution in these subsets.
    ''' </summary>
    ''' <param name="tasks">A query that will return the tasks to group.</param>
    ''' <returns>A new list of <see cref="PlatformTasks"/> created from the grouped tasks.</returns>
    Public Shared Function groupByPlatformAndResoltuion(tasks As IQueryable(Of TaskInstance)) As List(Of PlatformTasks)
		Dim ts = From t In tasks
				 Group t By platform = t.platform Into g1 = Group, total = Count()
				 Order By platform.rank Ascending, platform.id Ascending
				 Select platform, total, g1

		Dim platformsList As New List(Of PlatformTasks)
        For Each platformTask In ts
            Dim resCounts = groupByResolution(platformTask.g1.AsQueryable())
			platformsList.Add(New PlatformTasks With {.name = platformTask.platform.name,
													  .numberOfLeavesByResolution = resCounts,
													  .total = platformTask.total,
													  .platform = platformTask.platform})
		Next

        Return platformsList
    End Function

    ''' <summary>
    ''' Group the given tasks by assignee, and then count occurrances of each resolution in these subsets.
    ''' Will ignore unassigned tasks.
    ''' </summary>
    ''' <param name="tasks">A query that will return the tasks to group.</param>
    ''' <returns>A new list of <see cref="UserTasks"/> created from the grouped tasks.</returns>
    Public Shared Function groupByUserAndResoltuion(tasks As IQueryable(Of TaskInstance)) As List(Of UserTasks)
        Dim ts = From t In tasks
                Where t.assignee IsNot Nothing
                Group t By assignee = t.assignee Into g1 = Group, total = Count()
                Select assignee, total, g1

        Dim usersList As New List(Of UserTasks)
        For Each userTask In ts
            Dim resCounts = groupByResolution(userTask.g1.AsQueryable())
            usersList.Add(New UserTasks With {.alltasksforuser = resCounts, .name = userTask.assignee.username, .total = userTask.total})
        Next

        Return usersList
    End Function

    ''' <summary>Count the number of occurrences of each resolution in the given tasks.</summary>
    ''' <param name="tasks">A query that will return the tasks to count.</param>
    ''' <returns>
    ''' A new list mapping each resolution to the number of tasks with this resolution.
    ''' The resolutions are sorted by id to ensure they always appear in the same order on the UI.
    ''' </returns>
    Public Shared Function groupByResolution(tasks As IQueryable(Of TaskInstance)) As List(Of ResolutionCount)
        Dim groupdedTasks = tasks.GroupBy(
            Function(t) t.resolution,
            Function(res As Resolution, nodes As IEnumerable(Of TaskInstance)) New With {.resolution = res, .Count = nodes.Count})
        Dim sortedGroupedTasks = groupdedTasks.OrderBy(Function(t) t.resolution.id)
        Dim list As New List(Of ResolutionCount)

        For Each g In sortedGroupedTasks
            list.Add(New ResolutionCount With {.resolution = g.resolution, .Count = g.Count})
        Next

        Return list
    End Function

    ''' <summary>Get the total number of tasks by adding the totals for each resolution.</summary>
    ''' <param name="resCounts">The total numbers of tasks for each resolution.</param>
    ''' <returns>How many tasks there are overall in the per-resolution breakdown.</returns>
    Public Shared Function totalTasks(resCounts As List(Of ResolutionCount)) As Integer
        Dim count As Integer = 0
        For Each resCount In resCounts
            count += resCount.Count
        Next
        Return count
    End Function

    ''' <summary>
    ''' Update the existing platforms list using the new list.
    ''' As tasks can never be deleted, existing items will be updated, and new items will be
    ''' added. This allows the user's currently selected platform to be maintained as items
    ''' are never removed from the list (only their fields are updated upon a refresh).
    ''' </summary>
    ''' <param name="newPlatforms">The new numbers of open/passed/failed etc. tasks per platform.</param>
    Private Sub setUpPlatformList(newPlatforms As List(Of PlatformTasks))
		For Each newPlatform In newPlatforms
			Dim oldPlatform As PlatformTasks = platformsInVersion.Where(Function(p) p.platform.id = newPlatform.platform.id).FirstOrDefault()
			If oldPlatform Is Nothing Then
				Me.platformsInVersion.Add(newPlatform)
			Else
				oldPlatform.numberOfLeavesByResolution = newPlatform.numberOfLeavesByResolution
				oldPlatform.total = newPlatform.total
			End If
		Next
	End Sub
#End Region

#Region "Inner Classes"
	'###################################################################
	'#                       Inner Classes                             #
	'###################################################################

	''' <summary>Stores the number of tasks for a single platform this version with each resolution .</summary>
	Public Class PlatformTasks
        Inherits NotifyPropertyChangedBase

        ''' <summary>The platform the tasks are for.</summary>
        Public Property platform As Platform

        ''' <summary>The platform's name.</summary>
        Public Property name As String

        ''' <summary>The internally stored number of tasks for this platform with each resolution.</summary>
        Private _numberOfLeavesByResolution As List(Of ResolutionCount)

        ''' <summary>The number of tasks for this platform with each resolution.</summary>
        Public Property numberOfLeavesByResolution As List(Of ResolutionCount)
            Get
                Return _numberOfLeavesByResolution
            End Get
            Set(value As List(Of ResolutionCount))
                setField(value, _numberOfLeavesByResolution, Sub(v) _numberOfLeavesByResolution = v)
            End Set
        End Property

        ''' <summary>The internally stored total number of tasks for this platform.</summary>
        Private _total As Integer

        ''' <summary>The total number of tasks for this platform.</summary>
        Public Property total As Integer
            Get
                Return _total
            End Get
            Set(value As Integer)
                setField(value, _total, Sub(v) _total = v)
            End Set
        End Property

        ''' <summary>
        ''' Two <see cref="PlatformTasks"/> objects will be equal of they are for the same platform.
        ''' </summary>
        Public Overrides Function Equals(obj As Object) As Boolean
            Dim objAsPlatformTasks As PlatformTasks = TryCast(obj, PlatformTasks)
            Return objAsPlatformTasks IsNot Nothing AndAlso objAsPlatformTasks.platform.id = Me.platform.id
        End Function
    End Class

    ''' <summary>Stores the number of tasks in this version for a single assignee with each resolution .</summary>
    Public Class UserTasks
        ''' <summary>The assignee's username.</summary>
        Public Property name As String

        ''' <summary>The number of tasks for this assignee with each resolution.</summary>
        Public Property alltasksforuser As List(Of ResolutionCount)

        ''' <summary>The total number of tasks for this assignee.</summary>
        Public Property total As Integer
    End Class

    ''' <summary>Stores the number of tasks with a particular resolution.</summary>
    Public Class ResolutionCount

        ''' <summary>The resolution the tasks share.</summary>
        Public Property resolution As Resolution

        ''' <summary>The number of tasks with this resolution.</summary>
        Public Property Count As Integer
    End Class

#End Region

End Class
