﻿'############################################################################
'AccessLevelUtils.vb
'
'Purpose: To act as an 'enum' class for AccessLevels
'
'Description: Contains each Access Level in Database (as of 16/07/15). 
'Will populate each Access Level with associated Access Level stored in database
'
'Modification: To create a new Access Level entry, copy...

'       Private Shared _dbYourAccessLevel As AccessLevel = GetAccessLevelByName("YourAccessLevel")
'       Public Shared ReadOnly Property yourAccessLevel As AccessLevel
'           Get
'               Return _dbYourAccessLevel
'           End Get
'       End Property
'
' ...into AccessLevelUtils class.
'############################################################################



Public Class AccessLevelUtils

#Region "AccessLevels"

    'Get _dbAdminLevel
    'Sync _dbAdminLevel with database on init. Always return _dbAdminLevel
    Private Shared _dbAdminLevel As AccessLevel = GetAccessLevelByName("admin")
    Public Shared ReadOnly Property adminLevel As AccessLevel
        Get
            Return _dbAdminLevel
        End Get
    End Property

    'Get _dbManagerLevel
    'Sync _dbManagerLevel with database on init. Always return _dbManagerLevel
    Private Shared _dbManagerLevel As AccessLevel = GetAccessLevelByName("manager")
    Public Shared ReadOnly Property managerLevel As AccessLevel
        Get
            Return _dbManagerLevel
        End Get
    End Property

    'Get _dbDeveloperLevel
    'Sync _dbDeveloperLevel with database on init. Always return _dbDeveloperLevel
    Private Shared _dbDeveloperLevel As AccessLevel = GetAccessLevelByName("developer")
    Public Shared ReadOnly Property developerLevel As AccessLevel
        Get
            Return _dbDeveloperLevel
        End Get
    End Property

    'Get _dbInactiveLevel
    'Sync _dbInactiveLevel with database on init. Always return _dbInactiveLevel
    Private Shared _dbInactiveLevel As AccessLevel = GetAccessLevelByName("inactive")
    Public Shared ReadOnly Property inactiveLevel As AccessLevel
        Get
            Return _dbInactiveLevel
        End Get
    End Property


    'Purpose: Checks to see if current user has admin privilages, returns true if yes, false if no
    Public Shared Function CanAccessAdminContent() As Boolean
        If UserInfo.getCurrentIntelligenceUser.accessLevel.id >= adminLevel.id Then
            Return True
        End If
        Return False
    End Function

    'Checks to see if current user has manager privilages, returns true if yes, false if no
    Public Shared Function CanAccessManagerContent() As Boolean
        If UserInfo.getCurrentIntelligenceUser.accessLevel.id >= managerLevel.id Then
            Return True
        End If
        Return False
    End Function

    'Checks to see if current user has developer privilages, returns true if yes, false if no
    Public Shared Function CanAccessDeveloperContent() As Boolean
        If UserInfo.getCurrentIntelligenceUser.accessLevel.id >= developerLevel.id Then
            Return True
        End If
        Return False
    End Function


    'Takes in 'name' and collects database accesslevel with name field matching name
    Private Shared Function GetAccessLevelByName(name As String) As AccessLevel
        Using db As New IntelligenceDB
            Return (From level In db.AccessLevels _
                    Where level.name.ToLower.Equals(name.ToLower) AndAlso level.active = True _
                    Select level).FirstOrDefault()
        End Using
    End Function

    Private Shared _allAccessLevels As IEnumerable(Of AccessLevel)
    Public Shared ReadOnly Property getAllAccessLevels As IEnumerable(Of AccessLevel)
        Get
            If _allAccessLevels Is Nothing Then
                Using db As New IntelligenceDB
                    _allAccessLevels = db.AccessLevels.Where(Function(a) a.active).ToList()
                End Using
            End If
            Return _allAccessLevels
        End Get
    End Property


#End Region

    Public Shared Function showLoginDialogForMantis() As Boolean
        Dim dlg As New LoginDialog("Enter Mantis Login", "Log in below...", True, False)
        dlg.ShowDialog()
        Return dlg.DialogResult
    End Function

End Class
