﻿Imports WPFAPP

Public Class AttachmentList(Of T As {Class, IHasID, New, IRanked, IAttachment})
	Inherits RankedWrapperList(Of T)

	Private initialiseTypeSpecificFields As Action(Of T, IntelligenceDB)

	Private _canBeEdited As Boolean = True
	Private idOfHolder As Integer

	Public Sub New(
				  contentsLoader As Func(Of IntelligenceDB, IEnumerable(Of T)),
				  wrap As Func(Of T, IntelligenceDB, Wrapper(Of T)),
				  initialiseTypeSpecificFields As Action(Of T, IntelligenceDB),
				  canBeEdited As Boolean,
				  idOfHolder As Integer)

		MyBase.New(contentsLoader, wrap)
		Me.initialiseTypeSpecificFields = initialiseTypeSpecificFields
		Me._canBeEdited = canBeEdited
		Me.idOfHolder = idOfHolder
	End Sub

	Public ReadOnly Property addAttachment As ICommand
		Get
			Return New GenericCommand(
				Sub()
					Dim openDlg As New Microsoft.Win32.OpenFileDialog()
					openDlg.Filter = "All files (*.*)|*.*"

					Dim result? As Boolean = openDlg.ShowDialog()

					If result = True Then

						Dim fullPathName As String = openDlg.FileName
						Dim shortFileName As String = openDlg.SafeFileName

						LoadWithAsyncGUITask.run(Of IAttachment)(
							"Uploading attachment",
							Function() AttachmentUtils.uploadFileForAttachment(Of T)(fullPathName, shortFileName, idOfHolder),
							Sub(attachmentDetails As IAttachment)
								If attachmentDetails Is Nothing Then
									Dim errorDialog As New ErrorDialog("Upload Error", "Unable to upload the attachment file", "Something went wrong, so the new attachment could not be added.")
									errorDialog.Show()
								Else
									Debug.Print("Adding to the list.")
									Dim success As Boolean = True
									Using db As New IntelligenceDB

										Dim newItem As New T
										db.Set(Of T).Add(newItem)

										newItem.dateSubmitted = attachmentDetails.dateSubmitted
										newItem.fileName = attachmentDetails.fileName
										newItem.fileUrl = attachmentDetails.fileUrl
										newItem.submitter = db.IntelligenceUsers.Find(UserInfo.getCurrentIntelligenceUser().id)
										newItem.rank = If(Not Me.Any, 0, Me.Max(Function(w) w.getWrappedItem.rank)) + 1

										initialiseTypeSpecificFields(newItem, db)

										Dim wrappedItem As Wrapper(Of T) = wrap(newItem, db)
										Me.Add(wrappedItem)

										success = DBManager.saveChanges(db)
									End Using
									If Not success Then
										Dim errorDialog As New ErrorDialog("Database Error", "Unable to add attachment to database", "Something went wrong with the database connection, so the new attachment could not be added.")
										errorDialog.Show()
									Else
										MessageDialog.display("Success", "Finished uploading " & shortFileName)
									End If
								End If
							End Sub
						)
					End If
				End Sub
			)
		End Get
	End Property

	Public ReadOnly Property deleteAttachment As ICommand
		Get
			Return New DeleteCommand(Of T)(Me, Function() True)
		End Get
	End Property

	Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of T)) As Boolean
		Dim wrappedItem As T = db.Set(Of T).Find(toDelete.id)
		If wrappedItem IsNot Nothing Then
			AttachmentUtils.deleteFileForAttachment(wrappedItem) 'Remove the actual file before the metadata
			db.Set(Of T).Remove(wrappedItem)
			Return DBManager.saveChanges(db)
		End If
		Return False
	End Function

	Public ReadOnly Property canBeEdited As Boolean
		Get
			Return _canBeEdited
		End Get
	End Property

End Class
