﻿Imports System.Net.Http
Imports System.IO
Imports System.Configuration

Public Class AttachmentUtils

	Public Shared Function getAttachmentsForTemplate(template As TaskTemplate, canBeEdited As Boolean) As AttachmentList(Of TemplateAttachment)
		Dim attachments As AttachmentList(Of TemplateAttachment) =
			New AttachmentList(Of TemplateAttachment)(
				Function(db As IntelligenceDB)
					Dim myId As Integer = template.id
					Return (
						From attachment As TemplateAttachment In db.TemplateAttachments
						Where attachment.template.id = myId
						Order By attachment.rank Ascending, attachment.id Ascending
						Select attachment
					)
				End Function,
				Function(a, db) New AttachmentWrapper(Of TemplateAttachment)(a, template.id, db),
				Sub(a, db)
					Dim templateFromDB = db.TaskTemplates.Find(template.id)
					a.template = templateFromDB
				End Sub,
				canBeEdited,
				template.id
			)
		Return attachments
	End Function

	Public Shared Function getAttachmentsForInstance(task As TaskInstance, canBeEdited As Boolean) As AttachmentList(Of InstanceAttachment)
		Dim attachments As AttachmentList(Of InstanceAttachment) =
			New AttachmentList(Of InstanceAttachment)(
				Function(db As IntelligenceDB)
					Return (
						From attachment As InstanceAttachment In db.InstanceAttachments
						Where attachment.instance.id = task.id
						Order By attachment.rank Ascending, attachment.id Ascending
						Select attachment
					)
				End Function,
				Function(a, db) New AttachmentWrapper(Of InstanceAttachment)(a, task.id, db),
				Sub(a, db)
					Dim instanceFromDB = db.TaskInstances.Find(task.id)
					a.instance = instanceFromDB
				End Sub,
				canBeEdited,
				task.id
			)
		Return attachments
	End Function

	'TODO Make this work - the TaskInstanceUtils.isAncestorOf part does not work, so there probably needs to be an imperative loop to add to a stack here, rather than using recursion directly in the LINQ
	Public Shared Function getInheritedAttachmentsForTemplate(template As TaskTemplate) As AttachmentList(Of TemplateAttachment)
		Dim attachments As AttachmentList(Of TemplateAttachment) =
			New AttachmentList(Of TemplateAttachment)(
				Function(db As IntelligenceDB)
					Return (
						From attachment As TemplateAttachment In db.TemplateAttachments
						Where attachment.shouldBeInherited _
						AndAlso False 'TaskInstanceUtils.isAncestorOf(template, attachment.template)
						Order By attachment.template.id Ascending, attachment.rank Ascending, attachment.id Ascending
						Select attachment
					)
				End Function,
				Function(a, db) New AttachmentWrapper(Of TemplateAttachment)(a, template.id, db),
				Sub(a, db)
					Throw New NotSupportedException("You cannot add new elements to the list of inherited attachments!")
				End Sub,
				False,
				template.id
			)
		Return attachments
	End Function

	Private Shared Function getPathToSharedDriveBaseFolder() As String

		Dim sharedDriveFromConfig As String = My.Settings.AttachmentsSharedDrivePath
		If sharedDriveFromConfig Is Nothing Then
			Throw New Exception("Unable to find shared drive name in config. Check your settings for ""AttachmentsSharedDrivePath"" in ""Intelligence.exe.config.deploy"" ")
		End If
		Return sharedDriveFromConfig
		'Dim config = ConfigurationManager.AppSettings("AttachmentsSharedDrivePath")
		'Return config
		'= ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath)
		'config.AppSettings.Settings.Add(Key, value);
		'config.Save(ConfigurationSaveMode.Minimal);
	End Function

	Private Shared Function getPathToSharedDriveLocationForAttachment(Of T As {Class, New, IHasID, IRanked, IAttachment})(attachment As T, idOfHodler As Integer) As String
		Dim baseDir As String = getPathToSharedDriveBaseFolder()
		Dim fileName As String = getStorageFileName(attachment, idOfHodler)
		Return baseDir + "/" + fileName
	End Function

	Public Shared Function uploadFileForAttachment(Of T As {IHasID, IRanked, IAttachment, New, Class})(localFilePath As String, shortFileName As String, idOfHolder As Integer) As T 'Return metadata with the URL it was uploaded to if successful
		Dim attachment As T = New T()

		'Placeholder HTTP urls
		'https://upload.wikimedia.org/wikipedia/commons/6/6d/Metallic_shield_bug444.jpg
		'https://www.bugs.com/images/pest-library/other-bugs.png
		'Dim uploadedUrl As String = "https://www.bugs.com/images/pest-library/other-bugs.png"


		attachment.dateSubmitted = DateTime.Now
		attachment.fileName = shortFileName

		'Copy to shared drive
		Dim uploadedUrl As String = getPathToSharedDriveLocationForAttachment(attachment, idOfHolder)

		'Rename if there is already a duplicate named file
		If File.Exists(uploadedUrl) Then
			Dim index As Integer = 1
			Dim newUploadUrl = ""

			Dim stopIndex As Integer = uploadedUrl.LastIndexOf(".")
			Dim baseName As String = If(stopIndex >= 0, uploadedUrl.Substring(0, stopIndex), uploadedUrl)
			Dim extension As String = If(stopIndex >= 0, uploadedUrl.Substring(stopIndex), "")
			Do
				newUploadUrl = baseName & "_" & padWithZeros(index, 3) & extension
				index += 1
			Loop While File.Exists(newUploadUrl)
			uploadedUrl = newUploadUrl
		End If

			makeAllDirsForFilePath(uploadedUrl)
		System.IO.File.Copy(localFilePath, uploadedUrl, True)
		attachment.fileUrl = uploadedUrl

		Return attachment
	End Function

	Public Shared Function deleteFileForAttachment(attachment As IAttachment) As Boolean

		'For normal file on shared drive
		System.IO.File.Delete(attachment.fileUrl)

		Return True
	End Function

	Public Shared Function downloadFileForAttachment(attachment As IAttachment) As Byte() 'Return temp file URL after download or Nothing if unsuccessfy
		Dim fileBytes As Byte()


		If attachment.fileUrl.StartsWith("http") OrElse attachment.fileUrl.StartsWith("https") Then
			'For HTTP
			Dim client As HttpClient = New HttpClient
			fileBytes = client.GetByteArrayAsync(attachment.fileUrl).Result
		Else
			'For normal file on shared drive
			fileBytes = System.IO.File.ReadAllBytes(attachment.fileUrl)
		End If

		Return fileBytes
	End Function

	Public Shared Function downloadTempFileForAttachment(Of T As {Class, New, IHasID, IRanked, IAttachment})(attachment As T, idOfHodler As Integer) As String 'Return temp file URL after download or Nothing if unsuccessfyk

		Dim storageFileName As String = getStorageFileName(attachment, idOfHodler)
		Dim fileString As String = System.IO.Path.GetTempPath + storageFileName

		'Dim fileString As String = System.IO.Path.GetTempPath & "intelligence\" & typeName & "_" & id & "_" & fileName

		Try
			Debug.Print("Writing " & attachment.id & " = " & attachment.fileName & " to " & fileString)
			makeAllDirsForFilePath(fileString)
			Debug.Print("made dir path")

			Debug.Print("Downloading " & attachment.id)
			Dim tempFile As Byte() = downloadFileForAttachment(attachment)
			If tempFile IsNot Nothing Then
				System.IO.File.WriteAllBytes(fileString, tempFile)
			Else
				fileString = Nothing
			End If
		Catch e As Exception
			Debug.Print(e.Message)
			fileString = Nothing
		End Try

		Debug.Print("set uri to " & fileString)
		Return fileString
	End Function

	Private Shared Sub makeAllDirsForFilePath(fullFilePath As String)
		Dim fInfo As New FileInfo(fullFilePath)
		Dim directoryPath = fInfo.Directory.FullName
		System.IO.Directory.CreateDirectory(directoryPath)
	End Sub

	Private Shared Function getStorageFileName(Of T As {Class, New, IHasID, IRanked, IAttachment})(attachment As T, idOfHolder As Integer) As String
		Dim typeName As String = getTypeName(Of T)()
		Dim fileName As String = attachment.fileName
		Dim id As String = padWithZeros(idOfHolder, 5)

		Dim fileString As String = String.Format("intelligence\{0}_{1}_{2}", typeName, id, fileName)
		Return fileString
	End Function

	Private Shared Function padWithZeros(stringToPad As String, desiredLength As Integer) As String
		While stringToPad.Length < desiredLength
			stringToPad = "0" & stringToPad
		End While
		Return stringToPad
	End Function

	Private Shared Function getTypeName(Of T As {Class, New, IHasID, IRanked, IAttachment})()
		Dim tp As Type = GetType(AttachmentWrapper(Of T))
		Dim g As Type = tp.GetGenericTypeDefinition()
		Dim typeName As String = g.Name
		Dim stopIndex As Integer = typeName.IndexOf("`")
		Dim result As String = typeName.Substring(0, stopIndex)

		Return result
	End Function

End Class
