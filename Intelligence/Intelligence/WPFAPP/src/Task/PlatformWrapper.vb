﻿''' <summary>A wrapper around a <see cref="Platform"/> from the database.</summary>
Public Class PlatformWrapper
	Inherits RankedWrapper(Of Platform)

    ''' <summary>Create a new wrapper around the given platform.</summary>
    ''' <param name="platform">The platform to wrap.</param>
    Sub New(platform As Platform)
        MyBase.New(platform)
    End Sub

    ''' <summary>
    ''' The human-readable name of the platform.
    ''' Setting this will automaticatically save the changes to the database and notify the UI.
    ''' </summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem().name
        End Get
        Set(value As String)
            setFieldAndSave(value, getWrappedItem().name, Sub(v) getWrappedItem().name = v)
        End Set
    End Property

End Class
