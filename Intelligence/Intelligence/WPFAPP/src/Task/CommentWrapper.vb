﻿Imports WPFAPP
''' <summary>
''' A wrapper around a <see cref="Comment"/> from the intelligence database.
''' Comments contain up to 64KB of text, and have metadata about the submitter, 
''' the submission time, and the last edited time. Each comment also contains 
''' the id of the <see cref="TaskInstance"/> they were found in.
''' </summary>
Public Class CommentWrapper
	Inherits Wrapper(Of Comment)

	Private _commenter As IntelligenceUserWrapper
	Private _isNew As Boolean
	Private task As TaskInstanceWrapper

    ''' <summary>Create a new wrapper around the given comment.</summary>
    ''' <param name="comment">The comment to wrap.</param>
	''' <param name="db">The database context the comment was retreived from.</param>
    Sub New(comment As Comment, task As TaskInstanceWrapper, Optional db As IntelligenceDB = Nothing)
		MyBase.New(comment, db)
		Me.task = task
	End Sub

	Protected Overrides Sub initializeFields(newItem As Comment, Optional db As IntelligenceDB = Nothing)
		MyBase.initializeFields(newItem, db)
		If db IsNot Nothing Then
			_commenter = New IntelligenceUserWrapper(newItem.commenter)
			_isNew = False
		Else
			_commenter = UserInfo.getCurrentIntelligenceUser()
			_isNew = True
		End If
	End Sub

    ''' <summary>Returns the text of the comment. Cannot be set here.</summary>
    Public Overrides Property name As String
		Get
			Return "" 'Comment #" & getWrappedItem.id & ": """ & Me.text & """
		End Get
		Set(value As String)
		End Set
	End Property


	''' <summary>Returns the text of the comment. Cannot be set here.</summary>
	Public Property text As String
		Get
			Return getWrappedItem.text
		End Get
		Set(value As String)
			If canEdit Then
				Using db As New IntelligenceDB
					Dim commenter As IntelligenceUser = db.Set(Of IntelligenceUser).Find(_commenter.getWrappedItem.id)
					If _isNew Then
						setFieldAndMarkModified(DateTime.Now(), getWrappedItem.dateSubmitted, Sub(v) getWrappedItem.dateSubmitted = v, db)
					Else
						setFieldAndMarkModified(DateTime.Now(), getWrappedItem.dateEdited, Sub(v) getWrappedItem.dateEdited = v, db)
					End If
					setFieldAndMarkModified(commenter, commenter, Sub(v) getWrappedItem().commenter = v, db)
					setFieldAndMarkModified(value, getWrappedItem.text, Sub(v) getWrappedItem().text = v, db)
					If saveChanges(db) Then
						onAllPropertiesChanged()
						task.scanCommentsForBugIds(value)
					End If
				End Using
			End If
		End Set
	End Property

	Public ReadOnly Property canEdit As Boolean
		Get
			Dim currentUser As IntelligenceUserWrapper = UserInfo.getCurrentIntelligenceUser()
			Return currentUser.id.Equals(_commenter.id)
		End Get
	End Property

	Public ReadOnly Property commenterName As String
		Get
			Return _commenter.name
		End Get
	End Property

	Public ReadOnly Property submittedDate As DateTime?
		Get
			If _isNew Then
				Return Nothing
			Else
				Return getWrappedItem.dateSubmitted
			End If
		End Get
	End Property

	Public ReadOnly Property editedDate As DateTime?
		Get
			Return getWrappedItem.dateEdited
		End Get
	End Property

	Public ReadOnly Property wasEdited As Boolean
		Get
			Return editedDate IsNot Nothing
		End Get
	End Property
End Class
