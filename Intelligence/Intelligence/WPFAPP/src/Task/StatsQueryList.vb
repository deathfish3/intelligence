﻿''' <summary>A list containing all <see cref="StatsQueryWrapper"/>s visible to the given user.</summary>
Public Class StatsQueryList
    Inherits WrapperList(Of StatsQuery)

    ''' <summary>
    ''' Create a new list of all stats queries that are either created
    ''' by the given user, or have been marked as publicly accessible.
    ''' </summary>
    ''' <param name="user">The user accessing the stats queries.</param>
    Sub New(user As IntelligenceUser)
        MyBase.New(Function(db As IntelligenceDB) (From statQuery In db.StatsQueries
                                                  Where statQuery.is_visible_to_others _
                                                  OrElse statQuery.creator.id = user.id
                                                  Select statQuery),
                  Function(s As StatsQuery, db As IntelligenceDB) New StatsQueryWrapper(s))
    End Sub

    ''' <summary>Initialise a newly created stats query with default values.</summary>
    ''' <param name="newItem">The newly created stats query.</param>
    ''' <param name="db">The open database context the query was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As StatsQuery, db As IntelligenceDB)
        MyBase.initialiseNewItem(newItem, db)
        newItem.timeCreated = DateTime.Now
        newItem.creator = db.IntelligenceUsers.Find(UserInfo.getCurrentIntelligenceUser().id)
        newItem.type = db.StatsQueryTypes.Find(StatsQueryTypeUtils.intelligenceTaskStats.id)
        newItem.is_visible_to_others = False
        newItem.name = ""
        newItem.description = ""
        newItem.queryString = ""
    End Sub

    ''' <summary>Delete the given query permenantly from the database and save the changes.</summary>
    ''' <param name="db">The open database context to delete the stats query from.</param>
    ''' <param name="toDelete">A wrapper around the stats query to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of StatsQuery)) As Boolean
        Dim toDeleteFromDB As StatsQuery = db.StatsQueries.Find(toDelete.id)
        db.StatsQueries.Remove(toDeleteFromDB)
        Return DBManager.saveChanges(db)
    End Function

End Class
