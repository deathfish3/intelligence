﻿''' <summary>
''' A list containing <see cref="TaskTemplateWrapper"/>s from a given project.
''' This list can represent all top-level root templates, or a list of
''' templates for subtasks of an existing template.
''' </summary>
Public Class TaskTemplateList
	Inherits RankedWrapperList(Of TaskTemplate)

    ''' <summary>A wrapper around the product the versions are for.</summary>
    Private product As ProductWrapper

    ''' <summary>The template that the tasks in this list are subtasks of.</summary>
    Private _parent As TaskTemplateWrapper
    ''' <summary>
    ''' The template that the tasks in this list are subtasks of.
    ''' If this is <c>Nothing</c> then the tasks in this list are
    ''' top-level root tasks for the given product.
    ''' </summary>
    Public ReadOnly Property parent As TaskTemplateWrapper
        Get
            Return _parent
        End Get
    End Property

    '''<summary>Constructor for creating list of all top level tasks for a product.</summary>
    ''' <param name="product">The product the templates belong to.</param>
    Public Sub New(product As ProductWrapper)
        Me.New(product, Nothing,
               Function(db As IntelligenceDB)
                   Return From task As TaskTemplate In db.TaskTemplates
                          Where task.product.id = product.id _
                          AndAlso task.active _
                          AndAlso task.parentTask Is Nothing
                          Order By task.rank Ascending, task.id Ascending
                          Select task
               End Function)
    End Sub

    ''' <summary>
    ''' Constructor for creating list of subtasks for the given parent task.
    ''' The <paramref name="parent"/> must not be <c>Nothing</c>.
    ''' </summary>
    ''' <param name="product">The product the templates belong to.</param>
    ''' <param name="parent">
    ''' The template that the tasks in this list are subtasks of.
    ''' Must not be <c>Nothing</c>. 
    ''' </param>
    Public Sub New(product As ProductWrapper, parent As TaskTemplateWrapper)
        Me.New(product, parent,
               Function(db As IntelligenceDB)
                   Return From task In db.TaskTemplates
                          Where task.parentTask.id = parent.id _
                          AndAlso task.active
                          Order By task.rank Ascending, task.id Ascending
                          Select task
               End Function)
    End Sub


    ''' <summary>The internal constructor used by both top-level task lists, and subtask lists.</summary>
    ''' <param name="product">The product the templates belong to.</param>
    ''' <param name="parent">
    ''' The template that the tasks in this list are subtasks of.
    ''' If this is <c>Nothing</c> then the tasks in this list are
    ''' top-level root tasks for the given product.
    ''' </param>
    ''' <param name="getContents">
    ''' A function for loading the contents from the database.
    ''' This function will be different depending on whether the list
    ''' is for top-level root tasks, or subtasks.
    ''' </param>
    Private Sub New(product As ProductWrapper, parent As TaskTemplateWrapper, getContents As System.Func(Of IntelligenceDB, IEnumerable(Of TaskTemplate)))
        MyBase.New(getContents)
        Me.product = product
        Me._parent = parent
        Me.wrap = AddressOf wrapTemplate
        reloadContents()
    End Sub

    ''' <summary>
    ''' Create a new <see cref="TaskTemplateWrapper"/> around the given template.
    ''' Will set up its parent wrapper, and container fields as well.
    ''' </summary>
    ''' <param name="t">The template to wrap.</param>
    ''' <param name="db">The database context the template retreived from.</param>
    ''' <returns>A newly created wrapper around the given template.</returns>
    Private Function wrapTemplate(t As TaskTemplate, db As IntelligenceDB) As TaskTemplateWrapper
        Return New TaskTemplateWrapper(t, parent, product, Me)
    End Function

    ''' <summary>Initialize a new blank task template with default values.</summary>
    ''' <param name="newItem">The newly created task template.</param>
    ''' <param name="db">The database context the template was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As TaskTemplate, db As IntelligenceDB)
        If parent IsNot Nothing Then
            newItem.parentTask = db.TaskTemplates.Find(parent.id)
        End If

        newItem.name = ""
        newItem.description = ""
        newItem.active = True
        newItem.product = db.Products.Find(product.id)
        newItem.rank = If(Not Me.Any, 0, Me.Max(Function(w) w.getWrappedItem.rank)) + 1

        If parent Is Nothing Then
            newItem.platforms = New List(Of Platform)
        Else
            newItem.platforms = parent.getCopyOfPlatforms(db)
        End If
    End Sub

    ''' <summary>
    ''' A flag to specify whether the compatible platforms for a parent template
    ''' should be updated when a subtask is deleted from it. Used in
    ''' <see cref="copyExistingTemplate"/> to minimize the amount of database
    ''' updates required by <see cref="deleteFromDB"/>.
    ''' </summary>
    Private shouldUpdatePlatformsOnDeletion As Boolean = True

    ''' <summary>
    ''' Delete the given task template, along with all its subtasks.
    ''' Will also update the parent's compatible platforms if the
    ''' <see cref="shouldUpdatePlatformsOnDeletion"/> is set (which it will
    ''' be by default).
    ''' </summary>
    ''' <param name="db">The database context to delete from.</param>
    ''' <param name="toDeleteWrapper">A wrapper around the template to delete.</param>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDeleteWrapper As Wrapper(Of TaskTemplate)) As Boolean
		Dim toDelete As TaskTemplate = db.TaskTemplates.Find(toDeleteWrapper.id)
		If shouldUpdatePlatformsOnDeletion AndAlso parent IsNot Nothing AndAlso parent.subTasks.Any() Then
			For Each platform As Platform In toDelete.platforms
				parent.updateTaskWhenPlatformRemovedFromChild(platform, db)
			Next
		End If
		Return DBManager.deleteTaskTemplate(db, toDelete)
	End Function

	Overrides Function getCopyToMoveIntoList(wrapperToMove As RankedWrapper(Of TaskTemplate), db As IntelligenceDB) As RankedWrapper(Of TaskTemplate)
		Return copyExistingTemplate(wrapperToMove, db, True, True)
	End Function

    ''' <summary>
    ''' Take the given wrapper, and recursively create a copy of it and all
    ''' its sub-tasks within this list. If <paramref name="shouldDeleteOldTemplate"/>
    ''' is <c>True</c> it will also recursively delete the old wrapper and its subtasks
    ''' from the database. The new copy will inherit any compatible platforms from its new
    ''' parent task (it will keep the existing platforms if this list is for a root task).
    ''' Used for both duplication and moving wrappers to new parents.
    ''' </summary>
    ''' <param name="toCopy">A wrapper around the template to copy.</param>
    ''' <param name="db">The database context to modify the templates in.</param>
    ''' <param name="shouldDeleteOldTemplate">
    ''' If true, the old template and all its subtasks will be removed from its
    ''' previous parent and deleted from the database. Otherwise, this function will
    ''' act as if duplicating the template rather than moving it.
    ''' </param>
    ''' <param name="shouldUpdatePlatforms">
    ''' If both this and <paramref name=" shouldDeleteOldTemplate"/> are <c>True</c>
    ''' then the platforms that the wrapper's old parent task was compatible with will
    ''' be updated when the wrapper is deleted (to avoid a parent template being compatible
    ''' with a platform despite having no children that are compatible with that platform).
    ''' This only needs to be <c>True</c> for the top-level calling of this function,
    ''' as further recursive calls will be deleting the parent task anyway, making it
    ''' unnecessary to update any platforms in them.
    ''' </param>
    ''' <returns>
    ''' A wrapper around the newly moved template 
    ''' (because this function may destroy the original wrapper, and replace it).
    ''' </returns>
    Public Function copyExistingTemplate(toCopy As TaskTemplateWrapper, db As IntelligenceDB, shouldDeleteOldTemplate As Boolean, shouldUpdatePlatforms As Boolean) As Wrapper(Of TaskTemplate)
        If Not shouldDeleteOldTemplate OrElse canAddCopy(toCopy) Then

            Dim newParentTask = If(parent IsNot Nothing, db.TaskTemplates.Find(parent.id), Nothing)

            Dim copyFrom As TaskTemplate = db.TaskTemplates.Find(toCopy.id)
            Dim copyTo As New TaskTemplate
            db.TaskTemplates.Add(copyTo)

            Dim platforms As IEnumerable(Of Platform)
            If Not shouldDeleteOldTemplate Then
                platforms = toCopy.getCopyOfPlatforms(db)
            ElseIf parent IsNot Nothing Then
                platforms = parent.getCopyOfPlatforms(db)
            Else
                platforms = toCopy.getCopyOfPlatforms(db)
            End If

            copyTemplate(copyFrom, copyTo, newParentTask, platforms, db)

            Dim copiedWrapper = wrapTemplate(copyTo, db)

            MyBase.Add(copiedWrapper)

            DBManager.saveChanges(db)

            For Each child In toCopy.subTasks.ToList()
                copiedWrapper.subTasks.copyExistingTemplate(child, db, shouldDeleteOldTemplate, False)
            Next

            If shouldDeleteOldTemplate Then
                shouldUpdatePlatformsOnDeletion = shouldUpdatePlatforms
                toCopy.container.Remove(toCopy)
                toCopy.container.deleteFromDB(db, toCopy)
                shouldUpdatePlatformsOnDeletion = True
            End If

            Return copiedWrapper
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Avoid infinite loops caused by copying a parent below one of its children in the tree.
    ''' Also avoid redundant copying if the parent does not change.
    ''' </summary>
    ''' <param name="toCopy">A wrapper around the template to copy.</param>
    ''' <returns>True if the wrapper can be safely duplicated into this list.</returns>
    Public Function canAddCopy(toCopy As TaskTemplateWrapper) As Boolean
        Return Not isAncestorOf(parent, toCopy) AndAlso Not isParentOf(toCopy, parent)
    End Function

    ''' <summary>Check whether the given node is an ancestor of the given child node.</summary>
    ''' <param name="child">The lower level node to check the ancestors of.</param>
    ''' <param name="ancestor">The higher level node to check whether it is an ancestor.</param>
    ''' <returns>
    ''' Returns <c>True</c> if <paramref name="child"/> is a descendant of <paramref name="ancestor"/>
    ''' </returns>
    Public Shared Function isAncestorOf(child As TaskTemplateWrapper, ancestor As TaskTemplateWrapper) As Boolean
        Return child IsNot Nothing AndAlso ancestor IsNot Nothing _
        AndAlso (ancestor.id = child.id OrElse isAncestorOf(child.parent, ancestor))
    End Function

    ''' <summary>Check whether the given node is the direct parent of the given child node.</summary>
    ''' <param name="child">The lower level node to check the parent of.</param>
    ''' <param name="parent">The higher level node to check whether it is the parent.</param>
    ''' <returns>
    ''' Returns <c>True</c> if <paramref name="child"/> is a the direct child of <paramref name="parent"/>
    ''' </returns>
    Public Shared Function isParentOf(child As TaskTemplateWrapper, parent As TaskTemplateWrapper) As Boolean
        Return child IsNot Nothing AndAlso parent IsNot Nothing AndAlso child.parent IsNot Nothing _
        AndAlso (parent.id = child.parent.id)
    End Function

    ''' <summary>
    ''' Fill in the fields of <paramref name="copyTo"/> using the data in the
    ''' fields of <paramref name="copyFrom"/>. Used to initialize duplicated templates.
    ''' </summary>
    ''' <param name="copyFrom">The tempalte to copy the fields from.</param>
    ''' <param name="copyTo">The newly created template to initialize the fields in.</param>
    ''' <param name="parentTask">
    ''' The new parent template of <paramref name="copyTo"/>.
    ''' May be <c>Nothing</c> if <paramref name="copyTo"/> is a root task.
    ''' </param>
    ''' <param name="platforms">
    ''' A list containing all the platforms that <paramref name="copyTo"/> should
    ''' be compatible with. This must be a new list of platforms that have been found
    ''' in the currently open database context (it must not simply be a reference to
    ''' an existing list of platforms from a different node, as linking two nodes with
    ''' the same platform list will break the compatibility tree).
    ''' </param>
    ''' <param name="db">The database context that both templates came from. Must still be open.</param>
    Public Sub copyTemplate(copyFrom As TaskTemplate, copyTo As TaskTemplate, parentTask As TaskTemplate, platforms As IEnumerable(Of Platform), db As IntelligenceDB)
        copyTo.active = True
        copyTo.description = copyFrom.description
        copyTo.name = copyFrom.name
        copyTo.product = copyFrom.product
        copyTo.parentTask = parentTask
        copyTo.platforms = platforms

        If parentTask IsNot Nothing AndAlso parentTask.subTasks.Any Then
            copyTo.rank = parentTask.subTasks.Max(Function(t) t.rank) + 1
        End If

    End Sub


End Class
