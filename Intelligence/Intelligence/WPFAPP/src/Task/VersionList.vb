﻿''' <summary>A list containing all <see cref="VersionWrapper"/>s for a given project.</summary>
Public Class VersionList
    Inherits WrapperList(Of Version)

    ''' <summary>A wrapper around the product the <see cref="VersionWrapper"/>s belong to.</summary>
    Private product As ProductWrapper

    ''' <summary>Create a list of all <see cref="VersionWrapper"/>s in the given project.</summary>
    ''' <param name="product">A wrapper around the product the versions are for.</param>
    Sub New(product As ProductWrapper)
        MyBase.New(Function(db As IntelligenceDB)
                       Return From version As Version In db.Versions
                              Where version.product.id = product.id
                              Select version
                              Order By version.timeCreated Descending
                   End Function,
                   Function(v, db) New VersionWrapper(v, product, False))
        Me.product = product
    End Sub

    ''' <summary>
    ''' Initialise the fields for a newly created <see cref="Version"/> with default
    ''' values (and the id if the <see cref="ProductWrapper"/> this list is for.
    ''' </summary>
    ''' <param name="newItem">The newly created <see cref="Version"/>.</param>
    ''' <param name="db">The database context it was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As Version, db As IntelligenceDB)
        newItem.name = ""
        newItem.product = db.Products.Find(product.id)
        newItem.resolution = db.VersionResolutions.Find(getDefaultResolutionForNewVersion().id)
        newItem.timeCreated = DateTime.Now()
        newItem.creator = db.IntelligenceUsers.Find(UserInfo.getCurrentIntelligenceUser().id)
    End Sub

    ''' <summary>The default resolution for new versions (currently <see cref="VersionResolutionUtils.openResolution"/>).</summary>
    Private Function getDefaultResolutionForNewVersion() As VersionResolution
        Return VersionResolutionUtils.openResolution
    End Function

    ''' <summary>Newly added versions should be added to the top of the list rather than the bottom.</summary>
    Protected Overrides Property shouldAddNewItemsToTop As Boolean = True

End Class
