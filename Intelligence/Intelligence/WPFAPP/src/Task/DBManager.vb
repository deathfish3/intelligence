﻿Imports System.Data.Entity.Validation

''' <summary>
''' A bag of global utility functions for dealing with the intelligence
''' database. It's main purpose is the error handling available while saving using
''' <see cref="DBManager.saveChanges"/>, but it also has numerous methods for deleting
''' database items in ways that will not violate foregin key constraints, as well
''' as utility methods for closing and re-opening versions.
''' </summary>
Public Class DBManager

    ''' <summary>
    ''' Save the changes to the given database context.
    ''' If an exception occurs, catch it and display an error message explaining what went wrong.
    ''' If the exception has inner exceptions, error multiple dialogs will be displayed recursively
    ''' to make sure the user has access to all the error information available.
    ''' </summary>
    ''' <param name="db">The open database context to save changes in.</param>
    ''' <returns>True if the save was successful. False if an error occured.</returns>
    Public Shared Function saveChanges(db As IntelligenceDB) As Boolean
        Try
            db.SaveChanges()

            Debug.Print("Saved successfully.")
            Return True
        Catch ex As DbEntityValidationException
            Debug.Print("Error saving to database.")
            For Each validationErrors In ex.EntityValidationErrors
                For Each validationError In validationErrors.ValidationErrors
                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)

                    Dim dlg As New ErrorDialog("Database Error",
                                               "Error modifying property: " & validationError.PropertyName,
                                               validationError.ErrorMessage)
                    dlg.ShowDialog()
                Next
            Next
        Catch ex As System.Data.Entity.Infrastructure.DbUpdateException
            displayException(ex, "Database Error", "Error updating database:")
        Catch ex As System.Data.Entity.Core.OptimisticConcurrencyException
            displayException(ex, "Database Concurrency Error", "Error updating database:")
        Catch ex As System.Data.Entity.Core.ObjectNotFoundException
            displayException(ex, "Database Object Error", "Database Object was not found:")
        End Try


        Return False
    End Function

    ''' <summary>Display the given exception in an error dialog box, and recursively display its inner exceptions if necessary.</summary>
    ''' <param name="ex">The excetion to display.</param>
    ''' <param name="title">The title text for the error message dialog box.</param>
    ''' <param name="header">The header text for the error message dialog box.</param>
    Public Shared Sub displayException(ex As Exception, title As String, header As String)
        Dim dlg As New ErrorDialog(title, header, ex.Message)
        dlg.ShowDialog()
        If ex.InnerException IsNot Nothing Then
            displayException(ex.InnerException, title, header)
        End If
    End Sub

    ''' <summary>
    ''' Delete the given task template, along with all its subtasks.
    ''' If there are any instances created from his template, its
    ''' <see cref="TaskTemplate.active"/> field will be set to false insted
    ''' of being permanantly deleted (to keep the instances valid for history
    ''' and statistics).
    ''' </summary>
    ''' <param name="db">The database context to delete from.</param>
    ''' <param name="templateToDelete">The template to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Public Shared Function deleteTaskTemplate(db As IntelligenceDB, templateToDelete As TaskTemplate) As Boolean
        templateToDelete = db.TaskTemplates.Find(templateToDelete.id)
        If templateToDelete Is Nothing Then Return True
        For Each child In templateToDelete.subTasks.ToList()
            deleteTaskTemplate(db, child)
        Next
        If templateToDelete.taskInstances.Count = 0 Then
            db.TaskTemplates.Remove(templateToDelete)
        Else
            templateToDelete.active = False
        End If
        Return saveChanges(db)
    End Function

    ''' <summary>
    ''' Delete the given product from the database and save the changes.
    ''' If it had any templates or versions attached to it though, it will
    ''' have its <see cref="Product.active"/> field set to <c>False</c> rather
    ''' than being permanantly deleted (to keep it around for stats and historical purposes).
    ''' </summary>
    ''' <param name="db">The open database context to delete the product from.</param>
    ''' <param name="productToDelete">The product to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Public Shared Function deleteProduct(db As IntelligenceDB, productToDelete As Product) As Boolean
        productToDelete = db.Products.Find(productToDelete.id)
        If productToDelete Is Nothing Then Return True
        If Not productToDelete.taskTemplates.Any() _
        AndAlso Not productToDelete.versions.Any() Then
            db.Products.Remove(productToDelete)
        Else
            productToDelete.active = False

            For Each templ In productToDelete.taskTemplates
                setTemplateAndChildrenInactive(db, templ)
            Next

            For Each version In productToDelete.versions
                closeVersion(db, version)
            Next
        End If
        Return saveChanges(db)
    End Function

    ''' <summary>Recursively set the given template and all its descendants as inactive.</summary>
    ''' <param name="db">The open database context to make the changes in.</param>
    ''' <param name="template">The template to set inactive.</param>
    ''' <returns>Always returs <c>True</c>.</returns>
    Private Shared Function setTemplateAndChildrenInactive(db As IntelligenceDB, template As TaskTemplate) As Boolean
        template.active = False
        For Each child As TaskTemplate In template.subTasks
            setTemplateAndChildrenInactive(db, child)
        Next
        Return True
    End Function

    ''' <summary>
    ''' Delete the given platform from the database and save the changes.
    ''' If any templates are compatible with this platform, it will be removed
    ''' from their compatibility lists to stop any new tasks being generated for this platform.
    ''' If it had any instances attached to it, it will have its
    ''' <see cref="Platform.active"/> field set to <c>False</c> rather
    ''' than being permanantly deleted (to keep it around for stats and historical purposes).
    ''' </summary>
    ''' <param name="db">The open database context to delete the platform from.</param>
    ''' <param name="platformToDelete">The platform to delete.</param>
    ''' <returns><c>True</c> if successfully deleted.</returns>
    Public Shared Function deletePlatform(db As IntelligenceDB, platformToDelete As Platform) As Boolean

        platformToDelete = db.Platforms.Find(platformToDelete.id)

		If platformToDelete Is Nothing Then Return True
			If Not platformToDelete.taskTemplates Is Nothing Then
		
            For Each template As TaskTemplate In platformToDelete.taskTemplates
                template.platforms.Remove(platformToDelete)
            Next
        End If

        If saveChanges(db) Then

            If Not platformToDelete.taskInstances.Any() Then
                db.Platforms.Remove(platformToDelete)
            Else
                platformToDelete.active = False
                saveChanges(db)
            End If
            Return saveChanges(db)
        End If
        Return False
    End Function

    ''' <summary>
    ''' Set the closing time for the given version, and set the resolution for any unfinished
    ''' tasks in that version to <see cref="ResolutionUtils.notCompleteResolution"/>.
    ''' </summary>
    ''' <param name="db">The database context to make changes in.</param>
    ''' <param name="version">The version to close. Assumes its resolution has already been updated.</param>
    Public Shared Sub closeVersion(db As IntelligenceDB, version As Version)
        Dim now As DateTime = DateTime.Now()
        Dim tasks = From task In db.TaskInstances
                    Where task.version.id = version.id _
                    AndAlso (task.resolution.id = ResolutionUtils.openResolution.id OrElse task.resolution.id = ResolutionUtils.inProgressResolution.id)
                    Select task

        Dim notCompleteResolution As Resolution = db.Resolutions.Find(ResolutionUtils.notCompleteResolution.id)
        For Each taskInst In tasks
            taskInst.resolution = notCompleteResolution
            taskInst.timeFinished = now
        Next
        version.timeClosed = now
    End Sub

    ''' <summary>
    ''' Remove the closing time for the given version, and set the resolution for tasks
    ''' in that version marked as <see cref="ResolutionUtils.notCompleteResolution"/> to
    ''' <see cref="ResolutionUtils.openResolution"/>, indicating that they are now re-opened too.
    ''' </summary>
    ''' <param name="db">The database context to make changes in.</param>
    ''' <param name="version">The version to re-open. Assumes its resolution has already been updated.</param>
    Public Shared Sub reopenVersion(db As IntelligenceDB, version As Version)
        Dim now As DateTime = DateTime.Now()
        Dim tasks = From task In db.TaskInstances
            Where task.version.id = version.id _
            AndAlso (task.resolution.id = ResolutionUtils.notCompleteResolution.id)
            Select task

        Dim openResolution As Resolution = db.Resolutions.Find(ResolutionUtils.openResolution.id)
        For Each taskInst In tasks
            taskInst.resolution = openResolution
            taskInst.timeFinished = Nothing
        Next
        version.timeClosed = Nothing
    End Sub

End Class
