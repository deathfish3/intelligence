﻿Imports WPFAPP

Public Class AttachmentWrapper(Of T As {Class, New, IHasID, IRanked, IAttachment})
	Inherits Wrapper(Of T)
	Implements IAttachment

	Dim _submitter As IntelligenceUserWrapper
	Dim idOfHolder As Integer

	Public Sub New(itemToWrap As T, idOfHolder As Integer, Optional db As IntelligenceDB = Nothing, Optional shouldCallInitializeFields As Boolean = True)
		MyBase.New(itemToWrap, db, shouldCallInitializeFields)
	End Sub

	Protected Overrides Sub initializeFields(newItem As T, Optional db As IntelligenceDB = Nothing)
		MyBase.initializeFields(newItem, db)
		If db IsNot Nothing Then
			_submitter = New IntelligenceUserWrapper(newItem.submitter)
		Else
			_submitter = UserInfo.getCurrentIntelligenceUser()
		End If
	End Sub

	Public Overrides Property name As String
		Get
			Return getWrappedItem.fileName
		End Get
		Set(value As String)
		End Set
	End Property

	Public Property fileName As String Implements IAttachment.fileName
		Get
			Return getWrappedItem.fileName
		End Get
		Set(value As String)
			Throw New NotImplementedException()
		End Set
	End Property

	Public Property fileUrl As String Implements IAttachment.fileUrl
		Get
			Return getWrappedItem.fileUrl
		End Get
		Set(value As String)
			Throw New NotImplementedException()
		End Set
	End Property

	Public Property dateSubmitted As Date Implements IAttachment.dateSubmitted
		Get
			Return getWrappedItem.dateSubmitted
		End Get
		Set(value As Date)
			Throw New NotImplementedException()
		End Set
	End Property

	Public Property submitter As IntelligenceUser Implements IAttachment.submitter
		Get
			Return _submitter.getWrappedItem
		End Get
		Set(value As IntelligenceUser)
			Throw New NotImplementedException()
		End Set
	End Property

	Public ReadOnly Property wrappedSubmitter As IntelligenceUserWrapper
		Get
			Return _submitter
		End Get
	End Property

	Public ReadOnly Property downloadCommand As ICommand
		Get
			Return New GenericCommand(
			Sub()
				Dim saveDlg As New Microsoft.Win32.SaveFileDialog()

				saveDlg.FileName = fileName
				saveDlg.DefaultExt = System.IO.Path.GetExtension(fileName)
				saveDlg.Filter = "All files (*.*)|*.*"

				Dim result? As Boolean = saveDlg.ShowDialog()
				Dim saveLocation As String = saveDlg.FileName
				Dim urlToDownloadFrom As String = fileUrl

				If result = True Then
					LoadWithAsyncGUITask.run(Of Boolean)(
						"Downloading file...",
						Function()
							Dim tempFile As Byte() = AttachmentUtils.downloadFileForAttachment(Me)
							If tempFile IsNot Nothing Then
								System.IO.File.WriteAllBytes(saveLocation, tempFile)
							End If
							Return My.Computer.FileSystem.FileExists(saveLocation)
						End Function,
						Sub(success) MessageDialog.display("Success", "Finished downloading " & fileName)
					)
				End If
			End Sub)
		End Get
	End Property

	''' <summary>
	''' If this attachment's filename ends in ".jpg", ".png", or ".gif" then
	''' accessing this property will download the image to the temp directory
	''' so that it can be displayed on the UI. This field should be threadsafe,
	''' so it is safe to bind it using "isAsync" in the XAML binding to download
	''' the image in a background thread. Will return <c>Nothing</c> if the
	''' attachment is not an image, the image is corrupted, or the download fails.
	''' </summary>
	ReadOnly Property previewImage() As BitmapImage
		Get
			If fileName.EndsWith(".jpg") OrElse fileName.EndsWith(".png") OrElse fileName.EndsWith(".gif") Then
				Dim bi As New BitmapImage()
				bi.BeginInit()

				Dim tempFileURL As String = AttachmentUtils.downloadTempFileForAttachment(getWrappedItem(), idOfHolder)
				If tempFileURL Is Nothing Then Return Nothing

				bi.UriSource = New Uri(tempFileURL)

				Try
					bi.EndInit()
					bi.Freeze()
					Return bi
				Catch e As Exception
					Return Nothing
				End Try
			End If
			Return Nothing
		End Get
	End Property

End Class
