﻿''' <summary>A wrapper around a <see cref="Product"/> from the database.</summary>
Public Class ProductWrapper
	Inherits RankedWrapper(Of Product)

    ''' <summary>Create a new wrapper around the given product.</summary>
    ''' <param name="product">The product to wrap.</param>
    Sub New(product As Product)
		MyBase.New(product)
	End Sub

    ''' <summary>
    ''' The human-readable name of the product.
    ''' Setting this will automaticatically save the changes to the database and notify the UI.
    ''' </summary>
    Public Overrides Property name As String
		Get
			Return getWrappedItem.name
		End Get
		Set(value As String)
			setFieldAndSave(value, getWrappedItem.name, Sub(v) getWrappedItem.name = v)
		End Set
	End Property

    ''' <summary>Get a list of all wrappers around all the versions for this product.</summary>
    Public Function getVersions() As VersionList
        Return New VersionList(Me)
    End Function
End Class