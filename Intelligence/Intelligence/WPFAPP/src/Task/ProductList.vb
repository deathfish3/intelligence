﻿''' <summary>A list containing all active <see cref="ProductWrapper"/>s.</summary>
Public Class ProductList
	Inherits RankedWrapperList(Of Product)

    ''' <summary>Create a new list of all active products.</summary>
    Sub New()
		MyBase.New(
			Function(db As IntelligenceDB) (
				From p In db.Products
				Where (p.active = True)
				Order By p.rank Ascending, p.id Ascending
				Select p
			),
			Function(p, db) New ProductWrapper(p)
		)
	End Sub

    ''' <summary>
    ''' Delete the given product from the database and save the changes.
    ''' If it had any templates or versions attached to it though, it will
    ''' have its <see cref="Product.active"/> field set to <c>False</c> rather
    ''' than being permanantly deleted (to keep it around for stats and historical purposes).
    ''' </summary>
    ''' <param name="db">The open database context to delete the product from.</param>
    ''' <param name="toDelete">A wrapper around the product to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of Product)) As Boolean
        Return DBManager.deleteProduct(db, toDelete.getWrappedItem())
    End Function

    ''' <summary>Initialise a newly created product with default values.</summary>
    ''' <param name="newItem">The newly created product.</param>
    ''' <param name="db">Unused.</param>
    Protected Overrides Sub initialiseNewItem(newItem As Product, db As IntelligenceDB)
		newItem.active = True
		newItem.name = ""
		newItem.rank = If(Not Me.Any, 0, Me.Max(Function(w) w.getWrappedItem.rank)) + 1
	End Sub
End Class
