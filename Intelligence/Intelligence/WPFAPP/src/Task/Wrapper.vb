﻿Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Linq.Expressions

''' <summary>
''' Wraps objects from the Intelligence database. Properties here can be bound to
''' the UI, and will update the database when changed.
''' </summary>
''' <remarks>
''' Contains numerous helper methods for notifying the UI when properties change, as 
''' well as reloading and saving fields to the database in a consistant way. Database 
''' changes can be saved immediately or submitted in batches with using same database 
''' context for better performance. The UI will be notified of changes if the save was 
''' successful. Wrappers can also be used to store utility methods for queries related 
''' to each type of database item that may be required by the UI.
''' </remarks>
''' <typeparam name="T">The type of database item to wrap.</typeparam>
Public MustInherit Class Wrapper(Of T As {Class, IHasID, New})
    Inherits NotifyPropertyChangedBase

#Region "Properties"
    '########################################################
    '#                     Properties                       #
    '########################################################

    ''' <summary> The id number that is the primary key for the database item being wrapped. </summary>
    Public ReadOnly Property id As Integer
        Get
            Return getWrappedItem.id
        End Get
    End Property

    ''' <summary> The human readable name of the database item being wrapped. </summary>
    ''' <remarks> 
    ''' Is used when converting wrappers to strings, for instance when confirming 
    ''' deletion, or printing debug messages.
    ''' </remarks>
    Public MustOverride Property name As String

    ''' <summary>The database item this wrapper stores internally, but allows read only access to.</summary>
    Private wrappedItem As T

    ''' <summary>Read only access to the database item this wrapper contains.</summary>
    ''' <returns>The database item this wrapper contains.</returns>
    Public Function getWrappedItem() As T
        Return wrappedItem
    End Function

    ''' <summary>
    ''' Compare the type and id of the object.
    ''' The id is the database primary key, so two wrappers will
    ''' be treated as the same if they refer to the same database item (regardless of the other fields).
    ''' </summary>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return obj IsNot Nothing _
            AndAlso obj.GetType().Equals(Me.GetType()) _
            AndAlso id = obj.id
    End Function

    ''' <summary>Return the wrapped item's human readable name. Used for delete confirmation and debugging.</summary>
    Public Overrides Function ToString() As String
        Return name
    End Function
#End Region

#Region "Initialization"
    '########################################################
    '#                   Initialization                     #
    '########################################################

    ''' <summary>
    ''' Create a new wrapper around the given item (which should still
    ''' be connected to an open <see cref="IntelligenceDB"/>).
    ''' </summary>
    ''' <param name="itemToWrap">The database item, which should be connected to an open dbcontext.</param>
    ''' <param name="db">
    ''' The database that <paramref name="itemToWrap"/> came from.
    ''' Defaults to <c>Nothing</c>, but must be passed in if <see cref="initializeFields"/>
    ''' requires a dbcontext to set up all the fields properly.
    ''' </param>
    ''' <param name="shouldCallInitializeFields">
    ''' Whether <see cref="initializeFields"/> should be called in the constructor.
    ''' Defaults to true, but may be set to false if <see cref="initializeFields"/> requires
    ''' additional members to be initialized before being called. If this is set to false,
    ''' the caller must manually call <see cref="initializeFields"/> in their constructor
    ''' after all the necessary properties are in place.
    ''' </param>
    ''' <remarks>This should </remarks>
    Public Sub New(itemToWrap As T, Optional db As IntelligenceDB = Nothing, Optional shouldCallInitializeFields As Boolean = True)
        Me.wrappedItem = itemToWrap
        If shouldCallInitializeFields Then
            initializeFields(itemToWrap, db)
        End If
        wrapperCount += 1
        Me._wrapperId = wrapperCount
    End Sub

    ''' <summary>
    ''' Performs basic setup when the database item has been loaded or reloaded.
    ''' Fills in all the fields that require a connection to an open database context, such
    ''' as all the foregein key values. Does nothing by default, but can be overriden by subclasses as needed.
    ''' </summary>
    ''' <param name="newItem">The newly loaded database item, which should be connected to an open dbcontext.</param>
    ''' <param name="db">
    ''' The database context this item came from. This will always be provided when the wrapper
    ''' is reloaded, but will only be supplied from the constructor when using a constructor that accepts a dbcontext.
    ''' </param>
    Protected Overridable Sub initializeFields(newItem As T, Optional db As IntelligenceDB = Nothing)
        'Do Nothing by default.
    End Sub

    ''' <summary>
    ''' Reload this wrapper's contents with the latest version
    ''' from the database, and notify the UI that all properties have changed.
    ''' </summary>
    Public Overridable Sub reloadWrappedItem()
        Using db As New IntelligenceDB
            wrappedItem = db.Set(Of T).Find(id)
            initializeFields(getWrappedItem, db)
        End Using
        onAllPropertiesChanged()
    End Sub
#End Region

#Region "Modifying Fields"
    '########################################################
    '#                   Modifying Fields                   #
    '########################################################

    ''' <summary>
    ''' Set the given field to the given value, and save changes to the database.
    ''' If the save is successful, the UI will be notified, otherwise, the change will be reverted.
    ''' </summary>
    ''' <typeparam name="F">The type of the field being set.</typeparam>
    ''' <param name="value">The new value for the field.</param>
    ''' <param name="oldValue">The old value of the field.</param>
    ''' <param name="setter">
    ''' A function for setting the field to a given value.
    ''' Will be called to set the field, and also to revert changes if necessary.
    ''' Can also be used for other logic, like updating cached values.
    ''' A simple example setter for the name field of the wrapped item would be:
    ''' <example><code>Sub(v) getWrappedItem.name = v</code></example>
    ''' </param>
    ''' <param name="memberName">
    ''' The name of the property being set. Will be auto-generated by the compiler 
    ''' when calling from within a setter, so can usually be omitted.
    ''' </param>
    ''' <returns>
    ''' True if the value was updated and saved successfully.
    ''' Will also return false if the value was the same as the old value, indicating that no change was required.
    ''' </returns>
    ''' <remarks>
    ''' Should only be used for setting single fields. 
    ''' For setting multiple fields at once, use <see cref="setFieldAndMarkModified"/> to
    ''' batch the changes, and then <see cref="saveChanges"/> to submit them all at once.
    ''' </remarks>
    Protected Function setFieldAndSave(Of F)(value As F, oldValue As F, setter As System.Action(Of F), <System.Runtime.CompilerServices.CallerMemberName> Optional memberName As String = Nothing) As Boolean
        Dim passed = False
        Using db As New IntelligenceDB
            passed = setFieldAndMarkModified(value, oldValue, setter, db, memberName) _
                     AndAlso saveChanges(db)
        End Using
        Return passed
    End Function

    ''' <summary>
    ''' Set the given field in the local copy, but do not upload it
    ''' to the database or notify the UI until <see cref="saveChanges"/> is called.
    ''' </summary>
    ''' <typeparam name="F">The type of field to set.</typeparam>
    ''' <param name="value">The new value for the field.</param>
    ''' <param name="oldValue">The old value of the field.</param>
    ''' <param name="setter">
    ''' A function for setting the field to a given value.
    ''' Will be called to set the field, and also to revert changes if necessary.
    ''' Can also be used for other logic, like updating cached values.
    ''' A simple example setter for the name field of the wrapped item would be:
    ''' <example><code>Sub(v) getWrappedItem.name = v</code></example>
    ''' </param>
    ''' <param name="propertyExpression">
    ''' A lambda expression indicating which property has been updated when notifying the UI.
    ''' For instance, the <code>name</code> property would be specified as:
    ''' <example><code>Function() Me.name</code></example>
    ''' </param>
    ''' <param name="db">The database context where the changes will eventually be submitted to.</param>
    ''' <returns>Returns false if the value was the same as the old value, indicating that no change was required.</returns>
    ''' <remarks>
    ''' The local copies will be updated immediately when this method is called,
    ''' but the changes will be recored in <see cref="Wrapper(Of T).modifications"/> .
    ''' When <see cref="saveChanges"/> is called successfully, the UI will be notified
    ''' of all the changes. If the save fails, all the changes will be reverted.
    ''' </remarks>
    Protected Function setFieldAndMarkModified(Of F)(value As F, oldValue As F, setter As System.Action(Of F), propertyExpression As Expression(Of Func(Of F)), db As IntelligenceDB) As Boolean
        Return setFieldAndMarkModified(value, oldValue, setter, db, getPropertyName(propertyExpression))
    End Function

    ''' <summary>
    ''' Set the given field in the local copy, but do not upload it
    ''' to the database or notify the UI until <see cref="saveChanges"/> is called.
    ''' </summary>
    ''' <typeparam name="F">The type of field to set.</typeparam>
    ''' <param name="value">The new value for the field.</param>
    ''' <param name="oldValue">The old value of the field.</param>
    ''' <param name="setter">
    ''' A function for setting the field to a given value.
    ''' Will be called to set the field, and also to revert changes if necessary.
    ''' Can also be used for other logic, like updating cached values.
    ''' A simple example setter for the name field of the wrapped item would be:
    ''' <example><code>Sub(v) getWrappedItem.name = v</code></example>
    ''' </param>
    ''' <param name="memberName">
    ''' The name of the property being set. Will be auto-generated by the compiler 
    ''' when calling from within a setter, so can usually be omitted.
    ''' </param>
    ''' <param name="db">The database context where the changes will eventually be submitted to.</param>
    ''' <returns>Returns false if the value was the same as the old value, indicating that no change was required.</returns>
    ''' <remarks>
    ''' The local copies will be updated immediately when this method is called,
    ''' but the changes will be recored in <see cref="Wrapper(Of T).modifications"/> .
    ''' When <see cref="saveChanges"/> is called successfully, the UI will be notified
    ''' of all the changes. If the save fails, all the changes will be reverted.
    ''' </remarks>
    Protected Function setFieldAndMarkModified(Of F)(value As F, oldValue As F, setter As System.Action(Of F), db As IntelligenceDB, <System.Runtime.CompilerServices.CallerMemberName> Optional memberName As String = Nothing) As Boolean
		wrappedItem = db.Set(Of T).Find(id)
		If wrappedItem IsNot Nothing Then
			If Not EqualityComparer(Of F).Default.Equals(value, oldValue) Then
				wrappedItem = db.Set(Of T).Find(id)
				If wrappedItem IsNot Nothing Then
					'This is needed to avoid adding duplicate rows in the database for
					'foreign key fields.
					Dim safeSetter As System.Action(Of F) = Sub(v)
																Dim dbEntry As IHasID = TryCast(v, IHasID)
																If dbEntry IsNot Nothing Then
																	v = db.Set(dbEntry.GetType).Find(dbEntry.id)
																End If
																setter(v)
															End Sub
					safeSetter(value)
					addModification(value, oldValue, safeSetter, memberName, Me)
					Return True

				End If
			End If
		Else
            'Popup 
            Dim dlg As New ErrorDialog("Item does not exist", "Item does not exist", "This item does not exist in IntelligenceDB. It could have been deleted by another user. Your change WILL NOT be commited to IntelligenceDB")
			dlg.ShowDialog()
		End If
		Return False
	End Function

    ''' <summary>
    ''' Save changes to the given database. If successful, notify
    ''' the UI of all the changes. If unsuccessful, revert all the changes.
    ''' </summary>
    ''' <param name="db">The database context in which the changes took place.</param>
    ''' <returns>
    ''' True if successfully saved to the database. 
    ''' False if the save failed and the changes were reverted.
    ''' </returns>
    ''' <remarks>
    ''' Multiple changes can be saved at once using <see cref="setFieldAndMarkModified"/>
    ''' as long as they all use the same dbContext.
    ''' </remarks>
    Public Function saveChanges(db As IntelligenceDB) As Boolean
        If DBManager.saveChanges(db) Then
            notifyAllModifications()
            Return True
        Else
            undoAllModifications()
            Return False
        End If
    End Function

#End Region

#Region "Wrapper ID (for debugging only)"
    '########################################################
    '#           Wrapper ID (for debugging only)            #
    '########################################################

    ''' <summary>A shared counter to give all wrappers a unique id (for debug purposes only)</summary>
    Private Shared wrapperCount = 0


    ''' <summary>The internally stored value of <see cref="wrapperId"/> </summary>
    Private _wrapperId As Integer
    ''' <summary>
    ''' A unique id for wrapper instances. This is only used for debugging, 
    ''' and is different from <see cref="id"/>, which refers to the wrapped item's database id.
    ''' </summary>
    Public ReadOnly Property wrapperId As Integer
        Get
            Return _wrapperId
        End Get
    End Property
#End Region

#Region "Modification Stack"
    '########################################################
    '#                   Modification stack                 #
    '########################################################

    ''' <summary>
    ''' An interface for modifications which have responses for notifying the UI 
    ''' upon a successful save, or undoing a change if the save fails.
    ''' </summary>
    Private Interface IModification
        ''' <summary>Called if <see cref="saveChanges"/> was successful, to notify the UI.</summary>
        Sub notify()
        ''' <summary>Called if <see cref="saveChanges"/> was fails, and the modification needs undone.</summary>
        Sub undo()
    End Interface

    ''' <summary>
    ''' A list of all the modifications that have taken place since <see cref="saveChanges"/> 
    ''' was last called. All changes are stored here to allow them to be reverted, or the
    ''' UI to be updated when the database is saved to.
    ''' </summary>
    Private Shared modifications As List(Of IModification) = New List(Of IModification)

    ''' <summary>
    ''' Send update notifications to the UI about all the changes that have 
    ''' just been saved, and remove them from the list.
    ''' </summary>
    Private Shared Sub notifyAllModifications()
        For Each m As IModification In modifications
            m.notify()
        Next
        modifications.Clear()
    End Sub

    ''' <summary>Revert all the changes, and remove them from the list.</summary>
    Private Shared Sub undoAllModifications()
        For Each m As IModification In modifications
            m.undo()
        Next
        modifications.Clear()
    End Sub

    ''' <summary>
    ''' Add a property to the list of modifications the UI should be informed 
    ''' of when <see cref="saveChanges"/> is called. This is used when multiple
    ''' wrapper properties are affected by a single call to <see cref="setFieldAndMarkModified"/>
    ''' </summary>
    ''' <typeparam name="F">The type of field that changed.</typeparam>
    ''' <typeparam name="H">The type of database object the given wrapper contains.</typeparam>
    ''' <param name="propertyExpression">
    ''' A lambda expression indicating which property has been updated when notifying the UI.
    ''' For instance, the <code>name</code> property would be specified as:
    ''' <example><code>Function() Me.name</code></example>
    ''' </param>
    ''' <param name="wrapper">The wrapper where the property was changed.</param>
    Protected Shared Sub addDelayedPropertyChange(Of F, H As {Class, IHasID, New})(propertyExpression As Expression(Of Func(Of F)), wrapper As Wrapper(Of H))
        Dim modification As New Modification(Of F, H)(Nothing, Nothing, Nothing, getPropertyName(propertyExpression), wrapper)
        modifications.Add(modification)
    End Sub

    ''' <summary>
    ''' Add a property to the list of modifications the UI should be informed 
    ''' of when <see cref="saveChanges"/> is called. This is used when multiple
    ''' wrapper properties are affected by a single call to <see cref="setFieldAndMarkModified"/>
    ''' </summary>
    ''' <typeparam name="F">The type of field that changed.</typeparam>
    ''' <typeparam name="H">The type of database object the given wrapper contains.</typeparam>
    ''' <param name="value">The new value for the field.</param>
    ''' <param name="oldValue">The old value of the field.</param>
    ''' <param name="setter">
    ''' A function for setting the field to a given value. Will be called if the changes need reverted.
    ''' Can also be used for other logic, like updating cached values.
    ''' A simple example setter for the name field of the wrapped item would be:
    ''' <example><code>Sub(v) getWrappedItem.name = v</code></example>
    ''' </param>
    ''' <param name="propertyName">The name of the property being set (used by UI notifications).</param>
    ''' <param name="wrapper">The wrapper where the property was changed.</param>
    Private Shared Sub addModification(Of F, H As {Class, IHasID, New})(value As F, oldValue As F, setter As System.Action(Of F), propertyName As String, wrapper As Wrapper(Of H))
        Dim modification As New Modification(Of F, H)(value, oldValue, setter, propertyName, wrapper)
        modifications.Add(modification)
    End Sub

#End Region

#Region "Default Modification Implementation"
    '########################################################
    '#         Default Modification Implementation          #
    '########################################################

    ''' <summary>A modification of a given field on a wrapper, which can be reversed.</summary>
    ''' <typeparam name="F">The type of field modified.</typeparam>
    ''' <typeparam name="H">The type of database item the wrapper contains.</typeparam>
    Private Class Modification(Of F, H As {Class, IHasID, New})
        Implements IModification

        ''' <summary>The old value of the field being modified.</summary>
        Private oldValue As F
        ''' <summary>The new value of the field being modified.</summary>
        Private newValue As F
        ''' <summary>A function for setting the field to a given value if the changes need reverted.</summary>
        Private setter As System.Action(Of F)
        ''' <summary>The name of the property being set (used by UI notifications).</summary>
        Private propertyName As String
        ''' <summary>The wrapper where the property was changed.</summary>
        Private wrapper As Wrapper(Of H)

        '''<summary>Initialize all the modification's private fields required to notify the UI or revert changes.</summary>
        ''' <param name="value">The new value for the field.</param>
        ''' <param name="oldValue">The old value of the field.</param>
        ''' <param name="setter">
        ''' A function for setting the field to a given value. Will be called if the changes need reverted.
        ''' Can also be used for other logic, like updating cached values.
        ''' A simple example setter for the name field of the wrapped item would be:
        ''' <example><code>Sub(v) getWrappedItem.name = v</code></example>
        ''' </param>
        ''' <param name="propertyName">The name of the property being set (used by UI notifications).</param>
        ''' <param name="wrapper">The wrapper where the property was changed.</param>
        Public Sub New(value As F, oldValue As F, setter As System.Action(Of F), propertyName As String, wrapper As Wrapper(Of H))
            Me.oldValue = oldValue
            Me.newValue = value
            Me.setter = setter
            Me.propertyName = propertyName
            Me.wrapper = wrapper
        End Sub

        ''' <summary>Notify the UI that the property has changed.</summary>
        Public Sub notify() Implements IModification.notify
            wrapper.onPropertyChanged(propertyName)
        End Sub

        ''' <summary>Set the field to the old value if a setter is provided.</summary>
        Public Sub undo() Implements IModification.undo
            If setter IsNot Nothing Then
                setter(oldValue)
            End If
        End Sub
    End Class
#End Region

End Class
