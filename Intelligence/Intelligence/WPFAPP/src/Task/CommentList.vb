﻿''' <summary>A list containing all <see cref="CommentWrapper"/>s for a given task instance.</summary>
Public Class CommentList
	Inherits WrapperList(Of Comment)

    ''' <summary>The task instance these bugs belong to.</summary>
    Private instance As TaskInstanceWrapper

    ''' <summary>Create a new list of all <see cref="CommentWrapper"/>s for a given task instance.</summary>
    Sub New(instance As TaskInstanceWrapper)
		MyBase.New(Function(db As IntelligenceDB)
					   Return From comment In db.Comments
							  Where comment.taskInstance.id = instance.id
							  Select comment
				   End Function,
				   Function(c As Comment, db As IntelligenceDB) New CommentWrapper(c, instance, db))
		Me.instance = instance
	End Sub

    ''' <summary>Initialise a newly created comment with default values.</summary>
    ''' <param name="newItem">The newly created comment.</param>
    ''' <param name="db">The open database context the comment was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As Comment, db As IntelligenceDB)
		newItem.dateSubmitted = DateTime.Now
		newItem.dateEdited = Nothing
		newItem.commenter = db.IntelligenceUsers.Find(UserInfo.getCurrentIntelligenceUser().id)
		newItem.taskInstance = db.TaskInstances.Find(instance.id)
		newItem.text = ""
	End Sub

    ''' <summary>Delete the given comment permenantly from the database and save the changes.</summary>
    ''' <param name="db">The open database context to delete the comment from.</param>
    ''' <param name="toDelete">A wrapper around the comment to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of Comment)) As Boolean
		Dim commentFromDB As Comment = db.Comments.Find(toDelete.id)
		If commentFromDB IsNot Nothing Then
			db.Comments.Remove(commentFromDB)
			Return DBManager.saveChanges(db)
		End If
		Return False
	End Function
End Class
