﻿'############################################################################
'VersionResolutionsUtils.vb
'
'Purpose: To act as an 'enum' class for VersionResolutions
'
'Description: Contains each versionresolution in Database (as of 16/07/15). 
'Will populate each versionresolution with associated versionresolution stored in database
'
'Modification: To create a new versionresolution entry, copy...

'       Private Shared _dbYourVerRes As VersionResolution = GetVersionResolutionByName("YourVersionResolutionInDb")
'       Public Shared ReadOnly Property yourVersionResolution As VersionResolution
'           Get
'               Return _dbYourVerRes
'           End Get
'       End Property
'
' ...into VersionResolutionsUtils class.
'############################################################################



Public Class VersionResolutionUtils

    'Get _dbOpenRes
    'Sync _dbOpenRes with database on init. Always return _dbOpenRes
    Private Shared _dbOpenRes As VersionResolution = GetVersionResolutionByName("open")
    Public Shared ReadOnly Property openResolution As VersionResolution
        Get
            Return _dbOpenRes
        End Get
    End Property

    'Get _dbReleasedRes
    'Sync _dbReleasedRes with database on init. Always return _dbReleasedRes
    Private Shared _dbReleasedRes As VersionResolution = GetVersionResolutionByName("released")
    Public Shared ReadOnly Property releasedResolution As VersionResolution
        Get
            Return _dbReleasedRes
        End Get
    End Property

    'Get _dbReplacedNewBuildRes
    'Sync _dbReplacedNewBuildRes with database on init. Always return _dbReplacedNewBuildRes
    Private Shared _dbReplacedNewBuildRes As VersionResolution = GetVersionResolutionByName("replaced with new build")
    Public Shared ReadOnly Property replacedWithNewBuildRes As VersionResolution
        Get
            Return _dbReplacedNewBuildRes
        End Get
    End Property

    'Get _dbFailedRes
    'Sync _dbFailedRes with database on init. Always return _dbFailedRes
    Private Shared _dbFailedRes As VersionResolution = GetVersionResolutionByName("failed")
    Public Shared ReadOnly Property failedResolution As VersionResolution
        Get
            Return _dbFailedRes
        End Get
    End Property

    'Takes in 'res' and checks if it matches database collected 'open' resolution
    Public Shared Function isVersionOpen(res As VersionResolution) As Boolean
        Return res.Equals(openResolution)
    End Function



    'Takes in 'name' and collects database versionresolution with name field matching name
    Private Shared Function GetVersionResolutionByName(name As String)
        Using db As New IntelligenceDB
            Return (From vres In db.VersionResolutions
                    Where vres.name.ToLower.Equals(name.ToLower) AndAlso vres.active = True
                    Select vres).FirstOrDefault
        End Using
    End Function

    Private Shared _allResolutions As IEnumerable(Of VersionResolution)
    Public Shared ReadOnly Property allResolutions As IEnumerable(Of VersionResolution)
        Get
            If _allResolutions Is Nothing Then
                Using db As New IntelligenceDB
                    _allResolutions = db.VersionResolutions.Where(Function(r) r.active).ToList()
                End Using
            End If
            Return _allResolutions
        End Get
    End Property

End Class
