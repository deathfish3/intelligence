﻿Imports System.Linq.Expressions.DynamicExpression

''' <summary>
''' A list containing all <see cref="IntelligenceUserWrapper"/>s ordered by access level.
''' Contains slightly different logic than other <see cref="WrapperList(Of T)"/>s for 
''' adding new wrappers to avoid adding blank users to the database (every user needs to
''' have a username and password set up correctly before being added).
''' </summary>
Public Class IntelligenceUserList
    Inherits WrapperList(Of IntelligenceUser)

    ''' <summary>
    ''' A flag to specify whether in <see cref="wrapUser"/> that the
    ''' user being wrapped is a newly added one (rather than one downloaded
    ''' from the database.
    ''' </summary>
    Private addingNew As Boolean = False

    ''' <summary>Create a new list containing all <see cref="IntelligenceUserWrapper"/>s ordered by access level.</summary>
    Sub New()
        MyBase.New(Function(db As IntelligenceDB) db.IntelligenceUsers.OrderByDescending(Function(u) u.accessLevel.id))
        Me.wrap = AddressOf wrapUser
        reloadContents()
    End Sub

    ''' <summary>
    ''' Initialise the newly created <see cref="IntelligenceUser"/> with the default
    ''' access level. All other fields are filled in by the user on the GUI before it
    ''' is uploaded to the database.
    ''' </summary>
    ''' <param name="newItem">The newly created <see cref="IntelligenceUser"/>.</param>
    ''' <param name="db">The database context it was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As IntelligenceUser, db As IntelligenceDB)
        newItem.accessLevel = db.AccessLevels.Find(AccessLevelUtils.developerLevel.id)
    End Sub

    ''' <summary>
    ''' Create a wrapper around the given user.
    ''' The wrapper will be initialized differently depending in whether
    ''' it was newly created, or simply downloaded from the database.
    ''' </summary>
    ''' <param name="u">The newly intelligence user to wrap.</param>
    ''' <param name="db">Unused.</param>
    ''' <returns>A new wrapper around the given user.</returns>
    Protected Function wrapUser(u As IntelligenceUser, db As IntelligenceDB) As IntelligenceUserWrapper
        Return New IntelligenceUserWrapper(u, Not addingNew)
    End Function

    ''' <summary>
    ''' Get a custom command for adding a new intelligence user to this list and the database.
    ''' The usual <see cref="AddCommand"/> for <see cref="WrapperList(Of T)"/>s
    ''' is insufficient, as blank users should not be added to the database (every user needs to
    ''' have a username and password set up correctly before being added). This
    ''' command opens a dialog box for all the new users details, and will not
    ''' close until all fields are added. Only once all fields are entered correctly
    ''' will the user be added to the database.
    ''' </summary>
    Public Function getAddNewUserCommand() As ICommand
        Return New GenericCommand(
            Sub()
                Me.selectedItem = Nothing
                Dim success As Boolean = False
                Dim newItem As IntelligenceUserWrapper
                Using db As New IntelligenceDB

                    addingNew = True
                    newItem = Me.addNew(db, False)
                    addingNew = False

                    newItem = CustomEditorDialog.showEditorDialog(Of IntelligenceUserWrapper, IntelligenceUserEditor)(newItem, New IntelligenceUserEditor)

                    If newItem IsNot Nothing Then
                        Dim accessId As Integer = newItem.accessLevel.id
                        Dim newAccessLevel As AccessLevel = db.AccessLevels.Where(Function(a) a.id = accessId).FirstOrDefault()
                        newItem.accessLevel = newAccessLevel
                        success = DBManager.saveChanges(db)
                    End If
                End Using
                If success Then
                    Me.Add(newItem)
                    Me.selectedItem = newItem
                End If
            End Sub)
    End Function
End Class
