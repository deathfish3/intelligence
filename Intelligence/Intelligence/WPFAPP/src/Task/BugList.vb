﻿''' <summary>A list containing all <see cref="BugWrapper"/>s for a given task instance.</summary>
Public Class BugList
    Inherits WrapperList(Of Bug)

    ''' <summary>The task instance these bugs belong to.</summary>
    Private instance As TaskInstanceWrapper

    ''' <summary>Create a new list of all <see cref="BugWrapper"/>s for a given task instance.</summary>
    Sub New(instance As TaskInstanceWrapper)
        MyBase.New(Function(db As IntelligenceDB)
                       Return From bug In db.Bugs
                              Where bug.taskInstance.id = instance.id
                              Select bug
                   End Function,
                   Function(b As Bug, db As IntelligenceDB) New BugWrapper(b))
        Me.instance = instance
    End Sub

    ''' <summary>Initialise a newly created bug with default values.</summary>
    ''' <param name="newItem">The newly created bug.</param>
    ''' <param name="db">The open database context the bug was created in.</param>
    Protected Overrides Sub initialiseNewItem(newItem As Bug, db As IntelligenceDB)
        newItem.taskInstance = db.TaskInstances.Find(instance.id)
        newItem.mantisID = 0
    End Sub

    ''' <summary>Delete the given bug permenantly from the database and save the changes.</summary>
    ''' <param name="db">The open database context to delete the bug from.</param>
    ''' <param name="toDelete">A wrapper around the bug to delete.</param>
    ''' <returns>True if successfully deleted.</returns>
    Protected Overrides Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of Bug)) As Boolean
        Dim bugFromDB As Bug = db.Bugs.Find(toDelete.id)
        db.Bugs.Remove(bugFromDB)
        Return DBManager.saveChanges(db)
    End Function
End Class
