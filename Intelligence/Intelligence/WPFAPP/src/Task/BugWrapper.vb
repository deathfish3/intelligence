﻿''' <summary>
''' A wrapper around a <see cref="Bug"/> from the intelligence database.
''' Bugs are contain the id number of a Mantis bug, and are associated with the
''' <see cref="TaskInstance"/> they were found in.
''' </summary>
Public Class BugWrapper
    Inherits Wrapper(Of Bug)

    ''' <summary>Create a new wrapper around the given bug.</summary>
    ''' <param name="bug">The bug to wrap.</param>
    Sub New(bug As Bug)
        MyBase.New(bug)
    End Sub

    ''' <summary>The Mantis id of the bug. Cannot be set here.</summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem.mantisID
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary>The id of the bug in Mantis. Cannot be set here.</summary>
    Public ReadOnly Property mantisID As Integer
        Get
            Return getWrappedItem().mantisID
        End Get
    End Property

    ''' <summary>
    ''' Set the Mantis ID of this bug to the given value, but
    ''' delay updating the database until <see cref="saveChanges"/> is called.
    ''' This allows batch updates to occur with a single save.
    ''' The UI will be notified if the values are saved successfully, but
    ''' if the save fails, the changes will be reverted.
    ''' </summary>
    Public Sub setMantisID(mantisID As Integer, db As IntelligenceDB)
        setFieldAndMarkModified(mantisID, getWrappedItem.mantisID, Sub(v) getWrappedItem.mantisID = v, Function() Me.mantisID, db)
    End Sub
End Class
