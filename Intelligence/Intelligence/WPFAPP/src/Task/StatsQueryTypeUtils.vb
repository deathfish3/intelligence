﻿Public Class StatsQueryTypeUtils


    'Get _dbAdminLevel
    'Sync _dbAdminLevel with database on init. Always return _dbAdminLevel
    Private Shared _dbIntelligenceTaskStats As StatsQueryType = getStatsQueryTypeByName("intelligence task stats")
    Public Shared ReadOnly Property intelligenceTaskStats As StatsQueryType
        Get
            Return _dbIntelligenceTaskStats
        End Get
    End Property

    'Takes in 'name' and collects database accesslevel with name field matching name
    Private Shared Function getStatsQueryTypeByName(name As String) As StatsQueryType
        Dim statsQueryType As StatsQueryType = Nothing
        Using db As New IntelligenceDB
            statsQueryType =
                   (From type In db.StatsQueryTypes
                    Where type.active AndAlso type.name.ToLower.Equals(name.ToLower)
                    Select type).FirstOrDefault()
        End Using
        Return statsQueryType
    End Function

    Private Shared _allStatsQueryTypes As IEnumerable(Of StatsQueryType)
    Public Shared ReadOnly Property getAllStatsQueryTypes As IEnumerable(Of StatsQueryType)
        Get
            If _allStatsQueryTypes Is Nothing Then
                Using db As New IntelligenceDB
                    _allStatsQueryTypes = db.StatsQueryTypes.Where(Function(s) s.active).ToList()
                End Using
            End If
            Return _allStatsQueryTypes
        End Get
    End Property
End Class
