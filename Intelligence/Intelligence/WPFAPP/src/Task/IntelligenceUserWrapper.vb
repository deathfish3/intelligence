﻿Imports System.Collections.ObjectModel

''' <summary>
''' A wrapper around an <see cref="IntelligenceUser"/> from the database.
''' Allows access to the user's account details, and also the lists of tasks assigned to them.
''' </summary>
Public Class IntelligenceUserWrapper
    Inherits Wrapper(Of IntelligenceUser)

    ''' <summary>A flag to indicate whether this user exists in the database yet.</summary>
    Private hasBeenAddedToDatabase As Boolean

    ''' <summary>Create a new wrapper around the given user.</summary>
    ''' <param name="user">The user to wrap.</param>
    ''' <param name="hasBeenAddedToDatabase">
    ''' A flag to indicate whether this user exists in the database yet.
    ''' Defaults to true, so can be omitted in most cases.
    ''' </param>
    Sub New(user As IntelligenceUser, Optional hasBeenAddedToDatabase As Boolean = True)
        MyBase.New(user)
        Me.hasBeenAddedToDatabase = hasBeenAddedToDatabase
    End Sub

    ''' <summary>Initialize the user's access level in this wrapper while the databse connection is still open.</summary>
    ''' <param name="newItem">The newly downloaded user.</param>
    ''' <param name="db">Unused.</param>
    Protected Overrides Sub initializeFields(newItem As IntelligenceUser, Optional db As IntelligenceDB = Nothing)
        MyBase.initializeFields(newItem, db)
        _accessLevel = newItem.accessLevel
    End Sub

#Region "Account Details"
    '#####################################################
    '#                  Account Details                  #
    '#####################################################

    ''' <summary>
    ''' The user's username.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Public Overrides Property name As String
        Get
            Return getWrappedItem.username
        End Get
        Set(value As String)
            If hasBeenAddedToDatabase Then
                setFieldAndSave(value, getWrappedItem.username, Sub(v) getWrappedItem.username = v)
            Else
                getWrappedItem.username = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' The user's email address.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Property email As String
        Get
            Return getWrappedItem.email
        End Get
        Set(value As String)
            If hasBeenAddedToDatabase Then
                setFieldAndSave(value, getWrappedItem.email, Sub(v) getWrappedItem.email = v)
            Else
                getWrappedItem.email = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' The hash of the user's password.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Property hash As String
        Get
            Return getWrappedItem.hash
        End Get
        Set(value As String)
            If hasBeenAddedToDatabase Then
                setFieldAndSave(value, getWrappedItem.hash, Sub(v) getWrappedItem.hash = v)
            Else
                getWrappedItem.hash = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' A flag to say whether the user should use the default QA Zendesk account.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Property defaultzenlogin As Boolean
        Get
            Return getWrappedItem.defaultzenlogin
        End Get
        Set(value As Boolean)
            If hasBeenAddedToDatabase Then
                setFieldAndSave(value, getWrappedItem.defaultzenlogin, Sub(v) getWrappedItem.defaultzenlogin = v)
            Else
                getWrappedItem.defaultzenlogin = value
            End If
        End Set
    End Property

    ''' <summary>The internally stored access level of the user. Determines whether they can log in, and which panels they can see.</summary>
    Private _accessLevel As AccessLevel
    ''' <summary>
    ''' The access level of the user. Determines whether they can log in, and which panels they can see.
    ''' Setting this value will automatically update the database and notify the UI.
    ''' </summary>
    Property accessLevel As AccessLevel
        Get
            Return _accessLevel
        End Get
        Set(value As AccessLevel)
            If hasBeenAddedToDatabase Then
                setFieldAndSave(value, _accessLevel, Sub(v)
                                                         _accessLevel = v
                                                         getWrappedItem.accessLevel = v
                                                     End Sub)
            Else
                _accessLevel = value
                getWrappedItem.accessLevel = value
            End If
        End Set
    End Property

    ''' <summary>A list of all possible access levels (used for binding to the UI).</summary>
    Public ReadOnly Property allAccessLevels As IEnumerable(Of AccessLevel)
        Get
            Return AccessLevelUtils.getAllAccessLevels()
        End Get
    End Property
#End Region

#Region "Assigned Tasks"
    '#####################################################
    '#                Assigned Tasks                     #
    '#####################################################

    ''' <summary>The internally stored list of all in-progress leaf tasks assigned to the user.</summary>
    Private _tasksInProgressForCurrentUser As ObservableCollection(Of TaskInstanceWrapper) = Nothing
    ''' <summary>
    ''' The list of all in-progress leaf tasks assigned to the user, sorted in the order determined by
    ''' <see cref="TaskInstanceWrapper.CompareTo"/>. 
    ''' This will be cached the first time it is accessed, and can be refreshed using <see cref="refreshTasks"/>.
    ''' </summary>
    Public ReadOnly Property tasksInProgressForCurrentUser As ObservableCollection(Of TaskInstanceWrapper)
        Get
            If _tasksInProgressForCurrentUser Is Nothing Then
                reloadTaskLists()
            End If
            Return _tasksInProgressForCurrentUser
        End Get
    End Property

    ''' <summary>
    ''' The number of leaf node tasks the user currently has in progress.
    ''' Used to enforce a limit on the number of tasks a user can have open at once.
    ''' </summary>
    Public ReadOnly Property tasksInProgressForCurrentUserCount As Integer
        Get
            Dim inProgress As Resolution = ResolutionUtils.inProgressResolution
            Dim user As IntelligenceUser = getWrappedItem()
            Dim result As Integer
            Using db As New IntelligenceDB
                Dim myTasks As IQueryable(Of TaskInstance) = getLeafTasksForCurrentUser(user, db)
                Dim myInProgressTasks As IQueryable(Of TaskInstance) = myTasks.Where(Function(t) t.resolution.id = inProgress.id)
                result = myInProgressTasks.Count
            End Using
            Return result
        End Get
    End Property

    ''' <summary>The internally stored list of all closed leaf tasks assigned to the user.</summary>
    Private _tasksClosedForCurrentUser As ObservableCollection(Of TaskInstanceWrapper) = Nothing
    ''' <summary>
    ''' The list of all closed leaf tasks assigned to the user, sorted in the order determined by <see cref="FinishedComparison"/>. 
    ''' This will be cached the first time it is accessed, and can be refreshed using <see cref="refreshTasks"/>.
    ''' </summary>
    Public ReadOnly Property tasksClosedForCurrentUser As ObservableCollection(Of TaskInstanceWrapper)
        Get
            If _tasksClosedForCurrentUser Is Nothing Then
                reloadTaskLists()
            End If
            Return _tasksClosedForCurrentUser
        End Get
    End Property

    ''' <summary>The internally stored list of all open leaf tasks assigned to the user.</summary>
    Private _tasksForCurrentUser As ObservableCollection(Of TaskInstanceWrapper) = Nothing
    ''' <summary>
    ''' The list of all open leaf tasks assigned to the user, sorted in the order determined by
    ''' <see cref="TaskInstanceWrapper.CompareTo"/>. 
    ''' This will be cached the first time it is accessed, and can be refreshed using <see cref="refreshTasks"/>.
    ''' </summary>
    Public ReadOnly Property tasksForCurrentUser As ObservableCollection(Of TaskInstanceWrapper)
        Get
            If _tasksForCurrentUser Is Nothing Then
                reloadTaskLists()
            End If
            Return _tasksForCurrentUser
        End Get
    End Property

    ''' <summary>Connect to the intelligence database, and reload all the list of tasks assigned to this user.</summary>
    Private Sub reloadTaskLists()
        Using db As New IntelligenceDB
            Dim user As IntelligenceUser = getWrappedItem()
			Dim leafTasks = getTopLevelTasksForCurrentUser(user, db) 'getLeafTasksForCurrentUser

            Dim open As Resolution = ResolutionUtils.openResolution
            Dim myOpenTasks As IQueryable(Of TaskInstance) = leafTasks.Where(Function(t) t.resolution.id = open.id)
            fillWithSortedWrappedTasks(Me._tasksForCurrentUser, myOpenTasks, db)

            Dim inProgress As Resolution = ResolutionUtils.inProgressResolution
            Dim inProgressTasks As IQueryable(Of TaskInstance) = leafTasks.Where(Function(t) t.resolution.id = inProgress.id)
			fillWithSortedWrappedTasks(Me._tasksInProgressForCurrentUser, inProgressTasks, db, AddressOf Me.StartedComparison)

			Dim closedTasks As IQueryable(Of TaskInstance) = leafTasks.Where(Function(t) t.resolution.id <> open.id AndAlso t.resolution.id <> inProgress.id)
            fillWithSortedWrappedTasks(Me._tasksClosedForCurrentUser, closedTasks, db, AddressOf Me.FinishedComparison)

        End Using
        onPropertyChanged(Function() Me.tasksInProgressForCurrentUserCount)
    End Sub

    ''' <summary>
    ''' Get a query for the that will return all leaf nodes assigned to the current user
    ''' in versions that are still open (but do not actually execute the query at this point).
    ''' </summary>
    ''' <param name="user">The user tasks are assigned to.</param>
    ''' <param name="db">The open database context to search for tasks in.</param>
    ''' <returns>An unexecuted query that will return all leaf tasks assigned to the given user when it is run.</returns>
    Private Function getLeafTasksForCurrentUser(user As IntelligenceUser, db As IntelligenceDB) As IQueryable(Of TaskInstance)
		Dim versionOpen = VersionResolutionUtils.openResolution
		Dim tasks As IQueryable(Of TaskInstance) =
			From task In db.TaskInstances
			Where task.assignee IsNot Nothing _
			AndAlso task.assignee.id = user.id _
			AndAlso task.version.resolution.id = versionOpen.id _
			AndAlso task.taskTemplate.product.active
			Select task

		Dim leafTasks As IQueryable(Of TaskInstance) = TaskInstanceUtils.filterLeafNodes(tasks, db)
		Return leafTasks
	End Function

	Private Function getTopLevelTasksForCurrentUser(user As IntelligenceUser, db As IntelligenceDB) As IQueryable(Of TaskInstance)
		Dim versionOpen As VersionResolution = VersionResolutionUtils.openResolution
		Dim tasks As IQueryable(Of TaskInstance) = (
			From task In db.TaskInstances
			Where task.assignee IsNot Nothing _
			AndAlso task.assignee.id = user.id _
			AndAlso task.version.resolution.id = versionOpen.id _
			AndAlso task.taskTemplate.product.active _
			AndAlso (task.taskTemplate.parentTask Is Nothing _
				 OrElse Not (
					From parent In task.version.taskInstances
					Where parent.taskTemplate.id = task.taskTemplate.parentTask.id _
					AndAlso parent.platform.id = task.platform.id _
					AndAlso parent.assignee IsNot Nothing _
					AndAlso parent.assignee.id = user.id
					Select parent
				).Any()
			)
			Select task
		)
		Return tasks
    End Function

	''' <summary>
	''' Fill the given list with the results of the given query converted
	''' to <see cref="TaskInstanceWrapper"/>s, and sort them in the correct order.
	''' </summary>
	''' <param name="listToFill">
	''' The list to put the new <see cref="TaskInstanceWrapper"/>s in.
	''' The list passed by referebce, so if it is <c>Nothing</c>, it will be initialized.
	''' </param>
	''' <param name="tasks">A query that will return all the tasks to put in the list.</param>
	''' <param name="db">The database to perform the query on.</param>
	''' <param name="comparison">
	''' An optional custom comparison function for sorting the list with.
	''' If nothing is supplied, it will use <see cref="TaskInstanceWrapper.CompareTo"/> by default.
	''' </param>
	Private Sub fillWithSortedWrappedTasks(ByRef listToFill As ICollection(Of TaskInstanceWrapper), tasks As IQueryable(Of TaskInstance), db As IntelligenceDB, Optional comparison As System.Comparison(Of TaskInstanceWrapper) = Nothing)
        If listToFill Is Nothing Then
            listToFill = New ObservableCollection(Of TaskInstanceWrapper)
        End If
        Dim tempList As New List(Of TaskInstanceWrapper)
        For Each instance In tasks.ToList
            tempList.Add(New TaskInstanceWrapper(instance, Nothing, db))
        Next
        If comparison Is Nothing Then
            tempList.Sort()
        Else
            tempList.Sort(comparison)
        End If

        listToFill.Clear()
        For Each item In tempList
            listToFill.Add(item)
        Next
    End Sub

    ''' <summary>Refresh all the list of tasks assined to this user from the database, an notify the UI.</summary>
    Public Sub refreshTasks()
        reloadTaskLists()
        onPropertyChanged(Function() Me.tasksForCurrentUser)
        onPropertyChanged(Function() Me.tasksClosedForCurrentUser)
        onPropertyChanged(Function() Me.tasksInProgressForCurrentUser)
    End Sub

    ''' <summary>Compare the two tasks based on which one was finished most recently.</summary>
    ''' <param name="a">One task to compare.</param>
    ''' <param name="b">The other task to compare.</param>
    ''' <returns>Will return 1 if a &gt; b, -1 if a &lt; b, or 0 if a == b.</returns>
    Private Function FinishedComparison(a As TaskInstanceWrapper, b As TaskInstanceWrapper)
		Return b.timeFinished.GetValueOrDefault.CompareTo(a.timeFinished.GetValueOrDefault)
	End Function

	Private Function StartedComparison(a As TaskInstanceWrapper, b As TaskInstanceWrapper)
		Return a.timeStarted.GetValueOrDefault.CompareTo(b.timeStarted.GetValueOrDefault)
	End Function

    ''' <summary>
    ''' When one of this user's tasks changes its resolution,
    ''' move it to the correct list based on its new resolution.
    ''' </summary>
    ''' <param name="task">The task whos resolution changed.</param>
    Public Sub onTaskResolutionChanged(task As TaskInstanceWrapper)
        If task.resolution.Equals(ResolutionUtils.openResolution) Then
            Me.tasksInProgressForCurrentUser.Remove(task)
            Me.tasksClosedForCurrentUser.Remove(task)
            Me.tasksForCurrentUser.Insert(0, task)
        ElseIf task.resolution.Equals(ResolutionUtils.inProgressResolution) Then
            Me.tasksForCurrentUser.Remove(task)
            Me.tasksClosedForCurrentUser.Remove(task)
            Me.tasksInProgressForCurrentUser.Insert(0, task)
        Else
            Me.tasksInProgressForCurrentUser.Remove(task)
            Me.tasksForCurrentUser.Remove(task)
            If Not tasksClosedForCurrentUser.Contains(task) Then
                Me.tasksClosedForCurrentUser.Insert(0, task)
            End If
        End If
    End Sub
#End Region

End Class
