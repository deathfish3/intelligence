﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data.Entity
Imports WPFAPP.NotifyPropertyChangedBase

''' <summary>
''' A collection of <see cref="Wrapper(Of T)"/> items. Contains helper methods
''' for adding, deleting, and reloading items from the database, as well as updating
''' the UI and keeping track of the selected item.
''' </summary>
''' <typeparam name="T">
''' The type of database item that the <see cref="Wrapper(Of T)"/> 
''' in this list wrap.
''' </typeparam>
Public MustInherit Class WrapperList(Of T As {Class, IHasID, New})
    Inherits ObservableCollection(Of Wrapper(Of T))

#Region "Properties"
    '#########################################################################
    '#                             Properties                                #
    '#########################################################################

    ''' <summary>
    ''' Whether newly added items should be added to the top of the list rather 
    ''' than the bottom. Subclasses can override this to change the default behaviour
    ''' </summary>
    Protected Overridable Property shouldAddNewItemsToTop As Boolean = False

    ''' <summary>Will be True if the list is empty.</summary>
    ''' <remarks>Adding and removing entries will trigger property changed events for this.</remarks>
    Public ReadOnly Property isEmpty
        Get
            Return Not Me.Any()
        End Get
    End Property

    ''' <summary>The internal field holding the currently selected item.</summary>
    Private _selectedItem As Wrapper(Of T)

    ''' <summary>
    ''' The currently selected item in the list. 
    ''' Can be bound to the UI using <see cref="ListView.SelectedItem"/> with a two-way binding to 
    ''' programatically select an item, or simply track the user's current selection.
    ''' </summary>
    Public Overridable Property selectedItem As Wrapper(Of T)
        Get
            Return _selectedItem
        End Get
        Set(value As Wrapper(Of T))
            If value IsNot selectedItem OrElse (value IsNot Nothing AndAlso Not value.Equals(selectedItem)) Then
                _selectedItem = value
                OnPropertyChanged(getPropertyChangedArgs(Function() Me.selectedItem))
                If onSelectionChangedListener IsNot Nothing Then
                    onSelectionChangedListener.onSelectionChanged(value)
                End If
            End If
        End Set
    End Property

#End Region

#Region "Loading Contents"
    '#########################################################################
    '#                      Loading Contents                                 #
    '#########################################################################

    ''' <summary>
    ''' Create a new wrapper object around the given item.
    ''' New wrappers are created when the list's contents are loaded or reloaded.
    ''' </summary>
    ''' <remarks>    
    ''' By default, this function is passed in to the constructor to allow <see cref="reloadContents"/>
    ''' to be called, but wrap functions that require more arguments will need to be initialized
    ''' manually in the constructor before calling <see cref="reloadContents"/>.
    ''' </remarks>
    Protected Property wrap As System.Func(Of T, IntelligenceDB, Wrapper(Of T))

    ''' <summary>Load in the objects from the database that should populate this list.</summary>
    ''' <remarks>
    ''' This function is passed in via the constructor, and is called whenever the contents
    ''' are loaded or reloaded.
    ''' </remarks>
    Private loadContents As System.Func(Of IntelligenceDB, IEnumerable(Of T))

    ''' <summary>Create a new list, and load in its initial contents.</summary>
    ''' <param name="contentsLoader">A function for loading in the list's contents from the database.</param>
    ''' <param name="wrap">A function for creating a new <see cref="Wrapper(Of T)"/> object around a given database item.</param>
    ''' <remarks>
    ''' This constructor will call <see cref="reloadContents"/>, which will use both
    ''' <paramref name="contentsLoader"/> and <paramref name="wrap"/>, which is why they are both
    ''' passed into the constructor. If <see cref="wrap"/> requires more parameters,
    ''' the other constructor should be called, and <see cref="reloadContents"/> will need called
    ''' manually after <see cref="wrap"/> has been set up.
    ''' </remarks>
    Public Sub New(contentsLoader As System.Func(Of IntelligenceDB, IEnumerable(Of T)), wrap As System.Func(Of T, IntelligenceDB, Wrapper(Of T)))
        MyBase.New()
        Me.loadContents = contentsLoader
        Me.wrap = wrap
        reloadContents()
    End Sub

    ''' <summary>
    ''' Create a new list without loading any contents. 
    ''' The function <see cref="wrap"/> must be initialized manually after calling this, and
    ''' <see cref="reloadContents"/> will need to be called too after <see cref="wrap"/> has been set.
    ''' </summary>
    ''' <param name="contentsLoader">A function for loading in the list's contents from the database.</param>
    ''' <remarks>
    ''' The other constructor should be used in most cases, as it will call <see cref="reloadContents"/>.
    ''' This constructor should only be used if <see cref="wrap"/> requires more parameters than can be passed
    ''' in via the lambda in the other constructor (e.g. passing in a reference to <c>Me</c> is invalid when
    ''' calling <c>MyBase.New</c>). When this constructor is used, <see cref="reloadContents"/> will need called
    ''' manually after <see cref="wrap"/> has been set up.
    ''' </remarks>
    Protected Sub New(contentsLoader As System.Func(Of IntelligenceDB, IEnumerable(Of T)))
        MyBase.New()
        Me.loadContents = contentsLoader
    End Sub

    ''' <summary>Reload the contents of this list from the database, while maintaining the user's current selection.</summary>
    ''' <param name="inDB">
    ''' The database context to reload the contents with. 
    ''' If omitted, or set as <c>Nothing</c>, a new context will be created.
    ''' </param>
    ''' <remarks>
    ''' New wrappers will be created whenever this function is called, so anything bound to the
    ''' old set of wrappers will need to be manually updated.
    ''' 
    ''' This function makes use of both <see cref="loadContents"/> and <see cref="wrap"/>, so these
    ''' will need to be initialized before calling it. The default constructor will set these up by
    ''' before calling this method, but the other constructor requires the caller to set up
    ''' <see cref="wrap"/> manually before manually calling <see cref="reloadContents"/>.
    ''' </remarks>
    Public Overridable Sub reloadContents(Optional inDB As IntelligenceDB = Nothing)
        'Remember the last selected item.
        Dim lastSelectedItem As Wrapper(Of T) = selectedItem

        'Remove all contents, and let the UI know.
        Clear()

        Dim db As IntelligenceDB = If(inDB Is Nothing, New IntelligenceDB, inDB)

        'Fill in the list with new wrappers around the new contents.
        For Each item As T In loadContents(db).ToList()
            Add(wrap(item, db))
        Next

        If inDB Is Nothing Then
            db.Dispose()
        End If

        'Re-select the last item if it is still in the list.
        If lastSelectedItem IsNot Nothing AndAlso Me.Contains(lastSelectedItem) Then
            selectedItem = lastSelectedItem
        Else
            selectedItem = Nothing
        End If

        'Let the listener know the contents have changed.
        If onReloadListener IsNot Nothing Then
            onReloadListener.onContentsReloaded()
        End If
    End Sub
#End Region

#Region "Deleting"
    '#########################################################################
    '#                              Deleting                                 #
    '#########################################################################

    ''' <summary>
    ''' Remove the contents of the given wrapper from the database.
    ''' This is up to subclasses to implement, as it often requires more logic
    ''' than simply removing the row, such as setting fields to inactive, or propagating
    ''' deletion or de-activation up a tree.
    ''' </summary>
    ''' <param name="db">The database context to remove from.</param>
    ''' <param name="toDelete">The wrapper containing the item to be removed.</param>
    ''' <returns>True if the deletion was successful. False otherwise.</returns>
    ''' <remarks>Not all lists should support deletion, so overriding this is optional.</remarks>
    Protected Overridable Function deleteFromDB(db As IntelligenceDB, toDelete As Wrapper(Of T)) As Boolean
        Throw New NotImplementedException("Trying to remove " & toDelete.ToString & " from a list that does not support it.")
    End Function

    ''' <summary>Delete an item from the database, and update the UI.</summary>
    ''' <param name="toDelete">The wrapper around the database item to be deleted.</param>
    ''' <returns>True if the deletion was saved successfully to the database.</returns>
    Public Function deleteAndSave(toDelete As Wrapper(Of T)) As Boolean
        Dim success As Boolean = False
        Using db As New IntelligenceDB
            delete(toDelete, Me.IndexOf(toDelete), db)
            success = DBManager.saveChanges(db)
        End Using
        Return success
    End Function

    ''' <summary>Delete an item from the database, and update the UI.</summary>
    ''' <param name="index">The index in this list of the database item to be deleted.</param>
    ''' <returns>True if the deletion was saved successfully to the database.</returns>
    Public Function deleteAndSave(index As Integer) As Boolean
        Dim success As Boolean = False
        Using db As New IntelligenceDB
            delete(Me.Item(index), index, db)
            success = DBManager.saveChanges(db)
        End Using
        Return success
    End Function

    ''' <summary>
    ''' Delete the item at the given index from the given database without 
    ''' calling <see cref="DBManager.saveChanges"/>, and refresh the UI. 
    ''' Used for batch deletion from a single database context.
    ''' </summary>
    ''' <param name="index">The index in this list of the database item to be deleted.</param>
    ''' <param name="db">The database context to delete entries from.</param>
    Public Sub delete(index As Integer, db As IntelligenceDB)
        delete(Me.Item(index), index, db)
    End Sub

    ''' <summary>Delete an item from the database using <see cref="deleteFromDB"/>, and remove it from the UI.</summary>
    ''' <param name="toDelete">The wrapper around the database item to delete.</param>
    ''' <param name="index">The index in this list where the item resides.</param>
    ''' <param name="db">The database context to remove the item from.</param>
    Private Sub delete(toDelete As Wrapper(Of T), index As Integer, db As IntelligenceDB)
        If deleteFromDB(db, toDelete) Then
            Me.RemoveItem(index)
        End If
    End Sub

    ''' <summary>
    ''' Remove the item at the given index witout deleting it from the database.
    ''' Will trigger UI updates as neccessary.
    ''' </summary>
    ''' <param name="index">The index of the item to remove.</param>
    Protected Overrides Sub RemoveItem(index As Integer)
        MyBase.RemoveItem(index)
        MyBase.OnPropertyChanged(getPropertyChangedArgs(Function() Me.isEmpty))
    End Sub

#End Region

#Region "Adding"
    '#########################################################################
    '#                                Adding                                 #
    '#########################################################################

    ''' <summary>Initialize a newly added item with default values.</summary>
    ''' <param name="newItem">The new item that has been added to the database, but not saved yet.</param>
    ''' <param name="db">The database context to item was added to.</param>
    ''' <remarks>
    ''' Not all lists support addition, or require any default initialization, 
    ''' so overriding this is optional.
    ''' </remarks>
    Protected Overridable Sub initialiseNewItem(newItem As T, db As IntelligenceDB)
    End Sub

    ''' <summary>
    ''' Create a new item with default values, and add it to both the database, and this list.
    ''' Will call <see cref="DBManager.saveChanges"/> immediately, and remove the item from the
    ''' list if the save fails.
    ''' </summary>
    ''' <returns>A wrapper around the new database item. Will return <c>Nothing</c> if the save fails.</returns>
    Public Function addNewAndSave() As Wrapper(Of T)
        Dim newItem As Wrapper(Of T)
        Using db As New IntelligenceDB
            newItem = addNew(db)
            If Not DBManager.saveChanges(db) Then
                Me.Remove(newItem)
                newItem = Nothing
            End If
        End Using
        Return newItem
    End Function

    ''' <summary>
    ''' Create a new item with default values, and add it to both the given database context,
    ''' and this list. Does not call <see cref="DBManager.saveChanges"/> to enable batch adding.
    ''' </summary>
    ''' <param name="db">The database context to add the item to.</param>
    ''' <param name="shouldAddToList">
    ''' True by default, so is only needed in cases where the item should not be added
    ''' to the UI immediately (e.g. when creating new user accounts).
    ''' </param>
    ''' <returns>A wrapper around the new database item.</returns>
    Public Overridable Function addNew(db As IntelligenceDB, Optional shouldAddToList As Boolean = True) As Wrapper(Of T)
        Dim newItem As New T
        db.Set(Of T).Add(newItem)
        initialiseNewItem(newItem, db)

        Dim wrappedItem As Wrapper(Of T) = wrap(newItem, db)

        If shouldAddToList Then
            If (shouldAddNewItemsToTop) Then
                Insert(0, wrappedItem)
            Else
                Add(wrappedItem)
            End If
        End If
        Return wrappedItem
    End Function

    ''' <summary>
    ''' Insert an item to the list at the given index without updating the database.
    ''' Will trigger UI updates as neccessary.
    ''' </summary>
    ''' <param name="index">The index of the list to insert the item at. All subsequent entries are shifted up.</param>
    ''' <param name="item">The item to add to the list.</param>
    Protected Overrides Sub InsertItem(index As Integer, item As Wrapper(Of T))
        Dim wasEmpty = isEmpty
        MyBase.InsertItem(index, item)
        If wasEmpty Then
            MyBase.OnPropertyChanged(getPropertyChangedArgs(Function() Me.isEmpty))
        End If
    End Sub

#End Region


#Region "Listener"
    '#########################################################################
    '#                               Listeners                               #
    '#########################################################################

    ''' <summary>An interface for responding to listviews whenever <see cref="reloadContents"/> is called.</summary>
    Public Interface IReloadContentsListener
        Sub onContentsReloaded()
    End Interface

    ''' <summary>A single object that can respond to this list when it calls <see cref="reloadContents"/></summary>
    Private onReloadListener As IReloadContentsListener = Nothing

    ''' <summary>Replaces the old listener with the given one, so that calls to <see cref="reloadContents"/> can be responded to.</summary>
    ''' <param name="onReloadListener">The new listener for this list's <see cref="reloadContents"/> calls.</param>
    Public Sub setReloadListener(onReloadListener As IReloadContentsListener)
        Me.onReloadListener = onReloadListener
    End Sub

    ''' <summary>Sets the existing reload listener to <c>Nothing</c>.</summary>
    ''' <param name="onReloadListener">Should be this WrapperList's current listener, but is not used so could be anything.</param>
    Public Sub removeReloadListener(onReloadListener As IReloadContentsListener)
        Me.onReloadListener = Nothing
    End Sub

    ''' <summary>An interface for responding to listviews whenever <see cref="selectedItem"/> is changed.</summary>
    Public Interface ISelectionChanedListener(Of F As {Class, IHasID, New})
        Sub onSelectionChanged(newlySelectedItem As Wrapper(Of F))
    End Interface

    ''' <summary>A single object for responding to listviews whenever <see cref="selectedItem"/> is changed.</summary>
    Private onSelectionChangedListener As ISelectionChanedListener(Of T)

    ''' <summary>Replaces the old listener with the given one, so that changes to <see cref="selectedItem"/> can be responded to.</summary>
    ''' <param name="onSelectionChangedListener">The new listener for this list's <see cref="selectedItem"/>.</param>
    Public Sub setSelectionChangedListener(onSelectionChangedListener As ISelectionChanedListener(Of T))
        Me.onSelectionChangedListener = onSelectionChangedListener
    End Sub

    ''' <summary>Sets the existing selection changed listener to <c>Nothing</c>.</summary>
    ''' <param name="onSelectionChangedListener">Should be this WrapperList's current listener, but is not used so could be anything.</param>
    Public Sub removeSelectionChangedListener(onSelectionChangedListener As ISelectionChanedListener(Of T))
        Me.onSelectionChangedListener = Nothing
    End Sub

#End Region

End Class
