﻿'############################################################################
'ResolutionsUtils.vb
'
'Purpose: To act as an 'enum' class for Resolutions
'
'Description: Contains each resolution in Database (as of 16/07/15). 
'Will populate each resolution with associated resolution stored in database
'
'Modification: To create a new resolution entry, copy...

'       Private Shared _dbYourRes As Resolution = GetResolutionByName("YourResolutionInDb")
'       Public Shared ReadOnly Property yourResolution As Resolution
'           Get
'               Return _dbYourRes
'           End Get
'       End Property
'
' ...into ResolutionsUtils class.
'############################################################################


Public Class ResolutionUtils
    'Get current resolution from DB,
    'Provide storage for them too


    'Get _dbOpenRes
    'Sync _dbOpenRes with database on init. Always return _dbOpenRes
    Private Shared _dbOpenRes As Resolution = GetResolutionByName("Open")
    Public Shared ReadOnly Property openResolution As Resolution
        Get
            Return _dbOpenRes
        End Get
    End Property

    'Get _dbPassRes
    'Sync _dbPassRes with database on init. Always return _dbPassRes
    Private Shared _dbPassRes As Resolution = GetResolutionByName("Pass")
    Public Shared ReadOnly Property passResolution As Resolution
        Get
            Return _dbPassRes
        End Get
    End Property

    'Get _dbFailRes
    'Sync _dbFailRes with database on init. Always return _dbFailRes
    Private Shared _dbFailRes As Resolution = GetResolutionByName("Fail")
    Public Shared ReadOnly Property failResolution As Resolution
        Get
            Return _dbFailRes
        End Get
    End Property

    'Get _dbNARes
    'Sync _dbNARes with database on init. Always return _dbNARes
    Private Shared _dbNARes As Resolution = GetResolutionByName("N/A")
    Public Shared ReadOnly Property naResolution As Resolution
        Get
            Return _dbNARes
        End Get
    End Property

    'Get _dbFinSubtasks
    'Sync _dbFinSubtasks with database on init. Always return _dbFinSubtasks
    Private Shared _dbFinSubtasks As Resolution = GetResolutionByName("Finished Subtasks")
    Public Shared ReadOnly Property finSubtasksResolution As Resolution
        Get
            Return _dbFinSubtasks
        End Get
    End Property

    'Get _dbNotComplete
    'Sync _dbNotComplete with database on init. Always return _dbNotComplete
    Private Shared _dbNotComplete As Resolution = GetResolutionByName("Not Complete")
    Public Shared ReadOnly Property notCompleteResolution As Resolution
        Get
            Return _dbNotComplete
        End Get
    End Property

    'Get _dbInProgress
    'Sync _dbInProgress with database on init. Always return _dbInProgress
    Private Shared _dbInProgress As Resolution = GetResolutionByName("In Progress")
    Public Shared ReadOnly Property inProgressResolution As Resolution
        Get
            Return _dbInProgress
        End Get
    End Property

    'Check if resolution is closed. Calls to 'isOpen(resolution As Resolution) as Boolean'
    Public Shared Function isClosed(resolution As Resolution) As Boolean
        Return Not isOpen(resolution)
    End Function

    'Check if resolution is open
    Public Shared Function isOpen(resolution As Resolution) As Boolean
        Return resolution.active AndAlso (resolution.id = openResolution.id OrElse isInProgress(resolution))
    End Function

    'Check if resolution in progress
    Public Shared Function isInProgress(resolution As Resolution) As Boolean
        Return resolution.id = inProgressResolution.id
    End Function

    'Takes in 'name' and collects database resolution with name field matching name
    Private Shared Function GetResolutionByName(name As String) As Resolution
        Using db As New IntelligenceDB
            Return (From res In db.Resolutions
                    Where res.name.ToLower.Equals(name.ToLower) And res.active = True
                    Select res).FirstOrDefault
        End Using
    End Function

    Private Shared _allResolutions As IEnumerable(Of Resolution)
    Public Shared ReadOnly Property allResolutions As IEnumerable(Of Resolution)
        Get
            If _allResolutions Is Nothing Then
                Using db As New IntelligenceDB
                    _allResolutions = db.Resolutions.ToList()
                End Using
            End If
            Return _allResolutions
        End Get
    End Property

    Private Shared _userSelectableResolutions As IEnumerable(Of Resolution)
    Public Shared ReadOnly Property userSelectableResolutions As IEnumerable(Of Resolution)
        Get
            If _userSelectableResolutions Is Nothing Then
                _userSelectableResolutions = allResolutions.Where(AddressOf isSelectableByUser).ToList()
            End If
            Return _userSelectableResolutions
        End Get
    End Property

    Public Shared Function isSelectableByUser(res As Resolution) As Boolean
        Return res.active _
        AndAlso Not res.Equals(finSubtasksResolution) _
        AndAlso Not res.Equals(notCompleteResolution)
    End Function
End Class
