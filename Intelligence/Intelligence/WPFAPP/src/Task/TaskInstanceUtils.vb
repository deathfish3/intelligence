﻿''' <summary>
''' A group of shared utility methods for dealing with <see cref="TaskInstance"/>s and
''' <see cref="TaskInstanceWrapper"/>s. Many of the fields for task instances have effects
''' that propagate up and down the whole tree of tasks, and most of the methods in this class
''' are for altering these fields while keeping the tree consitisant. Most methods come in
''' two groups - wrapped ones (for operating on <see cref="TaskInstanceWrapper"/>s) and unwrapped
''' ones (for operating on raw <see cref="TaskInstance"/>s). Wrapped methods will make use of
''' the extra features of wrappers, such as using locally cached versions, notifying the UI of the
''' changes, and adding the modifications to a stack that can be reverted if the final save fails.
''' The unwrapped methods operate directly on the database objects, and are sometimes called from
''' the wrapped methods if the wrapper does not have a reference to its parent but the changes
''' still need propagated up the tree.
''' </summary>
Public Class TaskInstanceUtils

#Region "Assigning Wrapped"
    '#######################################################
    '#                Assigning Wrapped                    #
    '#######################################################

    ''' <summary>
    ''' Starting from the given wrapper, assign this task and all its subtasks to
    ''' the given user, and then update this wrapper's ancestors to reflect this change.
    ''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
    ''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
    ''' 
    ''' If all children of a node are assigned to the same user, then that node becomes assigned to that user.
    ''' Otherwise, that node's assignee is set to <c>Nothing</c>.
    ''' 
    ''' For leaf nodes, <c>Nothing</c> means that the task is unassigned,
    ''' but for parent nodes, <c>Nothing</c> means that either at least one subtask is unassigned, or the subtasks
    ''' are assigned to multiple different users.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper from to which set the assignee for this task and all its subtasks.</param>
    ''' <param name="assignee">
    ''' The new assignee to set. 
    ''' If <c>Nothing</c>, then this task and all its subtasks will be unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user assigning the task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Shared Sub assignInstance(taskInstance As TaskInstanceWrapper, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        If assignInstanceAndSubtasks(taskInstance, assignee, assigner, timeAssigned, db) Then
            refreshAssignmentFromChildren(taskInstance.parent, assignee, assigner, timeAssigned, db)
        End If
    End Sub

    ''' <summary>Starting from the given wrapper, assign this task and all its subtasks to the given user.</summary>
    ''' <param name="taskInstance">The wrapper from which to set the assignee for this task and all its subtasks.</param>
    ''' <param name="assignee">
    ''' The new assignee to set. 
    ''' If <c>Nothing</c>, then this task and all its subtasks will be unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user assigning the task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function assignInstanceAndSubtasks(taskInstance As TaskInstanceWrapper, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB) As Boolean
        If taskInstance IsNot Nothing Then
            If assignSingleInstance(taskInstance, assignee, assigner, timeAssigned, db) Then
                For Each subTask In taskInstance.subTasks
                    assignInstanceAndSubtasks(subTask, assignee, assigner, timeAssigned, db)
                Next
                Return True
            End If
        End If
        Return False
    End Function

    ''' <summary>
    ''' Call this when one of the given task's children has changed its assignee.
    ''' 
    ''' If all children are assigned to the same user, then this node becomes assigned to that user.
    ''' Otherwise, this node's assignee is set to <c>Nothing</c>.
    ''' 
    ''' For leaf nodes, <c>Nothing</c> means that the task is unassigned,
    ''' but for parent nodes, <c>Nothing</c> means that either at least one subtask is unassigned, or the subtasks
    ''' are assigned to multiple different users.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper whose child has changed assignee.</param>
    ''' <param name="assignee">
    ''' The new assignee that the child has been assigned to. 
    ''' If <c>Nothing</c>, then it has been unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user that assigned the child task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task was unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time the child task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task was unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshAssignmentFromChildren(taskInstance As TaskInstanceWrapper, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            If assignee IsNot Nothing _
            AndAlso (taskInstance.assignee Is Nothing OrElse taskInstance.assignee.id <> assignee.id) _
            AndAlso areAllChildrenAssignedTo(taskInstance, assignee) Then
                assignSingleInstance(taskInstance, assignee, assigner, timeAssigned, db)
                refreshAssignmentFromChildren(taskInstance.parent, assignee, assigner, timeAssigned, db)
            Else
                If assignSingleInstance(taskInstance, Nothing, Nothing, Nothing, db) Then
                    refreshAssignmentFromChildren(taskInstance.parent, Nothing, Nothing, Nothing, db)
                End If
            End If
        End If
    End Sub

    ''' <summary>Whether all the the given wrapper's subtasks are assigned to the given user.</summary>
    ''' <param name="taskInstance">The wrapper whos subtasks will be tested.</param>
    ''' <param name="assigneeToTest">The assignee to check the subtasks for. Must not be <c>Nothing</c>.</param>
    ''' <returns>True if all the given wrapper's subtasks are assigned to the given use.</returns>
    Private Shared Function areAllChildrenAssignedTo(taskInstance As TaskInstanceWrapper, assigneeToTest As IntelligenceUser) As Boolean
        Return taskInstance.subTasks.All(
            Function(subTask As TaskInstanceWrapper)
                Return subTask.assignee IsNot Nothing _
                AndAlso subTask.assignee.id = assigneeToTest.id
            End Function)
    End Function

    ''' <summary>
    ''' Set the assignee for only the given wrapper without altering the rest of the tree.
    ''' Uses <see cref="TaskInstanceWrapper.setAssigneeWithoutAlteringTree"/> for this.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper to the assignee for.</param>
    ''' <param name="newAssignee">The new assignee to set. If <c>Nothing</c>, then this task will be unassigned. </param>
    ''' <param name="assigner">
    ''' The user assigning the task. If <paramref name="assignee"/> is <c>Nothing</c>, then this
    ''' will be ignored, and set to <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned. If <paramref name="assignee"/> is <c>Nothing</c>,  
    ''' then this will be ignored, and set to <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function assignSingleInstance(taskInstance As TaskInstanceWrapper, newAssignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB) As Boolean
        If taskInstance IsNot Nothing Then
            If newAssignee IsNot Nothing Then
                If taskInstance.assignee Is Nothing OrElse taskInstance.assignee.id <> newAssignee.id Then
                    taskInstance.setAssigneeWithoutAlteringTree(newAssignee, assigner, timeAssigned, db)
                    Return True
                End If
            ElseIf taskInstance.assignee IsNot Nothing Then
                taskInstance.setAssigneeWithoutAlteringTree(Nothing, Nothing, Nothing, db)
                Return True
            End If
        End If
        Return False
    End Function

#End Region

#Region "Assigning Unwrapped"
    '#######################################################
    '#               Assigning Unwrapped                   #
    '#######################################################

    ''' <summary>
    ''' Starting from the given instance, assign this task and all its subtasks to
    ''' the given user, and then update this instance's ancestors to reflect this change.
    ''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
    ''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
    ''' 
    ''' If all children of a node are assigned to the same user, then that node becomes assigned to that user.
    ''' Otherwise, that node's assignee is set to <c>Nothing</c>.
    ''' 
    ''' For leaf nodes, <c>Nothing</c> means that the task is unassigned,
    ''' but for parent nodes, <c>Nothing</c> means that either at least one subtask is unassigned, or the subtasks
    ''' are assigned to multiple different users.
    ''' </summary>
    ''' <param name="taskInstance">The instance from to which set the assignee for this task and all its subtasks.</param>
    ''' <param name="assignee">
    ''' The new assignee to set. 
    ''' If <c>Nothing</c>, then this task and all its subtasks will be unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user assigning the task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Shared Sub assignInstance(taskInstance As TaskInstance, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        assignInstanceAndSubtasks(taskInstance, assignee, assigner, timeAssigned, db)
        refreshAssignmentFromChildren(getParentInstance(taskInstance), assignee, assigner, timeAssigned, db)
    End Sub

    ''' <summary>Starting from the given instance, assign this task and all its subtasks to the given user.</summary>
    ''' <param name="taskInstance">The instance from which to set the assignee for this task and all its subtasks.</param>
    ''' <param name="assignee">
    ''' The new assignee to set. 
    ''' If <c>Nothing</c>, then this task and all its subtasks will be unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user assigning the task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub assignInstanceAndSubtasks(taskInstance As TaskInstance, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            assignSingleInstance(taskInstance, assignee, assigner, timeAssigned)
            For Each subTask As TaskInstance In getSubtasks(taskInstance, db)
                assignInstanceAndSubtasks(subTask, assignee, assigner, timeAssigned, db)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Call this when one of the given task's children has changed its assignee.
    ''' 
    ''' If all children are assigned to the same user, then this node becomes assigned to that user.
    ''' Otherwise, this node's assignee is set to <c>Nothing</c>.
    ''' 
    ''' For leaf nodes, <c>Nothing</c> means that the task is unassigned,
    ''' but for parent nodes, <c>Nothing</c> means that either at least one subtask is unassigned, or the subtasks
    ''' are assigned to multiple different users.
    ''' </summary>
    ''' <param name="taskInstance">The instance whose child has changed assignee.</param>
    ''' <param name="assignee">
    ''' The new assignee that the child has been assigned to. 
    ''' If <c>Nothing</c>, then it has been unassigned.
    ''' </param>
    ''' <param name="assigner">
    ''' The user that assigned the child task.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task was unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time the child task was assigned.
    ''' If <paramref name="assignee"/> is <c>Nothing</c>, then this will be ignored, and set to 
    ''' <c>Nothing</c> to signal that the task was unassigned.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshAssignmentFromChildren(taskInstance As TaskInstance, assignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            If assignee IsNot Nothing _
            AndAlso (taskInstance.assignee Is Nothing OrElse taskInstance.assignee.id <> assignee.id) _
            AndAlso areAllChildrenAssignedTo(taskInstance, assignee, db) Then
                assignSingleInstance(taskInstance, assignee, assigner, timeAssigned)
                refreshAssignmentFromChildren(getParentInstance(taskInstance), assignee, assigner, timeAssigned, db)
            Else
                assignSingleInstance(taskInstance, Nothing, Nothing, Nothing)
                refreshAssignmentFromChildren(getParentInstance(taskInstance), Nothing, Nothing, Nothing, db)
            End If
        End If
    End Sub

    ''' <summary>Whether all the the given instance's subtasks are assigned to the given user.</summary>
    ''' <param name="taskInstance">The instance whos subtasks will be tested.</param>
    ''' <param name="assigneeToTest">The assignee to check the subtasks for. Must not be <c>Nothing</c>.</param>
    ''' <param name="db">The open database context to look up the task's children in.</param>
    ''' <returns>True if all the given instance's subtasks are assigned to the given use.</returns>
    Private Shared Function areAllChildrenAssignedTo(taskInstance As TaskInstance, assigneeToTest As IntelligenceUser, db As IntelligenceDB) As Boolean
        Return getSubtasks(taskInstance, db).All(
            Function(subTask)
                Return subTask.assignee IsNot Nothing _
                AndAlso subTask.assignee.id = assigneeToTest.id
            End Function)
    End Function

    ''' <summary>Set the assignee for only the given instance without altering the rest of the tree.</summary>
    ''' <param name="taskInstance">The instance to the assignee for.</param>
    ''' <param name="newAssignee">The new assignee to set. If <c>Nothing</c>, then this task will be unassigned. </param>
    ''' <param name="assigner">
    ''' The user assigning the task. If <paramref name="assignee"/> is <c>Nothing</c>, then this
    ''' will be ignored, and set to <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    ''' <param name="timeAssigned">
    ''' The date and time this task was assigned. If <paramref name="assignee"/> is <c>Nothing</c>,  
    ''' then this will be ignored, and set to <c>Nothing</c> to signal that the task is unassigned.
    ''' </param>
    Private Shared Sub assignSingleInstance(taskInstance As TaskInstance, newAssignee As IntelligenceUser, assigner As IntelligenceUser, timeAssigned As DateTime?)
        If taskInstance IsNot Nothing Then
            Dim unusedVariableToLoadPreviousAssigneeAndMakeUpdateWorkProperly As IntelligenceUser = taskInstance.assignee
            taskInstance.assignee = newAssignee
            taskInstance.assigner = If(newAssignee Is Nothing, Nothing, assigner)
            taskInstance.timeAssigned = If(newAssignee Is Nothing, Nothing, timeAssigned)
        End If
    End Sub
#End Region

#Region "Priority Wrapped"
    '#######################################################
    '#                Priority Wrapped                     #
    '#######################################################

    ''' <summary>
    ''' Starting from the given wrapper, set the priority for this task and all its subtasks,
    ''' and then update this wrapper's ancestors to reflect this change.
    ''' An ancestor's priority will become the highest priority of all its children.
    ''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
    ''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper from to which set the priority for this task and all its subtasks.</param>
    ''' <param name="priority">The new priority to set for this task and all its subtasks.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Shared Sub setPriorityForInstance(taskInstance As TaskInstanceWrapper, priority As Priority, db As IntelligenceDB)
        If setPriorityIncludingChildren(taskInstance, priority, db) Then
            updatePriorityFromChildren(taskInstance.parent, db)
        End If
    End Sub

    ''' <summary>Starting from the given wrapper, set the priority for this task and all its subtasks.</summary>
    ''' <param name="taskInstance">The wrapper from to which set the priority for this task and all its subtasks.</param>
    ''' <param name="priority">The new priority to set for this task and all its subtasks.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setPriorityIncludingChildren(taskInstance As TaskInstanceWrapper, priority As Priority, db As IntelligenceDB) As Boolean
        Dim wasSet As Boolean = setPriority(taskInstance, priority, db)
        For Each subTask As TaskInstanceWrapper In taskInstance.subTasks
            setPriorityIncludingChildren(subTask, priority, db)
        Next
        Return wasSet
    End Function

    ''' <summary>
    ''' Call this when one of the given task's children has changed its priority.
    ''' This task's priority will become the highest priority of all its children.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper whose child has changed priority.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub updatePriorityFromChildren(taskInstance As TaskInstanceWrapper, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            Dim highestPriority As Priority = PriorityUtils.maxPriorityOfSubtasks(taskInstance, db)
            If setPriority(taskInstance, highestPriority, db) Then
                updatePriorityFromChildren(taskInstance.parent, db)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Set the priority for only the given wrapper without altering the rest of the tree.
    ''' Uses <see cref="TaskInstanceWrapper.setPriorityWithoutAlteringTree"/> for this.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper to the priority for.</param>
    ''' <param name="priority">The new priority to set.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setPriority(taskInstance As TaskInstanceWrapper, priority As Priority, db As IntelligenceDB) As Boolean
        If taskInstance IsNot Nothing AndAlso Not taskInstance.priority.id = priority.id Then
            taskInstance.setPriorityWithoutAlteringTree(priority, db)
            Return True
        End If
        Return False
    End Function


#End Region

#Region "Priority Unwrapped"
    '#######################################################
    '#                  Priority Unwrapped                 #
    '#######################################################

    ''' <summary>
    ''' Starting from the given instance, set the priority for this task and all its subtasks,
    ''' and then update this instance's ancestors to reflect this change.
    ''' An ancestor's priority will become the highest priority of all its children.
    ''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
    ''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
    ''' </summary>
    ''' <param name="taskInstance">The instance from to which set the priority for this task and all its subtasks.</param>
    ''' <param name="priority">The new priority to set for this task and all its subtasks.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Shared Sub setPriorityForInstance(taskInstance As TaskInstance, priority As Priority, db As IntelligenceDB)
        setPriorityIncludingChildren(taskInstance, priority, db)
        updatePriorityFromChildren(getParentInstance(taskInstance), db)
    End Sub

    ''' <summary>Starting from the given instance, set the priority for this task and all its subtasks.</summary>
    ''' <param name="taskInstance">The instance from to which set the priority for this task and all its subtasks.</param>
    ''' <param name="priority">The new priority to set for this task and all its subtasks.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub setPriorityIncludingChildren(taskInstance As TaskInstance, priority As Priority, db As IntelligenceDB)
        setPriority(taskInstance, priority)
        For Each subTask As TaskInstance In getSubtasks(taskInstance, db)
            setPriorityIncludingChildren(subTask, priority, db)
        Next
    End Sub

    ''' <summary>
    ''' Call this when one of the given task's children has changed its priority.
    ''' This task's priority will become the highest priority of all its children.
    ''' </summary>
    ''' <param name="taskInstance">The instance whose child has changed priority.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub updatePriorityFromChildren(taskInstance As TaskInstance, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            Dim highestPriority As Priority = PriorityUtils.maxPriorityOfSubtasks(taskInstance, db)
            setPriority(taskInstance, highestPriority)
            updatePriorityFromChildren(getParentInstance(taskInstance), db)
        End If
    End Sub

    ''' <summary>
    ''' Set the priority for only the given instance without altering the rest of the tree.
    ''' </summary>
    ''' <param name="taskInstance">
    ''' The instance to the priority for. 
    ''' Must be connected to an open database context.
    ''' </param>
    ''' <param name="priority">The new priority to set.</param>
    Private Shared Sub setPriority(taskInstance As TaskInstance, priority As Priority)
        If taskInstance IsNot Nothing Then
            taskInstance.priority = priority
        End If
    End Sub

#End Region

#Region "Deadline (Wrapped Only)"
    '#######################################################
    '#              Deadline (Wrapped Only)                #
    '#######################################################

    ''' <summary>
    ''' Starting from the given wrapper, set the deadline for this task and all its subtasks,
    ''' and then update this wrapper's ancestors to reflect this change.
    ''' An ancestor's priority will become the soonest deadline of all its children.
    ''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
    ''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper from to which set the deadline for this task and all its subtasks.</param>
    ''' <param name="deadline">
    ''' The new deadline to set for this task and all its subtasks.
    ''' If this is <c>Nothing</c>, then the tasks will have no deadline.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Public Shared Sub setDeadlineForInstance(taskInstance As TaskInstanceWrapper, deadline As DateTime?, db As IntelligenceDB)
        If setDeadlineIncludingChildren(taskInstance, deadline, db) Then
            updateDeadlineFromChildren(taskInstance.parent, db)
        End If
    End Sub

    ''' <summary>Starting from the given wrapper, set the deadline for this task and all its subtasks.</summary>
    ''' <param name="taskInstance">The wrapper from to which set the deadline for this task and all its subtasks.</param>
    ''' <param name="deadline">
    ''' The new deadline to set for this task and all its subtasks.
    ''' If this is <c>Nothing</c>, then the tasks will have no deadline.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setDeadlineIncludingChildren(taskInstance As TaskInstanceWrapper, deadline As DateTime?, db As IntelligenceDB) As Boolean
        If setDeadline(taskInstance, deadline, db) Then
            For Each subTask As TaskInstanceWrapper In taskInstance.subTasks
                setDeadlineIncludingChildren(subTask, deadline, db)
            Next
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Call this when one of the given task's children has changed its deadline.
    ''' This task's deadline will become the soonest deadline of all its children.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper whose child has changed deadline.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub updateDeadlineFromChildren(taskInstance As TaskInstanceWrapper, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            Dim soonestDeadline As DateTime? = soonestDeadlineOfChildren(taskInstance)
            If setDeadline(taskInstance, soonestDeadline, db) Then
                updateDeadlineFromChildren(taskInstance.parent, db)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Set the deadline for only the given wrapper without altering the rest of the tree.
    ''' Uses <see cref="TaskInstanceWrapper.setDeadlineWithoutAlteringTree"/> for this.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper to the priority for.</param>
    ''' <param name="deadline">
    ''' The new deadline to set for this task and all its subtasks.
    ''' If this is <c>Nothing</c>, then the tasks will have no deadline.
    ''' </param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setDeadline(taskInstance As TaskInstanceWrapper, deadline As DateTime?, db As IntelligenceDB) As Boolean
        If taskInstance IsNot Nothing AndAlso Not taskInstance.deadline.Equals(deadline) Then
            taskInstance.setDeadlineWithoutAlteringTree(deadline, db)
            Return True
        End If
        Return False
    End Function

    ''' <summary>Calculate the soonest deadline of a given wrapper's subtasks.</summary>
    ''' <param name="taskInstance">The wrapper whose subtasks should be checked.</param>
    ''' <returns>
    ''' The soonest deadline in the given wrapper's subtasks 
    ''' (or <c>Nothing</c> if none of the subtasks has a deadline set).
    ''' </returns>
    Private Shared Function soonestDeadlineOfChildren(taskInstance As TaskInstanceWrapper) As DateTime?
        Dim nonNullDeadlines As ICollection(Of DateTime?) =
            (From child As TaskInstanceWrapper In taskInstance.subTasks
            Where child.deadline IsNot Nothing
            Select child.deadline).ToList()
        If Not nonNullDeadlines.Any Then Return Nothing
        Return nonNullDeadlines.Min()
    End Function
#End Region

#Region "Resolution Wrapped"
	'#######################################################
	'#               Resolution Wrapped                    #
	'#######################################################

	''' <summary>
	''' Set the resolution for a given leaf node, and update this wrapper's ancestors to reflect this change.
	''' 
	''' A parent will be "open" if its children are all "open".
	''' It will be set to"finished subtasks" if its subtasks are all closed (i.e. not "open" or "in-progress").
	''' Otherwise a parent will be "in-progress".
	''' 
	''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
	''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
	''' </summary>
	''' <param name="taskInstance">The leaf node wrapper to set the resolution for.</param>
	''' <param name="resolution">The new resolution to set for this task.</param>
	''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
	''' <param name="db">The open database context to make the changes in.</param>
	Public Shared Sub setResolutionForInstance(taskInstance As TaskInstanceWrapper, resolution As Resolution, now As DateTime?, db As IntelligenceDB)
		If setResolutionForInstanceAndSubtasks(taskInstance, resolution, now, db) Then
			refreshParentResolutionFromChildren(taskInstance, resolution, now, db)
		End If
	End Sub

	Private Shared Function setResolutionForInstanceAndSubtasks(taskInstance As TaskInstanceWrapper, resolution As Resolution, now As DateTime?, db As IntelligenceDB) As Boolean
		If taskInstance IsNot Nothing Then
			If setResolutionForSingleInstance(taskInstance, resolution, now, db) Then
				For Each subTask In taskInstance.subTasks
					setResolutionForInstanceAndSubtasks(subTask, resolution, now, db)
				Next
				Return True
			End If
		End If
		Return False
	End Function

    ''' <summary>
    ''' Called when a wrapper changes resolution, and its ancestors need updated.
    ''' If the wrapper has a cached parent, it will call refreshResolutionFromChildren with a wrapper.
    ''' If there is no cached parent, it will use the unwrapped version to search for a parent and update it.
    ''' 
    ''' The parent will be set to "open" if its children are all "open".
    ''' The parent will be set to "finished subtasks" if its subtasks are all closed (i.e. not "open" or "in-progress").
    ''' Otherwise a parent will be "in-progress".
    ''' </summary>
    ''' <param name="taskWrapper">The wrapper whose child has changed resolution.</param>
    ''' <param name="resolution">The new resolution of the child node.</param>
    ''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshParentResolutionFromChildren(taskWrapper As TaskInstanceWrapper, resolution As Resolution, now As DateTime?, db As IntelligenceDB)
        If taskWrapper IsNot Nothing Then
            If taskWrapper.parent IsNot Nothing Then
                refreshResolutionFromChildren(taskWrapper.parent, resolution, now, db)
            Else
                Dim parentInstance As TaskInstance = getParentInstance(taskWrapper.getWrappedItem)
                refreshResolutionFromChildren(parentInstance, resolution, now, db)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Called when a one of a wrapper's children changes resolution.
    ''' 
    ''' It will be set to "open" if its children are all "open".
    ''' It will be set to"finished subtasks" if its subtasks are all closed (i.e. not "open" or "in-progress").
    ''' Otherwise a parent will be "in-progress".
    ''' </summary>
    ''' <param name="taskInstance">The wrapper whose child has changed resolution.</param>
    ''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshResolutionFromChildren(taskInstance As TaskInstanceWrapper, newChildResolution As Resolution, now As DateTime?, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            Dim open As Resolution = ResolutionUtils.openResolution
            Dim inProgress As Resolution = ResolutionUtils.inProgressResolution
            Dim resToSet As Resolution
            If newChildResolution.Equals(inProgress) Then
                resToSet = inProgress
            ElseIf newChildResolution.Equals(open) Then
                If allChildrenHaveResolution(taskInstance, open) Then
                    resToSet = open
                Else
                    resToSet = inProgress
                End If
            Else
                If allChildrenAreClosed(taskInstance) Then
                    resToSet = ResolutionUtils.finSubtasksResolution
                Else
                    resToSet = inProgress
                End If
            End If
            resToSet = db.Resolutions.Find(resToSet.id)
            setResolutionForSingleInstance(taskInstance, resToSet, now, db)
            refreshParentResolutionFromChildren(taskInstance, resToSet, now, db)
        End If
    End Sub

    ''' <summary>
    ''' Set the resolution for only the given wrapper without altering the rest of the tree.
    ''' Uses <see cref="TaskInstanceWrapper.setResolutionWithoutAlteringTree"/> for this.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper to the set resolution for.</param>
    ''' <param name="resolution">The new resolution to set.</param>
    ''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setResolutionForSingleInstance(taskInstance As TaskInstanceWrapper, resolution As Resolution, now As DateTime?, db As IntelligenceDB) As Boolean
        If resolution.id <> taskInstance.resolution.id Then
            Dim finishTime As DateTime? = If(ResolutionUtils.isClosed(resolution), now, Nothing)
            taskInstance.setFinishedTime(finishTime, db)
            taskInstance.setResolutionWithoutAlteringTree(resolution, db)
            Return True
        End If
        Return False
    End Function

    ''' <summary>Checks whether all the given wrapper's subtasks are closed (i.e. not "open" or "in-progress").</summary>
    ''' <param name="taskInstance">The wrapper whos subtasks to check.</param>
    ''' <returns><c>True</c> if all the wrapper's subtask are closed (i.e. not "open" or "in-progress").</returns>
    Private Shared Function allChildrenAreClosed(taskInstance As TaskInstanceWrapper)
        Return taskInstance.subTasks.All(Function(child As TaskInstanceWrapper) ResolutionUtils.isClosed(child.resolution))
    End Function

    ''' <summary>Checks whether all the given wrapper's subtasks have the specified resolution.</summary>
    ''' <param name="taskInstance">The wrapper whos subtasks to check.</param>
    ''' <returns><c>True</c> if all the wrapper's subtask have the specified resolution.</returns>
    Private Shared Function allChildrenHaveResolution(taskInstance As TaskInstanceWrapper, res As Resolution)
        Return taskInstance.subTasks.All(Function(child As TaskInstanceWrapper) child.resolution.Equals(res))
    End Function
#End Region

#Region "Resolution Unwrapped"
	'#######################################################
	'#              Resolution Unwrapped                   #
	'#######################################################

	''' <summary>
	''' Set the resolution for a given leaf node, and update this instance's ancestors to reflect this change.
	''' 
	''' A parent will be "open" if its children are all "open".
	''' It will be set to"finished subtasks" if its subtasks are all closed (i.e. not "open" or "in-progress").
	''' Otherwise a parent will be "in-progress".
	''' 
	''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
	''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
	''' </summary>
	''' <param name="taskInstance">The leaf node instance to set the resolution for.</param>
	''' <param name="resolution">The new resolution to set for this task.</param>
	''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
	''' <param name="db">The open database context to make the changes in.</param>
	Public Shared Sub setResolutionForInstance(taskInstance As TaskInstance, resolution As Resolution, now As DateTime?, db As IntelligenceDB)
		setResolutionForInstanceAndSubtasks(taskInstance, resolution, now, db)
		refreshResolutionFromChildren(getParentInstance(taskInstance), resolution, now, db)
	End Sub

	Private Shared Sub setResolutionForInstanceAndSubtasks(taskInstance As TaskInstance, resolution As Resolution, now As DateTime?, db As IntelligenceDB)
		If taskInstance IsNot Nothing Then
			setResolutionForSingleInstance(taskInstance, resolution, now)
			For Each subTask As TaskInstance In getSubtasks(taskInstance, db)
				setResolutionForInstanceAndSubtasks(taskInstance, resolution, now, db)
			Next
		End If
	End Sub

    ''' <summary>
    ''' Called when a one of an instance's subtasks changes resolution.
    ''' 
    ''' It will be set to "open" if its children are all "open".
    ''' It will be set to"finished subtasks" if its subtasks are all closed (i.e. not "open" or "in-progress").
    ''' Otherwise a parent will be "in-progress".
    ''' </summary>
    ''' <param name="taskInstance">The instance whose child has changed resolution.</param>
    ''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshResolutionFromChildren(taskInstance As TaskInstance, newChildResolution As Resolution, now As DateTime?, db As IntelligenceDB)
        If taskInstance IsNot Nothing Then
            Dim open As Resolution = ResolutionUtils.openResolution
            Dim inProgress As Resolution = ResolutionUtils.inProgressResolution
            Dim resToSet As Resolution
            If newChildResolution.Equals(inProgress) Then
                resToSet = inProgress
            ElseIf newChildResolution.Equals(open) Then
                If allChildrenHaveResolution(taskInstance, open, db) Then
                    resToSet = open
                Else
                    resToSet = inProgress
                End If
            Else
                If allChildrenAreClosed(taskInstance, db) Then
                    resToSet = ResolutionUtils.finSubtasksResolution
                Else
                    resToSet = inProgress
                End If
            End If
            resToSet = db.Resolutions.Find(resToSet.id)
            setResolutionForSingleInstance(taskInstance, resToSet, now)
            refreshResolutionFromChildren(getParentInstance(taskInstance), resToSet, now, db)
        End If
    End Sub

    ''' <summary> Set the resolution for only the given instance without altering the rest of the tree. </summary>
    ''' <param name="taskInstance">The instance to set the resolution for.</param>
    ''' <param name="resolution">The new resolution to set.</param>
    ''' <param name="now">The date and time the resolution was changed. Ensures batch changes have the same timestamp.</param>
    Private Shared Sub setResolutionForSingleInstance(taskInstance As TaskInstance, resolution As Resolution, now As DateTime?)
        If taskInstance IsNot Nothing Then
            taskInstance.timeFinished = If(ResolutionUtils.isClosed(resolution), now, Nothing)
            taskInstance.resolution = resolution
        End If
    End Sub

    ''' <summary>Checks whether all the given instance's subtasks are closed (i.e. not "open" or "in-progress").</summary>
    ''' <param name="taskInstance">The instance whos subtasks to check.</param>
    ''' <returns><c>True</c> if all the instance's subtask are closed (i.e. not "open" or "in-progress").</returns>
    Private Shared Function allChildrenAreClosed(taskInstance As TaskInstance, db As IntelligenceDB)
        Return getSubtasks(taskInstance, db).All(Function(child) ResolutionUtils.isClosed(child.resolution))
    End Function

    ''' <summary>Checks whether all the given instance's subtasks have the specified resolution.</summary>
    ''' <param name="taskInstance">The instance whos subtasks to check.</param>
    ''' <returns><c>True</c> if all the instance's subtask have the specified resolution.</returns>
    Private Shared Function allChildrenHaveResolution(taskInstance As TaskInstance, res As Resolution, db As IntelligenceDB)
        Return getSubtasks(taskInstance, db).All(Function(child) child.resolution.id = res.id)
    End Function

    'Defaults

    ''' <summary>Get the default priority for a newly created <see cref="TaskInstance"/>.</summary>
    ''' <returns>Medium priority.</returns>
    Public Shared Function getDefaultPriorityForNewTaskInstance() As Priority
        Return PriorityUtils.mediumPriority
    End Function

    ''' <summary>Get the default resolution for a newly created <see cref="TaskInstance"/>.</summary>
    ''' <returns>The "open" resolution.</returns>
    Public Shared Function getDefaultResolutionForNewTaskInstance() As Resolution
        Return ResolutionUtils.openResolution
    End Function

    ''' <summary>
    ''' Group the following list of tasks by their resolution.
    ''' Returns an anonymous type with "resolution", "Count" field,
    ''' and a "grp" field containing the instances in that group.
    ''' </summary>
    ''' <param name="tasks">The list of tasks to split into groups.</param>
    ''' <returns>
    ''' An anonymous type with "resolution", "Count" field,
    ''' and a "grp" field containing the instances in that group.
    ''' </returns>
    Public Shared Function groupByResolution(tasks As ICollection(Of TaskInstance))
        Return (From task As TaskInstance In tasks
                Group By resolution = task.resolution
                Into grp = Group, Count()).OrderBy(Function(t) t.resolution.id).ToList()
    End Function
#End Region

#Region "Start Time Wrapped"
	'#######################################################
	'#                Start Time Wrapped                   #
	'#######################################################

	''' <summary>
	''' Set the start time for a given leaf node, and update this wrapper's ancestors to reflect this change.
	''' A parent's start time will be the earliest start time of all its children 
	''' (or <c>Nothing</c> if none of the tasks are started).
	''' 
	''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
	''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
	''' </summary>
	''' <param name="taskInstance">The leaf node wrapper to set the start time for.</param>
	''' <param name="startTime">The new start time to set for this task.</param>
	''' <param name="db">The open database context to make the changes in.</param>
	Public Shared Sub setStartTimeForInstance(taskInstance As TaskInstanceWrapper, startTime As DateTime?, db As IntelligenceDB)
		If taskInstance IsNot Nothing Then
			If setStartTimeForInstanceAndSubtasks(taskInstance, startTime, db) Then
				setStartTimeForParentFromChildren(taskInstance, startTime, db)
			End If
		End If
	End Sub

	Private Shared Function setStartTimeForInstanceAndSubtasks(taskInstance As TaskInstanceWrapper, startTime As DateTime?, db As IntelligenceDB) As Boolean
		If taskInstance IsNot Nothing Then
			If setStartTimeForSingleInstance(taskInstance, startTime, db) Then
				For Each subTask In taskInstance.subTasks
					setStartTimeForInstanceAndSubtasks(subTask, startTime, db)
				Next
				Return True
			End If
		End If
		Return False
	End Function

    ''' <summary>
    ''' Called when a wrapper changes its start time, and its ancestors need updated.
    ''' If the wrapper has a cached parent, it will call refreshStartTimeFromChildren with a wrapper.
    ''' If there is no cached parent, it will use the unwrapped version to search for a parent and update it.
    ''' 
    ''' A parent's start time will be the earliest start time of all its children 
    ''' (or <c>Nothing</c> if none of the tasks are started).
    ''' </summary>
    ''' <param name="taskWrapper">The wrapper whose child has changed its start time.</param>
    ''' <param name="startTime">The new start time of the child node.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub setStartTimeForParentFromChildren(taskWrapper As TaskInstanceWrapper, startTime As DateTime?, db As IntelligenceDB)
        If taskWrapper IsNot Nothing Then
            If taskWrapper.parent IsNot Nothing Then
                refreshStartTimeFromChildren(taskWrapper.parent, startTime, db)
            Else
                Dim parent As TaskInstance = getParentInstance(taskWrapper.getWrappedItem)
                refreshStartTimeFromChildren(parent, startTime, db)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Called when a one of a wrapper's children changes start time.
    ''' 
    ''' A parent's start time will be the earliest start time of all its children 
    ''' (or <c>Nothing</c> if none of the tasks are started).
    ''' </summary>
    ''' <param name="taskInstance">The wrapper whose child has changed its start time.</param>
    ''' <param name="startTime">The new start time of the child node.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshStartTimeFromChildren(taskInstance As TaskInstanceWrapper, startTime As DateTime?, db As IntelligenceDB)
		If taskInstance IsNot Nothing Then
			Dim newTime As DateTime? = Nothing
			If startTime Is Nothing AndAlso taskInstance.timeStarted Is Nothing Then Return
			If startTime IsNot Nothing AndAlso taskInstance.timeStarted IsNot Nothing AndAlso taskInstance.timeStarted <= startTime Then Return

			startTime = earliestStartTimeFromSubtasks(taskInstance, db)
			If setStartTimeForSingleInstance(taskInstance, startTime, db) Then
				setStartTimeForParentFromChildren(taskInstance, startTime, db)
			End If
		End If
	End Sub

	Public Shared Function earliestStartTimeFromSubtasks(taskInstance As TaskInstanceWrapper, db As IntelligenceDB) As DateTime?
		Dim minStartTime As DateTime? =
			(From child In TaskInstanceUtils.getSubtasks(taskInstance.getWrappedItem, db)
			 Select child.timeStarted).DefaultIfEmpty(Nothing).Min()
		Return minStartTime
	End Function

    ''' <summary>
    ''' Set the resolution for only the given wrapper without altering the rest of the tree.
    ''' Uses <see cref="TaskInstanceWrapper.setResolutionWithoutAlteringTree"/> for this.
    ''' </summary>
    ''' <param name="taskInstance">The wrapper to set the start time for.</param>
    ''' <param name="startTime">The new start time to set for this task.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setStartTimeForSingleInstance(taskInstance As TaskInstanceWrapper, startTime As DateTime?, db As IntelligenceDB) As Boolean
		If taskInstance Is Nothing Then Return False
		If startTime Is Nothing AndAlso taskInstance.timeStarted Is Nothing Then Return False
		If startTime IsNot Nothing AndAlso taskInstance.timeStarted IsNot Nothing AndAlso taskInstance.timeStarted <= startTime Then Return False
		taskInstance.setStartTimeWithoutModifyingTree(startTime, db)
		Return True
	End Function

#End Region

#Region "Start Time Unwrapped"
	'#######################################################
	'#               Start Time Unwrapped                  #
	'#######################################################

	''' <summary>
	''' Set the start time for a given leaf node, and update this instance's ancestors to reflect this change.
	''' A parent's start time will be the earliest start time of all its children 
	''' (or <c>Nothing</c> if none of the tasks are started).
	''' 
	''' Changes are not saved to the database in this method, so either <see cref="Wrapper(Of T).saveChanges"/>
	''' or <see cref="DBManager.saveChanges"/> will need to be called before these changes are written to the database.
	''' </summary>
	''' <param name="taskInstance">The leaf node instance to set the start time for.</param>
	''' <param name="startTime">The new start time to set for this task.</param>
	''' <param name="db">The open database context to make the changes in.</param>
	Public Shared Sub setStartTimeForInstance(taskInstance As TaskInstance, startTime As DateTime?, db As IntelligenceDB)
		If setStartTimeForInstanceAndSubtasks(taskInstance, startTime, db) Then
			refreshStartTimeFromChildren(getParentInstance(taskInstance), startTime, db)
		End If
	End Sub

	Private Shared Function setStartTimeForInstanceAndSubtasks(taskInstance As TaskInstance, startTime As DateTime?, db As IntelligenceDB) As Boolean
		If taskInstance IsNot Nothing Then
			If setStartTimeForSingleInstance(taskInstance, startTime, db) Then
				For Each subTask In getSubtasks(taskInstance)
					setStartTimeForInstanceAndSubtasks(subTask, startTime, db)
				Next
				Return True
			End If
		End If
		Return False
	End Function

    ''' <summary>
    ''' Called when a one of a instance's children changes start time.
    ''' 
    ''' A parent's start time will be the earliest start time of all its children 
    ''' (or <c>Nothing</c> if none of the tasks are started).
    ''' </summary>
    ''' <param name="taskInstance">The instance whose child has changed its start time.</param>
    ''' <param name="startTime">The new start time of the child node.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Sub refreshStartTimeFromChildren(taskInstance As TaskInstance, startTime As DateTime?, db As IntelligenceDB)
		If taskInstance IsNot Nothing Then
			Dim newTime As DateTime? = Nothing
			If startTime Is Nothing AndAlso taskInstance.timeStarted Is Nothing Then Return
			If startTime IsNot Nothing AndAlso taskInstance.timeStarted IsNot Nothing AndAlso taskInstance.timeStarted <= startTime Then Return

			startTime = earliestStartTimeFromSubtasks(taskInstance, db)
			If setStartTimeForSingleInstance(taskInstance, startTime, db) Then
				refreshStartTimeFromChildren(getParentInstance(taskInstance), startTime, db)
			End If
		End If
	End Sub

	Public Shared Function earliestStartTimeFromSubtasks(taskInstance As TaskInstance, db As IntelligenceDB) As DateTime?
		Dim minStartTime As DateTime? =
			(From child In TaskInstanceUtils.getSubtasks(taskInstance, db)
			 Select child.timeStarted).DefaultIfEmpty(Nothing).Min()
		Return minStartTime
	End Function

    ''' <summary>Set the resolution for only the given instance without altering the rest of the tree.</summary>
    ''' <param name="taskInstance">The instance to set the start time for.</param>
    ''' <param name="startTime">The new start time to set for this task.</param>
    ''' <param name="db">The open database context to make the changes in.</param>
    Private Shared Function setStartTimeForSingleInstance(taskInstance As TaskInstance, startTime As DateTime?, db As IntelligenceDB) As Boolean
		If taskInstance Is Nothing Then Return False
		If startTime Is Nothing AndAlso taskInstance.timeStarted Is Nothing Then Return False
		If startTime IsNot Nothing AndAlso taskInstance.timeStarted IsNot Nothing AndAlso taskInstance.timeStarted <= startTime Then Return False
		taskInstance.timeStarted = startTime
		Return True
	End Function
#End Region

#Region "Tree Navigation"
    '#######################################################
    '#                 Tree Navigation                     #
    '#######################################################

    ''' <summary>
    ''' Get all the subtasks of the given instance.
    ''' The instance will first be searched for in the database to get a recent copy.
    ''' </summary>
    ''' <param name="taskInstance">
    ''' The instance whos subtasks need returned.
    ''' Does not need to be connected to the given databse context.
    ''' </param>
    ''' <param name="db">The open database context to search for subtasks in.</param>
    ''' <returns>The list of all the task's subtasks (which will be empty or leaf nodes).</returns>
    Public Shared Function getSubtasks(taskInstance As TaskInstance, db As IntelligenceDB) As ICollection(Of TaskInstance)
        taskInstance = db.Set(Of TaskInstance).Find(taskInstance.id)
        Return (From subTask As TaskInstance In db.TaskInstances
               Where taskInstance.version.id = subTask.version.id _
               AndAlso subTask.platform.id = taskInstance.platform.id _
               AndAlso subTask.taskTemplate.parentTask IsNot Nothing _
               AndAlso subTask.taskTemplate.parentTask.id = taskInstance.taskTemplate.id
               Select subTask).ToList()
    End Function

    ''' <summary>
    ''' Get all the subtasks of the given instance.
    ''' The instance must be connected to an open database context, or this will crash.
    ''' </summary>
    ''' <param name="taskInstance">
    ''' The instance whos subtasks need returned.
    ''' The instance must be connected to an open database context, or this will crash.
    ''' </param>
    ''' <returns>The list of all the task's subtasks (which will be empty for leaf nodes).</returns>
    Public Shared Function getSubtasks(taskInstance As TaskInstance) As IEnumerable(Of TaskInstance)
        Return From subTask As TaskInstance In taskInstance.version.taskInstances
               Where subTask.platform.id = taskInstance.platform.id _
               AndAlso subTask.taskTemplate.parentTask IsNot Nothing _
               AndAlso subTask.taskTemplate.parentTask.id = taskInstance.taskTemplate.id
               Select subTask
    End Function

    ''' <summary> Add a clause to the given query that will cause it to return only leaf nodes.</summary>
    ''' <param name="tasksToFilter">The query to add the "leaf nodes only" clause to.</param>
    ''' <param name="db">The database context the query is for.</param>
    ''' <returns>A new query which narrows the results of the existing query to return only leaf nodes.</returns>
    Public Shared Function filterLeafNodes(tasksToFilter As IQueryable(Of TaskInstance), db As IntelligenceDB) As IQueryable(Of TaskInstance)
        Dim query As IQueryable(Of TaskInstance) =
            From leaf In tasksToFilter
            Where Not (From subTask As TaskInstance In db.TaskInstances
                       Where subTask.version.id = leaf.version.id _
                       AndAlso subTask.platform.id = leaf.platform.id _
                       AndAlso subTask.taskTemplate.parentTask.id = leaf.taskTemplate.id).Any()
            Select leaf
        Return query
    End Function

    ''' <summary>Get the parent of a task instance that is connected to an open dbcontext.</summary>
    ''' <param name="taskInstance">
    ''' The task instance whos parent needs found.
    ''' Must be conected to an open database context.
    ''' </param>
    ''' <returns>The parent instance of the task (or Nothing if it is a root node).</returns>
    Public Shared Function getParentInstance(taskInstance As TaskInstance) As TaskInstance
        If taskInstance Is Nothing Then Return Nothing
        If taskInstance.taskTemplate.parentTask Is Nothing Then
            Return Nothing
        End If

        Return (From parent In taskInstance.version.taskInstances
                   Where parent.taskTemplate.id = taskInstance.taskTemplate.parentTask.id _
                   AndAlso parent.platform.id = taskInstance.platform.id
                   Select parent).FirstOrDefault()
        Return Nothing
    End Function

    ''' <summary>
    ''' Whether the given task instance is a leaf node (the lowest level task that 
    ''' the user must complete, rather than a group of tasks).
    ''' </summary>
    ''' <param name="taskInstance">The task instance to check.</param>
    ''' <returns><c>True</c> if the task has no subtasks.</returns>
    Public Shared Function isLeafNode(taskInstance As TaskInstance) As Boolean
        Return Not getSubtasks(taskInstance).Any()
    End Function

    ''' <summary>Whether <paramref name="child"/> is a descenant of <paramref name="ancestor"/>.</summary>
    ''' <param name="child">The lower level node to check the ancestors of.</param>
    ''' <param name="ancestor">The higher level node to see if it is an ancestor.</param>
    ''' <returns><c>True</c> if <paramref name="child"/> is a descenant of <paramref name="ancestor"/>.</returns>
    Public Shared Function isAncestorOf(child As TaskInstance, ancestor As TaskInstance)
        Return ancestor IsNot Nothing AndAlso child IsNot Nothing _
            AndAlso child.version.id = ancestor.version.id _
            AndAlso child.platform.id = ancestor.platform.id _
            AndAlso isAncestorOf(child.taskTemplate, ancestor.taskTemplate)
    End Function

    ''' <summary>Whether <paramref name="child"/> is a descenant of <paramref name="ancestor"/>.</summary>
    ''' <param name="child">The lower level node to check the ancestors of.</param>
    ''' <param name="ancestor">The higher level node to see if it is an ancestor.</param>
    ''' <returns><c>True</c> if <paramref name="child"/> is a descenant of <paramref name="ancestor"/>.</returns>
    Public Shared Function isAncestorOf(child As TaskTemplate, ancestor As TaskTemplate)
        Return child.parentTask IsNot Nothing _
            AndAlso (child.parentTask.id = ancestor.id OrElse isAncestorOf(child.parentTask, ancestor))
    End Function

    ''' <summary>
    ''' Get the list of all leaf nodes descended from <paramref name="taskInstance"/>.
    ''' If <paramref name="taskInstance"/> is a leaf node itself, a list single element list containing
    ''' <paramref name="taskInstance"/> will be returned.
    ''' </summary>
    ''' <param name="taskInstance">The task whose leaf node descendants to return.</param>
    ''' <param name="db">The open database context to search for tasks in.</param>
    Public Shared Function getLeafNodes(taskInstance As TaskInstance, db As IntelligenceDB) As ICollection(Of TaskInstance)
        If isLeafNode(taskInstance) Then
            Return {taskInstance}
        Else
            Return (From leafNode As TaskInstance In taskInstance.version.taskInstances
                   Where isLeafNode(leafNode) AndAlso isAncestorOf(leafNode, taskInstance)
                   Select leafNode).ToList()
        End If
    End Function

#End Region

End Class
