﻿''' <summary>
''' A base class for objects that load things in a background thread.
''' Provides ways for updating listeners of the task's progress as it changes,
''' as well as giving contextual error messages to describe which stage
''' was reached before the error occurred. Can be used with
''' <see cref="LoadWithAsyncGUITask"/> to provide most of these features automatically.
''' </summary>
Public MustInherit Class Loader

#Region "Progress Updates"
    '#########################################################
    '#                    Progress Updates                   #
    '#########################################################

    ''' <summary>
    '''A description of what the loader is now doing, which can be displayed to the user.
    ''' Is updated whenever <see cref="updateListeners"/> is called, and
    ''' will also be shown in <see cref="ErrorDialog"/>s if an exception is thrown.
    ''' </summary>
    Public Property currentLoadingStageDescription As String = ""

    ''' <summary>
    ''' An interface for anything that can respond to changes in the loader's
    ''' progress (e.g. a loading dialog to display the updated progress text).
    ''' </summary>
    Public Interface LoadingProgressListener
        ''' <summary>Respond to the loader's progress having changed by displaying the given description text.</summary>
        ''' <param name="desc">Text describing what the current step in the loading process is.</param>
        Sub onProgressUpdated(desc As String)
    End Interface

    ''' <summary>A lock to prevent mulitiple threads from modifying <see cref="loadingListeners"/> at once.</summary>
    Private listenerLock As New Object

    ''' <summary>Internal storage for everything that needs notified when the loader's progress changes.</summary>
    Private _loadingListeners As New List(Of LoadingProgressListener)
    ''' <summary>Everything that needs notified when the loader's progress changes.</summary>
    Private ReadOnly Property loadingListeners
        Get
            Return _loadingListeners
        End Get
    End Property

    ''' <summary>Add the given listener to the list of all objects to notify when the loader's progress changes.</summary>
    ''' <param name="listener">The new listener to respond to this loader's progress updates.</param>
    Sub addListener(listener As LoadingProgressListener)
        SyncLock listenerLock
            loadingListeners.Add(listener)
        End SyncLock
    End Sub

    ''' <summary>
    ''' Call this when the given listener needs to stop responding to
    ''' updates in this loader's progress (e.g. if it is about to be closed or destroyed).
    ''' </summary>
    ''' <param name="listener">The listener to unregister as a responder for progress updates.</param>
    Sub removeListener(listener As LoadingProgressListener)
        SyncLock listenerLock
            loadingListeners.Remove(listener)
        End SyncLock
    End Sub

    ''' <summary>Notify all listeners that this loader's progress has changed.</summary>
    ''' <param name="desc">A description of what the loader is now doing, which can be displayed to the user.</param>
    Sub updateListeners(desc As String)
        currentLoadingStageDescription = desc
        SyncLock listenerLock
            For Each listener In loadingListeners
                listener.onProgressUpdated(desc)
            Next
        End SyncLock
    End Sub
#End Region

#Region "Error Messages"
    '#########################################################
    '#                     Error Messages                    #
    '#########################################################

    ''' <summary>
    ''' The topmost title to be shown in an <see cref="ErrorDialog"/>
    ''' if the loader throws an exception during its current phase.
    ''' </summary>
    Public Property currentErrorTitle As String = "Error"

    ''' <summary>The internally stored header for an <see cref="ErrorDialog"/> if the loader throws an exception.</summary>
    Private _currentErrorHeader As String = Nothing

    ''' <summary>
    ''' The header to be shown in an <see cref="ErrorDialog"/>
    ''' if the loader throws an exception during its current phase.
    ''' This will be automatically generated from <see cref="currentLoadingStageDescription"/>,
    ''' and will appear below <see cref="currentErrorTitle"/>.
    ''' </summary>
    Public Property currentErrorHeader As String
        Get
            If Not _currentErrorHeader Is Nothing Then
                Return _currentErrorHeader
            ElseIf Not currentLoadingStageDescription Is Nothing AndAlso currentLoadingStageDescription.Length > 1 Then
                Dim firstChar As String = currentLoadingStageDescription.First
                firstChar.ToLower()
                Dim dot As Char = "."
                Return "Error " & firstChar.ToLower() & currentLoadingStageDescription.Substring(1).TrimEnd({dot}) & ":"
            Else
                Return "Error"
            End If
        End Get
        Set(value As String)
            _currentErrorHeader = value
        End Set
    End Property
#End Region

End Class
