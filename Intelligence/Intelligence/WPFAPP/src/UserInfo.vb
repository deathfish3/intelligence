﻿''' <summary>
''' Stores all a users login credentials for Intelligence, Zendesk and Mantis.
''' The currently logged in user's details can be accessed via the 
''' global <see cref="UserInfo.user"/> field.
''' </summary>
Public Class UserInfo

    ''' <summary>The user's Mantis username.</summary>
    Public Property mantisUsername As String
    ''' <summary>The user's plaintext Mantis password.</summary>
    Public Property mantisPassword As String

    ''' <summary>The user's plaintext Mantis password.</summary>s
    Public Property zendeskUsername As String
    ''' <summary>The user's plaintext Zendesk password.</summary>
    Public Property zendeskPassword As String

    ''' <summary>sThe email address of the current user's intelligence account.</summary>
    Public Property intelligenceEmail As String

    Public Shared user As UserInfo = New UserInfo With {
        .zendeskUsername = "qa@yoyogames.com",
        .zendeskPassword = "qapass1234",
        .intelligenceEmail = Nothing
    }

    ''' <summary>The internally cached Intelligence account details for the current user.</summary>
    Private Shared currentIntelligenceUser As IntelligenceUserWrapper = Nothing
    ''' <summary>
    ''' Get the Intelligence account details for the current user using their email address.
    ''' Will crash if there is no user for that email 
    ''' (as this would mean their account was permanantly deleted while they were logged in).
    ''' </summary>
    Public Shared Function getCurrentIntelligenceUser() As IntelligenceUserWrapper
        If currentIntelligenceUser Is Nothing AndAlso user.intelligenceEmail IsNot Nothing Then
            Using db As New IntelligenceDB
                Return New IntelligenceUserWrapper(
                   (From user In db.IntelligenceUsers
                   Where user.email.Equals(UserInfo.user.intelligenceEmail)
                   Select user).First())
            End Using
        End If
        Return currentIntelligenceUser
    End Function

End Class
