﻿Imports System.Runtime.Serialization

''' <summary>
''' Represents the JSON object returned from
''' Zendesk's user API. Most of the fields are unused, but the
''' id, email, and name will be. This class was auto-generated
''' using a converter at http://jsonutils.com/ (with minor
''' modifications to avoid collisions with VB keywords).
''' </summary>
Public Class ZendeskUser
    Public Property user As ZenUser
End Class

Public Class Thumbnail
    Public Property url As String
    Public Property id As Integer
    Public Property file_name As String
    Public Property content_url As String
    Public Property mapped_content_url As String
    Public Property content_type As String
    Public Property size As Integer
    Public Property inline As Boolean
End Class

Public Class Photo
    Public Property url As String
    Public Property id As Integer
    Public Property file_name As String
    Public Property content_url As String
    Public Property mapped_content_url As String
    Public Property content_type As String
    Public Property size As Integer
    Public Property inline As Boolean
    Public Property thumbnails As Thumbnail()
End Class

Public Class UserFields
End Class

<DataContract(Name:="User")> _
Public Class ZenUser
    Public Property id As Integer
    Public Property url As String
    Public Property name As String
    Public Property email As String
    Public Property created_at As DateTime
    Public Property updated_at As DateTime
    Public Property time_zone As String
    Public Property phone As Object
    Public Property photo As Photo
    Public Property locale_id As Integer
    Public Property locale As String
    Public Property organization_id As Integer?
    Public Property role As String
    Public Property verified As Boolean
    Public Property external_id As Integer?
    Public Property tags As Object()

    <DataMember(Name:="alias")>
    Public Property aliasName As String

    Public Property active As Boolean

    <DataMember(Name:="shared")> _
    Public Property isShared As Boolean

    Public Property shared_agent As Boolean
    Public Property last_login_at As DateTime
    Public Property signature As String
    Public Property details As String
    Public Property notes As String
    Public Property custom_role_id As Object
    Public Property moderator As Boolean
    Public Property ticket_restriction As Object
    Public Property only_private_comments As Boolean
    Public Property restricted_agent As Boolean
    Public Property suspended As Boolean
    Public Property chat_only As Boolean
    Public Property user_fields As UserFields
End Class


