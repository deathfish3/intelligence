﻿Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization.Json
Imports System.ServiceModel.Channels
Imports System.Runtime.Serialization
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Text
Imports WPFAPP.ZendeskTicket

''' <summary>
''' Loads various types of data asynchronously from Zendesk's JSON API.
''' Can download tickets, users, and the total number of open tickets for the current user.
''' When used with <see cref="LoadWithAsyncGUITask"/> it will dynamically
''' update the text in the <see cref="LoadingDialog"/> as it performs each task.
''' </summary>
''' <remarks><example><code>
''' Dim zenLoader As New ZendeskLoader
''' LoadWithAsyncGUITask.run(Of Integer)(zenLoader, Nothing, zenLoader.loadNumberOfOpenTicketsForCurrentUser(UserInfo.user), Sub(i As Integer) Debug.Print(i))
''' </code></example></remarks>
Public Class ZendeskLoader
    Inherits Loader

    ''' <summary>
    ''' The prefix of the YoYoGames Zendesk subdomain.
    ''' Urls will be of the form: https://yoyogames.zendesk.com/api/v2/tickets/id.json
    ''' </summary>
    Private zendeskSubdomain As String = "yoyogames"

#Region "Ticket"
    '###############################################
    '#                    Ticket                   #
    '###############################################

    ''' <summary>Asynchronously download and parse the Zendesk ticket with the given id number.</summary>
    ''' <param name="id">The id number of the ticket to download.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns the ticket with the given id.</returns>
    Public Async Function loadZendeskTicketFromId(id As String, user As UserInfo) As Task(Of ZendeskTicket)
        updateListeners("Loading Zendesk ticket """ & id & """...")
        Dim jsonUrl As String = getTicketUrl(id)
        Return Await loadZendeskTicketFromJsonUrl(jsonUrl, user)
    End Function

    ''' <summary>Asynchronously download and parse the Zendesk ticket from the given url.</summary>
    ''' <param name="jsonUrl">The url to download a ticket from.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns the ticket from the given url.</returns>
    Public Async Function loadZendeskTicketFromJsonUrl(ByVal jsonUrl As String, ByVal user As UserInfo) As Task(Of ZendeskTicket)
        updateListeners("Loading Zendesk ticket from """ & jsonUrl & """...")
        Dim jsonString As String = Await getJsonStringFromZendeskUrl(jsonUrl, user)
        Debug.WriteLine(jsonString)
        Return loadZendeskTicketFromJsonString(jsonString)
    End Function

    ''' <summary>Deserialize the given JSON string to obtain a Zendesk ticket.</summary>
    ''' <param name="jsonString">The Zendesk ticket stored as a serialized JSON string.</param>
    ''' <returns>A ticket extracted from the given string.</returns>
    Public Function loadZendeskTicketFromJsonString(ByVal jsonString As String) As ZendeskTicket
        updateListeners("Loading Zendesk ticket from JSON string...")
        Dim json As New JavaScriptSerializer
        Dim zenTicket As ZendeskTicket = json.Deserialize(Of ZendeskTicket)(jsonString)
        Return zenTicket
    End Function
#End Region

#Region "Total Open Tasks"
    '###############################################
    '#             Total Open Tasks                #
    '###############################################

    ''' <summary>Asynchronously download and parse the total number of open Zendesk tickets assigned to the given user.</summary>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns total number of open Zendesk tickets assigned to the given user.</returns>
    Public Async Function loadNumberOfOpenTicketsForCurrentUser(user As UserInfo) As Task(Of Integer)
        updateListeners("Loading number of open tickets...")
        Dim jsonUrl As String = getAllOpenTicketsForMeUrl()
        Return Await loadNumberOfOpenTicketsJsonUrl(jsonUrl, user)
    End Function

    ''' <summary>Asynchronously download and parse the total number of open Zendesk tickets	from the given url.</summary>
    ''' <param name="jsonUrl">The url to download a ticket from.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns the total number of open Zendesk tickets from the given url.</returns>
    Public Async Function loadNumberOfOpenTicketsJsonUrl(ByVal jsonUrl As String, ByVal user As UserInfo) As Task(Of Integer)
        updateListeners("Loading number of open tickets from """ & jsonUrl & """...")
        Dim jsonString As String = Await getJsonStringFromZendeskUrl(jsonUrl, user)
        Debug.WriteLine(jsonString)
        Return loadNumberOfOpenTicketsFromJsonString(jsonString)
    End Function

    ''' <summary>Deserialize the given JSON string to obtain the count of the returned search results.</summary>
    ''' <param name="jsonString">The results of a Zendesk search serialized JSON string, with a "count" field at the end.</param>
    ''' <returns>The count extracted from the given string.</returns>
    Public Function loadNumberOfOpenTicketsFromJsonString(ByVal jsonString As String) As Integer
        updateListeners("Loading Zendesk ticket from JSON string...")
        Dim json As New JavaScriptSerializer
        Dim zenResult As ZendeskSearchResult = json.Deserialize(Of ZendeskSearchResult)(jsonString)
        Return If(zenResult Is Nothing, 0, zenResult.count)
    End Function

    Private Class ZendeskSearchResult
        Public Property count As Integer
    End Class
#End Region

#Region "User"
    '###############################################
    '#                    User                     #
    '###############################################

    ''' <summary>Asynchronously download and parse the Zendesk user with the given id number.</summary>
    ''' <param name="id">The id number of the user to download.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns the user with the given id.</returns>
    Public Async Function loadZendeskUserFromId(id As Integer, ByVal user As UserInfo) As Task(Of ZendeskUser)
        updateListeners("Loading Zendesk user """ & id & """...")
        Dim jsonUrl As String = getUserUrl(id)
        Debug.WriteLine(jsonUrl)
        Return Await loadZendeskUserFromUrl(jsonUrl, user)
    End Function

    ''' <summary>Asynchronously download and parse the Zendesk user from the given url.</summary>
    ''' <param name="jsonUrl">The url to download a user from.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    ''' <returns>A task that returns the user from the given url.</returns>
    Public Async Function loadZendeskUserFromUrl(ByVal jsonUrl As String, ByVal user As UserInfo) As Task(Of ZendeskUser)
        updateListeners("Loading Zendesk user from """ & jsonUrl & """...")
        Dim jsonString As String = Await getJsonStringFromZendeskUrl(jsonUrl, user)
        Debug.WriteLine(jsonString)
        Return loadZendeskUserFromJsonString(jsonString)
    End Function

    ''' <summary>Deserialize the given JSON string to obtain a Zendesk user.</summary>
    ''' <param name="jsonString">The Zendesk user stored as a serialized JSON string.</param>
    ''' <returns>A Zendesk user extracted from the given string.</returns>
    Public Function loadZendeskUserFromJsonString(ByVal jsonString As String) As ZendeskUser
        updateListeners("Loading Zendesk user from JSON string...")
        Dim json As New JavaScriptSerializer
        Dim zenUser As ZendeskUser = json.Deserialize(Of ZendeskUser)(jsonString)
        Return zenUser
    End Function

    ''' <summary>Asynchronously download and parse the account details of the current Zendesk user.</summary>
    ''' <param name="user">The user whose credentials to log in to Zendesk with, and whose details to return.</param>
    ''' <returns>A task that returns account details of the current Zendesk user.</returns>
    Public Async Function getCurrentZendeskUser(ByVal user As UserInfo) As Task(Of ZendeskUser)
        updateListeners("Getting Current Zendesk User...")
        Dim jsonUrl As String = getCurrentUserUrl()
        Debug.WriteLine(jsonUrl)
        Return Await loadZendeskUserFromUrl(jsonUrl, user)

    End Function
#End Region

#Region "General URL"
    '###############################################
    '#                 General URL                 #
    '###############################################

    ''' <summary>Get the url for downloading the JSON for a Zendesk ticket with the given id.</summary>
    ''' <param name="id">The id number of the desired Zendesk ticket.</param>
    ''' <returns>The url for downloading the JSON for a Zendesk ticket with the given id.</returns>
    Public Function getTicketUrl(id As Integer)
        Return "https://" & zendeskSubdomain & ".zendesk.com/api/v2/tickets/" & id & ".json"
    End Function

    ''' <summary>Get the url for downloading the JSON for a Zendesk user with the given id.</summary>
    ''' <param name="id">The id number of the desired Zendesk user.</param>
    ''' <returns>The url for downloading the JSON for a Zendesk user with the given id.</returns>
    Public Function getUserUrl(id As Integer)
        Return "https://" & zendeskSubdomain & ".zendesk.com/api/v2/users/" & id & ".json"
    End Function

    ''' <summary>Get the url for downloading the JSON for the currently logged in Zendesk user.</summary>
    ''' <returns>The url for downloading the JSON for the currently logged in Zendesk user.</returns>
    Public Function getCurrentUserUrl()
        Return "https://" & zendeskSubdomain & ".zendesk.com/api/v2/users/me.json"
    End Function

    ''' <summary>Get the url for downloading the JSON of all the open tasks assigned to currently logged in Zendesk user.</summary>
    ''' <returns>The url for downloading the JSON of all the open tasks assigned to currently logged in Zendesk user.</returns>
    Public Function getAllOpenTicketsForMeUrl()
        Return "https://" & zendeskSubdomain & ".zendesk.com/api/v2/search.json?query=type:ticket status:open assignee:me"
    End Function

    ''' <summary>Asynchronously download a string of serialized JSON from the given Zendesk API url with the given login credentials.</summary>
    ''' <param name="jsonUrl">The Zendesk API url to download JSON from.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with, and whose details to return.</param>
    ''' <returns>A task that returns a string of serialized JSON downloaded from the Zendesk API.</returns>
    Public Async Function getJsonStringFromZendeskUrl(ByVal jsonUrl As String, ByVal user As UserInfo) As Task(Of String)
        Dim client As HttpClient = New HttpClient
        client.DefaultRequestHeaders.Accept.Add(New System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"))

        Dim encoded As String = Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", user.zendeskUsername, user.zendeskPassword)))
        client.DefaultRequestHeaders.Authorization = New AuthenticationHeaderValue("Basic", encoded)

        Return Await client.GetStringAsync(jsonUrl)  ' .ConfigureAwait(False)
    End Function
#End Region

#Region "Closing Tickets"
    '###############################################
    '#             Closing Tickets                 #
    '###############################################

    ''' <summary>
    ''' Close the Zendesk ticket with the given id.
    ''' Sets the status to "solved".
    ''' Sets the category to "Bug".
    ''' Sets the assignee to "GameMaker QA".
    ''' Sets the mantis issue id field to the id of the Mantis issue that was created from this ticket.
    ''' Sets the product to "GameMaker: Studio".
    ''' Adds a comment to tell the ticket's reporter that the ticket was closed, and to
    ''' point them to the newly created Mantis issue.
    ''' </summary>
    ''' <param name="zenId">The id of the Zendesk ticket being closed.</param>
    ''' <param name="zenUser">The Zendesk user who reported the ticket.</param>
    ''' <param name="mantisId">The id of the Mantis issue created from the ticket.</param>
    ''' <param name="user">The user whose credentials to log in to Zendesk with.</param>
    Public Async Sub closeTicket(zenId As Integer, zenUser As ZendeskUser, mantisId As Integer, user As UserInfo)
        updateListeners("Solving Zendesk ticket """ & zenId & """...")

        Dim jsonTicketString As String = getJsonStringForClosingTicket(zenUser, mantisId)
        Debug.Print("Json string for closing = " & Environment.NewLine & jsonTicketString)
        Dim content As New StringContent(jsonTicketString)
        content.Headers.ContentType.MediaType = "application/json"

        Dim client As HttpClient = New HttpClient
        client.DefaultRequestHeaders.Accept.Add(New System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"))

        Dim encoded As String = Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", user.zendeskUsername, user.zendeskPassword)))
        client.DefaultRequestHeaders.Authorization = New AuthenticationHeaderValue("Basic", encoded)

        Dim ticketUrl As String = getTicketUrl(zenId)
        Dim response As HttpResponseMessage = Await client.PutAsync(ticketUrl, content)
        Debug.Print("Response status from ticket closing: " & response.StatusCode)
        Debug.Print("Response from ticket closing: " & Await response.Content.ReadAsStringAsync)
    End Sub

    ''' <summary>
    ''' Get the list of fields to change when closing the ticket
    ''' as a serialied JSON object for passing to the Zendesk API.
    ''' </summary>
    ''' <param name="zenUser">The user who reported the issue. Their name will be added to the closing comment.</param>
    ''' <param name="mantisId">The id of the Mantis issue created from this ticket.</param>
    ''' <returns>A JSON string for sending to Zendesk to close the ticket.</returns>
    Public Function getJsonStringForClosingTicket(zenUser As ZendeskUser, mantisId As Integer) As String

        'Change status to solved:
        'ticket.status
        '<option value="1">Open</option>
        '<option value="2">Pending</option>
        '<option value="3">Solved</option>


        'Change category to bug
        '<option value="ticket_fields_21579232">Ticket: Category</option>
        '<option value="22346083">Bug</option>

        'Change assignee to GameMaker QA
        '.assignee_id
        '<option value="372276401">GameMaker QA</option>

        'Change product to GameMaker: Studio
        '<option value="ticket_fields_21897986">Ticket: Product</option>
        '<option value="21016586">GameMaker: Studio</option>

        'Mantis Issue id field: 22437503

        'GameMaker QA id: 372276401


        Dim message As String = cleanStringForJson( _
<p>Hello <%= zenUser.user.name %>,

Thanks for reporting this issue. I've now investigated it and I'm adding your report into our bugs database. If you are a new user you'll get an e-mail with your Mantis login details and also a confirmation of your new bug report. The second e-mail will give you a link where you can monitor the bug's progress. If you have an existing Mantis account tied to this email address it will be used and you will only get the report confirmation e-mail.

I''m going to solve this ticket as there's no further information we require from you at this time and if we need to contact you further (or if you want to supply more information) it will be done via the report page on Mantis.

Thanks again,
-
GameMaker QA
</p>.Value)

        Dim jsonString As String = _
        <p>{"ticket":{
            "status": "solved",
            "assignee_id": 372276401,
            "comment": {"public": true, "body": "<%= message %>"},
            "fields": [
            {"id":21897986, "value": "gamemaker__studio"},
            {"id":22437503, "value": "<%= mantisId %>"},
            {"id":21579232, "value": "bug"}
            ]
        }}</p>.Value

        Return jsonString
    End Function

    ''' <summary>
    ''' Escape all the characters in the given string to make it safe to send as
    ''' JSON to the Zendesk API.
    ''' Translated from: http://stackoverflow.com/questions/1242118/how-to-escape-json-string
    ''' </summary>
    ''' <param name="json">The string to escape characters in.</param>
    ''' <returns>A JSON string with all necessary characters escaped.</returns>
    Public Function cleanStringForJson(json As String) As String
        If json Is Nothing OrElse 0 = json.Length Then
            Return ""
        End If

        Dim sb As StringBuilder = New StringBuilder(json.Length() + 4)

        For Each c As Char In json
            Select Case c
                Case "\" : sb.Append("\\")
                Case """" : sb.Append("\""")
                Case "/" : sb.Append("\/")
                Case Chr(8) : sb.Append(Chr(92) & "b") '\b
                Case vbTab : sb.Append(Chr(92) & "t")  '\t
                Case vbLf : sb.Append(Chr(92) & "n")   '\n
                Case vbFormFeed : sb.Append(Chr(92) & "f")  '\f
                Case vbCr : sb.Append(Chr(92) & "r")  '\r
                Case Else
                    If Asc(c) < Asc(" ") Then
                        Dim t As String = "000" & String.Format("X", c)
                        sb.Append(Chr(92) & "u" + t.Substring(t.Length - 4))
                    Else
                        sb.Append(c)
                    End If
            End Select
        Next
        Return sb.ToString()
    End Function
#End Region

End Class
