﻿Imports System.Runtime.Serialization

#Region "Wrapper Class for Accessing Useful Fields"
'################################################################
'#          Wrapper Class for Accessing Useful Fields           #
'################################################################

''' <summary>
''' Represents the JSON object returned from Zendesk's ticket API.
''' Wraps all the fields necessary for Mantis import to make the
''' Zendesk fields more easily translatable to Mantis fields.
''' </summary>
Public Class ZendeskTicket

    ''' <summary>
    ''' The root of the deserialized json object
    ''' All properties act as wrappers retrieving data from here.
    ''' </summary>
    Public Property ticket As Ticket

    ''' <summary>The id number of Zendesk's description field.</summary>
    Const descriptionFieldId As Integer = 22608892
    ''' <summary>The id number of Zendesk's steps to reproduce field.</summary>
    Const reproStepsFieldId As Integer = 22618137
    ''' <summary>The id number of Zendesk's version field.</summary>
    Const versionNumFieldId As Integer = 22730742
    ''' <summary>The id number of Zendesk's platform field.</summary>
    Const platformFieldId As Integer = 22436973
    ''' <summary>The id number of Zendesk's operating system field.</summary>
    Const osFieldId As Integer = 22436963
    ''' <summary>The id number of Zendesk's reproducibility field.</summary>
    Const reproLevelFieldId As Integer = 22450796
    ''' <summary>The id number of Zendesk's category field.</summary>
    Const categoryFieldId As Integer = 22447011
    ''' <summary>The id number of Zendesk's view state field (public or private).</summary>
    Const viewFieldId As Integer = 22456891
    ''' <summary>The id number of Zendesk's project field.</summary>
    Const projectFieldId As Integer = 23850083

    ''' <summary>The Zendesk user id of the person who reported this Ticket.</summary>
    ReadOnly Property reporterId As String
        Get
            Return ticket.requester_id
        End Get
    End Property

    ''' <summary>The description string that would be added to Mantis from this ticket.</summary>
    ReadOnly Property description As String
        Get
            Dim descFromCustomField As String = getCustomFieldValue(descriptionFieldId)
            If descFromCustomField Is Nothing OrElse 0 = descFromCustomField.Length Then
                Return ticket.description
            Else
                Return descFromCustomField
            End If
        End Get
    End Property

    ''' <summary>The steps to reproduce the bug in this ticket as a string.</summary>
    ReadOnly Property stepsToReproduce As String
        Get
            Return getCustomFieldValue(reproStepsFieldId)
        End Get
    End Property

    ''' <summary>The GameMaker version this ticket is for.</summary>
    ReadOnly Property getVersionNum As String
        Get
            Return getCustomFieldValue(versionNumFieldId)
        End Get
    End Property

    ''' <summary>The summary string for this ticket when it is imported to Mantis.</summary>
    ReadOnly Property summary As String
        Get
            Return categoryPrefix & ticket.subject
        End Get
    End Property

    ''' <summary>The platform this ticket is for (as shown on Mantis).</summary>
    ReadOnly Property platform As String
        Get
            Dim zendeskPlatform As String
            zendeskPlatform = getCustomFieldValue(platformFieldId)
            Dim mantisPlatform As String
            Select Case zendeskPlatform
                Case "plat_win" : mantisPlatform = "Windows"
                Case "plat_ubuntu" : mantisPlatform = "Ubuntu"
                Case "plat_html5" : mantisPlatform = "HTML5"
                Case "plat_ios" : mantisPlatform = "iOS"
                Case "plat_android" : mantisPlatform = "Android"
                Case "plat_wp8" : mantisPlatform = "Windows Phone 8"
                Case "plat_win8js" : mantisPlatform = "Windows 8 (JS)"
                Case "plat_win8n" : mantisPlatform = "Windows 8 (Native)"
                Case "plat_steam" : mantisPlatform = "Steam"
                Case "plat_tizenn" : mantisPlatform = "Tizen (Native)"
                Case "plat_tizenjs" : mantisPlatform = "Tizen (JS)"
                Case "plat_win_yyc" : mantisPlatform = "Windows (YYC)"
                Case "plat_ios_yyc" : mantisPlatform = "iOS (YYC)"
                Case Else : mantisPlatform = Nothing
            End Select
            Return mantisPlatform
        End Get
    End Property

    ''' <summary>The operating system this ticket is for (as shown on Mantis).</summary>
    ReadOnly Property os As String
        Get
            Dim zendeskOS As String
            zendeskOS = getCustomFieldValue(osFieldId)
            Dim mantisOS As String
            Select Case zendeskOS
                Case "os_windows8" : mantisOS = "Windows 8"
                Case "os_windows7" : mantisOS = "Windows 7"
                Case "os_windowsv" : mantisOS = "Windows Vista"
                Case "os_windowsxp" : mantisOS = "Windows XP"
                Case "os_osx" : mantisOS = "OSX"
                Case "os_ubuntu" : mantisOS = "Ubuntu"
                Case "os_android" : mantisOS = "Android"
                Case "os_ios" : mantisOS = "iOS"
                Case "os_wp8" : mantisOS = "Windows Phone 8"
                Case "os_tizen" : mantisOS = "Tizen"
                Case Else : mantisOS = Nothing
            End Select
            Return mantisOS
        End Get
    End Property

    ''' <summary>The id number of the reproducibility for the bug in this ticket (as used in Mantis).</summary>
    ReadOnly Property reproducibility As Integer
        Get
            Dim zendeskRepro As String
            zendeskRepro = getCustomFieldValue(reproLevelFieldId)
            Dim mantisRepro As Integer
            Select Case zendeskRepro
                Case "repro_25" : mantisRepro = 70
                Case "repro_50" : mantisRepro = 50
                Case "repro_75" : mantisRepro = 30
                Case "repro_100" : mantisRepro = 10
                Case "repro_unable" : mantisRepro = 90
                Case "repro_na" : mantisRepro = 100
                Case Else : mantisRepro = 70
            End Select
            Return mantisRepro
        End Get
    End Property

    ''' <summary>The category this ticket is for (as shown on Mantis).</summary>
    ReadOnly Property category As String
        Get
            Dim zendeskCat As String
            zendeskCat = getCustomFieldValue(osFieldId)
            Dim mantisCat As String
            Select Case zendeskCat
                Case "cat_debugger" : mantisCat = "[All Projects] Debugger"
                Case "cat_installer" : mantisCat = "[All Projects] Installer"
                Case "cat_surfaces" : mantisCat = "[All Projects] Surfaces"
                Case "cat_3d" : mantisCat = "3D"
                Case "cat_android" : mantisCat = "Android"
                Case "cat_audio" : mantisCat = "Audio"
                Case "cat_bg" : mantisCat = "Background"
                Case "cat_compiler" : mantisCat = "Compiler"
                Case "cat_demos" : mantisCat = "Demos"
                Case "cat_documentation" : mantisCat = "Documentation"
                Case "cat_events" : mantisCat = "Events"
                Case "cat_export" : mantisCat = "Export"
                Case "cat_extensions" : mantisCat = "Extensions"
                Case "cat_font" : mantisCat = "Font"
                Case "cat_functions" : mantisCat = "Functions"
                Case "cat_general" : mantisCat = "General"
                Case "cat_ggs" : mantisCat = "Global Game Settings"
                Case "cat_html5" : mantisCat = "HTML5"
                Case "cat_image" : mantisCat = "Image Editor"
                Case "cat_input" : mantisCat = "Input Devices"
                Case "cat_ios" : mantisCat = "iOS"
                Case "cat_mac" : mantisCat = "Mac"
                Case "cat_net" : mantisCat = "Networking"
                Case "cat_objects" : mantisCat = "Objects"
                Case "cat_paths" : mantisCat = "Paths"
                Case "cat_physics" : mantisCat = "Physics"
                Case "cat_rooms" : mantisCat = "Rooms"
                Case "cat_saving" : mantisCat = "Saving"
                Case "cat_scripts" : mantisCat = "Scripts"
                Case "cat_source" : mantisCat = "Source Control"
                Case "cat_sprites" : mantisCat = "Sprites"
                Case "cat_steam" : mantisCat = "Steam"
                Case "cat_tiles" : mantisCat = "Tiles"
                Case "cat_time" : mantisCat = "Time Lines"
                Case "cat_tutorials" : mantisCat = "Tutorials"
                Case "cat_ubuntu" : mantisCat = "Ubuntu"
                Case "cat_ui" : mantisCat = "User Interface"
                Case "cat_var" : mantisCat = "Variables"
                Case "cat_views" : mantisCat = "Views"
                Case "cat_web" : mantisCat = "Web Server"
                Case "cat_win" : mantisCat = "Windows"
                Case "cat_win8js" : mantisCat = "Windows 8 (Javascript)"
                Case "cat_winp8" : mantisCat = "Windows Phone 8"
                Case "cat_win8n" : mantisCat = "Windows 8 (Native)"
                Case "cat_tizenn" : mantisCat = "Tizen (Native)"
                Case "cat_tizenjs" : mantisCat = "Tizen (Javascript)"
                Case "cat_shaders" : mantisCat = "Shaders"
                Case "cat_winyyc" : mantisCat = "Windows (YYC)"
                Case "cat_buffers" : mantisCat = "Buffers"
                Case "cat_iaps" : mantisCat = "IAPs"
                Case "cat_social" : mantisCat = "Social"
                Case "cat_advertising" : mantisCat = "Advertising"
                Case Else : mantisCat = "General"
            End Select
            Return mantisCat
        End Get
    End Property

    ''' <summary>The category prefix to put in front of the issue's summary on Mantis.</summary>
    ReadOnly Property categoryPrefix As String
        Get
            Dim zendeskCat As String
            zendeskCat = getCustomFieldValue(osFieldId)
            Dim prefix As String
            Select Case zendeskCat
                Case "cat_debugger" : prefix = "Debugger: "
                Case "cat_installer" : prefix = "Installer: "
                Case "cat_surfaces" : prefix = "Surfaces: "
                Case "cat_3d" : prefix = "3D: "
                Case "cat_android" : prefix = "Android: "
                Case "cat_audio" : prefix = "Audio: "
                Case "cat_bg" : prefix = "BG: "
                Case "cat_compiler" : prefix = "Compiler: "
                Case "cat_demos" : prefix = "Demos: "
                Case "cat_documentation" : prefix = "Documentation: "
                Case "cat_events" : prefix = "Events: "
                Case "cat_export" : prefix = "Export: "
                Case "cat_extensions" : prefix = "Extensions: "
                Case "cat_font" : prefix = "Font: "
                Case "cat_functions" : prefix = "Functions: "
                Case "cat_general" : prefix = "General: "
                Case "cat_ggs" : prefix = "GGS: "
                Case "cat_html5" : prefix = "HTML5: "
                Case "cat_image" : prefix = "Image Editor: "
                Case "cat_input" : prefix = "Input Devices: "
                Case "cat_ios" : prefix = "iOS: "
                Case "cat_mac" : prefix = "Mac: "
                Case "cat_net" : prefix = "Networking: "
                Case "cat_objects" : prefix = "Objects: "
                Case "cat_paths" : prefix = "Paths: "
                Case "cat_physics" : prefix = "Physics: "
                Case "cat_rooms" : prefix = "Rooms: "
                Case "cat_saving" : prefix = "Saving: "
                Case "cat_scripts" : prefix = "Scripts: "
                Case "cat_source" : prefix = "SVN: "
                Case "cat_sprites" : prefix = "Sprites: "
                Case "cat_steam" : prefix = "Steam: "
                Case "cat_tiles" : prefix = "Tiles: "
                Case "cat_time" : prefix = "Time Lines: "
                Case "cat_tutorials" : prefix = "Tutorials: "
                Case "cat_ubuntu" : prefix = "Ubuntu: "
                Case "cat_ui" : prefix = "UI: "
                Case "cat_var" : prefix = "Variables: "
                Case "cat_views" : prefix = "Views: "
                Case "cat_web" : prefix = "Web Server: "
                Case "cat_win" : prefix = "Windows: "
                Case "cat_win8js" : prefix = "Windows 8(JS): "
                Case "cat_winp8" : prefix = "WP8: "
                Case "cat_win8n" : prefix = "Win8(N): "
                Case "cat_tizenn" : prefix = "Tizen(N): "
                Case "cat_tizenjs" : prefix = "Tizen(JS): "
                Case "cat_shaders" : prefix = "Shaders: "
                Case "cat_winyyc" : prefix = "Win(YYC): "
                Case "cat_buffers" : prefix = "Buffers: "
                Case "cat_iaps" : prefix = "IAPs: "
                Case "cat_social" : prefix = "Social: "
                Case "cat_advertising" : prefix = "Advertising: "
                Case Else : prefix = ""
            End Select
            Return prefix
        End Get
    End Property

    ''' <summary>The id number of the bug's view state (public/private as used in Mantis).</summary>
    ReadOnly Property view_state As Integer
        Get
            Dim zendeskViewState As String = getCustomFieldValue(viewFieldId)
            Dim mantisViewState As Integer
            Select Case zendeskViewState
                Case "view_public" : mantisViewState = 10
                Case "view_private" : mantisViewState = 50
                Case Else : mantisViewState = 10
            End Select
            Return mantisViewState
        End Get
    End Property

    ''' <summary>Additional information for the Mantis issue stating which ticket it came from.</summary>
    ReadOnly Property additional_information As String
        Get
            Return "Original helpdesk ticket: http://help.yoyogames.com/tickets/" & ticket.id
        End Get
    End Property

    ''' <summary>The Mantis project id this bug is for. Will usually be 1 (for GameMaker: Studio).</summary>
    ReadOnly Property project As Integer
        Get
            Dim zendeskProject As String = getCustomFieldValue(viewFieldId)
            Dim mantisProject As Integer
            Select Case zendeskProject
                Case "res_ps4" : mantisProject = 28
                Case "res_ps3" : mantisProject = 29
                Case "res_psv" : mantisProject = 33
                Case "res_xone" : mantisProject = 30
                Case "res_wiiu" : mantisProject = 31
                Case "res_3ds" : mantisProject = 32
                Case "res_yys" : mantisProject = 34
                Case Else : mantisProject = 1
            End Select
            Return mantisProject
        End Get
    End Property

    ''' <summary>A default priority for this bug.</summary>
    ReadOnly Property priority As Integer
        Get
            Return 10
        End Get
    End Property

    ''' <summary>
    ''' Used to get the string value of a field in the underlying deserialized Zendesk ticket
    ''' using its numerical id number.
    ''' </summary>
    ''' <param name="fieldNum">The id number of the custom field to access.</param>
    ''' <returns>The string value of the specified field (of <c>Nothing</c> if the field is not found).</returns>
    Private Function getCustomFieldValue(ByVal fieldNum As Integer) As String
        For Each f As CustomField In ticket.custom_fields
            If f.id = fieldNum Then
                Return f.value
            End If
        Next
        Return Nothing
    End Function
End Class
#End Region

#Region "Internal Objects for Deserializing"
'################################################################
'#          Internal Objects for Deserializing                  #
'################################################################

' The following classes represent the JSON objects returned from
' Zendesk's ticket API. Most of the fields are unused. 
' This class was auto-generated using a converter at http://jsonutils.com/ 
' (with minor modifications to avoid collisions with VB keywords).

Public Class Source
    Public Property from As Object

    <DataMember(Name:="to")> _
    Public Property receiver As Object

    Public Property rel As Object
End Class

Public Class Via
    Public Property channel As String
    Public Property source As Source
End Class

Public Class CustomField
    Public Property id As Integer
    Public Property value As String
End Class

Public Class SatisfactionRating
    Public Property score As String
End Class

Public Class Field
    Public Property id As Integer
    Public Property value As String
End Class

Public Class Comment
    Public Property body As String
End Class

Public Class Ticket
    Public Property url As String
    Public Property id As Integer?
    Public Property external_id As Object
    Public Property via As Via
    Public Property created_at As DateTime
    Public Property updated_at As DateTime
    Public Property type As String
    Public Property comment As Comment
    Public Property subject As String
    Public Property raw_subject As String
    Public Property description As String
    Public Property priority As String
    Public Property status As String
    Public Property recipient As Object
    Public Property requester_id As Integer?
    Public Property submitter_id As Integer?
    Public Property assignee_id As Integer?
    Public Property organization_id As Object
    Public Property group_id As Integer?
    Public Property collaborator_ids As Integer()
    Public Property forum_topic_id As Object
    Public Property problem_id As Object
    Public Property has_incidents As Boolean?
    Public Property due_at As Object
    Public Property tags As String()
    Public Property custom_fields As CustomField()
    Public Property satisfaction_rating As SatisfactionRating
    Public Property sharing_agreement_ids As Object()
    Public Property fields As Field()
    Public Property brand_id As Integer?
End Class
#End Region