﻿Imports System.ComponentModel

''' <summary>
''' All the different types of chart and graph that can be
''' displayed in the <see cref="StatsPanel"/>.
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum ChartType
    <Description("Column")>
    COLUMN

    <Description("Bar")>
    BAR

    <Description("Pie")>
    PIE

    <Description("Tree Map")>
    TREEMAP

    <Description("Scatter")>
    SCATTER

    <Description("Area")>
    AREA

    <Description("Bubble")>
    BUBBLE

    <Description("Line")>
    LINE
End Enum
