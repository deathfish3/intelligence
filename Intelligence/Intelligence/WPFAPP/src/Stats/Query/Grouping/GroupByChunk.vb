﻿''' <summary>
''' A special type of <see cref="QueryChunk"/> that instead of refining the results
''' returned in the query will split the results into several different groups.
''' By splitting the results into groups, this allows separate columns and series to be
''' displayed on the graphs, or multiple different graphs to be displayed.
''' 
''' Queries can be subdivied any number of times using multiple <see cref="GroupByChunk"/>s
''' to form a tree of results. With no <see cref="GroupByChunk"/>s, a graph with a single
''' column for all the tasks is shown. With one <see cref="GroupByChunk"/> a graph with a
''' single series but multiple columns will be shown (one for each of the resulting groups).
''' With two <see cref="GroupByChunk"/>s a graph with multiple series can be generated (e.g
''' multiple lines with multiple point per line). Any higher number of <see cref="GroupByChunk"/>s
''' will result in multiple separate graphs being shown, one for each subdivision.
''' </summary>
Public Class GroupByChunk
    Inherits QueryChunk

    ''' <summary>Create a new GroupByChunk.</summary>
    Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' DO NOT USE!
    ''' Grouping does not work with the rest of the query stack, so use <see cref="splitIntoGroups"/> instead
    ''' for grouping. Calling this method will throw a <see cref="NotImplementedException"/>.
    ''' </summary>
    ''' <param name="query">DO NOT USE!</param>
    ''' <returns>DO NOT USE! Will throw a <see cref="NotImplementedException"/> without returning anything.</returns>
    ''' <exception cref="NotImplementedException">Always throws this exception. Use <see cref="splitIntoGroups"/> instead.</exception>
    Public Overrides Function addToQuery(query As IQueryable(Of TaskInstance)) As IQueryable(Of TaskInstance)
        Throw New NotImplementedException("Grouping does not work with the rest of the query stack, so has to use splitIntoGroups instead.")
    End Function

    ''' <summary>
    ''' Split the result of the given query into groups based on the <see cref="selectedField"/>
    ''' and returns a list of the resulting groups sorted according to the current <see cref="sortType"/>,
    ''' and <see cref="sortAscending"/> parameters. If <see cref="shouldIncludeEmptyColumns"/> is <c>True</c> 
    ''' then the list of groups will be padded out with empty groups for all the values of 
    ''' <see cref="selectedField"/> that had no tasks associated with them.
    ''' </summary>
    ''' <param name="query">A query returning the tasks to split into groups.</param>
    ''' <returns>The results of <paramref name="query"/> divided up into different groups.</returns>
    Public Function splitIntoGroups(query As IQueryable(Of TaskInstance)) As IEnumerable(Of Grouping)
        Dim groupings As IEnumerable(Of Grouping) = Nothing
        Dim allColumns As IEnumerable = If(shouldIncludeEmptyColumns, getAllFieldValues(Me.selectedField), {})

        Select Case Me.selectedField
            Case FieldType.RESOLUTION : groupings = createGroupings(Of Resolution)(query.GroupBy(Function(t) t.resolution), Function(r) r.name, Me.selectedField, allColumns)
            Case FieldType.ASSIGNEE : groupings = createGroupings(Of IntelligenceUser)(query.GroupBy(Function(t) t.assignee), Function(u) If(u Is Nothing, "Unassigned", u.username), Me.selectedField, allColumns)
            Case FieldType.ASSIGNER : groupings = createGroupings(Of IntelligenceUser)(query.GroupBy(Function(t) t.assigner), Function(u) If(u Is Nothing, "Unassigned", u.username), Me.selectedField, allColumns)
            Case FieldType.PLATFORM : groupings = createGroupings(Of Platform)(query.GroupBy(Function(t) t.platform), Function(p) p.name, Me.selectedField, allColumns)
            Case FieldType.VERSION : groupings = createGroupings(Of Version)(query.GroupBy(Function(t) t.version), Function(v) v.name, Me.selectedField, allColumns)
            Case FieldType.PRODUCT : groupings = createGroupings(Of Product)(query.GroupBy(Function(t) t.version.product), Function(p) p.name, Me.selectedField, allColumns)
            Case FieldType.PRIORITY : groupings = createGroupings(Of Priority)(query.GroupBy(Function(t) t.priority), Function(p) p.name, Me.selectedField, allColumns)
            Case FieldType.CREATED_TIME : groupings = createTimeGroupings(query, Function(t) t.timeCreated, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
            Case FieldType.ASSIGNED_TIME : groupings = createTimeGroupings(query, Function(t) t.timeAssigned, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
            Case FieldType.START_TIME : groupings = createTimeGroupings(query, Function(t) t.timeStarted, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
            Case FieldType.FINISH_TIME : groupings = createTimeGroupings(query, Function(t) t.timeFinished, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
            Case FieldType.VERSION_CREATED_TIME : groupings = createTimeGroupings(query, Function(t) t.version.timeCreated, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
            Case FieldType.VERSION_FINISHED_TIME : groupings = createTimeGroupings(query, Function(t) t.version.timeClosed, Me.timeType, Me.selectedField, Me.shouldIncludeEmptyColumns)
        End Select
        Return GroupByChunk.sortGroupings(groupings, Me.sortType, Me.aggregateSortType, Me.sortAscending)
    End Function

    ''' <summary>The internally stored flag to determine whether empty groups should be added to provide a grouping for every key possible.</summary>
    Private _shouldIncludeEmptyColumns = True
    ''' <summary>
    ''' A flag to determine whether empty groups should be added to provide a grouping for every key possible.
    ''' Setting this will notify the UI of the change.
    ''' </summary>
    Public Property shouldIncludeEmptyColumns As Boolean
        Get
            Return _shouldIncludeEmptyColumns
        End Get
        Set(value As Boolean)
            setField(value, _shouldIncludeEmptyColumns, Sub(v) _shouldIncludeEmptyColumns = v)
        End Set
    End Property

    ''' <summary>A <see cref="QueryChunk"/> that splits the query result into different groups.</summary>
    Public Overrides ReadOnly Property type As QueryType
        Get
            Return QueryType.GROUP_BY
        End Get
    End Property

    ''' <summary>Most values of <see cref="FieldType"/> can be used to group results.</summary>
    Protected Overrides Property allFields As IEnumerable(Of FieldType) = {
            FieldType.ASSIGNEE,
            FieldType.ASSIGNER,
            FieldType.PLATFORM,
            FieldType.RESOLUTION,
            FieldType.VERSION,
            FieldType.PRODUCT,
            FieldType.PRIORITY,
            FieldType.CREATED_TIME,
            FieldType.ASSIGNED_TIME,
            FieldType.START_TIME,
            FieldType.FINISH_TIME,
            FieldType.VERSION_CREATED_TIME,
            FieldType.VERSION_FINISHED_TIME
        }

    ''' <summary>The internally stored unit of time tasks will be grouped by.</summary>
    Private _timeType As TimeUnitType = TimeUnitType.WEEKS
    ''' <summary>The unit of time tasks will be grouped by if <see cref="selectedField"/> is a time.</summary>
    Public Property timeType As TimeUnitType
        Get
            Return _timeType
        End Get
        Set(value As TimeUnitType)
            setField(value, _timeType, Sub(v) _timeType = v)
        End Set
    End Property

    ''' <summary>Whether <see cref="selectedField"/> is a time field.</summary>
    Public ReadOnly Property isTimeGrouping As Boolean
        Get
            Return {
            FieldType.CREATED_TIME,
            FieldType.ASSIGNED_TIME,
            FieldType.START_TIME,
            FieldType.FINISH_TIME,
            FieldType.VERSION_CREATED_TIME,
            FieldType.VERSION_FINISHED_TIME
            }.Contains(Me.selectedField)
        End Get
    End Property

    ''' <summary>A list of all types of time unit that tasks can be grouped by.</summary>
    Public ReadOnly Property allTimeTypes As IEnumerable(Of TimeUnitType)
        Get
            Return [Enum].GetValues(GetType(TimeUnitType))
        End Get
    End Property

    ''' <summary>
    ''' Split the given list of tasks into groups where <see cref="selectedField"/>
    ''' falls between a time-slice specified by <see cref="timeType"/>.
    ''' </summary>
    ''' <param name="query">A query returning the list of tasks to group.</param>
    ''' <param name="getTime">A function selecting the correct time field of each task.</param>
    ''' <param name="timeType">The type of time-slice to split the tasks into (e.g. week, month etc.).</param>
    ''' <param name="field">The value of <see cref="selectedField"/>.</param>
    ''' <param name="includeEmptyGroups">
    ''' The value of <see cref="shouldIncludeEmptyColumns"/>.
    ''' If this is <c>True</c>, empty groups between the maximum time-slice index and 0 will be added
    ''' to pad out any groupings that had no tasks.
    ''' </param>
    ''' <returns>A list of <see cref="Grouping"/>s where the tasks are split into different time-slices.</returns>
    Public Shared Function createTimeGroupings(query As IQueryable(Of TaskInstance), getTime As System.Func(Of TaskInstance, DateTime?), timeType As TimeUnitType, field As FieldType, includeEmptyGroups As Boolean) As IEnumerable(Of Grouping)
        Dim now As DateTime = DateTime.Now
        Dim timeSpanSelector As System.Func(Of DateTime, TimeSpan, Integer) = Function(t, s) 0
        Select Case timeType
            Case TimeUnitType.HOURS : timeSpanSelector = Function(t, s) Math.Floor(s.TotalHours)
            Case TimeUnitType.DAYS : timeSpanSelector = Function(t, s) Math.Floor(s.TotalDays)
            Case TimeUnitType.WEEKS : timeSpanSelector = Function(t, s) Math.Floor(s.TotalDays) \ 7
            Case TimeUnitType.MONTHS : timeSpanSelector = Function(t, s) ((12 * now.Year) + now.Month) - ((12 * t.Year) + t.Month)
            Case TimeUnitType.YEARS : timeSpanSelector = Function(t, s) now.Year - t.Year
        End Select

        Dim groupsEnumerable As IEnumerable(Of IGrouping(Of Integer, TaskInstance)) =
            query.AsEnumerable.GroupBy(
                Function(t)
                    Dim time As DateTime? = getTime(t)
                    If time Is Nothing Then Return 0
                    Dim span = (now - time)
                    Return If(span IsNot Nothing, timeSpanSelector(time, span), 0)
                End Function)

        Dim groupsList As List(Of IGrouping(Of Integer, TaskInstance)) = groupsEnumerable.ToList
        If includeEmptyGroups AndAlso groupsList.Count > 0 Then
            Dim max As Integer = groupsList.Max(Function(g As IGrouping(Of Integer, TaskInstance)) g.Key)
            Dim min As Integer = 0
            For i As Integer = min To max
                Dim localI As Integer = i
                If Not groupsList.Any(Function(g As IGrouping(Of Integer, TaskInstance)) g.Key = localI) Then
                    groupsList.Add(New EmptyGrouping(i))
                End If
            Next
        End If

        Return createGroupings(Of Integer)(groupsList.AsQueryable, Function(i) If(i Is Nothing, "Nothing", i.ToString), field, {})
    End Function


    ''' <summary>
    ''' Create <see cref="Grouping"/>s around the results of the query,
    ''' and pad out any unused keys with empty groups if necessary.
    ''' </summary>
    ''' <typeparam name="T">The type of key the tasks are grouped by.</typeparam>
    ''' <param name="groups">A query returning the task instances in the required groups.</param>
    ''' <param name="keyToString">A function for retreiving the string value of the group's key.</param>
    ''' <param name="field">The field the tasks are grouped by.</param>
    ''' <param name="allKeys">
    ''' A list of all possible keys available for the given field. This
    ''' is used to pad out unused keys with empty groups. If this list is
    ''' empty, no additional empty groups will be created.
    ''' </param>
    ''' <returns>A list of <see cref="Grouping"/>s created from the results of the query.</returns>
    Public Shared Function createGroupings(Of T)(
                                           groups As IQueryable(Of IGrouping(Of T, TaskInstance)),
                                           keyToString As System.Func(Of Object, String),
                                           field As FieldType,
                                           allKeys As IEnumerable
                                           ) As IEnumerable(Of Grouping)
        Dim groupingList As New List(Of Grouping)
        For Each group As IGrouping(Of T, TaskInstance) In groups
            Dim newGrouping As New Grouping(group.Key, keyToString(group.Key), group.AsQueryable, field)
            groupingList.Add(newGrouping)
        Next
        For Each key As T In allKeys
            If Not groupingList.Any(Function(g) g.key IsNot Nothing AndAlso g.key.Equals(key)) Then
                groupingList.Add(New Grouping(key, keyToString(key), Enumerable.Empty(Of TaskInstance).AsQueryable, field))
            End If
        Next
        Return groupingList
    End Function

    ''' <summary>Retstrict the different types of sort available for the <see cref="selectedField"/>.</summary>
    Protected Overrides Sub OnSelectedFieldChanged()
        MyBase.OnSelectedFieldChanged()
        onPropertyChanged(Function() Me.availableSortTypes)
        onPropertyChanged(Function() Me.isTimeGrouping)
        If Not Me.availableSortTypes.Contains(Me.sortType) Then
            Me.sortType = availableSortTypes.FirstOrDefault
        End If
    End Sub

    ''' <summary>Which types of sort are applicable for the current <see cref="selectedField"/>.</summary>
    Public ReadOnly Property availableSortTypes As IEnumerable(Of GroupSortType)
        Get
            If isTimeGrouping Then
                Return {GroupSortType.TIME,
                        GroupSortType.AGGREGATE,
                        GroupSortType.NONE
                }
            End If
            Select Case Me.selectedField
                Case FieldType.VERSION
                    Return {GroupSortType.NONE,
                            GroupSortType.AGGREGATE,
                            GroupSortType.NAME,
                            GroupSortType.VERSION_CLOSED,
                            GroupSortType.VERSION_CREATED
                    }
                Case FieldType.PRIORITY
                    Return {GroupSortType.NONE,
                            GroupSortType.AGGREGATE,
                            GroupSortType.PRIORITY
                    }
                Case Else
                    Return {GroupSortType.NONE,
                            GroupSortType.AGGREGATE,
                            GroupSortType.NAME
                    }
            End Select
        End Get
    End Property

    ''' <summary>The internally stored type of sort to perform on the groups.</summary>
    Private _sortType As GroupSortType
    ''' <summary>
    ''' What type of sort to perform on the groups (if any).
    ''' Setting this will notify the UI.
    ''' </summary>
    Public Property sortType As GroupSortType
        Get
            Return _sortType
        End Get
        Set(value As GroupSortType)
            setField(value, _sortType, Sub(v) _sortType = v)
        End Set
    End Property

    ''' <summary>
    ''' If <see cref="sortType"/> is <see cref="GroupSortType.AGGREGATE"/> then
    ''' this field determines what kind of aggregate value to sort by.
    ''' </summary>
    Public Property aggregateSortType As AggregatorType = AggregatorType.COUNT

    ''' <summary>A list of all possible aggregator types for <see cref="aggregateSortType"/> to take.</summary>
    Public ReadOnly Property allAggregateSortTypes As IEnumerable(Of AggregatorType)
        Get
            Return [Enum].GetValues(GetType(AggregatorType))
        End Get
    End Property

    ''' <summary>A flag to determine whether the groups should be sorted in ascending or descending order.</summary>
    Public Property sortAscending As Boolean = True

    ''' <summary>Sort the given list of groups.</summary>
    ''' <param name="groupings">The list of groups to sort.</param>
    ''' <param name="sortType">Which value to sort the groups by.</param>
    ''' <param name="aggregateSortType">
    ''' If <paramref name="sortType"/> is <see cref="GroupSortType.AGGREGATE"/> then this
    ''' determines what kind of aggregator to use when sorting. Otherwise, it is ignored.
    ''' </param>
    ''' <param name="sortAscending">A flag to determine whether the groups should be sorted in ascending or descending order.</param>
    ''' <returns>A sorted list of all the groups.</returns>
    Private Shared Function sortGroupings(
                                          groupings As List(Of Grouping),
                                          sortType As GroupSortType,
                                          aggregateSortType As AggregatorType,
                                          sortAscending As Boolean
                                         ) As List(Of Grouping)
        Select Case sortType
            Case GroupSortType.NAME : groupings.Sort(Function(a, b) If(sortAscending, a.keyString.CompareTo(b.keyString), b.keyString.CompareTo(a.keyString)))
            Case GroupSortType.TIME : groupings.Sort(Function(a, b) If(sortAscending, a.key.CompareTo(b.key), b.key.CompareTo(a.key)))
            Case GroupSortType.PRIORITY : groupings.Sort(
                Function(a, b)
                    Dim aPriority As Priority = TryCast(a.key, Priority)
                    Dim bPriority As Priority = TryCast(b.key, Priority)
                    Return If(sortAscending, aPriority.id.CompareTo(bPriority.id), bPriority.id.CompareTo(aPriority.id))
                End Function)
            Case GroupSortType.VERSION_CREATED : groupings.Sort(
                    Function(a, b)
                        Dim aVersion As Version = TryCast(a.key, Version)
                        Dim bVersion As Version = TryCast(b.key, Version)
                        Dim comparison As Integer = aVersion.timeCreated.CompareTo(bVersion.timeCreated)
                        Return If(sortAscending, comparison, -comparison)
                    End Function)
            Case GroupSortType.VERSION_CLOSED : groupings.Sort(
                Function(a, b)
                    Dim aVersion As Version = TryCast(a.key, Version)
                    Dim bVersion As Version = TryCast(b.key, Version)
                    Dim aClosed As DateTime? = aVersion.timeClosed
                    Dim bClosed As DateTime? = bVersion.timeClosed
                    Dim comparison As Integer = 0
                    If aClosed Is Nothing AndAlso bClosed Is Nothing Then
                        comparison = 0
                    ElseIf aClosed Is Nothing Then
                        comparison = 1
                    ElseIf bClosed Is Nothing Then
                        comparison = -1
                    Else
                        comparison = aClosed.Value.CompareTo(bClosed.Value)
                    End If
                    Return If(sortAscending, comparison, -comparison)
                End Function)
            Case GroupSortType.AGGREGATE : sortAggregateGroupings(groupings, aggregateSortType, sortAscending)
        End Select
        Return groupings
    End Function

    ''' <summary>Sort the list of groups using an aggregated value.</summary>
    ''' <param name="groupings">The list of groups to sort.</param>
    ''' <param name="aggregateSortType">What type of aggregator to use when determining values to sort by.</param>
    ''' <param name="sortAscending">Whether the groups should be sorted in ascending or descending order.</param>
    Private Shared Sub sortAggregateGroupings(
                                                    groupings As List(Of Grouping),
                                                    aggregateSortType As AggregatorType,
                                                    sortAscending As Boolean
                                                    )
        Dim aggregator As GroupingAggregator = GroupingAggregator.create(aggregateSortType)
        Dim numGroupings As Integer = groupings.Count
        Dim aggregatedList As New List(Of AggregatedGrouping)(numGroupings)
        For Each group In groupings
            aggregatedList.Add(New AggregatedGrouping With {
                                    .grouping = group,
                                    .aggregateValue = aggregator.getValue(group)
                               })
        Next
        aggregatedList.Sort(Function(a, b)
                                Dim comparison = a.aggregateValue.CompareTo(b.aggregateValue)
                                Return If(sortAscending, comparison, -comparison)
                            End Function)
        For i = 0 To numGroupings - 1
            groupings.Item(i) = aggregatedList.Item(i).grouping
        Next
    End Sub

    ''' <summary>A container for a grouping and the result of an <see cref="GroupingAggregator"/>.</summary>
    Private Class AggregatedGrouping
        Public aggregateValue As Double
        Public grouping As Grouping
    End Class

    ''' <summary>Write the paramters for creating this type of group to the given StringBuilder.</summary>
    ''' <param name="sb">The StringBuilder to export this chunk to.</param>
    Protected Overrides Sub exportContentsTo(sb As Text.StringBuilder)
        exportField(Me.selectedField, sb)
        exportField(Me.sortType, sb)
        exportField(Me.aggregateSortType, sb)
        exportField(Me.sortAscending, sb)
        exportField(Me.shouldIncludeEmptyColumns, sb)
        exportField(Me.timeType, sb)
    End Sub

    ''' <summary>Read in the settings for this chunk from the given list of strings.</summary>
    ''' <param name="fieldStrings">A list of strings representing the fields of this chunk.</param>
    Protected Overrides Sub importContentsFrom(fieldStrings As IEnumerable(Of String))
        [Enum].TryParse(fieldStrings(0), Me.selectedField)
        [Enum].TryParse(fieldStrings(1), Me.sortType)
        [Enum].TryParse(fieldStrings(2), Me.aggregateSortType)
        Boolean.TryParse(fieldStrings(3), Me.sortAscending)
        If fieldStrings.Count > 4 Then
            Boolean.TryParse(fieldStrings(4), Me.shouldIncludeEmptyColumns)
        End If
        If fieldStrings.Count > 5 Then
            [Enum].TryParse(fieldStrings(5), Me.timeType)
        End If
    End Sub

    ''' <summary>
    ''' A class representing an empty LINQ-compatible grouping with integer keys. 
    ''' Used to add in extra empty columns for time groupings in <see cref="createTimeGroupings"/>
    ''' </summary>
    Private Class EmptyGrouping
        Implements IGrouping(Of Integer, TaskInstance)

        ''' <summary>An empty list of contents.</summary>
        Private contents As New List(Of TaskInstance)

        ''' <summary>Create a new empty grouping for the given key.</summary>
        ''' <param name="key">The key for the new group.</param>
        Sub New(key As Integer)
            Me._key = key
        End Sub

        ''' <summary>Get an enumerator for the empty list of contents to conform to the interface.</summary>
        ''' <returns>An enumerator for the empty list of contents.</returns>
        Public Function GetEnumerator() As IEnumerator(Of TaskInstance) Implements IEnumerable(Of TaskInstance).GetEnumerator
            Return contents.GetEnumerator
        End Function

        ''' <summary>Get an enumerator for the empty list of contents to conform to the interface.</summary>
        ''' <returns>An enumerator for the empty list of contents.</returns>
        Public Function GetEnumerator1() As IEnumerator Implements IEnumerable.GetEnumerator
            Return contents.GetEnumerator
        End Function

        ''' <summary>The internally stored key for this empty group.</summary>
        Private _key As Integer = 0
        ''' <summary>The integer key for this empty group.</summary>
        Public ReadOnly Property Key As Integer Implements IGrouping(Of Integer, TaskInstance).Key
            Get
                Return Me._key
            End Get
        End Property
    End Class
End Class
