﻿''' <summary>A set of <see cref="TaskInstance"/>s grouped together by a common key.</summary>
Public Class Grouping

    ''' <summary>Initialize a grouping by passing in values for all its fields.</summary>
    ''' <param name="key">The value that all these tasks have in common for the key field.</param>
    ''' <param name="keyString">The string representation of <paramref name="key"/>.</param>
    ''' <param name="tasks">A list of all tasks in this group.</param>
    ''' <param name="field">The field which the tasks have been grouped by.</param>
    Public Sub New(key As Object, keyString As String, tasks As IQueryable(Of TaskInstance), field As FieldType)
        Me._key = key
        Me._keyString = keyString
        Me._tasks = tasks
        Me._keyField = field
    End Sub

    ''' <summary>The internally stored value represented as a string that all these tasks have in common for the key field.</summary>
    Private _keyString As String
    ''' <summary>The value represented as a string that all these tasks have in common for the key field.</summary>
    Public ReadOnly Property keyString As String
        Get
            Return _keyString
        End Get
    End Property

    ''' <summary>The internally stored field the tasks have been grouped by.</summary>
    Private _keyField As FieldType
    ''' <summary>Which field the tasks have been grouped by.</summary>
    Public ReadOnly Property keyField As FieldType
        Get
            Return _keyField
        End Get
    End Property

    ''' <summary>The internally stored value that all these tasks have in common for the key field.</summary>
    Private _key As Object
    ''' <summary>The value that all these tasks have in common for the key field.</summary>
    Public ReadOnly Property key As Object
        Get
            Return _key
        End Get
    End Property

    ''' <summary>The internally stored list of all tasks in this group.</summary>
    Private _tasks As IQueryable(Of TaskInstance)
    ''' <summary>A list of all tasks in this group.</summary>
    Public ReadOnly Property tasks As IQueryable(Of TaskInstance)
        Get
            Return _tasks
        End Get
    End Property

End Class
