﻿Imports System.ComponentModel

''' <summary>
''' The different ways that <see cref="Grouping"/>s can be sorted,
''' which will determine which order the columns appear on graphs,
''' or which order graphs appear if multiple there are multiple groupings.
''' 
''' <see cref="GroupSortType.AGGREGATE"/> requires another parameter in
''' <see cref=" GroupByChunk"/> to specify which type of aggregation
''' to sort by (chosen from the types in <see cref="AggregatorType"/>.
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum GroupSortType
    <Description("None")>
    NONE
    <Description("Aggregate")>
    AGGREGATE
    <Description("Name")>
    NAME
    <Description("Priority")>
    PRIORITY
    <Description("Version Closed")>
    VERSION_CLOSED
    <Description("Version Created")>
    VERSION_CREATED
    <Description("Time")>
    TIME
End Enum
