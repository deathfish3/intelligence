﻿Imports System.ComponentModel

''' <summary>
''' All the different types of field that can be selected
''' on a <see cref="TaskInstance"/> for filtering or grouping
''' in the <see cref="StatsPanel"/>
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum FieldType
    <Description("Product")>
    PRODUCT
    <Description("Version")>
    VERSION
    <Description("Platform")>
    PLATFORM
    <Description("Assignee")>
    ASSIGNEE
    <Description("Assigner")>
    ASSIGNER
    <Description("Resolution")>
    RESOLUTION
    <Description("Task Template")>
    TASK_TEMPLATE
    <Description("Priority")>
    PRIORITY

    <Description("Created Time")>
    CREATED_TIME
    <Description("Assigned Time")>
    ASSIGNED_TIME
    <Description("Start Time")>
    START_TIME
    <Description("Finish Time")>
    FINISH_TIME

    <Description("Version Created Time")>
    VERSION_CREATED_TIME
    <Description("Version Finished Time")>
    VERSION_FINISHED_TIME
End Enum
