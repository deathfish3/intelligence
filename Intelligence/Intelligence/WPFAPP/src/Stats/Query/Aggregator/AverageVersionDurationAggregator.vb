﻿''' <summary>
''' An aggregator for returning the average number of days between the starting and finishing times of the Version
''' that the tasks in the group are from. If the tasks are grouped by Version, this will be that Version's duration in days.
''' Will ignore versions that are still open.
''' </summary>
Public Class AverageVersionDurationAggregator
    Inherits GroupingAggregator

    ''' <summary>
    ''' Get the average number of days between the starting and finishing times of the Version that the tasks
    ''' in the group are from. If the tasks are grouped by Version, this will be that Version's duration in days.
    ''' Will ignore versions that are still open.
    ''' </summary>
    Public Overrides Function getValue(group As Grouping) As Double
        Dim durations = (From task In group.tasks
                Where task.version.timeClosed IsNot Nothing
                Select task)
        Dim list = durations.ToList()
        If Not list.Any() Then
            Return 0
        Else
            Dim average = list.Average(Function(t) (t.version.timeClosed - t.version.timeCreated).Value.TotalDays)
            Return average
        End If
    End Function

    ''' <summary>
    ''' An aggregator for returning the average number of days between the starting and
    ''' finishing times of the Version that the tasks in the group are from.
    ''' </summary>
    Public Overrides ReadOnly Property type As AggregatorType
        Get
            Return AggregatorType.AVERAGE_VERSION_DURATION
        End Get
    End Property
End Class