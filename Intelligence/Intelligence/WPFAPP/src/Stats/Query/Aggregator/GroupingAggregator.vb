﻿''' <summary>
''' The base class for aggregators, which take in a <see cref="Grouping"/> of
''' task instances, and convert them into a numerical value that can be graphed.
''' 
''' This base class contains the basic method stubs all subclasses should implement,
''' and also <see cref="GroupingAggregator.create"/> wich can be used to istantiate aggregators of each type.
''' </summary>
Public MustInherit Class GroupingAggregator

    ''' <summary>
    ''' What kind of value the aggregator is designed to produce
    ''' (such as the number of tasks, the total number of bugs, the average duration etc.).
    ''' Subclasses must override this property so they can be properly instantiated
    ''' using <see cref="create"/> from their stored type value.
    ''' </summary>
    Public MustOverride ReadOnly Property type As AggregatorType

    ''' <summary>
    ''' From the tasks in the given <see cref="Grouping"/>,
    ''' calculate some kind of graphable numeric value from them
    ''' (such as the number of tasks, the total number of bugs, the average duration etc.)
    ''' </summary>
    ''' <param name="group">The group of tasks to produce a value for.</param>
    ''' <returns>A graphable numeric value based on the given <see cref="Grouping"/>.</returns>
    Public MustOverride Function getValue(group As Grouping) As Double

    ''' <summary>
    ''' Instantiate a <see cref="GroupingAggregator"/> of the given type.
    ''' New aggregator types must be added to this method in order for them
    ''' to be correctly instantiated from the UI or from the database.
    ''' </summary>
    ''' <param name="type">Which type of aggregator to create.</param>
    ''' <returns>
    ''' A new <see cref="GroupingAggregator"/> of the given type.
    ''' Will return <c>Nothing</c> if no aggregator of that type was found.
    ''' </returns>
    Public Shared Function create(type As AggregatorType) As GroupingAggregator
        Select Case type
            Case AggregatorType.COUNT : Return New CountAggregator()
            Case AggregatorType.TOTAL_BUGS : Return New TotalBugsAggregator()
            Case AggregatorType.AVERAGE_START_TO_FINISH_TIME : Return New AverageTaskStartToFinishAggregator
            Case AggregatorType.AVERAGE_VERSION_DURATION : Return New AverageVersionDurationAggregator
        End Select
        Return Nothing
    End Function
End Class
