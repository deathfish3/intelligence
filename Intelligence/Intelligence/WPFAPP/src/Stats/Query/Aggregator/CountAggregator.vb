﻿''' <summary>An aggregator for returning the total number of tasks in a group.</summary>
Public Class CountAggregator
    Inherits GroupingAggregator

    ''' <summary>Get the total number of tasks in a group.</summary>
    Public Overrides Function getValue(group As Grouping) As Double
        Dim count = group.tasks.Count
        Return count
    End Function

    ''' <summary>An aggregator for returning the total number of tasks in a group.</summary>
    Public Overrides ReadOnly Property type As AggregatorType
        Get
            Return AggregatorType.COUNT
        End Get
    End Property
End Class