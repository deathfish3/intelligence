﻿''' <summary>An aggregator for returning the total number of bugs for all the tasks in the group combined.</summary>
Public Class TotalBugsAggregator
    Inherits GroupingAggregator

    ''' <summary>Get the total number of bugs for all the tasks in the group combined.</summary>
    Public Overrides Function getValue(group As Grouping) As Double
        Return (From task In group.tasks
                Select task.bugs.Count).Sum()
    End Function

    ''' <summary>An aggregator for returning the total number of bugs for all the tasks in the group combined.</summary>
    Public Overrides ReadOnly Property type As AggregatorType
        Get
            Return AggregatorType.TOTAL_BUGS
        End Get
    End Property
End Class