﻿Imports System.ComponentModel

''' <summary>
''' What kind of value the aggregator is designed to produce
''' (such as the number of tasks, the total number of bugs, the average duration etc.).
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum AggregatorType
    <Description("Count")>
    COUNT
    <Description("Total Bugs")>
    TOTAL_BUGS
    <Description("Average Time(hours) From Start to Finish")>
    AVERAGE_START_TO_FINISH_TIME
    <Description("Average Version Creation to Close Time(days)")>
    AVERAGE_VERSION_DURATION
End Enum