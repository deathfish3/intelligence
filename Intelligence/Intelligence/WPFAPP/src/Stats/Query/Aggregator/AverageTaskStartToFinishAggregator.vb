﻿''' <summary>
''' An aggregator for returning the average number of hours it took to complete each task in the group.
''' Will ignore all tasks that are not started yet, or not finished yet.
''' </summary>
Public Class AverageTaskStartToFinishAggregator
    Inherits GroupingAggregator

    ''' <summary>
    ''' Get the average number of hours it took to complete each task in the group.
    ''' Will ignore all tasks that are not started yet, or not finished yet.
    ''' </summary>
    Public Overrides Function getValue(group As Grouping) As Double
        Dim durations = (From task In group.tasks
                Where task.timeStarted IsNot Nothing AndAlso task.timeFinished IsNot Nothing
                Select task)
        Dim list = durations.ToList()
        If Not list.Any() Then
            Return 0
        Else
            Dim average = list.Average(Function(t) (t.timeFinished - t.timeStarted).Value.TotalHours)
            Return average
        End If
    End Function

    ''' <summary>An aggregator for returning the average number of hours it took to complete each task in the group.</summary>
    Public Overrides ReadOnly Property type As AggregatorType
        Get
            Return AggregatorType.AVERAGE_START_TO_FINISH_TIME
        End Get
    End Property
End Class