﻿Imports System.Collections.ObjectModel
Imports System.Text

''' <summary>
''' A base class for modular chunks that can be combined together
''' into a query to the Intelligence database that returns groups
''' of <see cref="TaskInstance"/>s from which stats can be generated
''' in the <see cref="StatsPanel"/>. This base contains properties common
''' amongst all sub-classes such as <see cref="QueryChunk.selectedField"/>,
''' as well as methods for importing and exporting query chunks from strings.
''' </summary>
Public MustInherit Class QueryChunk
    Inherits NotifyPropertyChangedBase

#Region "Common Fields"
    '#########################################################
    '#                     Common Fields                     #
    '#########################################################

    ''' <summary>
    ''' Identifies what kind of query chunk this is.
    ''' Subclasses must inherit this and retrun a value
    ''' from the <see cref="QueryType"/> enum so that they
    ''' can be correctly instantiated when importing from a string.
    ''' </summary>
    Public MustOverride ReadOnly Property type As QueryType

    ''' <summary>
    ''' All possible fields that can be selected for this query
    ''' (allows chunks to be available for the limited subset of
    ''' the enums <see cref="FieldType"/> that applies to them).
    ''' </summary>
    Protected MustOverride Property allFields As IEnumerable(Of FieldType)

    'Could bind to a dropdown displaying fields

    ''' <summary>The internally stored list of all the fields that the user can currently select.</summary>
    Private _availableFields As ObservableCollection(Of FieldType) = Nothing
    ''' <summary>
    ''' The list of all the fields that the user can currently select.
    ''' At the moment, this just mirrors <see cref="allFields"/>, but could
    ''' be extended later to contextually hide fields to avoid adding duplicate query chunks.
    ''' </summary>
    Public Overridable ReadOnly Property availableFields As ObservableCollection(Of FieldType)
        Get
            If _availableFields Is Nothing Then
                _availableFields = New ObservableCollection(Of FieldType)
                For Each f In allFields
                    _availableFields.Add(f)
                Next
            End If
            Return _availableFields
        End Get
    End Property

    ''' <summary>The internally stored field that this query chunk should act upon.</summary>
    Private _selectedField As FieldType
    ''' <summary>
    ''' The currently selected field this query chunk should act upon.
    ''' Setting this will notify the UI and call <see cref="OnSelectedFieldChanged"/>
    ''' so that subclasses can respond to it.
    ''' </summary>
    Public Property selectedField As FieldType
        Get
            If Not allFields.Contains(_selectedField) Then
                _selectedField = allFields.First
            End If
            Return _selectedField
        End Get
        Set(value As FieldType)
            setField(value, _selectedField, Sub(v) _selectedField = v)
            OnSelectedFieldChanged()
        End Set
    End Property


#End Region

#Region "Common Methods"
    '#########################################################
    '#                   Common Methods                      #
    '#########################################################

    ''' <summary>
    ''' The core functionality of a query chunk.
    ''' This takes a given query, and appends the constraints detailed by this chunk
    ''' to the existing query. The query is not executed here - it is simply built up.
    ''' Using this method, a query can be created by appending lots of separate modular
    ''' chunks in any order, which allows great flexibility for the user.
    ''' </summary>
    ''' <param name="query"></param>
    ''' <returns>
    ''' The old query with the constraints of this chunk appended to it.
    ''' This may return <c>Nothing</c> if the constraints in this chunk will return no results.
    ''' </returns>
    Public MustOverride Function addToQuery(query As IQueryable(Of TaskInstance)) As IQueryable(Of TaskInstance)

    ''' <summary>
    ''' Instantiate a new query chunk of the given type.
    ''' New types of query must be added to this method in order for
    ''' them to instantiate them correctly from the UI or from file.
    ''' </summary>
    ''' <param name="type">The type of query chunk to instantiate.</param>
    ''' <returns>
    ''' A new query chunk of the given type (or <c>Nothing</c> if the
    ''' type has not been added to this list yet).
    ''' </returns>
    Public Shared Function create(type As QueryType) As QueryChunk
        Select Case type
            Case QueryType.GROUP_BY : Return New GroupByChunk()
            Case QueryType.FILTER : Return New FilterChunk()
            Case QueryType.FILTER_TIME : Return New TimeFilterChunk()
            Case QueryType.FILTER_RELATIVE_TIME : Return New RelativeTimeChunk()
        End Select
        Return Nothing
    End Function

    ''' <summary>
    ''' Used by subclasses that need to respond to <see cref="selectedField"/>
    ''' changing. Does nothing by default, so must be overriden to provide
    ''' any custom behavoior required.
    ''' </summary>
    Protected Overridable Sub OnSelectedFieldChanged()
    End Sub

    ''' <summary>
    ''' Get the set of all possible values for the given field type.
    ''' Can be used to fill in gaps in groupings, or to display selectable
    ''' values in filters. When adding new fields to <see cref="FieldType"/>
    ''' this must be updated too, so that the field will work in filters and groupings.
    ''' </summary>
    ''' <param name="field">The field whos values need returned.</param>
    ''' <returns>
    ''' A list of all values the given field can have.
    ''' Will return an empty list if there are no values found for the given field.
    ''' </returns>
    Public Shared Function getAllFieldValues(field As FieldType) As IEnumerable
        Dim result As IEnumerable
        Using db As New IntelligenceDB
            Select Case field
                Case FieldType.ASSIGNEE : result = db.IntelligenceUsers.ToList
                Case FieldType.ASSIGNER : result = db.IntelligenceUsers.ToList
                Case FieldType.PLATFORM : result = db.Platforms.Where(Function(p) p.active).ToList
                Case FieldType.RESOLUTION : result = ResolutionUtils.userSelectableResolutions.ToList
                Case FieldType.VERSION : result = db.Versions.ToList
                Case FieldType.PRODUCT : result = db.Products.Where(Function(p) p.active).ToList
                Case FieldType.PRIORITY : result = db.Priorities.Where(Function(p) p.active).ToList
                Case Else : result = {}
            End Select
        End Using
        Return result
    End Function

#End Region

#Region "Import and Export"
    '#########################################################
    '#                Import and Export                      #
    '#########################################################

    ''' <summary>
    ''' Must be overriden by all subtasks to export the contents of this query chunk
    ''' to the given <see cref="StringBuilder"/>. Should use <see cref="exportField"/>
    ''' and <see cref="exportList"/> to handle the exporting and make sure the correct delimeters are written.
    ''' </summary>
    ''' <param name="sb">The <see cref="StringBuilder"/> to append the query chunk to.</param>
    Protected MustOverride Sub exportContentsTo(sb As StringBuilder)

    ''' <summary>
    ''' Must be overriden by all subtasks to import the contents of this query chunk
    ''' from the given list of strings (which represent each of the items fields as
    ''' exported in <see cref="exportContentsTo"/>).
    ''' </summary>
    ''' <param name="fieldStrings">
    ''' A list of all the fields in this query chunk as exported by <see cref="exportContentsTo"/>.
    ''' To read in field representing by a list of values, this should call <see cref="importList"/>.
    ''' </param>
    Protected MustOverride Sub importContentsFrom(fieldStrings As IEnumerable(Of String))


    ''' <summary>Separates two query chunks.</summary>
    Public Shared ReadOnly QUERY_DELIMITER As String = "//query//"
    ''' <summary>Separates the chart type from the rest of the query string.</summary>
    Public Shared ReadOnly CHART_DELIMITER As String = "//chart//"
    ''' <summary>Separates the boolean flag for displaying single-series or mult-series graphs from the rest of the query string.</summary>
    Public Shared ReadOnly SINGLE_GRAPH_DELIMITER As String = "//single_graph//"
    ''' <summary>Separates the aggregator type from the rest of the query string.</summary>
    Public Shared ReadOnly AGGREGATOR_DELIMITER As String = "//aggregator//"
    ''' <summary>Separates the query chunk type from the rest of the query chunk's string representation.</summary>
    Public Shared ReadOnly TYPE_DELIMITER As String = "//type//"
    ''' <summary>Separates two fields within a query chunk.</summary>
    Public Shared ReadOnly FIELD_DELIMITER As String = "//field//"
    ''' <summary>Separates two elements of a list within a field inside a query chunk.</summary>
    Public Shared ReadOnly LIST_ITEM_DELIMITER As String = "//item//"

    ''' <summary>Export this chunk by writing it to the given string builder.</summary>
    ''' <param name="sb">The string builder to export the query to.</param>
    Private Sub exportTo(sb As StringBuilder)
        sb.Append(QUERY_DELIMITER)
        sb.Append(Me.type)
        sb.Append(TYPE_DELIMITER)
        exportContentsTo(sb)
    End Sub

    ''' <summary>
    ''' Export the given list of queries and stats settings as a string
    ''' in a format that can be read back in later using <see cref="importQueriesFrom"/>.
    ''' </summary>
    ''' <param name="queryList">The list of all chunks that make up the query.</param>
    ''' <param name="aggregatorType">The type of <see cref="GroupingAggregator"/> used to produce the values to graph.</param>
    ''' <param name="chartType">The type of chart of graph to display the query results as.</param>
    ''' <param name="shouldDisplaySeperateGraphs">
    ''' If this is true, each graph will have only a single series. 
    ''' Otherwise, the lowest level graphs will be combined together as series on a single higher level graph.
    ''' </param>
    ''' <returns>
    ''' A string storing all the query data in a way that can be read back in using <see cref="importQueriesFrom"/>.
    ''' </returns>
    Public Shared Function exportQueries(queryList As IEnumerable(Of QueryChunk), aggregatorType As AggregatorType, chartType As ChartType, shouldDisplaySeperateGraphs As Boolean) As String
        Dim sb As New StringBuilder
        sb.Append(aggregatorType.ToString)
        sb.Append(AGGREGATOR_DELIMITER)
        sb.Append(chartType)
        sb.Append(CHART_DELIMITER)
        sb.Append(shouldDisplaySeperateGraphs)
        sb.Append(SINGLE_GRAPH_DELIMITER)
        For Each query In queryList
            query.exportTo(sb)
        Next
        Return sb.ToString
    End Function

    ''' <summary>
    ''' Reads in all the data necessary to display a list of queries
    ''' from a string of the format produced by <see cref="exportQueries"/>.
    ''' </summary>
    ''' <param name="inputString">A string of the format produced by <see cref="exportQueries"/> containing the queries.</param>
    ''' <returns>An object holding the list of query chunks, and the other stats settings necessary to display a stats graph.</returns>
    Public Shared Function importQueriesFrom(inputString As String) As ImportedQuery
        Dim result As New ImportedQuery

        If (inputString IsNot Nothing AndAlso inputString.Any()) Then

            Dim importedQueries As New List(Of QueryChunk)

            Dim aggregatorAndContents As String() = Split(inputString, AGGREGATOR_DELIMITER)
            [Enum].TryParse(aggregatorAndContents(0), result.aggregatorType)

            Dim chartAndContents As String() = Split(aggregatorAndContents(1), CHART_DELIMITER)
            [Enum].TryParse(chartAndContents(0), result.chartType)

            Dim singleGraphAndContents As String() = Split(chartAndContents(1), SINGLE_GRAPH_DELIMITER)
            If singleGraphAndContents.Length = 2 Then
                Boolean.TryParse(singleGraphAndContents(0), result.shouldDisplaySeperateGraphs)
            End If

            Dim queryStrings As String() = Split(singleGraphAndContents.Last, QUERY_DELIMITER)
            For Each queryString In queryStrings.Skip(1)
                Dim typeAndContents As String() = Split(queryString, TYPE_DELIMITER)
                If typeAndContents.Count = 2 Then
                    Dim typeString As String = typeAndContents(0)
                    Dim type As QueryType
                    Dim success = [Enum].TryParse(typeString, type)
                    If success Then
                        Dim newChunk As QueryChunk = QueryChunk.create(type)
                        Dim contents As String = typeAndContents(1)
                        Dim fieldStrings As String() = Split(contents, FIELD_DELIMITER)
                        newChunk.importContentsFrom(fieldStrings.Skip(1))
                        importedQueries.Add(newChunk)
                    End If
                End If
            Next

            result.chunks = importedQueries
        End If
        Return result
    End Function

    ''' <summary>A container for a list of query chunks, and other stats settings needed to display a stats graph.</summary>
    Public Class ImportedQuery
        Public chunks As IEnumerable(Of QueryChunk) = {}
        Public aggregatorType As AggregatorType = AggregatorType.COUNT
        Public chartType As ChartType = WPFAPP.ChartType.COLUMN
        Public shouldDisplaySeperateGraphs As Boolean = False
    End Class

    ''' <summary>Import a single query chunk from the given string representation.</summary>
    ''' <param name="importString">A string holding all the fields for this query chunk separated by <see cref="FIELD_DELIMITER"/>.</param>
    Protected Sub importFrom(importString As String)
        Dim fieldStringList As String() = Split(importString, FIELD_DELIMITER)
        importContentsFrom(fieldStringList)
    End Sub

    ''' <summary>
    ''' Export a list of values as strings separated by <see cref="LIST_ITEM_DELIMITER"/>
    ''' that can be read back in using <see cref="importList"/>. Values 
    ''' are converted to strings using their default ToString function.
    ''' </summary>
    ''' <typeparam name="T">The type of items in the list.</typeparam>
    ''' <param name="list">The list of items to export.</param>
    ''' <param name="sb">The stringbuilder to append the exported values to.</param>
    Protected Shared Sub exportList(Of T)(list As IEnumerable(Of T), sb As StringBuilder)
        exportList(list, Function(v) v.ToString, sb)
    End Sub

    ''' <summary>
    ''' Export a list of values as strings separated by <see cref="LIST_ITEM_DELIMITER"/>
    ''' that can be read back in using <see cref="importList"/>. Values 
    ''' are converted to strings using the custom <paramref name="toStringFunc"/> provided.
    ''' </summary>
    ''' <typeparam name="T">The type of items in the list.</typeparam>
    ''' <param name="list">The list of items to export.</param>
    ''' <param name="toStringFunc">A function to convert a item from the list into a string.</param>
    ''' <param name="sb">The stringbuilder to append the exported values to.</param>
    Protected Shared Sub exportList(Of T)(list As IEnumerable(Of T), toStringFunc As System.Func(Of T, String), sb As StringBuilder)
        sb.Append(FIELD_DELIMITER)
        For Each item In list
            sb.Append(LIST_ITEM_DELIMITER)
            sb.Append(toStringFunc(item))
        Next
    End Sub

    ''' <summary>
    ''' Import a list of values from the given string.
    ''' Values are separated using <see cref="LIST_ITEM_DELIMITER"/>, and strings
    ''' of this format can be produced by <see cref="exportList"/>.
    ''' </summary>
    ''' <typeparam name="T">The type of items in the new list.</typeparam>
    ''' <param name="inputString">A string representing the list value separated by <see cref="LIST_ITEM_DELIMITER"/>.</param>
    ''' <param name="convert">A function for converting the string representation of a single item into an object of type <paramref name="T"/>.</param>
    ''' <returns>A new list of all the values stored in the given string (which may be empty).</returns>
    Protected Shared Function importList(Of T)(inputString As String, convert As System.Func(Of String, T)) As IEnumerable(Of T)
        Dim stringList As String() = Split(inputString, LIST_ITEM_DELIMITER)
        Dim importedList As New List(Of T)
        For Each item In stringList.Skip(1)
            Dim listItem As T = convert(item)
            If listItem IsNot Nothing Then
                importedList.Add(listItem)
            End If
        Next
        Return importedList
    End Function

    ''' <summary>
    ''' Export a single field to the given <see cref="StringBuilder"/>.
    ''' The value will be converted to a string using its default ToString function.
    ''' </summary>
    ''' <typeparam name="T">The type of object to export.</typeparam>
    ''' <param name="value">The object to export.</param>
    ''' <param name="sb">The <see cref="StringBuilder"/> to export the object to.</param>
    Protected Shared Sub exportField(Of T)(value As T, sb As StringBuilder)
        exportField(value, Function(v) v.ToString, sb)
    End Sub

    ''' <summary>
    ''' Export a single field to the given <see cref="StringBuilder"/>.
    ''' The value will be converted to a string using the custom <paramref name="toStringFunc"/> provided.
    ''' </summary>
    ''' <typeparam name="T">The type of object to export.</typeparam>
    ''' <param name="value">The object to export.</param>
    ''' <param name="toStringFunc">A function to convert the given field value to a string.</param>
    ''' <param name="sb">The <see cref="StringBuilder"/> to export the object to.</param>
    Protected Shared Sub exportField(Of T)(value As T, toStringFunc As System.Func(Of T, String), sb As StringBuilder)
        sb.Append(FIELD_DELIMITER)
        sb.Append(toStringFunc(value))
    End Sub
#End Region

End Class
