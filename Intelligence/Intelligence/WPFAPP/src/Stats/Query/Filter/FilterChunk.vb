﻿Imports System.Collections.ObjectModel
Imports System.Linq.Expressions

Public Class FilterChunk
    Inherits QueryChunk

    Sub New()
        MyBase.New()
    End Sub

    Public Overrides Function addToQuery(query As IQueryable(Of TaskInstance)) As IQueryable(Of TaskInstance)

        Dim filteredResults As IQueryable(Of TaskInstance) = Nothing
        For Each acceptableValue As IHasID In acceptableFieldValues

            Dim condition As Expression(Of System.Func(Of TaskInstance, Boolean)) = Nothing

            Select Case Me.selectedField
                Case FieldType.ASSIGNEE : condition = Function(t) t.assignee.id = acceptableValue.id
                Case FieldType.ASSIGNER : condition = Function(t) t.assigner.id = acceptableValue.id
                Case FieldType.PLATFORM : condition = Function(t) t.platform.id = acceptableValue.id
                Case FieldType.RESOLUTION : condition = Function(t) t.resolution.id = acceptableValue.id
                Case FieldType.VERSION : condition = Function(t) t.version.id = acceptableValue.id
                Case FieldType.PRODUCT : condition = Function(t) t.version.product.id = acceptableValue.id
                Case FieldType.PRIORITY : condition = Function(t) t.priority.id = acceptableValue.id
            End Select

            If condition IsNot Nothing Then
                Dim chunk As IQueryable(Of TaskInstance) = query.Where(condition)
                filteredResults = If(filteredResults Is Nothing, chunk, filteredResults.Union(chunk))
            End If
        Next
        Return filteredResults
    End Function

    'Potentially useful

    '    Expression<Func<T, bool>> AndAll<T>(
    '    IEnumerable<Expression<Func<T, bool>>> expressions) {

    '    if(expressions == null) {
    '        throw new ArgumentNullException("expressions");
    '    }
    '    if(expressions.Count() == 0) {
    '        return t => true;
    '    }
    '    Type delegateType = typeof(Func<,>)
    '                            .GetGenericTypeDefinition()
    '                            .MakeGenericType(new[] {
    '                                typeof(T),
    '                                typeof(bool) 
    '                            }
    '                        );
    '    var combined = expressions
    '                       .Cast<Expression>()
    '                       .Aggregate((e1, e2) => Expression.AndAlso(e1, e2));
    '    return (Expression<Func<T,bool>>)Expression.Lambda(delegateType, combined);
    '}

    'Private Function FoldOr(orSoFar As System.Func(Of TaskInstance, Boolean), condition As System.Func(Of TaskInstance, Boolean)) As Expression(Of System.Func(Of TaskInstance, Boolean))
    '    Dim expression As Expression(Of System.Func(Of TaskInstance, Boolean)) = Function(t) orSoFar(t) OrElse condition(t)
    '    Return expression
    'End Function

    Protected Overrides Property allFields As IEnumerable(Of FieldType) = {
            FieldType.ASSIGNEE,
            FieldType.ASSIGNER,
            FieldType.PLATFORM,
            FieldType.RESOLUTION,
            FieldType.VERSION,
            FieldType.PRODUCT,
            FieldType.PRIORITY
        }

    Private _dbResults As IEnumerable(Of SelectedWrapper)
    Public ReadOnly Property dbResults As IEnumerable(Of SelectedWrapper)
        Get
            Dim results As New List(Of SelectedWrapper)
            Dim temp

            If _dbResults Is Nothing Then
                Using db As New IntelligenceDB

                    'Dim TaskKeyValues As IList(Of KeyValuePair(Of String, Integer))
                    ''init ilist
                    'TaskKeyValues = New List(Of KeyValuePair(Of String, Integer))

                    Select Case Me.selectedField
                        Case FieldType.ASSIGNEE : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of IntelligenceUser)(entry, entry.username, _acceptableFieldValues))
                            Next

                        Case FieldType.ASSIGNER : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of IntelligenceUser)(entry, entry.username, _acceptableFieldValues))
                            Next

                        Case FieldType.PLATFORM : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of Platform)(entry, entry.name, _acceptableFieldValues))
                            Next


                        Case FieldType.RESOLUTION : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of Resolution)(entry, entry.name, _acceptableFieldValues))
                            Next

                        Case FieldType.VERSION : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of Version)(entry, entry.name, _acceptableFieldValues))
                            Next

                        Case FieldType.PRODUCT : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of Product)(entry, entry.name, _acceptableFieldValues))
                            Next

                        Case FieldType.PRIORITY : temp = getAllFieldValues(Me.selectedField)
                            For Each entry In temp
                                results.Add(New SelectedWrapper(Of Priority)(entry, entry.name, _acceptableFieldValues))
                            Next

                    End Select
                    _dbResults = results
                End Using
            End If
            Return _dbResults
        End Get
    End Property

    Private _acceptableFieldValues As New ObservableCollection(Of IHasID)
    ''' <summary>
    ''' All values of the selected field that should be included 
    ''' in the query (all others are excluded).
    ''' </summary>
    Public ReadOnly Property acceptableFieldValues As ICollection(Of IHasID)
        Get
            Return _acceptableFieldValues
        End Get
    End Property

    Public Overrides ReadOnly Property type As QueryType
        Get
            Return QueryType.FILTER
        End Get
    End Property


    Public MustInherit Class SelectedWrapper
        Inherits NotifyPropertyChangedBase
        Public MustOverride ReadOnly Property id As Integer
        Public MustOverride Property isChecked As Boolean
    End Class
    Private Class SelectedWrapper(Of T As IHasID)
        Inherits SelectedWrapper

        Dim _name As String
        Dim acceptableFieldValueIn As ObservableCollection(Of IHasID)

        Sub New(item As T, name As String, collection As ObservableCollection(Of IHasID))
            _wrappedItem = item
            _name = name
            acceptableFieldValueIn = collection

        End Sub
        Dim _wrappedItem As T
        Public ReadOnly Property getWrappedItem
            Get
                Return _wrappedItem
            End Get
        End Property

        Public ReadOnly Property name
            Get

                Return _name

            End Get
        End Property

        Dim _isChecked As Boolean = False
        Public Overrides Property isChecked As Boolean
            Get
                Return _isChecked
            End Get
            Set(value As Boolean)
                setField(value, _isChecked, Sub(v) _isChecked = v)
                If isChecked = False Then
                    Dim matchingIdFromList = acceptableFieldValueIn.Where(Function(a) a.id = Me.id).FirstOrDefault()
                    If matchingIdFromList IsNot Nothing Then
                        acceptableFieldValueIn.Remove(matchingIdFromList)
                    End If
                ElseIf isChecked = True Then
                    acceptableFieldValueIn.Add(Me.getWrappedItem)
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property id As Integer
            Get
                Return getWrappedItem.id
            End Get
        End Property
    End Class

    Protected Overrides Sub OnSelectedFieldChanged()
        MyBase.OnSelectedFieldChanged()
        _acceptableFieldValues.Clear()
        Me._dbResults = Nothing
        onPropertyChanged(Function() Me.dbResults)
    End Sub

    Protected Overrides Sub exportContentsTo(sb As Text.StringBuilder)
        exportField(Me.selectedField, sb)
        exportList(Me.acceptableFieldValues, Function(i As IHasID) i.id.ToString, sb)
    End Sub

    Protected Overrides Sub importContentsFrom(fieldStrings As IEnumerable(Of String))
        [Enum].TryParse(fieldStrings(0), Me.selectedField)
        Dim importedList As IEnumerable(Of IHasID) = importList(fieldStrings(1),
                                                                Function(s)
                                                                    Dim id As Integer = 0
                                                                    Return If(Integer.TryParse(s, id), New emptyIDObject(id), Nothing)
                                                                End Function)
        Dim dbResults As IEnumerable(Of SelectedWrapper) = Me.dbResults
        For Each importedItem As IHasID In importedList
            Dim itemWithId = dbResults.Where(Function(res) res.id = importedItem.id).FirstOrDefault()
            If itemWithId IsNot Nothing Then
                itemWithId.isChecked = True
            End If
        Next
    End Sub

    Private Class emptyIDObject
        Implements IHasID
        Public Property id As Integer Implements IHasID.id
        Sub New(id As Integer)
            Me.id = id
        End Sub
    End Class
End Class
