﻿Public Class RelativeTimeChunk
    Inherits QueryChunk

    Public ReadOnly Property allTimeTypes As IEnumerable(Of TimeType)
        Get
            Return [Enum].GetValues(GetType(TimeUnitType))
        End Get
    End Property

    Private _timeType As TimeUnitType = TimeUnitType.WEEKS
    Public Property timeType As TimeUnitType
        Get
            Return _timeType
        End Get
        Set(value As TimeUnitType)
            setField(value, _timeType, Sub(v) _timeType = v)
        End Set
    End Property

    Private _timeAmount As Integer = 1
    Public Property timeAmount As Integer
        Get
            Return _timeAmount
        End Get
        Set(value As Integer)
            setField(value, _timeAmount, Sub(v) _timeAmount = v)
        End Set
    End Property

    Public Overrides Function addToQuery(query As IQueryable(Of TaskInstance)) As IQueryable(Of TaskInstance)
        Dim now As DateTime = DateTime.Now
        Dim startingPoint As DateTime
        Select Case Me.timeType
            Case TimeUnitType.HOURS : startingPoint = now.AddHours(-timeAmount)
            Case TimeUnitType.DAYS : startingPoint = now.AddDays(-timeAmount)
            Case TimeUnitType.WEEKS : startingPoint = now.AddDays(-timeAmount * 7)
            Case TimeUnitType.MONTHS : startingPoint = now.AddMonths(-timeAmount)
            Case TimeUnitType.YEARS : startingPoint = now.AddYears(-timeAmount)
        End Select

        Select Case Me.selectedField
            Case FieldType.CREATED_TIME : Return query.Where(Function(t) t.timeCreated >= startingPoint)
            Case FieldType.START_TIME : Return query.Where(Function(t) t.timeStarted IsNot Nothing AndAlso t.timeStarted >= startingPoint)
            Case FieldType.ASSIGNED_TIME : Return query.Where(Function(t) t.timeAssigned IsNot Nothing AndAlso t.timeAssigned >= startingPoint)
            Case FieldType.FINISH_TIME : Return query.Where(Function(t) t.timeFinished IsNot Nothing AndAlso t.timeFinished >= startingPoint)
            Case FieldType.VERSION_CREATED_TIME : Return query.Where(Function(t) t.version.timeCreated >= startingPoint)
            Case FieldType.VERSION_FINISHED_TIME : Return query.Where(Function(t) t.version.timeClosed IsNot Nothing AndAlso t.version.timeClosed >= startingPoint)
        End Select

        Return Nothing
    End Function

    Protected Overrides Property allFields As IEnumerable(Of FieldType) = {
        FieldType.CREATED_TIME,
        FieldType.START_TIME,
        FieldType.ASSIGNED_TIME,
        FieldType.FINISH_TIME,
        FieldType.VERSION_CREATED_TIME,
        FieldType.VERSION_FINISHED_TIME
    }

    Protected Overrides Sub exportContentsTo(sb As Text.StringBuilder)
        exportField(Me.selectedField, sb)
        exportField(Me.timeType, sb)
        exportField(Me.timeAmount, sb)
    End Sub

    Protected Overrides Sub importContentsFrom(fieldStrings As IEnumerable(Of String))
        [Enum].TryParse(fieldStrings(0), Me.selectedField)
        [Enum].TryParse(fieldStrings(1), Me.timeType)
        Integer.TryParse(fieldStrings(2), Me.timeAmount)
    End Sub

    Public Overrides ReadOnly Property type As QueryType
        Get
            Return QueryType.FILTER_RELATIVE_TIME
        End Get
    End Property
End Class
