﻿Imports System.Linq.Expressions

Public Class TimeFilterChunk
    Inherits QueryChunk

    Sub New()
        MyBase.New()
    End Sub

    'Enum.GetValues(TimeType) in a property and bind to it
    Public ReadOnly Property timeTypes As IEnumerable(Of TimeType)
        Get
            Dim enums As IEnumerable(Of TimeType) = [Enum].GetValues(GetType(TimeType))

            Return enums

        End Get
    End Property

    Public Overrides Function addToQuery(query As IQueryable(Of TaskInstance)) As IQueryable(Of TaskInstance)

        Dim condition As Expression(Of System.Func(Of TaskInstance, Boolean)) = Nothing

        If Me.timeFilterType = TimeType.AFTER Then
            Select Case Me.selectedField
                Case FieldType.CREATED_TIME : condition = Function(t) t.timeCreated >= Me.afterTime
                Case FieldType.START_TIME : condition = Function(t) t.timeStarted >= Me.afterTime
                Case FieldType.ASSIGNED_TIME : condition = Function(t) t.timeAssigned >= Me.afterTime
                Case FieldType.FINISH_TIME : condition = Function(t) t.timeFinished >= Me.afterTime
                Case FieldType.VERSION_CREATED_TIME : condition = Function(t) t.version.timeCreated >= Me.afterTime
                Case FieldType.VERSION_FINISHED_TIME : condition = Function(t) t.version.timeClosed >= Me.afterTime
            End Select
        ElseIf Me.timeFilterType = TimeType.BEFORE Then
            Select Case Me.selectedField
                Case FieldType.CREATED_TIME : condition = Function(t) t.timeCreated <= Me.beforeTime
                Case FieldType.START_TIME : condition = Function(t) t.timeStarted <= Me.beforeTime
                Case FieldType.ASSIGNED_TIME : condition = Function(t) t.timeAssigned <= Me.beforeTime
                Case FieldType.FINISH_TIME : condition = Function(t) t.timeFinished <= Me.beforeTime
                Case FieldType.VERSION_CREATED_TIME : condition = Function(t) t.version.timeCreated <= Me.beforeTime
                Case FieldType.VERSION_FINISHED_TIME : condition = Function(t) t.version.timeClosed <= Me.beforeTime
            End Select
        Else
            Select Case Me.selectedField
                Case FieldType.CREATED_TIME : condition = Function(t) t.timeCreated >= Me.afterTime AndAlso t.timeCreated <= Me.beforeTime
                Case FieldType.START_TIME : condition = Function(t) t.timeStarted >= Me.afterTime AndAlso t.timeStarted <= Me.beforeTime
                Case FieldType.ASSIGNED_TIME : condition = Function(t) t.timeAssigned >= Me.afterTime AndAlso t.timeAssigned <= Me.beforeTime
                Case FieldType.FINISH_TIME : condition = Function(t) t.timeFinished >= Me.afterTime AndAlso t.timeFinished <= Me.beforeTime
                Case FieldType.VERSION_CREATED_TIME : condition = Function(t) t.version.timeCreated >= Me.afterTime AndAlso t.version.timeCreated <= Me.beforeTime
                Case FieldType.VERSION_FINISHED_TIME : condition = Function(t) t.version.timeClosed >= Me.afterTime AndAlso t.version.timeClosed <= Me.beforeTime
            End Select
        End If
        If condition Is Nothing Then Return Nothing
        Return query.Where(condition)
    End Function

    ''' <summary>All fields that are times.</summary>
    Protected Overrides Property allFields As IEnumerable(Of FieldType) = {
            FieldType.CREATED_TIME,
            FieldType.START_TIME,
            FieldType.ASSIGNED_TIME,
            FieldType.FINISH_TIME,
            FieldType.VERSION_CREATED_TIME,
            FieldType.VERSION_FINISHED_TIME
        }

    Private _timeFilterType As TimeType
    ''' <summary>Whether times should be filtered before, after, or between certain dates.</summary>
    Public Property timeFilterType As TimeType
        Get
            Return _timeFilterType
        End Get
        Set(value As TimeType)
            setField(value, _timeFilterType, Sub(v) _timeFilterType = value)
        End Set
    End Property

    ''' <summary>
    ''' Only times before this time will be selected (if <see cref="timeFilterType"/> 
    ''' is <see cref="TimeType.BEFORE"/> or <see cref="TimeType.BETWEEN"/>).
    ''' </summary>
    Public Property beforeTime As DateTime

    ''' <summary>
    ''' Only times after this time will be selected (if <see cref="timeFilterType"/> 
    ''' is <see cref="TimeType.AFTER"/> or <see cref="TimeType.BETWEEN"/>).
    ''' </summary>
    Public Property afterTime As DateTime

    Public Overrides ReadOnly Property type As QueryType
        Get
            Return QueryType.FILTER_TIME
        End Get
    End Property

    Public ReadOnly Property testString As String
        Get
            Return "TestString!"
        End Get
    End Property

    Public ReadOnly Property timeNow As DateTime
        Get
            Debug.WriteLine(System.DateTime.Now)
            Return System.DateTime.Now
        End Get
    End Property

    Public ReadOnly Property minimumDate As DateTime
        Get
            Return System.DateTime.Parse("01/01/2005 00:00:01")
        End Get
    End Property

    Protected Overrides Sub exportContentsTo(sb As Text.StringBuilder)
        exportField(Me.selectedField, sb)
        exportField(Me.timeFilterType, sb)
        exportField(Me.beforeTime, sb)
        exportField(Me.afterTime, sb)
    End Sub

    Protected Overrides Sub importContentsFrom(fieldStrings As IEnumerable(Of String))
        [Enum].TryParse(fieldStrings(0), Me.selectedField)
        [Enum].TryParse(fieldStrings(1), Me.timeFilterType)
        Me.beforeTime = DateTime.Parse(fieldStrings(2))
        Me.afterTime = DateTime.Parse(fieldStrings(3))
    End Sub
End Class
