﻿Imports System.ComponentModel

''' <summary>
''' The different types of chunk that can be combined together to form
''' a query, such as filtering and grouping by different fields.
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum QueryType
    <Description("Filter")>
    FILTER
    <Description("Group By")>
    GROUP_BY
    <Description("Filter Absolute Time")>
    FILTER_TIME
    <Description("Filter Relative Time")>
    FILTER_RELATIVE_TIME
End Enum
