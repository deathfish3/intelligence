﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.DataVisualization.Charting
Imports WPFAPP.com.yoyogames.bugs


'This will be the datacontext for the stats panel
Public Class StatsViewModel
    Inherits NotifyPropertyChangedBase

    Private getGraphTreeView As System.Func(Of TreeView)

    Sub New(getGraphTreeView As System.Func(Of TreeView))
        Me.getGraphTreeView = getGraphTreeView
    End Sub

    Private _aggregatorType As AggregatorType = AggregatorType.COUNT

    Public Property aggregatorType As AggregatorType
        Get
            Return _aggregatorType
        End Get
        Set(value As AggregatorType)
            setField(value, aggregatorType, Sub(v) _aggregatorType = v)
            aggregator = GroupingAggregator.create(aggregatorType)
        End Set
    End Property

    Private aggregator As GroupingAggregator = GroupingAggregator.create(_aggregatorType)

    Private _queryChunks As New ObservableCollection(Of QueryChunk)
    Public ReadOnly Property queryChunks As ObservableCollection(Of QueryChunk)
        Get
            Return _queryChunks
        End Get
    End Property

    Public ReadOnly Property allQueryChunkTypes As IEnumerable(Of QueryType)
        Get
            Dim allQueryTypes As IEnumerable(Of QueryType) = [Enum].GetValues(GetType(QueryType))
            Return allQueryTypes
        End Get
    End Property

    Public ReadOnly Property allAggregatorTypes As IEnumerable(Of AggregatorType)
        Get
            Dim types As IEnumerable(Of AggregatorType) = [Enum].GetValues(GetType(AggregatorType))
            Return types
        End Get
    End Property

    'Bind command parameter to be a QueryType
    Public ReadOnly Property addCommand As ICommand
        Get
            Return New GenericCommand(
                Sub(type As QueryType)
                    _queryChunks.Add(QueryChunk.create(type))
                End Sub)
        End Get
    End Property

    Private statsQuery As StatsQueryWrapper
    Public Sub setStatsQuery(statsQuery As StatsQueryWrapper)
        saveQuery()
        Me.statsQuery = statsQuery
        If statsQuery IsNot Nothing Then
            loadChunksFromQueryString(statsQuery.queryString)
        Else
            Me.queryChunks.Clear()
        End If
        refreshGraphsCommand.Execute(Nothing)
    End Sub

    Public Sub loadChunksFromQueryString(inputString As String)
        Me.queryChunks.Clear()
        Dim importedQuery As QueryChunk.ImportedQuery = QueryChunk.importQueriesFrom(inputString)
        Me.chartType = importedQuery.chartType
        Me.aggregatorType = importedQuery.aggregatorType
        Me.shouldDisplaySeparateGraphs = importedQuery.shouldDisplaySeperateGraphs
        For Each query In importedQuery.chunks
            queryChunks.Add(query)
        Next
    End Sub

    Public ReadOnly Property saveQueryCommand As ICommand
        Get
            Return New GenericCommand(Sub() saveQuery())
        End Get
    End Property

    Public Sub saveQuery()
        If statsQuery IsNot Nothing Then
            statsQuery.queryString = QueryChunk.exportQueries(Me.queryChunks, Me.aggregatorType, Me.chartType, Me.shouldDisplaySeparateGraphs)
        End If
    End Sub

    'Command Parameter should be the QueryType you want to remove.
    Public ReadOnly Property removeCommand As ICommand
        Get
            Return New GenericCommand(
                Sub(toRemove As QueryChunk)
                    _queryChunks.Remove(toRemove)
                End Sub)
        End Get
    End Property

    Public ReadOnly Property refreshGraphsCommand As ICommand
        Get
            Return New GenericCommand(
                Sub()
                    executeQuery()
                    saveQuery()
                End Sub)
        End Get
    End Property

    Public ReadOnly Property generateDoc As ICommand
        Get
            Return New GenericCommand(
                Sub(targetProject)

                    Dim numerator As Integer = PDFGen.imageIndexToStartOn

                    Dim treeView = getGraphTreeView()
                    Dim thingToExport = TreeViewUtils.getAllLeafNodes(treeView)

                    For Each thing In thingToExport
                        Dim bounds As Rect = VisualTreeHelper.GetDescendantBounds(thing)

                        Dim renderBitmap As New RenderTargetBitmap(CInt(bounds.Width), CInt(bounds.Height), 96, 96, PixelFormats.Pbgra32)

                        Dim isolatedVisual As New DrawingVisual()
                        Using drawing As DrawingContext = isolatedVisual.RenderOpen()
                            ' Optional Background
                            drawing.DrawRectangle(Brushes.White, Nothing, New Rect(New Point(), bounds.Size))
                            'Actual image.
                            drawing.DrawRectangle(New VisualBrush(thing), Nothing, New Rect(New Point(), bounds.Size))
                        End Using


                        Dim uloz_obr As New Microsoft.Win32.SaveFileDialog()
                        uloz_obr.FileName = System.IO.Path.GetTempPath & "intelligence\" & "pdfimages\" & numerator.ToString & ".png"
                        uloz_obr.DefaultExt = "png"
                        System.IO.Directory.CreateDirectory(System.IO.Path.GetTempPath & "intelligence")
                        System.IO.Directory.CreateDirectory(System.IO.Path.GetTempPath & "intelligence\pdfimages")
                        If Not (My.Computer.FileSystem.FileExists(uloz_obr.FileName)) Then
                            Dim obr_cesta As String = uloz_obr.FileName

                            Using outStream As New System.IO.FileStream(obr_cesta, System.IO.FileMode.Create)
                                Dim encoder As New PngBitmapEncoder()
                                encoder.Frames.Add(BitmapFrame.Create(renderBitmap))
                                encoder.Save(outStream)
                            End Using
                        End If
                        numerator += 1
                    Next

                    Dim doc As New PDFGen(graphTree, Me.shouldDisplaySeparateGraphs)
                    doc.CompileDoc("TESTDOC" & numerator.ToString & ".pdf")

                    PDFGen.imageIndexToStartOn = numerator + 1


                End Sub)
        End Get
    End Property

    Private Sub executeQuery()
        graphTree.Clear()
        Using db As New IntelligenceDB

            Dim query As IQueryable(Of TaskInstance) = TaskInstanceUtils.filterLeafNodes(db.TaskInstances, db)

            Dim groupChunks As New List(Of GroupByChunk)

            For Each chunk In queryChunks
                If chunk.type = QueryType.GROUP_BY Then
                    groupChunks.Add(chunk)
                Else
                    query = chunk.addToQuery(query)
                    If query Is Nothing Then
                        Exit Sub
                    End If
                End If
            Next

            Debug.Print("")
            Debug.Print("")
            Debug.Print("Query start.")
            Debug.Print("")
            ' For Each result In query.ToList()
            'Debug.Print("Task {0} for {1} in {2} version {3} id {4} started: {5} finished {6} resolution {7}.", result.taskTemplate.name, If(result.assignee Is Nothing, "Nothing", result.assignee.username), result.taskTemplate.product.name, result.version.name, result.version.id, result.timeStarted, result.timeFinished, result.resolution.name)
            ' Next
            Debug.Print("")
            Debug.Print("Query end.")
            Debug.Print("")
            Debug.Print("")

            Dim childNodes = getGraphTree(query, groupChunks, 0)
            Dim allAsGrouping As Grouping = New Grouping("All", "All", query, FieldType.TASK_TEMPLATE)
            Dim children As IEnumerable(Of GraphTreeNode) = getGraphTree(query, groupChunks, 0)
            Dim rootNode As New GraphTreeNode(allAsGrouping, Me.aggregator, childNodes, 0, groupChunks.Count + 1)

            graphTree.Add(rootNode)
        End Using


        onPropertyChanged(Function() Me.graphTree)

    End Sub

    Private Function getGraphTree(tasksToGroup As IQueryable(Of TaskInstance), groupChunks As IList(Of GroupByChunk), groupChunkIndex As Integer) As IEnumerable(Of GraphTreeNode)
        Dim nodeList As New List(Of GraphTreeNode)

        If groupChunkIndex < groupChunks.Count Then
            Dim groupChunk As GroupByChunk = groupChunks.Item(groupChunkIndex)
            Dim groups As IEnumerable(Of Grouping) = groupChunk.splitIntoGroups(tasksToGroup)

            For Each group As Grouping In groups
                Dim distanceFromLeaves As Integer = groupChunks.Count - groupChunkIndex - 1
                Dim children As IEnumerable(Of GraphTreeNode) = getGraphTree(group.tasks, groupChunks, groupChunkIndex + 1)
                Dim newNode As New GraphTreeNode(group, Me.aggregator, children, groupChunkIndex + 1, groupChunks.Count + 1)
                nodeList.Add(newNode)
            Next

        End If

        Return nodeList

    End Function

    Public Property graphTree As New ObservableCollection(Of GraphTreeNode)

    Public ReadOnly Property chartTypes As IEnumerable(Of ChartType)
        Get
            Return [Enum].GetValues(GetType(ChartType))
        End Get
    End Property

    Private _shouldDisplaySeparateGraphs As Boolean
    Public Property shouldDisplaySeparateGraphs As Boolean
        Get
            Return _shouldDisplaySeparateGraphs
        End Get
        Set(value As Boolean)
            setField(value, _shouldDisplaySeparateGraphs, Sub(v) _shouldDisplaySeparateGraphs = v)
        End Set
    End Property

    Private _chartType As ChartType = chartTypes.FirstOrDefault()
    Public Property chartType As ChartType
        Get
            Return _chartType
        End Get
        Set(value As ChartType)
            setField(value, _chartType, Sub(v) _chartType = v)
        End Set
    End Property

    Public Interface IGraph
        ReadOnly Property seriesList As IEnumerable(Of ISeries)
    End Interface

    Public Interface ISeries
        ReadOnly Property dataPoints As IEnumerable(Of IDataPoint)
    End Interface

    Public Interface IDataPoint
        ReadOnly Property Key As String
        ReadOnly Property Value As Double
    End Interface

    Dim _mantisprojects As ProjectData()
    Public ReadOnly Property availableProjects
        Get
            Dim mantisClient As New MantisConnect
            Dim projectsDown = mantisClient.mc_projects_get_user_accessible(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)

            For Each project In projectsDown
                _mantisprojects.Add(project)
                If project.subprojects IsNot Nothing Then
                    For Each subproject In project.subprojects
                        subproject.name = "  >>  " & subproject.name
                        _mantisprojects.Add(subproject)
                    Next
                End If
            Next


            Return _mantisprojects
        End Get
    End Property

    Public Class GraphTreeNode
        Implements IGraph, ISeries, IDataPoint

        Public Sub New(grouping As Grouping, aggregator As GroupingAggregator, children As IEnumerable(Of GraphTreeNode), nodeHeight As Integer, totalGraphTreeHeight As Integer)
            Me.grouping = grouping
            Me.aggregator = aggregator
            Me.nodeHeight = nodeHeight
            Me._children = children
            Me.totalGraphTreeHeight = totalGraphTreeHeight
            If distanceFromLeaves = 0 Then
                Dim unusedVariableToLoadValue = Me.Value
            End If
        End Sub

        Private _children As IEnumerable(Of GraphTreeNode)
        Public ReadOnly Property children As ICollection(Of GraphTreeNode)
            Get
                Return _children
            End Get
        End Property

        Public ReadOnly Property childrenForMultigraph As ICollection(Of GraphTreeNode)
            Get
                Return If(isMultiGraphNode, {}, children)
            End Get
        End Property

        Public ReadOnly Property childrenForSinglegraph As ICollection(Of GraphTreeNode)
            Get
                Return If(isSingleGraphNode, {}, children)
            End Get
        End Property

        Private aggregator As GroupingAggregator
        Private grouping As Grouping

        Private _value As Double?
        Public ReadOnly Property Value As Double Implements IDataPoint.Value
            Get
                If _value Is Nothing Then
                    _value = aggregator.getValue(grouping)
                End If
                Return _value.Value
            End Get
        End Property

        Public ReadOnly Property Key As String Implements IDataPoint.Key
            Get
                Return grouping.keyString
            End Get
        End Property

        Public ReadOnly Property keyField As String
            Get
                Return EnumToStringConverter.enumToString(grouping.keyField)
            End Get
        End Property

        Public ReadOnly Property multiGraphTitle As String
            Get
                Dim prefix As String = "Tasks"
                Dim suffix As String = ""
                If children.Any() Then
                    Dim firstChild = children.First()
                    suffix = " by " & firstChild.keyField
                    If firstChild.children.Any() Then
                        Dim firstGrandChild = firstChild.children.First
                        prefix = firstGrandChild.keyField & "s"
                    End If
                End If
                Return prefix & suffix
            End Get
        End Property

        Public ReadOnly Property groupTitle As String
            Get
                Return If(isRootNode, "All", "Tasks where " & keyField & " is " & Key)
            End Get
        End Property

        Public ReadOnly Property graphTitle As String
            Get
                Return If(children.Any(), "Tasks by " & children.First.keyField, "All")
            End Get
        End Property

        Private totalGraphTreeHeight As Integer
        Private nodeHeight As Integer

        Private ReadOnly Property distanceFromLeaves As Integer
            Get
                Return totalGraphTreeHeight - nodeHeight - 1
            End Get
        End Property

        Public ReadOnly Property isMultiGraphNode As Boolean
            Get
                Return distanceFromLeaves = 2 OrElse totalGraphTreeHeight <= 2
            End Get
        End Property

        Public ReadOnly Property isSingleGraphNode As Boolean
            Get
                Return distanceFromLeaves = 1 OrElse totalGraphTreeHeight <= 1
            End Get
        End Property

        Public ReadOnly Property isRootNode As Boolean
            Get
                Return nodeHeight = 0
            End Get
        End Property

        Public ReadOnly Property seriesList As IEnumerable(Of ISeries) Implements IGraph.seriesList
            Get
                Return If(distanceFromLeaves > 1, Me.children, {Me})
            End Get
        End Property

        Public ReadOnly Property dataPoints As IEnumerable(Of IDataPoint) Implements ISeries.dataPoints
            Get
                Return If(distanceFromLeaves > 0, Me.children, {Me})
            End Get
        End Property


    End Class

End Class
