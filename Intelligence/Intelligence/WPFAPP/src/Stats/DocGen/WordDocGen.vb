﻿Imports Novacode
Imports WPFAPP.com.yoyogames.bugs
Imports Microsoft.Office.Interop

''' <summary>
''' Contains Functions to generate Word Documents using OFfice.Interop API.
''' NOTE: Office.Interop is very fiddly to use, so edit with care.
''' </summary>
''' <remarks></remarks>
Public Class WordDocGen

    Private mantisIssuesByRes As IssueData()
    ''' <summary>
    ''' Creates a document using imported Report Data.
    ''' </summary>
    ''' <param name="newReport">The report data to generate the report with.</param>
    ''' <param name="toggleSummary">Controls whether Summary section is printed (Useful for large timescales to prevent clogging)</param>
    ''' <param name="fileName">A custom filename, or the reportData filename.</param>
    ''' <returns>Returns a status message as String.</returns>
    ''' <remarks></remarks>
    Public Function CreateDocument(newReport As IStatsReportData, toggleSummary As Boolean, fileName As String) As String

        Dim messageToReturn = "Unknown error generating word document."

        'Start Word and open the document template.
        Dim oWord As Word.Application = CreateObject("Word.Application")
        Try
            oWord.Visible = False
        Catch e As Exception
        End Try

        Dim oDoc As Word.Document = oWord.Documents.Add

        Me.addHeaders(oDoc, newReport, toggleSummary)

        If toggleSummary = False Then
            printListOfIssues(oDoc, newReport.unresolvedTasksThisTimespan)
        End If

        Me.printTable(oDoc, newReport.totalOpenTasksBySeverityTable, Function(s) s.name)

        If toggleSummary = False Then
            printListOfIssues(oDoc, newReport.unresolvedCriticalTasks)
        End If

        'Insert a chart and change the chart.
        Me.printGraph(oDoc, oWord, newReport.totalOpenTasksBySeverityTable, Function(s) s.name)

        Me.printTable(oDoc, newReport.totalTasksOpenedPerTimespanBySeverityTable, Function(s) s.name)

        Me.printGraph(oDoc, oWord, newReport.totalTasksOpenedPerTimespanBySeverityTable, Function(s) s.name)

        Dim openedVSReolvedTable = New Dictionary(Of String, IList(Of Integer))
        openedVSReolvedTable.Add("Opened", newReport.totalTasksOpenedPerTimespanList)
        openedVSReolvedTable.Add("Resolved", newReport.totalTasksResolvedPerTimespanList)

        Me.printTable(oDoc, openedVSReolvedTable)
        Me.printGraph(oDoc, oWord, openedVSReolvedTable)
        Me.printPie(oDoc, oWord, newReport.totalTasksOpenedPerTimespanBySeverityTable, Function(s) s.name)

        Me.addNewLineAtEndOfDocument(oDoc)


        Try
            oDoc.SaveAs2(fileName)
            messageToReturn = "Successfully generated report and saved it to " & fileName
        Catch e As Exception
            messageToReturn = "Error saving: " & e.Message
        End Try

        Try
            oWord.Visible = True
        Catch e As Exception
            messageToReturn += (Environment.NewLine & "Error opening Word: " & e.Message)
        End Try

        ' Process.Start("WINWORD.EXE", oWord.Path)
        Return messageToReturn
    End Function

    ''' <summary>
    ''' Adds a newline at end of document (equivalent to pressing enter twice in Word) 
    ''' </summary>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <remarks></remarks>
    Private Sub addNewLineAtEndOfDocument(oDoc As Word.Document)
        Dim oRng As Word.Range = oDoc.Bookmarks.Item("\endofdoc").Range
        oRng.ListFormat.RemoveNumbers()
        oRng.InsertParagraphAfter()
    End Sub

    ''' <summary>
    ''' Adds the headers of a report: Title, Hyperlinks, Report Timescale, Opened and Resolved counts.
    ''' </summary>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="newReport">Reference to the report data to retrieve data from.</param>
    ''' <param name="toggleSummary">Whether or not to write the summary section.</param>
    ''' <remarks></remarks>
    Private Sub addHeaders(oDoc As Word.Document, newReport As IStatsReportData, toggleSummary As Boolean)

        Dim oTitle As Word.Paragraph = oDoc.Content.Paragraphs.Add
        oTitle.Range.Font.Name = "Segoe UI"
        oTitle.Range.Font.Size = 18
        oTitle.Range.Text = newReport.title
        oTitle.Range.InsertParagraphAfter()

        Dim hyper As Word.Paragraph = oDoc.Content.Paragraphs.Add(oDoc.Bookmarks.Item("\endofdoc").Range)
        hyper.Range.Font.Size = 10
        Dim address As Object = "http://bugs.yoyogames.com/summary_page.php"
        hyper.Range.Hyperlinks.Add(hyper.Range, address, TextToDisplay:="Mantis Summary")
        hyper.Range.InsertParagraphAfter()

        Dim oReportTimescale As Word.Paragraph = oDoc.Content.Paragraphs.Add(oDoc.Bookmarks.Item("\endofdoc").Range)
        oReportTimescale.Range.Font.Size = 10
        oReportTimescale.Range.Font.Italic = True
        oReportTimescale.Range.Text = "Covers " & newReport.endDate.ToShortDateString & " to " & newReport.startDate.ToShortDateString
        oReportTimescale.Range.InsertParagraphAfter()

        Dim oProjectHeaderOpened As Word.Paragraph = oDoc.Content.Paragraphs.Add(oDoc.Bookmarks.Item("\endofdoc").Range)
        oProjectHeaderOpened.Range.Font.Size = 10
        oProjectHeaderOpened.Range.Font.Bold = True
        Dim openedResolvedString As String
        Dim openedDeltaString = getDeltaString(newReport.totalTasksOpenedThisTimespan, newReport.totalTasksOpenedLastTimespan)
        Dim resolvedDeltaString = getDeltaString(newReport.totalTasksResolvedThisTimespan, newReport.totalTasksResolvedLastTimespan)
        openedResolvedString = "Opened " & newReport.totalTasksOpenedThisTimespan & " " & openedDeltaString & ", Resolved " & newReport.totalTasksResolvedThisTimespan & " " & resolvedDeltaString
        Dim mantisZendeskString As String
        mantisZendeskString = "Unassigned(New): Mantis " & newReport.totalUnassignedMantisIssuesThisTimespan & ", Zendesk " & newReport.totalOpenZendeskTickets
        If toggleSummary = False Then
            Dim importantUnresolvedString As String
            importantUnresolvedString = "Important unresolved this week:"
            oProjectHeaderOpened.Range.Text = newReport.projectName & " - " & newReport.totalOpenTasks & " Open" & vbCrLf & openedResolvedString & vbCrLf & mantisZendeskString & vbCrLf & importantUnresolvedString
        Else
            oProjectHeaderOpened.Range.Text = newReport.projectName & " - " & newReport.totalOpenTasks & " Open" & vbCrLf & openedResolvedString & vbCrLf & mantisZendeskString
        End If
        oProjectHeaderOpened.Range.InsertParagraphAfter()

    End Sub

    ''' <summary>
    ''' Prints a list of issues. Used for Important Issues this timescale, and for critical issues.
    ''' </summary>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="issues">The issues as <see cref="IssueHeaderData"/> to print.</param>
    ''' <remarks></remarks>
    Private Sub printListOfIssues(oDoc As Word.Document, issues As IEnumerable(Of IssueHeaderData))

        Dim para As Word.Paragraph = oDoc.Content.Paragraphs.Add

        For Each issue In issues
            para.Range.ListFormat.ApplyBulletDefault(Word.WdDefaultListBehavior.wdWord10ListBehavior)
            Dim idString As String = Nothing
            Dim summary As String = Nothing
            Dim handler As AccountData = Nothing
            Dim status As ObjectRef = Nothing
            Dim handlerInString As String = Nothing
            Dim statusInString As String = Nothing

            idString = issue.id
            summary = issue.summary
            handler = MantisCacheManager.userById(issue.handler)
            status = MantisCacheManager.statusById(issue.status)

            If (handler IsNot Nothing AndAlso Not handler.name.Equals("")) Then
                handlerInString = "[" & handler.name & "]"
            End If

            If (status IsNot Nothing AndAlso Not status.name.Equals("")) Then
                statusInString = "[" & status.name & "]"
            End If

            Dim concat As String = idString & ": " & summary & " | " & handlerInString & statusInString

            para.Range.Text = concat
            para.Range.Font.Bold = False
            para.Range.Font.Size = 10
            para.Range.ListFormat.ApplyBulletDefault(Word.WdDefaultListBehavior.wdWord10ListBehavior)
            para.Range.InsertParagraphAfter()
        Next
    End Sub

    ''' <summary>
    ''' Prints a table displaying data based on a timescale and specific content.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="table">The Dictionary with Key of type T, and IList(Of Integer).</param>
    ''' <param name="toStringFunction">The string to write as a lambda, example: Function(s) s.name - This returns the name of the Key.</param>
    ''' <remarks></remarks>
    Private Sub printTable(Of T)(oDoc As Word.Document, table As Dictionary(Of T, IList(Of Integer)), Optional toStringFunction As System.Func(Of T, String) = Nothing)
        Dim para = oDoc.Content.Paragraphs.Add()
        para.Range.ListFormat.RemoveNumbers()

        If table Is Nothing OrElse table.Count = 0 Then Return

        Dim oDeltaTable As Word.Table

        Dim totalRows As Integer = table.Count
        Dim totalTimeSlices = table.First.Value.Count
        Dim totalCols As Integer = (totalTimeSlices * 2)

        oDeltaTable = oDoc.Tables.Add(oDoc.Bookmarks.Item("\endofdoc").Range, totalRows, totalCols)
        oDeltaTable.Range.ListFormat.ApplyBulletDefault(Word.WdDefaultListBehavior.wdWord10ListBehavior)
        oDeltaTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent)
        oDeltaTable.Rows.SetHeight(12, Word.WdRowHeightRule.wdRowHeightExactly)

        Dim row As Integer = 1
        For Each key In table.Keys
            Dim col As Integer = 1
            oDeltaTable.Cell(row, col).Range.Text = If(toStringFunction Is Nothing, key.ToString, toStringFunction(key))
            oDeltaTable.Cell(row, col).Range.Font.Size = 10
            oDeltaTable.Cell(row, col).Range.Bold = True

            Dim totalsForKey = table.Item(key)
            For i As Integer = 0 To totalsForKey.Count - 1
                col += 1

                oDeltaTable.Cell(row, col).Range.Text = totalsForKey.Item(i)
                oDeltaTable.Cell(row, col).Range.Font.Size = 10

                If i <= totalsForKey.Count - 2 Then
                    col += 1
                    Dim deltaString As String = getDeltaString(totalsForKey.Item(i), totalsForKey(i + 1))
                    oDeltaTable.Cell(row, col).Range.Text = (deltaString)
                    oDeltaTable.Cell(row, col).Range.Font.Size = 10
                End If
            Next
            row += 1
        Next
    End Sub

    ''' <summary>
    ''' Prints a line-graph to display trends. Reads right to left, with left being startDate.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="oWord">The Word application.</param>
    ''' <param name="table">The Dictionary with Key of type T, and IList(Of Integer).</param>
    ''' <param name="toStringFunction">The string to write as a lambda, example: Function(s) s.name - This returns the name of the Key.</param>
    ''' <remarks></remarks>
    Private Sub printGraph(Of T)(oDoc As Word.Document, oWord As Word.Application, table As Dictionary(Of T, IList(Of Integer)), Optional toStringFunction As System.Func(Of T, String) = Nothing)
        If table Is Nothing OrElse table.Count = 0 Then Return

        Dim oShape As Word.InlineShape = oDoc.Bookmarks.Item("\endofdoc").Range.InlineShapes.AddOLEObject(
            ClassType:="MSGraph.Chart.8",
            FileName:="",
            LinkToFile:=False,
            DisplayAsIcon:=False)

        Dim oChart As Graph.Chart = oShape.OLEFormat.Object
        oChart.ChartType = Microsoft.Office.Interop.Graph.XlChartType.xlLineMarkers
        Dim dataSheet As Microsoft.Office.Interop.Graph.DataSheet = oChart.Application.DataSheet

        dataSheet.Cells.Clear()

        Dim totalRows As Integer = table.Count
        Dim totalTimeSlices = table.First.Value.Count

        Dim i As Integer = 1
        dataSheet.Cells(1, 1) = "X Values"
        For counter = 0 To totalTimeSlices - 1 Step 1
            dataSheet.Cells(1, counter + 2) = "/" & counter + 1 & "/"
        Next


        For Each Key In table.Keys
            Dim j As Integer = 1
            For Each item In table(Key)
                If j = 1 Then
                    dataSheet.Cells(i + 1, j) = If(toStringFunction Is Nothing, Key.ToString, toStringFunction(Key))
                    dataSheet.Cells(i + 1, j + 1) = item
                    j = j + 1
                Else
                    dataSheet.Cells(i + 1, j + 1) = item
                    j += 1
                End If
            Next
            i += 1
        Next
        Dim seriesCollection As Microsoft.Office.Interop.Graph.SeriesCollection = oChart.SeriesCollection()
        For Each item As Microsoft.Office.Interop.Graph.Series In seriesCollection
            item.Border.Weight = 3
        Next

        oChart.Application.Update()
        oChart.Application.Quit()
        'If desired, you can proceed from here using the Microsoft Graph 
        'Object model on the oChart object to make additional changes to the
        'chart.
        oShape.Width = oWord.InchesToPoints(6.25)
        oShape.Height = oWord.InchesToPoints(3.7)
    End Sub

    ''' <summary>
    ''' Iterator Sub for printing piecharts. Will print one piechart for each value in a dictionary key.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="oWord">The Word application.</param>
    ''' <param name="table">The Dictionary with Key of type T, and IList(Of Integer).</param>
    ''' <param name="toStringFunction">The string to write as a lambda, example: Function(s) s.name - This returns the name of the Key.</param>
    ''' <remarks></remarks>
    Private Sub printPie(Of T)(oDoc As Word.Document, oWord As Word.Application, table As Dictionary(Of T, IList(Of Integer)), Optional toStringFunction As System.Func(Of T, String) = Nothing)
        If table Is Nothing OrElse table.Count = 0 Then Return
        For slice = 0 To table.First.Value.Count - 1 Step 1
            singlePie(oDoc, oWord, table, slice, toStringFunction)
        Next

    End Sub

    ''' <summary>
    ''' Calculates the difference between two integers.
    ''' </summary>
    ''' <param name="current">The current value as Integer.</param>
    ''' <param name="prev">The previous value as Integer.</param>
    ''' <returns>Returns a string of the delta value ["(+/-val)]</returns>
    ''' <remarks></remarks>
    Private Function getDeltaString(current As Integer, prev As Integer) As String
        Dim delta As Integer = current - prev
        Dim sign As String = If(delta >= 0, "+", "")
        Return "(" & sign & delta.ToString & ")"
    End Function

    ''' <summary>
    ''' Prints a single pie-chart.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="oDoc">The document to act on.</param>
    ''' <param name="oWord">The Word application.</param>
    ''' <param name="table">The Dictionary with Key of type T, and IList(Of Integer).</param>
    ''' <param name="slice">The current iteration from calling function.</param>
    ''' <param name="toStringFunction">The string to write as a lambda, example: Function(s) s.name - This returns the name of the Key.</param>
    ''' <remarks></remarks>
    Private Sub singlePie(Of T)(oDoc As Word.Document, oWord As Word.Application, table As Dictionary(Of T, IList(Of Integer)), slice As Integer, Optional toStringFunction As System.Func(Of T, String) = Nothing)
        Dim oShape As Word.InlineShape = oDoc.Bookmarks.Item("\endofdoc").Range.InlineShapes.AddOLEObject(
            ClassType:="MSGraph.Chart.8",
            FileName:="",
            LinkToFile:=False,
            DisplayAsIcon:=False)



        Dim oChart As Graph.Chart = oShape.OLEFormat.Object
        oChart.HasTitle = True
        oChart.ChartType = Microsoft.Office.Interop.Graph.XlChartType.xlPie
        Dim dataSheet As Microsoft.Office.Interop.Graph.DataSheet = oChart.Application.DataSheet

        dataSheet.Cells.Clear()

        Dim totalRows As Integer = table.Count
        Dim totalTimeSlices = table.First.Value.Count


        Dim i As Integer = slice
        dataSheet.Cells(1, 1) = "X Values" 'TimeSlices is causing an issue here. Cuts off severities too early
        For counter = 0 To table.Keys.Count - 1 Step 1
            dataSheet.Cells(1, counter + 2) = If(toStringFunction Is Nothing, table.Keys(counter).ToString, toStringFunction(table.Keys(counter)))
        Next


        Dim j As Integer = 1
        For Each Key In table.Keys
            If j = 1 Then
                dataSheet.Cells(2, j) = "Slice " & i + 1
                dataSheet.Cells(2, j + 1) = table(Key)(i)
                j += 1
            Else
                dataSheet.Cells(2, j + 1) = table(Key)(i)
                j += 1
            End If

        Next


        Dim seriesCollection As Microsoft.Office.Interop.Graph.SeriesCollection = oChart.SeriesCollection()
        For Each item As Microsoft.Office.Interop.Graph.Series In seriesCollection
            item.Border.Weight = 3
        Next

        oChart.Application.Update()
        oChart.Application.Quit()
        'If desired, you can proceed from here using the Microsoft Graph 
        'Object model on the oChart object to make additional changes to the
        'chart.
        oShape.Width = oWord.InchesToPoints(6.25)
        oShape.Height = oWord.InchesToPoints(3.7)
    End Sub
End Class
