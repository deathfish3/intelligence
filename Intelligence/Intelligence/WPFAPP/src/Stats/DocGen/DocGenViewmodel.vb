﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' The Viewmodel for binding to/invoking Document Generation commands/properties.
''' </summary>
''' <remarks></remarks>
Public Class DocGenViewmodel
    Inherits NotifyPropertyChangedBase

	Private zenLoader As ZendeskLoader
	Private numOpenZendeskTickets As Integer = -1

	Private _timeController As New RelativeTimeControl
    ''' <summary>
    ''' All time related properties.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Access to RelativeTimeControl.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property timeControl As RelativeTimeControl
        Get
            Return _timeController
        End Get

    End Property

    Private _toggleSummary As Boolean = False
    ''' <summary>
    ''' Gets or Sets whether Summary section should be printed on document.
    ''' </summary>
    ''' <value></value>
    ''' <returns>_toggleSummary, which can be true/false.</returns>
    ''' <remarks></remarks>
    Public Property toggleSummary As Boolean
        Get
            Return _toggleSummary
        End Get
        Set(value As Boolean)
            _toggleSummary = value
        End Set
    End Property

    ''' <summary>
    ''' Executes Custom Generate Document Command on target Project. Consumes a Project as ProjectData.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Generic Command (that generates document).</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property generateDoc As ICommand
        Get
            Return New GenericCommand(
                Sub(targetProject As ProjectData)
                    If targetProject Is Nothing Then
                        MessageDialog.display("No Project Selected", "Please select a Mantis project to generate a report for.")
                        Exit Sub
                    End If

                    generateSpecificReport(targetProject, timeControl.timeType, timeControl.sliceAmount)

                End Sub)
        End Get
    End Property

    ''' <summary>
    ''' Executes Weekly Generate Document Command on target Project. Consumes a Project as ProjectData.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Generic Command (that generates document).</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property generateWeeklyDoc As ICommand
        Get
            Return New GenericCommand(
                Sub(targetProject As ProjectData)

                    generateSpecificReport(targetProject, TimeUnitType.WEEKS, timeControl.sliceAmount)

                End Sub)
        End Get
    End Property

    ''' <summary>
    ''' Executes Monthly Generate Document Command on target Project. Consumes a Project as ProjectData.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Generic Command (that generates document).</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property generateMonthlyDoc As ICommand
        Get
            Return New GenericCommand(
                Sub(targetProject As ProjectData)

                    generateSpecificReport(targetProject, TimeUnitType.MONTHS, timeControl.sliceAmount)

                End Sub)
        End Get
    End Property

    ''' <summary>
    ''' Executes Yearly Generate Document Command on target Project. Consumes a Project as ProjectData.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Generic Command (that generates document).</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property generateYearlyDoc As ICommand
        Get
            Return New GenericCommand(
                Sub(targetProject As ProjectData)

                    generateSpecificReport(targetProject, TimeUnitType.YEARS, timeControl.sliceAmount)

                End Sub)
        End Get
    End Property

    Private _mantisprojects As ProjectData()
    ''' <summary>
    ''' Retrieves all available Mantis Projects
    ''' </summary>
    ''' <value></value>
    ''' <returns>Mantis Projects as Array of ProjectData</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property availableProjects
        Get
            Return _mantisprojects
        End Get
    End Property

    ''' <summary>
    ''' Retrieves all user-accessible Mantis Projects.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub initMantisProjects()
        Dim mantisClient As New MantisConnect
        Dim projectsDown = mantisClient.mc_projects_get_user_accessible(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)

        For Each project In projectsDown
            _mantisprojects.Add(project)
            If project.subprojects IsNot Nothing Then
                For Each subproject In project.subprojects
                    subproject.name = "  >>  " & subproject.name
                    _mantisprojects.Add(subproject)
                Next
            End If
        Next
        onPropertyChanged(Function() Me.availableProjects)
        resetSelectedProject()
    End Sub

    ''' <summary>
    ''' Selects a default project from new Project list. Remembers previous default if it still exists.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub resetSelectedProject()
        If _mantisprojects IsNot Nothing Then
            If (selectedProject Is Nothing OrElse Not _mantisprojects.Contains(selectedProject)) Then
                selectedProject = _mantisprojects.Where(Function(p) p.id = "1").FirstOrDefault
            End If
        Else
            selectedProject = Nothing
        End If
    End Sub

    Private _selectedProject As ProjectData
    ''' <summary>
    ''' Gets or Sets the selected Project.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns the currently selected Project as ProjectData</returns>
    ''' <remarks></remarks>
    Public Property selectedProject As ProjectData
        Get
            Return _selectedProject
        End Get
        Set(value As ProjectData)
            setField(value, _selectedProject, Sub(v) _selectedProject = v)
        End Set
    End Property

    ''' <summary>
    ''' Generates a report using a target project, time-range and slice amount.
    ''' </summary>
    ''' <param name="targetProject">The target Mantis Project as ProjectData.</param>
    ''' <param name="timeType">The time-range in relative time of a project as Integer.</param>
    ''' <param name="sliceAmount">The amount of divisions on the reports charts and graphs.</param>
    ''' <remarks></remarks>
    Public Sub generateSpecificReport(targetProject As ProjectData, timeType As Integer, sliceAmount As Integer)
		If targetProject Is Nothing Then
			Dim dlg As New ErrorDialog("Generation Error", "No project referenced. Aborting report generation.", "This error may be caused by lack of connection to the Mantis Database or a project may not be selected")
			dlg.ShowDialog()
			Exit Sub
		End If

		Dim now As DateTime = timeControl.startTime

		Dim saveDlg As New Microsoft.Win32.SaveFileDialog()
		saveDlg.FileName = now.ToString("yyyy-MM-dd") & " - " & "QA"
		saveDlg.DefaultExt = ".docx"
		saveDlg.Filter = "All files (*.*)|*.*"
		Dim result? As Boolean = saveDlg.ShowDialog()

		If result = True Then

            'Remove the unwanted time element so all reports finish at midnight of the day BEFORE the one shown in Intel
            now = now.AddDays(-1)
			now = now.AddHours(-now.Hour)
			now = now.AddMinutes(-now.Minute)
			now = now.AddSeconds(-(now.Second))

			Dim startingPoint As DateTime

			Select Case timeType
				Case TimeUnitType.WEEKS : startingPoint = now.AddDays(-timeControl.timeAmount * 6) '*6 as we don't wan an 8-day range!
                Case TimeUnitType.MONTHS : startingPoint = now.AddMonths(-timeControl.timeAmount)
				Case TimeUnitType.YEARS : startingPoint = now.AddYears(-timeControl.timeAmount)
			End Select

			Dim fileName As String
			fileName = saveDlg.FileName

			Dim generator As New WordDocGen

			Me.messageString = "Downloading report data..."

			Me.zenLoader = New ZendeskLoader()

			Task.Delay(10).ContinueWith(
				(Async Sub()
					 numOpenZendeskTickets = Await zenLoader.loadNumberOfOpenTicketsForCurrentUser(UserInfo.user)
				 End Sub)
			).ContinueWith(
				Sub()
					Dim reportData = New MantisConnectStatsReportData(UserInfo.user, targetProject, now, startingPoint, sliceAmount)
					reportData.totalOpenZendeskTickets = numOpenZendeskTickets
					Me.messageString = "Downloading report data... Done" & vbCrLf & "Generating report..."
					Task.Delay(10).ContinueWith(
						Sub()
							Dim generationMessage = generator.CreateDocument(reportData, toggleSummary, fileName)
							Me.messageString = generationMessage
						End Sub, TaskScheduler.FromCurrentSynchronizationContext
					)
				End Sub, TaskScheduler.FromCurrentSynchronizationContext
			)
		End If
    End Sub

    Private _messageString As String = "Ready."
    ''' <summary>
    ''' Gets or Sets the message string to display status of report generation.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns message as String</returns>
    ''' <remarks></remarks>
    Public Property messageString As String
        Get
            Return _messageString
        End Get
        Set(value As String)
            setField(value, _messageString, Sub(v) _messageString = v)
        End Set
    End Property
End Class
