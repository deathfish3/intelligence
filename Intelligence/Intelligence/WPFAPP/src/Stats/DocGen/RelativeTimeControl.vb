﻿Imports System.ComponentModel

''' <summary>
''' Properties to store time-related variables for report generation.
''' </summary>
''' <remarks></remarks>
Public Class RelativeTimeControl

    Private _startTime As DateTime = DateTime.Now

    ''' <summary>
    ''' Gets all RelativeTimeType enums.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns all RelativeTimeType enums as IEnumerable(Of TimeType). [DEFAULT: DateTime.Now]</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property allTimeTypes As IEnumerable(Of TimeUnitType)
        Get
            Return {TimeUnitType.WEEKS, TimeUnitType.MONTHS, TimeUnitType.YEARS}
        End Get
    End Property


    Private _timeType As TimeUnitType = TimeUnitType.WEEKS
    ''' <summary>
    ''' Gets or Sets the current TimeUnitType.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns TimeType as TimeUnitType [DEFAULT: TimeUnitType.WEEKS]</returns>
    ''' <remarks></remarks>
    Public Property timeType As TimeUnitType
        Get
            Return _timeType
        End Get
        Set(value As TimeUnitType)
            _timeType = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the current startTime.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns startTime.</returns>
    ''' <remarks></remarks>
    Public Property startTime
        Get
            Return _startTime
        End Get
        Set(value)
            _startTime = value
        End Set
    End Property

    ''' <summary>
    ''' Gets minimumDate as DateTime
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns minimumDate as DateTime [DEFAULT: 01/01/2010 00:00:01]</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property minimumDate As DateTime
        Get
            Return System.DateTime.Parse("01/01/2010 00:00:01")
        End Get
    End Property

    ''' <summary>
    ''' Gets maximumDate as DateTime
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns maximumDate as DateTime [Default: DateTime.Now]</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property maximumDate As DateTime
        Get
            Return DateTime.Now
        End Get
    End Property

    Private _timeAmount As Integer = 1
    ''' <summary>
    ''' Gets or Sets the timeAmount to be used in relative time-range calculations.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns timeAmount as Integer. [DEFAULT: 1]</returns>
    ''' <remarks></remarks>
    Public Property timeAmount As Integer
        Get
            Return _timeAmount
        End Get
        Set(value As Integer)
            _timeAmount = value
        End Set
    End Property

    Private _sliceAmount As Integer = 3
    ''' <summary>
    ''' Gets or sets the sliceAmount to be used in generating Tables/Charts.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns sliceAmount as Integer. [DEFAULT: 3]</returns>
    ''' <remarks></remarks>
    Public Property sliceAmount As Integer
        Get
            Return _sliceAmount
        End Get
        Set(value As Integer)
            _sliceAmount = value
        End Set
    End Property

    

  
End Class

