﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' Collects the Data to be used in Report Generation.
''' </summary>
''' <remarks></remarks>
Public Class MantisConnectStatsReportData
    Implements IStatsReportData

    ''' <summary>
    ''' Declares new MantisConnectStatsReportData with default startTime as System time NOW, and 3 slices.
    ''' </summary>
    ''' <param name="user">user as <see cref="UserInfo"/> to access Mantis with.</param>
    ''' <param name="project">The target Mantis Project as <see cref="ProjectData"/> to collect data from.</param>
    ''' <remarks></remarks>
    Sub New(user As UserInfo, project As ProjectData)
        Me.New(user, project, DateTime.Now, 3)
    End Sub

    ''' <summary>
    ''' Declares new MantisConnectStatsReportData with timescale as a week.
    ''' </summary>
    ''' <param name="user">user as <see cref="UserInfo"/> to access Mantis with.</param>
    ''' <param name="project">The target Mantis Project as <see cref="ProjectData"/> to collect data from.</param>
    ''' <param name="startTimeIn">The start time of a time-range as DateTime (relative time calculated based on this time)</param>
    ''' <param name="numberOfTimeslicesForTable">The number of slices to display on a table/graph as Integer.</param>
    ''' <remarks></remarks>
    Sub New(user As UserInfo, project As ProjectData, startTimeIn As DateTime, numberOfTimeslicesForTable As Integer)
        Me.New(user, project, startTimeIn, startTimeIn.AddDays(-7), numberOfTimeslicesForTable)
    End Sub

    ''' <summary>
    ''' Declares new MantisConnectStatsReportData with custom timescale.
    ''' </summary>
    ''' <param name="user">user as <see cref="UserInfo"/> to access Mantis with.</param>
    ''' <param name="project">The target Mantis Project as <see cref="ProjectData"/> to collect data from.</param>
    ''' <param name="startDateIn">The start time of the query time-range as DateTime</param>
    ''' <param name="endDateIn">The end time of the query time-range as DateTime</param>
    ''' <param name="numberOfTimeslicesForTable">The number of slices to display on a table/graph as Integer.</param>
    ''' <remarks>Collects data for report, and templates report with default filename "IntelligenceReport" with FileTime, title as "QA Meeting - DD/MM/YY".</remarks>
    Sub New(user As UserInfo, project As ProjectData, startDateIn As DateTime, endDateIn As DateTime, numberOfTimeslicesForTable As Integer)
        'Unix Epoch for Unassigned Mantis bugs
        Dim epoch As DateTime = New DateTime(1970, 1, 1)
        Me.numberOfTimeSlicesForTable = Math.Min(1, numberOfTimeslicesForTable)
        Me.now = startDateIn
        Dim presentationDate = now.AddDays(1)
        Me.startDate = If(startDateIn > endDateIn, startDateIn, endDateIn)
        Me.endDate = If(startDateIn > endDateIn, endDateIn, startDateIn)
        'If startDate > now Then startDate = now
        'If endDate > startDate Then endDate = startDate
        Me.fileName = "IntelligenceReport" & presentationDate
        Me.title = "QA Meeting - " & presentationDate.ToShortDateString
        Me.projectName = project.name

        Dim mantisClient As New MantisConnect
        Me.totalOpenTasks = mantisClient.mc_filter_get_total_open_on_day(user.mantisUsername, user.mantisPassword, project.id, daysBetweenNowAndStart)

        Dim newStatus As String = 10
        Dim assignedStatus As String = 50
        Dim resolvedStatus As String = 80

        Dim allStatuses As IEnumerable(Of ObjectRef) = MantisCacheManager.MantisStatuses

        Dim allUnassignedStatuses As String() = (From status In allStatuses
                                                 Where status.id = newStatus
                                                 Select status.id).ToArray

        Dim allUnresolvedStatuses As String() = (From status In allStatuses
                                                 Where status.id < resolvedStatus
                                                 Select status.id).ToArray

        Dim projects As String() = mantisClient.mc_project_get_all_subprojects(user.mantisUsername, user.mantisPassword, project.id)
        projects.Add(project.id.ToString)
        Me.projectIds = projects

        Dim unassignedThisTimespanFilter As New FilterSearchData With {
            .project_id = projectIds,
            .status_id = allUnassignedStatuses,
            .start_day = epoch.Day,
            .start_month = epoch.Month,
            .start_year = epoch.Year,
            .end_day = now.Day,
            .end_month = now.Month,
            .end_year = now.Year
        }

        Dim h = mantisClient.mc_filter_search_issue_headers(user.mantisUsername, user.mantisPassword, unassignedThisTimespanFilter, 0, 50)
        Me.totalUnassignedMantisIssuesThisTimespan = mantisClient.mc_filter_search_issue_count(user.mantisUsername, user.mantisPassword, unassignedThisTimespanFilter)

        Dim unresolvedThisTimespanFilter As New FilterSearchData With {
            .project_id = projectIds,
            .status_id = allUnresolvedStatuses,
            .start_day = endDate.Day,
            .start_month = endDate.Month,
            .start_year = endDate.Year,
            .end_day = now.Day,
            .end_month = now.Month,
            .end_year = now.Year
        }

        Dim nextPageOfIssueHeaders As IEnumerable(Of IssueHeaderData) = {}
        Dim perPage As Integer = 50
        Dim pageNumber As Integer = 0
        Me.unresolvedTasksThisTimespan = New List(Of IssueHeaderData)
        Do
            nextPageOfIssueHeaders = mantisClient.mc_filter_search_issue_headers(user.mantisUsername, user.mantisPassword, unresolvedThisTimespanFilter, pageNumber, perPage)
            Me.unresolvedTasksThisTimespan.AddRange(nextPageOfIssueHeaders)
            pageNumber += 1
        Loop While (nextPageOfIssueHeaders.Count = perPage)


        Dim criticalSeverity As String = 80
        Dim unresolvedCriticalFilter As New FilterSearchData With {
            .project_id = projectIds,
            .status_id = allUnresolvedStatuses,
            .severity_id = {criticalSeverity}
        }

        nextPageOfIssueHeaders = {}
        perPage = 50
        pageNumber = 0
        Me.unresolvedCriticalTasks = New List(Of IssueHeaderData)
        Do
            nextPageOfIssueHeaders = mantisClient.mc_filter_search_issue_headers(user.mantisUsername, user.mantisPassword, unresolvedCriticalFilter, pageNumber, perPage)
            Me.unresolvedCriticalTasks.AddRange(nextPageOfIssueHeaders)
            pageNumber += 1
        Loop While (nextPageOfIssueHeaders.Count = perPage)


        Me.totalOpenTasksBySeverityTable = New Dictionary(Of ObjectRef, IList(Of Integer))
        Me.totalTasksOpenedPerTimespanBySeverityTable = New Dictionary(Of ObjectRef, IList(Of Integer))
        Dim allSeverities As IEnumerable(Of ObjectRef) = MantisCacheManager.MantisSeverities
        For Each severity In allSeverities
            Me.totalOpenTasksBySeverityTable.Add(severity, New List(Of Integer))
            Me.totalTasksOpenedPerTimespanBySeverityTable.Add(severity, New List(Of Integer))
        Next


        Me.totalTasksOpenedThisTimespan = mantisClient.mc_filter_get_total_opened_between(user.mantisUsername, user.mantisPassword, project.id, daysBetweenNowAndStart, daysBetweenNowAndEnd)
        Me.totalTasksOpenedLastTimespan = mantisClient.mc_filter_get_total_opened_between(user.mantisUsername, user.mantisPassword, project.id, daysBetweenNowAndEnd, daysBetweenNowAndPreviousEnd)

        Me.totalTasksResolvedThisTimespan = mantisClient.mc_filter_get_total_resolved_between(user.mantisUsername, user.mantisPassword, project.id, daysBetweenNowAndStart, daysBetweenNowAndEnd)
        Me.totalTasksResolvedLastTimespan = mantisClient.mc_filter_get_total_resolved_between(user.mantisUsername, user.mantisPassword, project.id, daysBetweenNowAndEnd, daysBetweenNowAndPreviousEnd)

        Me.totalTasksOpenedPerTimespanList = New List(Of Integer)
        Me.totalTasksResolvedPerTimespanList = New List(Of Integer)

        Dim daysAgo As Integer = daysBetweenNowAndStart
        For i As Integer = 0 To numberOfTimeslicesForTable
            Dim openedThisTimespan As Integer = mantisClient.mc_filter_get_total_opened_between(user.mantisUsername, user.mantisPassword, project.id, daysAgo, daysAgo + daysBetweenStartAndEnd)
            Dim resovledThisTimespan As Integer = mantisClient.mc_filter_get_total_resolved_between(user.mantisUsername, user.mantisPassword, project.id, daysAgo, daysAgo + daysBetweenStartAndEnd)

            Me.totalTasksOpenedPerTimespanList.Add(openedThisTimespan)
            Me.totalTasksResolvedPerTimespanList.Add(resovledThisTimespan)

            Dim startDay As DateTime = Me.startDate.AddDays(-daysAgo)
            Dim endDay As DateTime = Me.startDate.AddDays(-(daysAgo + daysBetweenStartAndEnd))
            Dim submittedDuringTimeSpanFilter As New FilterSearchData With {
                    .project_id = Me.projectIds,
                    .start_day = endDay.Day,
                    .start_month = endDay.Month,
                    .start_year = endDay.Year,
                    .end_day = startDay.Day,
                    .end_month = startDay.Month,
                    .end_year = startDay.Year
                }

            Dim severityCountsForTimeSlice As IList(Of String) = mantisClient.mc_filter_get_total_open_on_day_by_severity(user.mantisUsername, user.mantisPassword, project.id, daysAgo)
            Dim severityIndex As Integer = 0
            For Each severity In allSeverities

                submittedDuringTimeSpanFilter.severity_id = {severity.id.ToString}
                Dim nextPageOfIds As String() = {}
                perPage = 50
                pageNumber = 0
                Dim submittedInTimeslice As Integer = 0
                Do
                    nextPageOfIds = mantisClient.mc_filter_search_issue_ids(user.mantisUsername, user.mantisPassword, submittedDuringTimeSpanFilter, pageNumber, perPage)
                    submittedInTimeslice += nextPageOfIds.Length
                    pageNumber += 1
                Loop While (nextPageOfIssueHeaders.Count = perPage)
                Me.totalTasksOpenedPerTimespanBySeverityTable.Item(severity).Add(submittedInTimeslice)

                Dim itemToAdd As Integer = severityCountsForTimeSlice.Item(severityIndex)
                Me.totalOpenTasksBySeverityTable.Item(severity).Add(itemToAdd)
                severityIndex += 1
            Next

            daysAgo += daysBetweenStartAndEnd
        Next
    End Sub

    ''' <summary>
    ''' Gets or Sets number of slices a table/graph will display.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns number of slices as Integer.</returns>
    ''' <remarks></remarks>
    Private Property numberOfTimeSlicesForTable As Integer

    ''' <summary>
    ''' Gets or Sets stored sub-project ids as IEnumerable(Of String).
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns stored sub-project ids as IEnumerable(Of String).</returns>
    ''' <remarks></remarks>
    Private Property projectIds As IEnumerable(Of String)

    ''' <summary>
    ''' Gets or Sets now as DateTime.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns now as DateTime.</returns>
    ''' <remarks></remarks>
    Private Property now As DateTime

    ''' <summary>
    ''' Gets or Sets startDate as DateTime.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns startDate as DateTime.</returns>
    ''' <remarks></remarks>
    Private Property startDate As DateTime

    ''' <summary>
    ''' Gets or sets endDate as DateTime.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns endDate as DateTime.</returns>
    ''' <remarks></remarks>
    Private Property endDate As DateTime

    ''' <summary>
    ''' Gets the total number of days in a date-range.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Total number of days between two dates as Integer.</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property daysBetweenStartAndEnd As Integer
        Get
            Return Math.Round((startDate - endDate).TotalDays)
        End Get
    End Property

    ''' <summary>
    ''' Gets the total number of days between now and startDate.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Total number of days between now and startDate as Integer.</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property daysBetweenNowAndStart As Integer
        Get
            Return Math.Round((now - startDate).TotalDays)
        End Get
    End Property

    ''' <summary>
    ''' Gets the total number of days between now and endDate.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Total number of days between now and endDate as Integer.</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property daysBetweenNowAndEnd As Integer
        Get
            Return Math.Round((now - endDate).TotalDays)
        End Get
    End Property

    ''' <summary>
    ''' Gets the total number of days between now and previous endDate.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Total number of days between now and previous endDate as Integer.</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property daysBetweenNowAndPreviousEnd As Integer
        Get
            Return daysBetweenNowAndEnd + daysBetweenStartAndEnd
        End Get
    End Property

    Public Property fileName As String Implements IStatsReportData.fileName

    Public Property totalTasksOpenedLastTimespan As Integer Implements IStatsReportData.totalTasksOpenedLastTimespan

    Public Property totalTasksOpenedThisTimespan As Integer Implements IStatsReportData.totalTasksOpenedThisTimespan

    Public Property totalTasksResolvedLastTimespan As Integer Implements IStatsReportData.totalTasksResolvedLastTimespan

    Public Property totalTasksResolvedThisTimespan As Integer Implements IStatsReportData.totalTasksResolvedThisTimespan

    Public Property title As String Implements IStatsReportData.title

    Public Property totalOpenTasks As Integer Implements IStatsReportData.totalOpenTasks

    Public Property totalOpenTasksBySeverityTable As Dictionary(Of com.yoyogames.bugs.ObjectRef, IList(Of Integer)) Implements IStatsReportData.totalOpenTasksBySeverityTable

    Public Property totalUnassignedMantisIssuesThisTimespan As Integer Implements IStatsReportData.totalUnassignedMantisIssuesThisTimespan

    Public Property unresolvedCriticalTasks As New List(Of com.yoyogames.bugs.IssueHeaderData) Implements IStatsReportData.unresolvedCriticalTasks

    Public Property unresolvedTasksThisTimespan As New List(Of com.yoyogames.bugs.IssueHeaderData) Implements IStatsReportData.unresolvedTasksThisTimespan

    Public Property projectName As String Implements IStatsReportData.projectName

    Public Property reportEndDate As Date Implements IStatsReportData.endDate
        Get
            Return Me.endDate
        End Get
        Set(value As Date)

        End Set
    End Property

    Public Property reportStartDate As Date Implements IStatsReportData.startDate
        Get
            Return Me.startDate
        End Get
        Set(value As Date)

        End Set
    End Property

    Public Property totalTasksOpenedPerTimespanBySeverityTable As Dictionary(Of ObjectRef, IList(Of Integer)) Implements IStatsReportData.totalTasksOpenedPerTimespanBySeverityTable

    Public Property totalTasksOpenedPerTimespanList As IList(Of Integer) Implements IStatsReportData.totalTasksOpenedPerTimespanList

    Public Property totalTasksResolvedPerTimespanList As IList(Of Integer) Implements IStatsReportData.totalTasksResolvedPerTimespanList

    Public Property totalOpenZendeskTickets As Integer = -1 Implements IStatsReportData.totalOpenZendeskTickets
End Class
