﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' Provides access to public Report Data properties.
''' </summary>
''' <remarks></remarks>
Public Interface IStatsReportData
    ''' <summary>
    ''' Gets or Sets the filename of the report.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Report filename as String.</returns>
    ''' <remarks></remarks>
    Property fileName As String
    ''' <summary>
    ''' Gets or Sets the title of the report.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Report title as String.</returns>
    ''' <remarks></remarks>
    Property title As String
    ''' <summary>
    ''' Gets or Sets the project name of the report.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns project name as String.</returns>
    ''' <remarks></remarks>
    Property projectName As String
    ''' <summary>
    ''' Gets or Sets the start date of the search range of the report.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns start date as Date.</returns>
    ''' <remarks></remarks>
    Property startDate As Date
    ''' <summary>
    ''' Gets or Sets the end date of the search range of the report.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns end date as Date.</returns>
    ''' <remarks></remarks>
    Property endDate As Date

    ''' <summary>
    ''' Gets or Sets the total Open Tasks count.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total open tasks count as Integer.</returns>
    ''' <remarks></remarks>
    Property totalOpenTasks As Integer

    ''' <summary>
    ''' Gets or Sets the total Open Tasks count between start and end date.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total open tasks count between start and end date as Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksOpenedThisTimespan As Integer

    ''' <summary>
    ''' Gets or Sets the total Open Tasks count between previous start and end date as Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total open tasks count between previous start and end date as Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksOpenedLastTimespan As Integer

    ''' <summary>
    ''' Gets or Sets the total Resolved Tasks count between start and end date as Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total resolved tasks count between start and end date as Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksResolvedThisTimespan As Integer

    ''' <summary>
    ''' Gets or Sets the total Resolved Tasks count between previous start and end date as Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total resolved tasks count between previous start and end date as Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksResolvedLastTimespan As Integer

    ''' <summary>
    ''' Gets or Sets the total Unassigned Mantis Issues count between start and end date as Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total Unassigned Mantis Issues count between start and end date as Integer.</returns>
    ''' <remarks></remarks>
    Property totalUnassignedMantisIssuesThisTimespan As Integer

    ''' <summary>
    ''' Gets or Sets the total Open Zendesk Tickets count.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total open zendesk tickets count.</returns>
    ''' <remarks></remarks>
    Property totalOpenZendeskTickets As Integer

    ''' <summary>
    ''' Gets or Sets the Unresolved Tasks between start and end date as List of <see cref="IssueHeaderData"></see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns unresolved tasks between start and end date as List of <see cref="IssueHeaderData"></see>.</returns>
    ''' <remarks></remarks>
    Property unresolvedTasksThisTimespan As List(Of IssueHeaderData)

    ''' <summary>
    ''' Gets or Sets the Unresolved Critical Tasks as List of <see cref="IssueHeaderData"></see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns unresolved critical tasks as List of <see cref="IssueHeaderData"></see>.</returns>
    ''' <remarks></remarks>
    Property unresolvedCriticalTasks As List(Of IssueHeaderData)

    ''' <summary>
    ''' Gets or Sets the total Opened Tasks per timespan as IList of Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total opened tasks per timespan as IList of Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksOpenedPerTimespanList As IList(Of Integer)

    ''' <summary>
    ''' Gets or Sets the total Resolved Tasks per timespan as IList of Integer.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns total resolved tasks per timespan as IList of Integer.</returns>
    ''' <remarks></remarks>
    Property totalTasksResolvedPerTimespanList As IList(Of Integer)

    ''' <summary>
    ''' Gets or Sets a Dictionary of total Open Tasks by Severity (key, as ObjectRef)
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns Dictionary of counts with associated severity</returns>
    ''' <remarks></remarks>
    Property totalOpenTasksBySeverityTable As Dictionary(Of ObjectRef, IList(Of Integer))

    ''' <summary>
    ''' Gets or Sets a Dictionary of total Open Tasks per timespan by Severity (key, as ObjectRef)
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns Dictionary of counts with associated severity per timespan</returns>
    ''' <remarks></remarks>
    Property totalTasksOpenedPerTimespanBySeverityTable As Dictionary(Of ObjectRef, IList(Of Integer))
End Interface
