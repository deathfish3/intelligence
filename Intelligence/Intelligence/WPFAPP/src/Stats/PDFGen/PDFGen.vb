﻿Imports MigraDoc
Imports MigraDoc.DocumentObjectModel
Imports System.Collections.ObjectModel
Imports WPFAPP.StatsViewModel

''' <summary>
''' PDF Generation tools to generate PDF printouts of Stats results.
''' </summary>
''' <remarks></remarks>
Public Class PDFGen

    Private graphTree As IEnumerable(Of GraphTreeNode)
    Private displaySeperateGraphs As Boolean

    ''' <summary>
    ''' Collects the current graphTree and if the printout should display separate graphs
    ''' </summary>
    ''' <param name="graphTree">The graphTree to get the graphs from</param>
    ''' <param name="displaySeperateGraphs">Whether or not to display seperate graphs</param>
    ''' <remarks></remarks>
    Public Sub New(graphTree As ObservableCollection(Of StatsViewModel.GraphTreeNode), displaySeperateGraphs As Boolean)
        Me.graphTree = graphTree
        Me.displaySeperateGraphs = displaySeperateGraphs
    End Sub

    Public Shared imageIndexToStartOn As Integer = 0

    ''' <summary>
    ''' Compiles and creates a new PDF document at your target directory.
    ''' </summary>
    ''' <param name="directory">The target directory as String</param>
    ''' <remarks></remarks>
    Public Sub CompileDoc(directory As String)

        Dim document As New Document
        Dim imageCountSoFar As Integer = imageIndexToStartOn
        For Each item As StatsViewModel.GraphTreeNode In Me.graphTree
            imageCountSoFar = addToDocument(document, imageCountSoFar, Me.displaySeperateGraphs, item)
        Next

        'TODO Add a field for grouping in the GraphNode class to grab groupTitle data
        'Pass in the grouping to the nodes manually, using get parent and get leafnode.

        Dim pdfRenderer As New MigraDoc.Rendering.PdfDocumentRenderer(False, PdfSharp.Pdf.PdfFontEmbedding.Always)
        If My.Computer.FileSystem.FileExists(directory) Then
            'My.Computer.FileSystem.DeleteFile(directory)
        End If
        pdfRenderer.Document = document
        pdfRenderer.RenderDocument()
        pdfRenderer.PdfDocument.Save(directory)
        pdfRenderer.PdfDocument.Close()
        pdfRenderer.PdfDocument.Dispose()
        Process.Start(directory)
    End Sub

    ''' <summary>
    ''' Adds a generated chart to the graph
    ''' </summary>
    ''' <param name="document">The target PDF document</param>
    ''' <param name="countSoFar">How many charts have been added</param>
    ''' <param name="displaySeparateGraphs">Whether or not to display seperate graphs</param>
    ''' <param name="node">The current node in the GraphTree</param>
    ''' <returns>Returns countSoFar as Integer</returns>
    ''' <remarks></remarks>
    Private Function addToDocument(document As Document, countSoFar As Integer, displaySeparateGraphs As Boolean, node As GraphTreeNode) As Integer
        If node Is Nothing Then Return countSoFar
        Dim children = If(displaySeparateGraphs, node.childrenForSinglegraph, node.childrenForMultigraph)
        If Not children.Any() Then
            Dim section As Section = document.AddSection()
            section.AddParagraph(node.groupTitle)
            section.AddImage(System.IO.Path.GetTempPath & "intelligence\" & "pdfimages\" & countSoFar & ".png")
            Return countSoFar + 1
        Else
            For Each child In children
                countSoFar = addToDocument(document, countSoFar, displaySeparateGraphs, child)
            Next
            Return countSoFar
        End If
    End Function

End Class
