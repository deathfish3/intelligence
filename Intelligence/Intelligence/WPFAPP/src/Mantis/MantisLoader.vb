﻿Imports WPFAPP.com.yoyogames.bugs
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports WPFAPP.MantisIssue
Imports System.Text

''' <summary>
''' Used for sending and retreiving data to and from Mantis's SOAP API.
''' When used with <see cref="LoadWithAsyncGUITask"/> it will dynamically
''' update the text in the <see cref="LoadingDialog"/> as it performs each task.
''' </summary>
''' <remarks><example><code>
''' Dim zenLoader As New ZendeskLoader
''' LoadWithAsyncGUITask.run(Of MantisIssue)(mantisLoader, Nothing, Function() issueFromId("18160", UserInfo.user), Sub(m As MantisIssue) Debug.Print(m.Status.ToString))
''' </code></example></remarks>
Public Class MantisLoader
    Inherits Loader

    ''' <summary>
    ''' Create a new MantisLoader, and initialize the contextual error 
    ''' message headers for <see cref="ErrorDialog"/>
    ''' </summary>
    Sub New()
        currentErrorTitle = "Mantis Error"
        currentErrorHeader = Nothing
    End Sub

    ''' <summary>Download the mantis issue with the given id number.</summary>
    ''' <param name="issueId">The Mantis issue id number.</param>
    ''' <param name="user">The user whose Mantis login credentials will be used.</param>
    ''' <returns>A new <see cref="MantisIssue"/> downloaded via the Mantis SOAP API.</returns>
    Function issueFromId(issueId As String, user As UserInfo) As MantisIssue
        updateListeners("Downloading issue """ & issueId & """ from Mantis...")
        Dim issue As New MantisIssue()
        issue.source = IssueSource.Mantis
        Dim mantisClient As New MantisConnect

        issue.internalIssueData = mantisClient.mc_issue_get(user.mantisUsername, user.mantisPassword, issueId)
        issue.recordOriginalTextsOfAllNotes()
        loadEnums(issue, issue.internalIssueData, user)
        updateListeners("Done")
        Return issue
    End Function

    ''' <summary>Create a new blank <see cref="MantisIssue"/> for the given project but do not upload it yet.</summary>
    ''' <param name="user">
    ''' The user whose Mantis login credentials will be used to download 
    ''' applicable enum fields for the issue (e.g category, severity etc.)
    ''' </param>
    ''' <param name="project">
    ''' The project this issue will eventually be added to.
    ''' The project id number must be set, but the name is ignored.
    ''' Also determines which categories the new bug can have.
    ''' </param>
    ''' <returns>A new blank <see cref="MantisIssue"/> for the given project.</returns>
    Function newIssue(user As UserInfo, project As ObjectRef) As MantisIssue
        updateListeners("Creating new issue in Mantis...")
        Dim issue As New MantisIssue()
        issue.source = IssueSource.NewIssue
        Dim mantisClient As New MantisConnect
        issue.internalIssueData = New IssueData
        issue.recordOriginalTextsOfAllNotes()
        issue.internalIssueData.project = project
        loadEnums(issue, issue.internalIssueData, user)
        updateListeners("Done")
        Return issue
    End Function

    ''' <summary>Create a new <see cref="MantisIssue"/> that wraps the downloaded <see cref=" IssueData"/> from Mantis.</summary>
    ''' <param name="data">The downloaded data from Mantis.</param>
    ''' <param name="user">The user whose Mantis login credentials will be used.</param>
    ''' <param name="issueSource">Indicates where the issue came from (Zendesk, Mantis, or a newly created one).</param>
    ''' <returns></returns>
    Function issueFromData(data As IssueData, user As UserInfo, issueSource As IssueSource) As MantisIssue
        updateListeners("Creating mantis issue...")
        Dim issue As New MantisIssue()
        issue.source = issueSource
        issue.internalIssueData = data
        issue.recordOriginalTextsOfAllNotes()
        loadEnums(issue, data, user)
        updateListeners("Done")
        Return issue
    End Function

    ''' <summary>
    ''' Download all the possible enum values for the field in the given Mantis issue.
    ''' These values are wrapped as <see cref="ObjectRefWrapper"/>s to override some of the default
    ''' behaviours, and may be loaded from the cache if available.
    ''' </summary>
    ''' <param name="issue">The <see cref="MantisIssue"/> whose enum fields need filled in.</param>
    ''' <param name="data">The data for the issue downloaded from Mantis.</param>
    ''' <param name="user">The user whose Mantis login credentials will be used.</param>
    Private Sub loadEnums(issue As MantisIssue, data As IssueData, user As UserInfo)
        Dim mantisClient As New MantisConnect
        updateListeners("Downloading and caching categories for project """ & data.project.id & """...")
        issue.CategoryCollection = MantisCacheManager.MantisCategoriesInProject(data.project.id)
        updateListeners("Downloading and caching severities for project """ & data.project.id & """...")
        issue.SeverityCollection = wrap(MantisCacheManager.MantisSeverities)
        updateListeners("Downloading and caching reproducibilities for project """ & data.project.id & """...")
        issue.ReproducibilityCollection = wrap(MantisCacheManager.MantisReproducibilities)
        updateListeners("Downloading and caching priorities for project """ & data.project.id & """...")
        issue.PriorityCollection = wrap(MantisCacheManager.MantisPriorities)
        updateListeners("Downloading and caching statuses for project """ & data.project.id & """...")
        issue.StatusCollection = wrap(MantisCacheManager.MantisStatuses)
        updateListeners("Downloading and caching resolutions for project """ & data.project.id & """...")
        issue.ResolutionCollection = wrap(MantisCacheManager.MantisResolutions)
        updateListeners("Downloading and caching view states for project """ & data.project.id & """...")
        issue.ViewStatesCollection = wrap(MantisCacheManager.MantisViewStates)

    End Sub

    ''' <summary>
    ''' Create <see cref="ObjectRefWrapper"/>s around all the <see cref="ObjectRef"/>s in the list.
    ''' These values are wrapped to override some of the default behaviours without editing
    ''' the auto-generated code from the Mantis web service.
    ''' </summary>
    ''' <param name="objectRefs"></param>
    ''' <returns></returns>
    Public Shared Function wrap(objectRefs As IEnumerable(Of ObjectRef)) As IEnumerable(Of ObjectRefWrapper)
        Dim wrappers As New List(Of ObjectRefWrapper)
        For Each ref In objectRefs
            wrappers.Add(New ObjectRefWrapper(ref))
        Next
        Return wrappers
    End Function

    ''' <summary>Create a <see cref="MantisIssue"/> based on the information from the given Zendesk ticket.</summary>
    ''' <param name="zen">The Zendesk ticket containing the information about this bug.</param>
    ''' <param name="zenUser">The account details for the user that reported the bug.</param>
    ''' <param name="intelligenceUser">The user whose Mantis login credentials will be used.</param>
    ''' <returns>A newly created <see cref="MantisIssue"/> based on the information from the given Zendesk ticket.</returns>
    Function mantisIssueFromZendesk(zen As ZendeskTicket, zenUser As ZendeskUser, intelligenceUser As UserInfo) As MantisIssue
        updateListeners("Creating mantis issue from Zendesk ticket...")
        Dim issue As IssueData = New IssueData

        updateListeners("Acquiring Mantis reporter information for " & zenUser.user.name & "...")
        issue.reporter = getReporter(zenUser, intelligenceUser)

        updateListeners("Filling out issue details...")
        issue.description = zen.description
        issue.steps_to_reproduce = zen.stepsToReproduce

        'Add field for GM version if the project id is for GameMaker studio
        If 1 = zen.project Then
            Dim gmVersionField As CustomFieldValueForIssueData = New CustomFieldValueForIssueData
            gmVersionField.field = ObjectRefFactory.Create("1", "GM Version")
            gmVersionField.value = zen.getVersionNum
            issue.custom_fields = {gmVersionField}
        End If

        issue.summary = zen.summary
        issue.platform = zen.platform
        issue.os = zen.os
        issue.reproducibility = ObjectRefFactory.Create(zen.reproducibility)
        issue.category = zen.category
        issue.view_state = ObjectRefFactory.Create(zen.view_state)
        issue.additional_information = zen.additional_information
        issue.project = ObjectRefFactory.Create(zen.project)
        issue.priority = ObjectRefFactory.Create(zen.priority)
        Dim mantisIssue As MantisIssue = issueFromData(issue, intelligenceUser, IssueSource.Zendesk)

        mantisIssue.zendeskId = zen.ticket.id
        mantisIssue.zendeskUser = zenUser

        updateListeners("Done")
        Return mantisIssue
    End Function

    ''' <summary>
    ''' Get the Mantis account details for the given Zendesk user using
    ''' their email address. If the user has no Mantis account, one will be
    ''' created for them automatically.
    ''' </summary>
    ''' <param name="zenUser">The Zendesk user to find a Mantis account for.</param>
    ''' <param name="intelligenceUser">The user whose Mantis login credentials will be used to search for the other user's details.</param>
    ''' <returns>
    ''' The Mantis account details with the same email address as the given Zendesk user.
    ''' In the rare case where no details are found, and the creation of a new account is
    ''' unsuccessful, this may return <c>Nothing</c>.
    ''' </returns>
    Public Function getReporter(zenUser As ZendeskUser, intelligenceUser As UserInfo) As AccountData
        Dim zenEmail As String = zenUser.user.email
        Dim mantisAccountData As AccountData = getMantisAccountDataFromEmail(zenEmail, intelligenceUser)

        If mantisAccountData Is Nothing Then
            createNewMantisAccount(zenUser)
            mantisAccountData = getMantisAccountDataFromEmail(zenEmail, intelligenceUser)
        End If

        Return mantisAccountData
    End Function

    ''' <summary>Get the Mantis account details for the user with the given email address.</summary>
    ''' <param name="email">The email address to search for.</param>
    ''' <param name="user">The user whose Mantis login credentials will be used to search for the other user's details.</param>
    ''' <returns>The Mantis account details with the given email address, or <c>Nothing</c> if none exist.</returns>
    Public Function getMantisAccountDataFromEmail(email As String, user As UserInfo) As AccountData
        updateListeners("Searching for Mantis user with email address """ & email & """...")
        Dim mantis As New MantisConnect

        'Project id of 0 means all projects

        ' viewer : 10
        'reporter : 25
        'updater : 40
        'developer : 55
        'manager : 70
        'administrator : 90

        Dim accounts As AccountData() = MantisCacheManager.MantisUsers
        For Each account In accounts
            If account.email IsNot Nothing Then
                If account.email.Equals(email) Then
                    Debug.Write(account.id & " " & account.name & " " & account.email & Environment.NewLine)
                    Return account
                End If
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' Create a new Mantis account using the username and email from the given Zendesk account details.
    ''' This makes use of some code with very brittle urls and parameter (e.g. 34u5t94ut348u34fsdf0w0.php), so this
    ''' may need to be rewritten if the account creation code changes on Mantis.
    ''' </summary>
    ''' <param name="zenUser">The account details of an existing Zendesk user, whose email address and name should be used in Mantis.</param>
    Public Async Sub createNewMantisAccount(zenUser As ZendeskUser)

        Dim zenEmail As String = zenUser.user.email
        Dim zenName As String = zenUser.user.name

        updateListeners("Creating new Mantis account for """ & zenName & """ with email: """ & zenEmail & """...")

        Dim client As HttpClient = New HttpClient

        client.BaseAddress = New Uri("http://bugs.yoyogames.com")

        Dim content As New FormUrlEncodedContent({
            New KeyValuePair(Of String, String)("username", zenName),
            New KeyValuePair(Of String, String)("email", zenEmail),
            New KeyValuePair(Of String, String)("7p9TuNa", "7paDrag")
        })

        Dim result As HttpResponseMessage = Await client.PostAsync("/34u5t94ut348u34fsdf0w0.php", content)
        Debug.Print("Response status from user createion: " & result.StatusCode)
        Debug.Print("Response from user creation: " & Await result.Content.ReadAsStringAsync)
    End Sub

    ''' <summary>Download an attachment from Mantis to the given location.</summary>
    ''' <param name="saveLocation">The path to write the downloaded file to.</param>
    ''' <param name="attachmentId">The id number of the attachment on Mantis.</param>
    ''' <returns><c>True</c> if successfully downloaded.</returns>
    Public Function downloadMantisAttachment(saveLocation As String, attachmentId As Integer)
        updateListeners("Downloading attachment """ & attachmentId & """...")
        Dim tempFile As Byte()
        Dim mantisClient As New MantisConnect

        tempFile = mantisClient.mc_issue_attachment_get(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, attachmentId)

        updateListeners("Writing file to """ & saveLocation & """ ...")
        System.IO.File.WriteAllBytes(saveLocation, tempFile)

        updateListeners("Done")
        Return My.Computer.FileSystem.FileExists(saveLocation)
    End Function

    ''' <summary>Upload the given file as an attachment for the given Mantis issue.</summary>
    ''' <param name="filename">The path of the file to upload (on the local computer).</param>
    ''' <param name="uploadName">The filename to show on Mantis.</param>
    ''' <param name="issueId">The id number of the Mantis issue the attachment is for.</param>
    ''' <returns>The attachment metadata for the newly uploaded attachment.</returns>
    Public Function uploadMantisAttachment(filename As String, uploadName As String, issueId As Integer) As AttachmentData
        Dim mantisClient As New MantisConnect
        updateListeners("Reading """ & filename & """ ...")
        Dim uploadBytes As Byte() = System.IO.File.ReadAllBytes(filename)
        updateListeners("Uploading """ & uploadName & """ to issue """ & issueId & """...")
        Dim contentType As String
        contentType = System.Web.MimeMapping.GetMimeMapping(uploadName)
        Dim attachmentId As String = mantisClient.mc_issue_attachment_add(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, issueId, uploadName, contentType, uploadBytes)
        updateListeners("Done")
        Dim downloadUrl As String = "http://bugs.yoyogames.com/file_download.php?file_id=" & attachmentId & "&type=bug"
        Return New AttachmentData With {.content_type = contentType, .filename = uploadName, .id = attachmentId, .download_url = downloadUrl}
    End Function

    ''' <summary>Add a new relationship between the given <see cref="MantisIssue"/> and an issue with the given id.</summary>
    ''' <param name="issue">The <see cref="MantisIssue"/> to add the relationship to.</param>
    ''' <param name="idIn">The id number of the other Mantis issue that is the target of this relationship.</param>
    ''' <param name="typeIn">
    ''' The string representation of the type of relationship between the two issues.
    ''' Can be "parent of", "child of", "duplicate of", "has duplicate", or "related to".
    ''' </param>
    ''' <returns>The metadata for the newly uploaded relationship.</returns>
    Public Function addMantisRelationship(issue As MantisIssue, idIn As String, typeIn As String) As RelationshipData
        Dim newRelationship As New RelationshipData
        Dim mantisClient As New MantisConnect
        newRelationship.target_id = idIn
        Dim typeRef As New ObjectRef
        typeRef.name = typeIn
        Select Case typeRef.name
            Case "parent of"
                typeRef.id = 2
            Case "child of"
                typeRef.id = 3
            Case "duplicate of"
                typeRef.id = 0
            Case "has duplicate"
                typeRef.id = 4
            Case "related to"
                typeRef.id = 1
        End Select
        newRelationship.type = typeRef
        updateListeners("Creating new relationship to issue """ & issue.internalIssueData.id & """...")
        Dim relId As String = mantisClient.mc_issue_relationship_add(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, issue.internalIssueData.id, newRelationship)
        newRelationship.id = relId
        updateListeners("Done")
        Return newRelationship
    End Function

    ''' <summary>Upload a new node for the given issue on Mantis.</summary>
    ''' <param name="issue">The <see cref="MantisIssue"/> to add the note to.</param>
    ''' <param name="noteIn">The metadata for the new note to upload.</param>
    ''' <returns>The id number of the newly uploaded note.</returns>
    Public Function addMantisNote(issue As MantisIssue, noteIn As IssueNoteData) As String
        Dim noteToAdd As New IssueNoteData
        Dim noteId As String
        Dim mantisClient As New MantisConnect
        noteToAdd = noteIn
        updateListeners("Creating new note to issue """ & issue.internalIssueData.id & """...")
        noteId = mantisClient.mc_issue_note_add(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, issue.internalIssueData.id, noteToAdd)
        updateListeners("Done")
        Return noteId
    End Function

    ''' <summary>Delete the specified relationship from the given issue.</summary>
    ''' <param name="issueId">The id number of the issue whos relationship will be deleted.</param>
    ''' <param name="relationshipId">The id number of the relationship to delete.</param>
    ''' <returns>Always returns <c>True</c>.</returns>
    Public Function deleteRelationship(issueId As Integer, relationshipId As Integer) As Boolean
        updateListeners("Deleting relation """ & relationshipId & """ from issue """ & issueId & """...")
        Dim mantisClient As New MantisConnect
        mantisClient.mc_issue_relationship_delete(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, issueId, relationshipId)
        Return True
    End Function

    ''' <summary>Delete the specified attachment from the given issue.</summary>
    ''' <param name="issue">The <see cref="MantisIssue"/> whose attachment will be deleted.</param>
    ''' <param name="attachmentId">The id number of the attachment to delete.</param>
    ''' <returns>Always returns <c>True</c>.</returns>
    Public Function deleteMantisAttachment(issue As MantisIssue, attachmentId As String)
        Dim mantisClient As New MantisConnect
        updateListeners("Deleting attachment """ & attachmentId & """ from issue """ & issue.internalIssueData.id & """...")
        mantisClient.mc_issue_attachment_delete(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, attachmentId)
        updateListeners("Done")
        Return True
    End Function

    ''' <summary>Delete the specified note from the given issue.</summary>
    ''' <param name="issue">The <see cref="MantisIssue"/> whose note will be deleted.</param>
    ''' <param name="noteId">The id number of the note to delete.</param>
    ''' <returns>Always returns <c>True</c>.</returns>
    Public Function deleteMantisNote(issue As MantisIssue, noteId As String)
        Dim mantisClient As New MantisConnect
        updateListeners("Deleteing note """ & noteId & """ from issue """ & issue.internalIssueData.id & """...")
        mantisClient.mc_issue_note_delete(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, noteId)
        updateListeners("Done")
        Return True
    End Function

    ''' <summary>
    ''' Upload the given issue to Mantis.
    ''' If the issue came from Mantis, its fields and notes will be updated.
    ''' If it was a new issue, or came from Zendesk, a new issue will be added to Mantis.
    ''' </summary>
    ''' <param name="mantisIssue">A container for all the issue information to upload to Mantis.</param>
    ''' <param name="user">The user whose Mantis credentials will be used to log in to Mantis.</param>
    ''' <returns>The id number of the Mantis issue that was uploaded. May be for an existing issue, or a newly added one.</returns>
    Public Function submitToMantis(mantisIssue As MantisIssue, user As UserInfo) As Integer
        Dim mantisId As Integer = -1
        If Not mantisIssue Is Nothing Then
            Dim mantisClient As New MantisConnect
            Dim notesBackup As IssueNoteData() = mantisIssue.internalIssueData.notes
            mantisIssue.internalIssueData.notes = {}

            If IssueSource.Mantis = mantisIssue.source AndAlso mantisIssue.IssueID IsNot Nothing Then
                mantisId = mantisIssue.internalIssueData.id
                updateListeners("Updating mantis issue """ & mantisId & """...")
                mantisClient.mc_issue_update(user.mantisUsername, user.mantisPassword, mantisIssue.internalIssueData.id, mantisIssue.internalIssueData)

                If Not (notesBackup Is Nothing) Then
                    For Each issueNote In notesBackup
                        Debug.Print(issueNote.text)
                        If mantisIssue.noteHasBeenUpdated(issueNote) Then
                            updateListeners("Updating note """ & issueNote.id & """updating mantis issue """ & mantisId & """...")
                            mantisClient.mc_issue_note_update(user.mantisUsername, user.mantisPassword, issueNote)
                        Else
                            Debug.Print("Note not changed: NOT UPDATING")
                        End If
                    Next
                End If
            Else
                updateListeners("Adding new issue to mantis...")
                mantisId = mantisClient.mc_issue_add(user.mantisUsername, user.mantisPassword, mantisIssue.internalIssueData)
            End If
            updateListeners("Done")
        End If
        Return mantisId
    End Function
End Class


