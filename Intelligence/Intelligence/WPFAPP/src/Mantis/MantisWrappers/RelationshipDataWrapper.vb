﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' A wrapper around <see cref="RelationshipData"/> downloaded from Mantis
''' using its SOAP API. The main function of this wrapper is to override
''' the <see cref="RelationshipDataWrapper.Equals"/>
''' method to allow for comparison on the relationship ids.
''' 
''' The code for <see cref="RelationshipData"/> is
''' auto-generated from the Mantis WSDL, so this class is used to
''' extend its functionality without editing the auto-generated code
''' (which allows it to be safely re-generated whenever the API changes).
''' </summary>
Public Class RelationshipDataWrapper

    ''' <summary>The relationship contained by this wrapper.</summary>
    Public relationship As RelationshipData

    ''' <summary>Wrap the given <see cref="RelationshipData"/>.</summary>
    ''' <param name="relationship">The relationship to wrap.</param>
    Sub New(relationship As RelationshipData)
        Me.relationship = relationship
    End Sub

    ''' <summary>Return the id number of this relationship.</summary>
    Public Overrides Function ToString() As String
        Return Me.id
    End Function

    ''' <summary>
    ''' Check whether the given object has the same relationship id as this wrapper.
    ''' Will match both wrapped and unwrapped <see cref="RelationshipData"/> objects.
    ''' </summary>
    ''' <param name="obj">The object to check equality with.</param>
    ''' <returns><c>True</c> if the object has the same relationship id.</returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing Then Return False
        Dim wrapper As RelationshipDataWrapper = TryCast(obj, RelationshipDataWrapper)
        If wrapper IsNot Nothing AndAlso wrapper.id IsNot Nothing Then
            If wrapper.id.Equals(Me.id) Then
                Return True
            End If
        Else
            Dim objRef As RelationshipData = TryCast(obj, RelationshipData)
            If objRef IsNot Nothing AndAlso objRef.id IsNot Nothing AndAlso objRef.id.Equals(Me.id) Then
                Return True
            End If
        End If
        Return False
    End Function

    ''' <summary>
    ''' A unique identification number for this relationship
    ''' (not the id of either issue in the relationship).
    ''' </summary>
    Public Property id() As String
        Get
            If relationship Is Nothing Then
                Return 0
            End If
            Return relationship.id
        End Get
        Set(value As String)
            relationship.id = value
        End Set
    End Property

    ''' <summary>What kind of relationship this is (e.g. "child of", "duplicate of" etc.).</summary>
    Public Property type() As ObjectRefWrapper
        Get
            If relationship Is Nothing Then
                Return Nothing
            End If
            Return New ObjectRefWrapper(relationship.type)
        End Get
        Set(value As ObjectRefWrapper)
            relationship.type = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>The id number of the Mantis issue that is being related to.</summary>
    Public Property target_id() As String
        Get
            If relationship Is Nothing Then
                Return 0
            End If
            Return relationship.target_id
        End Get
        Set(value As String)
            relationship.target_id = value
        End Set
    End Property
End Class
