﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' A wrapper around <see cref="AttachmentData"/> downloaded from Mantis
''' using its SOAP API. The main function of this wrapper is to add
''' the <see cref="AttachmentWrapper.previewImage"/> field which
''' will download a bitmap image for this attahment if applicable, and
''' can be bound the the UI using "isAsync" in the XAML binding.
'''
''' The code for <see cref="AttachmentData"/> is
''' auto-generated from the Mantis WSDL, so this class is used to
''' extend its functionality without editing the auto-generated code
''' (which allows it to be safely re-generated whenever the API changes).
''' </summary>
Public Class AttachmentWrapper

    ''' <summary>The Mantis attachment this object wraps.</summary>
    Public Property attachment As AttachmentData

    ''' <summary>Wrap the given Mantis attachment.</summary>
    ''' <param name="attachment">The attachment to wrap.</param>
    Sub New(attachment As AttachmentData)
        Me.attachment = attachment
    End Sub

    ''' <summary>
    ''' The unique id number of this attachment
    ''' (not the id of the issue it is attached to).
    ''' </summary>
    Public Property id As String
        Get
            Return attachment.id
        End Get
        Set(value As String)
            attachment.id = value
        End Set
    End Property

    ''' <summary>
    ''' If this attachment's filename ends in ".jpg", ".png", or ".gif" then
    ''' accessing this property will download the image to the temp directory
    ''' so that it can be displayed on the UI. This field should be threadsafe,
    ''' so it is safe to bind it using "isAsync" in the XAML binding to download
    ''' the image in a background thread. Will return <c>Nothing</c> if the
    ''' attachment is not an image, the image is corrupted, or the download fails.
    ''' </summary>
    ReadOnly Property previewImage()
        Get
            If filename.EndsWith(".jpg") OrElse filename.EndsWith(".png") OrElse filename.EndsWith(".gif") Then
                Dim bi As New BitmapImage()
                bi.BeginInit()

                Dim mantisClient As New MantisConnect
                Dim tempFile As Byte()
                tempFile = mantisClient.mc_issue_attachment_get(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, id)
                Dim fileString As String = System.IO.Path.GetTempPath & "intelligence\" & id & "_" & filename
                Debug.Print("Writing " & id & " = " & filename & " to " & fileString)
                System.IO.Directory.CreateDirectory(System.IO.Path.GetTempPath & "intelligence")
                Debug.Print("made dir path")
                If Not (My.Computer.FileSystem.FileExists(fileString)) Then
                    Debug.Print("Downloading " & id)
                    System.IO.File.WriteAllBytes(fileString, tempFile)
                End If
                Debug.Print("set uri to " & fileString)
                bi.UriSource = New Uri(fileString)

                Try
                    bi.EndInit()
                    bi.Freeze()
                    Return bi
                Catch e As NotSupportedException
                    Return Nothing
                End Try
            End If
            Return Nothing
        End Get
    End Property

    ''' <summary>The name of the attachment as shown on Mantis (i.e. not a local file path).</summary>
    Public Property filename As String
        Get
            Return attachment.filename
        End Get
        Set(value As String)
            attachment.filename = value
        End Set
    End Property

    ''' <summary>The attachment's filesize in bytes.</summary>
    Public Property size As String
        Get
            Return attachment.size
        End Get
        Set(value As String)
            attachment.size = value
        End Set
    End Property

    ''' <summary>The mimetype of the attachment.</summary>
    Public Property content_type As String
        Get
            Return attachment.content_type
        End Get
        Set(value As String)
            attachment.content_type = value
        End Set
    End Property

    ''' <summary>When the attachment was uploaded.</summary>
    Public Property date_submitted As Date
        Get
            Return attachment.date_submitted
        End Get
        Set(value As Date)
            attachment.date_submitted = value
        End Set
    End Property

    ''' <summary>Whether the timestamp for when the attachment was uploaded was supplied.</summary>
    Public Property date_submittedSpecified As Boolean
        Get
            Return attachment.date_submittedSpecified
        End Get
        Set(value As Boolean)
            attachment.date_submittedSpecified = value
        End Set
    End Property

    ''' <summary>The url from which the attachment can be downloaded.</summary>
    <System.Xml.Serialization.SoapElementAttribute(DataType:="anyURI")> _
    Public Property download_url As String
        Get
            Return attachment.download_url
        End Get
        Set(value As String)
            attachment.download_url = value
        End Set
    End Property

    ''' <summary>The id of the Mantis user who uploaded the attachment.</summary>
    <System.Xml.Serialization.SoapElementAttribute(DataType:="integer")> _
    Public Property user_id As String
        Get
            Return attachment.user_id
        End Get
        Set(value As String)
            attachment.user_id = value
        End Set
    End Property
End Class