﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' A wrapper around <see cref="IssueNoteData"/> downloaded from Mantis
''' using its SOAP API. The main function of this wrapper is to override
''' the <see cref="IssueNoteDataWrapper.Equals"/> method to allow for
''' comparison on the note's id.
''' 
''' The code for <see cref="IssueNoteData"/> is
''' auto-generated from the Mantis WSDL, so this class is used to
''' extend its functionality without editing the auto-generated code
''' (which allows it to be safely re-generated whenever the API changes).
''' </summary>
Public Class IssueNoteDataWrapper

    ''' <summary>The note contained by this wrapper.</summary>
    Public note As IssueNoteData

    ''' <summary>Wrap the given note.</summary>
    ''' <param name="note">The note to wrap.</param>
    Sub New(note As IssueNoteData)
        Me.note = note
    End Sub

    ''' <summary>Return the note's text.</summary>
    ''' <returns>The text of the note.</returns>
    Public Overrides Function ToString() As String
        Return Me.text
    End Function

    ''' <summary>
    ''' Check if two notes are equal by comparing their ids.
    ''' Works for both wrapped and unwrapped <see cref="IssueNoteData"/>s.
    ''' </summary>
    ''' <param name="obj">The object to check equality for.</param>
    ''' <returns><c>True</c> if the notes have the same id.</returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing Then Return False
        Dim wrapper As IssueNoteDataWrapper = TryCast(obj, IssueNoteDataWrapper)
        If wrapper IsNot Nothing Then
            If wrapper.id IsNot Nothing AndAlso wrapper.id.Equals(Me.id) Then
                Return True
            End If
        Else
            Dim objRef As IssueNoteData = TryCast(obj, IssueNoteData)
            If objRef IsNot Nothing AndAlso objRef.id IsNot Nothing AndAlso objRef.id.Equals(Me.id) Then
                Return True
            End If
        End If
        Return False
    End Function

    ''' <summary>A unique identification number for this note on Mantis.</summary>
    Public Property id() As String
        Get
            If note Is Nothing Then
                Return 0
            End If
            Return note.id
        End Get
        Set(value As String)
            note.id = value
        End Set
    End Property

    ''' <summary>The details of the user who wrote this note on Mantis.</summary>
    Public Property reporter() As AccountData
        Get
            If note Is Nothing Then
                Return Nothing
            End If
            Return note.reporter
        End Get
        Set(value As AccountData)
            note.reporter = value
        End Set
    End Property

    ''' <summary>The actual textual content of the note.</summary>
    Public Property text() As String
        Get
            If note Is Nothing Then
                Return ""
            End If
            Return note.text
        End Get
        Set(value As String)
            note.text = value
        End Set
    End Property

    ''' <summary>Determines who can see the note (e.g. "public" or "private").</summary>
    Public Property view_state() As ObjectRefWrapper
        Get
            If note Is Nothing Then
                Return Nothing
            End If
            Return New ObjectRefWrapper(note.view_state)
        End Get
        Set(value As ObjectRefWrapper)
            note.view_state = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>When the note was uploaded to Mantis.</summary>
    Public Property date_submitted() As Date
        Get
            If note Is Nothing Then
                Return Nothing
            End If
            Return note.date_submitted
        End Get
        Set(value As Date)
            note.date_submitted = value
        End Set
    End Property

    ''' <summary>Whether a submission timestamp was given for this note.</summary>
    Public Property date_submittedSpecified() As Boolean
        Get
            If note Is Nothing Then
                Return 0
            End If
            Return note.date_submittedSpecified
        End Get
        Set(value As Boolean)
            note.date_submittedSpecified = value
        End Set
    End Property

    ''' <summary>When the note was last updated on Mantis.</summary>
    Public Property last_modified() As Date
        Get
            If note Is Nothing Then
                Return Nothing
            End If
            Return note.last_modified
        End Get
        Set(value As Date)
            note.last_modified = value
        End Set
    End Property

    ''' <summary>Whether a modification timestamp was given for this note.</summary>
    Public Property last_modifiedSpecified() As Boolean
        Get
            If note Is Nothing Then
                Return 0
            End If
            Return note.last_modifiedSpecified
        End Get
        Set(value As Boolean)
            note.last_modifiedSpecified = value
        End Set
    End Property
End Class
