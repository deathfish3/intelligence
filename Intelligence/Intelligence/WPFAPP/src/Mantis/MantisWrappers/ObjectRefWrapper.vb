﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' A wrapper around <see cref="ObjectRef"/> downloaded from Mantis
''' using its SOAP API. Throughout the SOAP API, <see cref="ObjectRef"/>s
''' are used to store values with a numerical id for the databse, and seperate 
''' human-readable name. Such fields include Severity, Priority, and Status.
''' 
''' The main function of this wrapper is to override
''' the <see cref="ObjectRefWrapper.Equals"/> method to allow for
''' comparison on the ObjectRef's id and name, and also make it displayable 
''' in the UI with its new <see cref="ObjectRefWrapper.ToString"/> method.
''' 
''' The code for <see cref="ObjectRef"/> is
''' auto-generated from the Mantis WSDL, so this class is used to
''' extend its functionality without editing the auto-generated code
''' (which allows it to be safely re-generated whenever the API changes).
''' </summary>
Public Class ObjectRefWrapper

    ''' <summary>The ObjectRef contained by this wrapper.</summary>
    Public Property objectRef As ObjectRef

    ''' <summary>Wrap the given <see cref="ObjectRef"/>.</summary>
    ''' <param name="objectRef">The <see cref="ObjectRef"/> to wrap.</param>
    Sub New(objectRef As ObjectRef)
        Me.objectRef = objectRef
    End Sub

    ''' <summary>Return the human-readable name of the <see cref="ObjectRef"/>.</summary>
    Public Overrides Function ToString() As String
        Return name
    End Function

    ''' <summary>
    ''' Override the default equality method to match other object refs
    ''' with the same id OR name. Will also match strings or integers
    ''' which match the id number or name of this ObjectRef to allow
    ''' for more flexibility.
    ''' </summary>
    ''' <param name="obj">The object to check equality with.</param>
    ''' <returns><c>True</c> if the id or name matches.</returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing Then Return False
        Dim wrapper As ObjectRefWrapper = TryCast(obj, ObjectRefWrapper)
        If wrapper IsNot Nothing Then
            If (wrapper.id IsNot Nothing AndAlso wrapper.id.Equals(Me.id)) _
            OrElse (wrapper.name IsNot Nothing AndAlso wrapper.name.Equals(Me.name)) Then
                Return True
            End If
        Else
            Dim objRef As ObjectRef = TryCast(obj, ObjectRef)
            If objRef IsNot Nothing Then
                If ((obj.id IsNot Nothing AndAlso objRef.id.Equals(Me.id)) _
                OrElse (objRef.name IsNot Nothing AndAlso objRef.name.Equals(name))) Then
                    Return True
                End If
            End If
        End If
        Return obj.ToString IsNot Nothing AndAlso (obj.ToString.Equals(Me.id) OrElse obj.ToString.Equals(Me.name))
    End Function

    ''' <summary>A unique number to identify which value this ObjectRef represents in the Mantis database.</summary>
    Public Property id As String
        Get
            If objectRef Is Nothing Then
                Return 0
            End If
            Return objectRef.id
        End Get
        Set(value As String)
            objectRef.id = value
        End Set
    End Property

    ''' <summary>The human readable name of this ObjectRef.</summary>
    Public Property name As String
        Get
            If objectRef Is Nothing Then
                Return ""
            End If
            Return objectRef.name
        End Get
        Set(value As String)
            objectRef.name = value
        End Set
    End Property
End Class
