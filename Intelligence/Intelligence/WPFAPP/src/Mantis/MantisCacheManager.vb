﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' Retrieves and stores Enums, Accounts and Categories from Mantis for quick access.
''' </summary>
''' <remarks></remarks>
Public Class MantisCacheManager

    Private Shared _mantisUserList As AccountData()
    ''' <summary>
    ''' Retrieves all current Mantis Accounts
    ''' </summary>
    ''' <value></value>
    ''' <returns>Users in array as AccountData, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisUsers As AccountData()
        Get
            If _mantisUserList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisUserList = mantis.mc_project_get_users(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, 0, 10)
            End If
            Return _mantisUserList
        End Get
    End Property

    Private Shared _mantisDevsList As AccountData()
    ''' <summary>
    ''' Retrieves all current Dev Mantis Accounts
    ''' </summary>
    ''' <value></value>
    ''' <returns>Devs in array as AccountData, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisDevs As AccountData()
        Get
            If _mantisDevsList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisDevsList = mantis.mc_project_get_users(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, 0, 55)
            End If
            Return _mantisDevsList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves a Mantis User after consuming a Mantis ID
    ''' </summary>
    ''' <param name="mantisUserId"></param>
    ''' <returns>Requested Mantis User as AccountData, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function userById(mantisUserId As Integer) As AccountData
        If mantisUserId = 0 Then Return Nothing
        Dim user As AccountData = MantisDevs.Where(Function(u) u.id = mantisUserId).FirstOrDefault
        If user Is Nothing Then user = MantisUsers.Where(Function(u) u.id = mantisUserId).FirstOrDefault
        Return user
    End Function

    Private Shared _mantisPrioritiesList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current Priority Enums on Mantis.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Priority Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisPriorities As ObjectRef()
        Get
            If _mantisPrioritiesList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisPrioritiesList = mantis.mc_enum_priorities(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisPrioritiesList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves Priority Enum by it's ID property from MantisPriorities. 
    ''' </summary>
    ''' <param name="priorityId">The Priority Enum ID to retrieve</param>
    ''' <returns>Requested Priority Enum as ObjectRef</returns>
    ''' <remarks></remarks>
    Public Shared Function priorityById(priorityId As Integer) As ObjectRef
        Return MantisPriorities.Where(Function(p) p.id = priorityId).FirstOrDefault
    End Function

    Private Shared _mantisStatusList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current Status Enums on Mantis
    ''' </summary>
    ''' <value></value>
    ''' <returns>Status Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisStatuses As ObjectRef()
        Get
            If _mantisStatusList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisStatusList = mantis.mc_enum_status(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisStatusList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves Status Enum by it's ID property from MantisStatuses
    ''' </summary>
    ''' <param name="statusId">The Status Enum ID to retrieve</param>
    ''' <returns>Requested Status Enum as ObjectRef</returns>
    ''' <remarks></remarks>
    Public Shared Function statusById(statusId As Integer) As ObjectRef
        Return MantisStatuses.Where(Function(s) s.id = statusId).FirstOrDefault
    End Function

    Private Shared _mantisResolutionsList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current Resolution Enums on Mantis
    ''' </summary>
    ''' <value></value>
    ''' <returns>Resolution Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisResolutions As ObjectRef()
        Get
            If _mantisResolutionsList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisResolutionsList = mantis.mc_enum_resolutions(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisResolutionsList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves Resolution Enum by it's ID property from MantisResolutions.
    ''' </summary>
    ''' <param name="resolutionId">The Resolution Enum ID to retrieve</param>
    ''' <returns>Requested Resolution Enum as ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function resolutionById(resolutionId As Integer) As ObjectRef
        Return MantisResolutions.Where(Function(r) r.id = resolutionId).FirstOrDefault
    End Function

    Private Shared _mantisSeveritiesList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current Severity Enums on Mantis
    ''' </summary>
    ''' <value></value>
    ''' <returns>Severity Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisSeverities As ObjectRef()
        Get
            If _mantisSeveritiesList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisSeveritiesList = mantis.mc_enum_severities(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisSeveritiesList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves Severity Enum by it's ID property from MantisSeverities
    ''' </summary>
    ''' <param name="severityId">The Severity Enum ID to retrieve.</param>
    ''' <returns>Requested Severity Enum as ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function severityById(severityId As Integer) As ObjectRef
        Return MantisSeverities.Where(Function(s) s.id = severityId).FirstOrDefault
    End Function

    Private Shared _mantisReproducibilitiesList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current Reproducibility Enums on Mantis
    ''' </summary>
    ''' <value></value>
    ''' <returns>Reproducibility Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisReproducibilities
        Get
            If _mantisReproducibilitiesList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisReproducibilitiesList = mantis.mc_enum_reproducibilities(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisReproducibilitiesList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves Reproducibility Enum by it's ID property from MantisReproducibilities
    ''' </summary>
    ''' <param name="reproId">The Reproducibility Enum ID to retrieve.</param>
    ''' <returns>Requested Reproducibility Enum as ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function reproducibilityById(reproId As Integer) As ObjectRef
        Return MantisSeverities.Where(Function(r) r.id = reproId).FirstOrDefault
    End Function

    Private Shared _mantisViewStatesList As ObjectRef()
    ''' <summary>
    ''' Retrieves all current View State Enums on Mantis
    ''' </summary>
    ''' <value></value>
    ''' <returns>View State Enums as Array of ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MantisViewStates
        Get
            If _mantisViewStatesList Is Nothing Then
                Dim mantis As New MantisConnect
                _mantisViewStatesList = mantis.mc_enum_view_states(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
            End If
            Return _mantisViewStatesList
        End Get
    End Property

    ''' <summary>
    ''' Retrieves View State Enum by it's ID property from MantisViewStates
    ''' </summary>
    ''' <param name="viewStateId">The View State Enum ID to retireve.</param>
    ''' <returns>Requested View State Enum as ObjectRef, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function viewStateById(viewStateId As Integer) As ObjectRef
        Return MantisSeverities.Where(Function(v) v.id = viewStateId).FirstOrDefault
    End Function

    Private Shared _mantisCategoryDictionary As New Dictionary(Of String, String())
    ''' <summary>
    ''' Retrieves all Mantis Categories in a Mantis Project.
    ''' </summary>
    ''' <param name="projectId">The Mantis Project ID to retrieve from.</param>
    ''' <returns>Mantis Categories as Array of String, else nothing.</returns>
    ''' <remarks></remarks>
    Public Shared Function MantisCategoriesInProject(projectId As String) As String()
        If Not _mantisCategoryDictionary.ContainsKey(projectId) Then
            Dim mantis As New MantisConnect
            _mantisCategoryDictionary.Add(projectId, mantis.mc_project_get_categories(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword, projectId))
        End If
        Return _mantisCategoryDictionary(projectId)
    End Function

End Class
