﻿Imports WPFAPP.com.yoyogames.bugs

''' <summary>
''' A utility class for creating <see cref="ObjectRef"/>s.
''' These are autmatically generated from Mantis's WSDL, so this
''' class is provided to avoid adding custom constructors in the auto-generated code
''' (which would need re-added every time the WSDL was refreshed).
''' </summary>
Public Class ObjectRefFactory

    ''' <summary>Create a new <see cref="ObjectRef"/> with the given id and name.</summary>
    ''' <param name="id">The id number of this <see cref="ObjectRef"/>.</param>
    ''' <param name="name">The human readable name of the <see cref="ObjectRef"/>.</param>
    ''' <returns>A new <see cref="ObjectRef"/> created with the given values.</returns>
    Public Shared Function Create(id As String, name As String) As ObjectRef
        Return New ObjectRef With {.id = id, .name = name}
    End Function

    ''' <summary>Create a new <see cref="ObjectRef"/> with the given id but no name.</summary>
    ''' <param name="id">The id number of this <see cref="ObjectRef"/>.</param>
    ''' <returns>A new <see cref="ObjectRef"/> created with the given id.</returns>
    Public Shared Function Create(id As String) As ObjectRef
        Return New ObjectRef With {.id = id}
    End Function

End Class
