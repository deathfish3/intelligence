﻿Imports WPFAPP.com.yoyogames.bugs
Imports System.Web.Services.Protocols
Imports System.ComponentModel

''' <summary>
''' A wrapper around an <see cref="IssueData"/> from the Mantis SOAP API.
''' Provides easier ways of binding the <see cref="IssueData"/>'s fields
''' to the UI, and methods for adding, deleting, and updating various
''' fields within the issue on the Mantis server.
''' </summary>
Public Class MantisIssue
    Inherits NotifyPropertyChangedBase

    ''' <summary>
    ''' A container that holds all the information about the Mantis
    ''' issue in a way that is compatible with the Mantis SOAP API.
    ''' </summary>
    Public internalIssueData As IssueData

#Region "Source Information"
    '#########################################################
    '#                   Source Information                  #
    '#########################################################

    ''' <summary>
    ''' All the different ways a <see cref="MantisIssue"/> can be created (from scratch
    '''  in Intelligence, downloaded from Mantis, or imported from Zendesk).
    ''' </summary>
    Enum IssueSource
        NewIssue
        Mantis
        Zendesk
    End Enum

    ''' <summary>
    ''' Where the Mantis issue came from
    ''' (created from scratch in Intelligence, downloaded from Mantis, or imported from Zendesk).
    ''' </summary>
    Public Property source As IssueSource

    ''' <summary>
    ''' If the issue was imported from Zendesk, this field stores the original
    ''' Zendesk ticket's ID. Unused otherwise.
    ''' </summary>
    Public Property zendeskId As Integer

    ''' <summary>
    ''' If the issue was imported from Zendesk, this field stores the reporter
    ''' of the original Zendesk ticket. Unused otherwise.
    ''' </summary>
    Public Property zendeskUser As ZendeskUser
#End Region

#Region "Collections of Possible Enum Values"
    '#########################################################
    '#         Collections of Possible Enum Values           #
    '#########################################################

    ''' <summary>All possible categories this issue could be in (determined by its project).</summary>
    Public Property CategoryCollection As String()

    ''' <summary>All possible severities this issue could have (e.g. "C - General", "A - Crash/Hang" etc.).</summary>
    Public Property SeverityCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All possible reproducibility values this issue could have (e.g. "100%", "&lt;25%" etc.).</summary>
    Public Property ReproducibilityCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All possible priorities this issue could have (e.g. "low", "high" etc.).</summary>
    Public Property PriorityCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All possible statuses this issue could have (e.g. "assigned", "resolved" etc.).</summary>
    Public Property StatusCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All possible resolutions this issue could have (e.g. "open", "fixed" etc.)</summary>
    Public Property ResolutionCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All possible view states this issue could have (e.g. "public", "private" etc.)</summary>
    Public Property ViewStatesCollection As IEnumerable(Of ObjectRefWrapper)

    ''' <summary>All types of relation issues can participate in (e.g. "child of", "duplicate of" etc.).</summary>
    Public Property RelationTypeCollection As IEnumerable(Of ObjectRefWrapper)

#End Region

#Region "Mantis Issue Fields"
    '#########################################################
    '#               Mantis Issue Fields                     #
    '#########################################################

    ''' <summary>The id number of the issue in Mantis.</summary>
    ReadOnly Property IssueID As String
        Get
            Return internalIssueData.id
        End Get
    End Property

    ''' <summary>
    ''' All custom fields and their values for the given issue.
    ''' Custom fields include fields like "GM Version" and "Player Version". 
    ''' </summary>
    Property CustomFields As CustomFieldValueForIssueData()
        Get
            Return internalIssueData.custom_fields
        End Get
        Set(value As CustomFieldValueForIssueData())
            internalIssueData.custom_fields = value
        End Set
    End Property

    ''' <summary>
    ''' The string value of the category this issue is in.
    ''' The available categories depend on which project the issue is for.
    ''' </summary>
    Property Category As String
        Get
            Return internalIssueData.category
        End Get
        Set(value As String)
            internalIssueData.category = value
        End Set
    End Property

    ''' <summary>The operation system this bug is for (e.g "Windows 8").</summary>
    Property OS As String
        Get
            Return internalIssueData.os
        End Get
        Set(value As String)
            internalIssueData.os = value
        End Set
    End Property

    ''' <summary>The platform this bug is for (e.g Windows).</summary>
    Property Platform As String
        Get
            Return internalIssueData.platform
        End Get
        Set(value As String)
            internalIssueData.platform = value
        End Set
    End Property

    ''' <summary>How severe the bug is (e.g "C - General", "A - Crash/Hang" etc.).</summary>
    Property Severity As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.severity)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.severity = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>A short textual summary of the bug.</summary>
    Property Summary As String
        Get
            Return internalIssueData.summary
        End Get
        Set(value As String)
            internalIssueData.summary = value
        End Set
    End Property

    ''' <summary>A detailed textual description of the bug.</summary>
    Property Description As String
        Get
            Return internalIssueData.description
        End Get
        Set(value As String)
            internalIssueData.description = value
        End Set
    End Property

    ''' <summary>A set of steps to follow in order to reproduce the bug.</summary>
    Property Steps As String
        Get
            Return internalIssueData.steps_to_reproduce
        End Get
        Set(value As String)
            internalIssueData.steps_to_reproduce = value
        End Set
    End Property

    ''' <summary>
    ''' I'm not sure what this field is for.
    ''' Possibly "OS Version"? 
    ''' It is not bound to the UI, and is never set anywhere, but
    ''' I'm leaving it in in case it should be added to the UI in future.
    ''' </summary>
    Property Version As String
        Get
            If internalIssueData.version = Nothing Then
                Return "N/A"
            Else
                Return internalIssueData.version
            End If
        End Get
        Set(value As String)
            internalIssueData.version = value
        End Set
    End Property

    ''' <summary>How often the bug can be reproduced (e.g. "100%", of "&lt;25% etc.).</summary>
    Property Reproducibility As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.reproducibility)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.reproducibility = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>How urgently the bug needs worked on (e.g. "low", "high" etc.).</summary>
    Property Priority As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.priority)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.priority = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>How state in the bug-fixing process this issue is in (e.g. "assigned", "resolved" etc.).</summary>
    Property Status As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.status)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.status = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>How the this issue was finally dealt with (e.g. "fixed", "unable to reproduce" etc.).</summary>
    Property Resolution As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.resolution)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.resolution = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>Determines who can view this issue (e.g. "public", "private" etc.).</summary>
    Property Privacy As ObjectRefWrapper
        Get
            Return New ObjectRefWrapper(internalIssueData.view_state)
        End Get
        Set(value As ObjectRefWrapper)
            internalIssueData.view_state = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>The name of the user who reported the bug (or "N/A" if it has not been submitted yet).</summary>
    ReadOnly Property Reporter As String
        Get
            If internalIssueData.reporter Is Nothing Then
                Return "N/A"
            Else
                Return internalIssueData.reporter.name
            End If
        End Get
    End Property

    ''' <summary>Any extra notes about the bug. A link to a Zendesk ticket will be given here if applicable.</summary>
    Property AdditionalInformation As String
        Get
            Return internalIssueData.additional_information
        End Get
        Set(value As String)
            internalIssueData.additional_information = value
        End Set
    End Property

    ''' <summary>The string value of the timestamp when the bug was first submitted to Mantis.</summary>
    ReadOnly Property DateSubmitted As String
        Get
            If Not source.Equals(IssueSource.Mantis) Then
                Return "N/A"
            End If
            Return internalIssueData.date_submitted
        End Get
    End Property

    ''' <summary>The string value of the timestamp when the bug was most recently changed in Mantis.</summary>
    ReadOnly Property DateUpdated As String
        Get
            If Not source.Equals(IssueSource.Mantis) Then
                Return "N/A"
            End If
            Return internalIssueData.last_updated
        End Get
    End Property
#End Region

#Region "Attachments"
    '#########################################################
    '#                    Attachments                        #
    '#########################################################

    ''' <summary>A list of all the files attached to this issue converted to <see cref="AttachmentWrapper"/>s.</summary>
    Public ReadOnly Property AttachmentWrappers As ICollection(Of AttachmentWrapper)
        Get
            Dim list As New List(Of AttachmentWrapper)
            If internalIssueData.attachments IsNot Nothing Then
                For Each attachment In internalIssueData.attachments
                    list.Add(New AttachmentWrapper(attachment))
                Next
            End If
            Return list
        End Get
    End Property

    ''' <summary>
    ''' A command to open a file-selection dialog, and upload the chosen file as
    ''' an attachment for this issue on Mantis.
    ''' </summary>
    Public ReadOnly Property upload As ICommand
        Get
            Return New GenericCommand(
                Sub()
                    Dim openDlg As New Microsoft.Win32.OpenFileDialog()
                    openDlg.Filter = "All files (*.*)|*.*"

                    Dim result? As Boolean = openDlg.ShowDialog()

                    If result = True Then
                        Dim mantisLoader As New MantisLoader
                        LoadWithAsyncGUITask.run(Of AttachmentData)(
                            mantisLoader,
                            Function() mantisLoader.uploadMantisAttachment(openDlg.FileName, openDlg.SafeFileName, internalIssueData.id),
                            Sub(attachment)
                                MessageDialog.display("Success", "Finished uploading " & openDlg.SafeFileName & " for issue " & internalIssueData.id)
                                internalIssueData.attachments.Add(attachment)
                                onPropertyChanged(Function() Me.AttachmentWrappers)
                            End Sub)
                    End If
                End Sub)
        End Get
    End Property

    ''' <summary>
    ''' A command to a open file-selection dialog, and download the attachment
    '''from Mantis to the chosen local file system directory.
    ''' </summary>
    ReadOnly Property download As ICommand
        Get
            Return New GenericCommand(
                Sub(parameter)
                    Dim attachmentWrapper As AttachmentWrapper = DirectCast(parameter, AttachmentWrapper)
                    Dim attachment As AttachmentData = attachmentWrapper.attachment
                    Dim saveDlg As New Microsoft.Win32.SaveFileDialog()

                    saveDlg.FileName = attachment.filename
                    saveDlg.DefaultExt = System.IO.Path.GetExtension(attachment.filename) 'extensionString
                    saveDlg.Filter = "All files (*.*)|*.*"

                    Dim result? As Boolean = saveDlg.ShowDialog()

                    If result = True Then
                        Dim mantisLoader As New MantisLoader
                        LoadWithAsyncGUITask.run(Of Boolean)(
                            mantisLoader,
                            Function() mantisLoader.downloadMantisAttachment(saveDlg.FileName, attachment.id),
                            Sub(success) MessageDialog.display("Success", "Finished downloading " & attachment.filename))
                    End If
                End Sub)
        End Get
    End Property

    ''' <summary>A command to a delete this attachment from Mantis after confirming with the user.</summary>
    Public ReadOnly Property deleteAttachment As ICommand
        Get
            Return New GenericCommand(
                Sub(parameter)
                    Dim attachment As AttachmentData = DirectCast(parameter, AttachmentData)
                    If (YesNoDialog.display("Are you sure you want to delete this attachment?")) Then
                        Dim mantisLoader As New MantisLoader
                        LoadWithAsyncGUITask.run(Of Boolean)(
                            mantisLoader,
                            Function() mantisLoader.deleteMantisAttachment(Me, attachment.id),
                            Sub(success)
                                MessageDialog.display("Success", "Finished deleting attachment " & attachment.filename)
                                internalIssueData.attachments.RemoveAll(Function(a) a.id.Equals(attachment.id))
                                onPropertyChanged(Function() Me.AttachmentWrappers)
                            End Sub)
                    End If
                End Sub)
        End Get
    End Property

#End Region

#Region "Relationships"
    '#########################################################
    '#                  Relationships                        #
    '#########################################################

    ''' <summary>
    ''' All relationships with other bugs that this issue participates in
    ''' wrapped up as <see cref="RelationshipDataWrapper"/>s.
    ''' </summary>
    Public ReadOnly Property Relationships As IEnumerable(Of RelationshipDataWrapper)
        Get
            Dim relationshipList As New List(Of RelationshipDataWrapper)
            If internalIssueData.relationships IsNot Nothing Then
                For Each rel In internalIssueData.relationships
                    relationshipList.Add(New RelationshipDataWrapper(rel))
                Next
            End If
            Return relationshipList
        End Get
    End Property

    ''' <summary>
    ''' Stores the id number of a Mantis issue that is related to this one
    ''' in some way. When <see cref="addRelationship"/> is executed, this
    ''' id number will be the one used for the newly added relationship.
    ''' </summary>
    Public Property newRelationshipTargetId As String

    ''' <summary>
    ''' Stores the string value of the type of a relationship this Mantis issue that is
    ''' in (e.g. "child of", "duplicate of").
    ''' When <see cref="addRelationship"/> is executed, a new relationship
    ''' of this type will be added to Mantis with the issue whose id is <see cref="newRelationshipTargetId"/>
    ''' </summary>
    Public Property newRelationshipType As String

    ''' <summary>
    ''' A command for adding a new relationship of the type specified
    ''' in <see cref="newRelationshipType"/> with another Mantis issue
    ''' whose id is <see cref="newRelationshipTargetId"/>.
    ''' </summary>
    Public ReadOnly Property addRelationship As ICommand
        Get
            Return New GenericCommand(
                Sub()

                    Dim mantisLoader As New MantisLoader
                    Dim targetIdOut As String
                    Dim typeOut As String

                    targetIdOut = newRelationshipTargetId
                    typeOut = newRelationshipType

                    LoadWithAsyncGUITask.run(Of RelationshipData)(
                        mantisLoader, Nothing,
                        Function() mantisLoader.addMantisRelationship(Me, targetIdOut, typeOut),
                        Sub(relData)
                            internalIssueData.relationships.Add(relData)
                            onPropertyChanged(Function() Me.Relationships)
                        End Sub)
                End Sub)
        End Get
    End Property

    ''' <summary>A command for deleting the specified relationship from Mantis after confirming with the user.</summary>
    ReadOnly Property deleteRelationship As ICommand
        Get
            Return New GenericCommand(
                Sub(parameter)
                    Dim relWrapper As RelationshipDataWrapper = DirectCast(parameter, RelationshipDataWrapper)
                    Dim rel As RelationshipData = relWrapper.relationship
                    If (YesNoDialog.display("Are you sure you want to delete this relationship?")) Then
                        Dim mantisLoader As New MantisLoader
                        LoadWithAsyncGUITask.run(Of Boolean) _
                            (mantisLoader,
                            Function() mantisLoader.deleteRelationship(internalIssueData.id, rel.id),
                            Sub()
                                MessageDialog.display("Success", "Deleted relationship """ & rel.id & """")
                                internalIssueData.relationships.RemoveAll(Function(r) r.id.Equals(rel.id))
                                onPropertyChanged(Function() Me.Relationships)
                            End Sub)
                    End If
                End Sub)
        End Get
    End Property

#End Region

#Region "Notes"
    '#########################################################
    '#                       Notes                           #
    '#########################################################

    ''' <summary>
    ''' Stores the original text for the notes in this issue (the dictionary
    ''' key is the note's id number, and the string value is the original text).
    ''' This is filled in when a new issue is created by calling
    ''' <see cref="recordOriginalTextsOfAllNotes"/>, and is used
    ''' to check whether notes have changed their values before uploading
    ''' them to the Mantis server (this avoids unnecessary uploads,
    ''' and useless note update spam in the issue's history).
    ''' </summary>
    Private originalTextOfNotes As New Dictionary(Of Integer, String)

    ''' <summary>All notes for this issue wrapped up as <see cref="IssueNoteDataWrapper"/>s.</summary>
    Property NoteCollection As IEnumerable(Of IssueNoteDataWrapper)
        Get
            Dim notesList As New List(Of IssueNoteDataWrapper)
            If internalIssueData.notes IsNot Nothing Then
                For Each n In internalIssueData.notes
                    notesList.Add(New IssueNoteDataWrapper(n))
                Next
            End If
            Return notesList
        End Get
        Set(value As IEnumerable(Of IssueNoteDataWrapper))
            Dim noteArray As IssueNoteData() = {}
            For Each wrapper In value
                noteArray.Add(wrapper.note)
            Next
            internalIssueData.notes = noteArray
        End Set
    End Property

    ''' <summary>
    ''' Loop through this issue's notes, and record their
    ''' text in <see cref="originalTextOfNotes"/> indexed by their note id.
    ''' This is used to check whether notes have changed their values before uploading
    ''' them to the Mantis server (this avoids unnecessary uploads,
    ''' and useless note update spam in the issue's history).
    ''' </summary>
    Public Sub recordOriginalTextsOfAllNotes()
        originalTextOfNotes.Clear()
        If Me.NoteCollection IsNot Nothing Then
            For Each noteItem In Me.NoteCollection
                originalTextOfNotes.Add(noteItem.id, noteItem.text)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Checks whether notes have changed their values since
    ''' they were downloaded (this avoids unnecessary uploads,
    ''' and useless note update spam in the issue's history).
    ''' </summary>
    Public Function noteHasBeenUpdated(note As IssueNoteData)
        Dim originalText = originalTextOfNotes.Item(note.id)
        Return originalText Is Nothing OrElse Not originalText.Equals(note.text)
    End Function

    ''' <summary>
    ''' The text for the next note that will be added when
    ''' <see cref="addnewnote"/> is executed.
    ''' </summary>
    Public Property newNoteText As String

    ''' <summary>
    ''' The internally stored view state (e.g. "public" or "private") that
    ''' the next note added when <see cref="addnewnote"/> is executed will be set to.
    ''' </summary>
    Private _newNotePrivacy As ObjectRef

    ''' <summary>
    ''' The view state (e.g. "public" or "private") that the next note added when 
    ''' <see cref="addnewnote"/> is executed will be set to. Defaults to "public".
    ''' </summary>
    Property newNotePrivacy As ObjectRefWrapper
        Get
            If Not _newNotePrivacy Is Nothing Then
                Return New ObjectRefWrapper(_newNotePrivacy)
            Else
                Return New ObjectRefWrapper(ObjectRefFactory.Create(10, "public"))
            End If
        End Get
        Set(value As ObjectRefWrapper)
            If Not value Is Nothing Then
                Debug.Print("Privacy set: " & value.id & " " & value.name)
            End If
            _newNotePrivacy = If(value Is Nothing, Nothing, value.objectRef)
        End Set
    End Property

    ''' <summary>
    ''' A command for uploading a new note to Mantis for this issue
    ''' with the text from <see cref="newNoteText"/> and the view
    ''' state set in <see cref="newNotePrivacy"/> (which defaults to "public").
    ''' </summary>
    Public ReadOnly Property addnewnote As ICommand
        Get
            Return New GenericCommand(
                Sub()
                    Dim newNote As New IssueNoteData() With {.text = newNoteText, .view_state = newNotePrivacy.objectRef}
                    Dim mantisloader As New MantisLoader
                    LoadWithAsyncGUITask.run(Of String)(
                        mantisloader,
                        Function() mantisloader.addMantisNote(Me, newNote),
                        Sub(noteId)
                            MessageDialog.display("Success", "Note added.")

                            Dim note As New IssueNoteData() With {
                                    .id = noteId,
                                    .reporter = New AccountData() With {.name = UserInfo.user.mantisUsername},
                                    .date_submitted = Date.Now,
                                    .text = newNote.text,
                                    .view_state = newNotePrivacy.objectRef
                                }
                            internalIssueData.notes.Add(note)
                            onPropertyChanged(Function() Me.NoteCollection)
                        End Sub)
                End Sub)
        End Get
    End Property

    ''' <summary>A command for deleting the specified note from Mantis after confirming with the user.</summary>
    Public ReadOnly Property deleteNote As ICommand
        Get
            Return New GenericCommand(
                Sub(parameter)
                    Dim noteWrapper As IssueNoteDataWrapper = DirectCast(parameter, IssueNoteDataWrapper)
                    Dim note As IssueNoteData = noteWrapper.note
                    If (YesNoDialog.display("Are you sure you want to delete this note?")) Then
                        Dim mantisLoader As New MantisLoader
                        LoadWithAsyncGUITask.run(Of Boolean)(
                            mantisLoader,
                            Function() mantisLoader.deleteMantisNote(Me, note.id),
                            Sub(success)
                                MessageDialog.display("Success", "Finished deleting note " & note.id)
                                internalIssueData.notes.RemoveAll(Function(n) n.id.Equals(note.id))
                                onPropertyChanged(Function() Me.NoteCollection)
                            End Sub)
                    End If
                End Sub)
        End Get
    End Property

#End Region

End Class
