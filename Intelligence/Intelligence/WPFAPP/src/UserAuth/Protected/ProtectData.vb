﻿Imports System
Imports System.Text
Imports System.Security.Cryptography

Namespace ProtectData
    ''' <summary>
    ''' Allows creation/reading of protected data.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ProtectData
        Private Shared s_aditionalEntropy As Byte() = {9, 8, 7, 6, 5}
        ''' <summary>
        ''' Takes in data to protect using Cryptography.
        ''' </summary>
        ''' <param name="data">The data to be protected.</param>
        ''' <returns>Protected data if protection successful, else nothing.</returns>
        ''' <remarks></remarks>
        Public Shared Function Protect(ByVal data() As Byte) As Byte()
            Try
                ' Encrypt the data using DataProtectionScope.CurrentUser. The result can be decrypted 
                '  only by the same current user. 
                Return ProtectedData.Protect(data, s_aditionalEntropy, DataProtectionScope.LocalMachine)
            Catch e As CryptographicException
                Console.WriteLine("Data was not encrypted. An error occurred.")
                Console.WriteLine(e.ToString())
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Consumes protected data and unprotects it using Cryptography.
        ''' </summary>
        ''' <param name="data">The proected data to be unprotected.</param>
        ''' <returns>Unprotected data if unprotection successful, else nothing.</returns>
        ''' <remarks></remarks>
        Public Shared Function Unprotect(data As Byte()) As Byte()
            Try
                'Decrypt the data using DataProtectionScope.CurrentUser. 
                Return ProtectedData.Unprotect(data, s_aditionalEntropy, DataProtectionScope.LocalMachine)
            Catch e As CryptographicException
                Console.WriteLine("Data was not decrypted. An error occurred.")
                Console.WriteLine(e.ToString())
                Return Nothing
            End Try

        End Function

    End Class
End Namespace
