﻿Imports System
Imports System.Text
Imports System.Security.Cryptography



Namespace PassHasher



    ''Salted PASSWORD HASHING with PBKDF2-SHA1
    ''' <summary>
    ''' Allows creation and validation of a hash. Can be used to both create/read hashes.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class PassHasher

#Region "Operational Constants"
        ''May be changed without breaking existing hashes

        Public Const SALT_BYTE_SIZE As Integer = 24
        Public Const HASH_BYTE_SIZE As Integer = 24
        Public Const PBKDF2_ITERATIONS As Integer = 1000

        Public Const ITERATION_INDEX As Integer = 0
        Public Const SALT_INDEX As Integer = 1
        Public Const PBKDF2_INDEX As Integer = 2

#End Region


        ''' <summary>
        ''' Creates a salted PBKDF2 hash of password
        ''' </summary>
        ''' <param name="password">The desired string to be hashed.</param>
        ''' <returns>A salted hashed version of string submitted</returns>
        ''' <remarks></remarks>
        Public Shared Function CreateHash(password As String) As String
            'Generate Random Salt
            Dim csprng As New RNGCryptoServiceProvider
            Dim salt As Byte() = New Byte(SALT_BYTE_SIZE) {}
            csprng.GetBytes(salt)

            'Hash password and encode parameters
            Dim hash As Byte() = PBKDF2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE)
            Dim saltedPass As String = PBKDF2_ITERATIONS & ":" & Convert.ToBase64String(salt) & ":" & Convert.ToBase64String(hash)
            Return saltedPass
        End Function



        'Validates a password given a hash of the correct one
        ''' <summary>
        ''' Takes in a password string and the hash to compare it against.
        ''' </summary>
        ''' <param name="password">The password to try the hash against.</param>
        ''' <param name="correctHash">The hash belonging to the correct password.</param>
        ''' <returns>True if password correct, else False.</returns>
        ''' <remarks></remarks>
        Public Shared Function ValidatePassword(password As String, correctHash As String) As Boolean
            Dim delimeter As Char() = {":"c}
            Dim split As String() = correctHash.Split(delimeter)
            Dim iterations As Integer = Int32.Parse(split(ITERATION_INDEX))
            Dim salt As Byte() = Convert.FromBase64String(split(SALT_INDEX))
            Dim hash As Byte() = Convert.FromBase64String(split(PBKDF2_INDEX))

            Dim testHash As Byte() = PBKDF2(password, salt, iterations, hash.Length)
            Return SlowEquals(hash, testHash)

        End Function

        ''' <summary>
        ''' Comparison function that checks hash A against hash B.
        ''' </summary>
        ''' <param name="a"></param>
        ''' <param name="b"></param>
        ''' <returns>True if a matches b, else false.</returns>
        ''' <remarks></remarks>
        Private Shared Function SlowEquals(a As Byte(), b As Byte()) As Boolean
            Dim diff As UInteger = CUInt(a.Length) Xor CUInt(b.Length)
            Dim i As Integer = 0
            While i < a.Length AndAlso i < b.Length
                diff = diff Or CUInt(a(i) Xor b(i))
                i += 1
            End While
            Return diff = 0

        End Function

        ''' <summary>
        ''' Hashes the password string to PBKDF2 format with salt.
        ''' </summary>
        ''' <param name="password"></param>
        ''' <param name="salt"></param>
        ''' <param name="iterations"></param>
        ''' <param name="outputBytes"></param>
        ''' <returns>PBKDF2 formatted hash</returns>
        ''' <remarks></remarks>
        Private Shared Function PBKDF2(password As String, salt As Byte(), iterations As Integer, outputBytes As Integer)
            Dim _pbkdf2 As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(password, salt)
            _pbkdf2.IterationCount = iterations
            Return _pbkdf2.GetBytes(outputBytes)
        End Function









    End Class
End Namespace
