﻿''' <summary>
''' A command for editing an object in an <see cref="ICustomEditor(Of T)"/>
''' either in a separate dialog box, or in an editor panel to the right of the current one.
''' </summary>
Public NotInheritable Class EditCommand

    ''' <summary>
    ''' Create a new command for editing an object in a dialog box.
    ''' The object to edit is passed in via the command's parameter, and
    ''' can be attached to the "CommandParameter" field of a XAML binding.
    ''' </summary>
    ''' <typeparam name="TToEdit">The type of object to edit.</typeparam>
    ''' <typeparam name="TEditor">The type of <see cref="ICustomEditor(Of TToEdit)"/> to edit the object in.</typeparam>
    ''' <param name="refreshUI">An action to perform after the object has been edited (e.g. updating the UI).</param>
    ''' <returns>A new command for editing an object in a dialog box.</returns>
    Public Shared Function create _
        (Of TToEdit As Class, TEditor As {New, UserControl, ICustomEditor(Of TToEdit)}) _
        (refreshUI As System.Action) As ICommand
        Return New GenericCommand(
            Sub(toEdit As TToEdit)
                If toEdit IsNot Nothing _
                AndAlso CustomEditorDialog.showEditorDialog(toEdit, New TEditor) IsNot Nothing Then
                    refreshUI()
                End If
            End Sub)
    End Function

    ''' <summary>
    ''' Create a new command for editing an object in a panel to the right of the current one.
    ''' The object to edit is passed in via the command's parameter, and
    ''' can be attached to the "CommandParameter" field of a XAML binding.
    ''' </summary>
    ''' <typeparam name="T">The type of object from the Intelligence database to edit.</typeparam>
    ''' <typeparam name="W">The type of <see cref="Wrapper(Of T)"/> that wraps the object.</typeparam>
    ''' <typeparam name="E">They type of <see cref="ICustomEditor(Of W)"/> to edit the wrapper in.</typeparam>
    ''' <param name="sender">
    ''' The UI element contained within <paramref name="parentPanel"/> that
    ''' the new editor should be opened to the right of.
    ''' </param>
    ''' <param name="parentPanel">
    ''' The panel containing <paramref name="sender"/> where the
    ''' new editor should be opened in.
    ''' </param>
    ''' <param name="list">The <see cref="WrapperList(Of T)"/> the item to edit resides in.</param>
    ''' <returns>A new command for editing an object in a panel to the right of the current one.</returns>
    Public Shared Function create _
        (Of T As {Class, IHasID, New}, W As Wrapper(Of T), E As {New, UserControl, ICustomEditor(Of W)}) _
        (sender As Object, parentPanel As CustomDockPanel, list As WrapperList(Of T)) As ICommand
        Return New GenericCommand(
            Function(o) list IsNot Nothing AndAlso list.selectedItem IsNot Nothing,
            Sub(toEdit As W)
                If toEdit IsNot Nothing Then
                    Dim newEditor As New E
                    newEditor.DataContext = toEdit
                    parentPanel.showNewPanel(sender, newEditor)
                End If
            End Sub)
    End Function
End Class