﻿

''' <summary>
''' A command for deleting an object of type <paramref name="T"/> after receiving confirmation via a dialogue box.
''' If the user pressed "Yes", then it will call the given deletion function (to allow for cascading deletion), and refresh the UI.
''' </summary>
''' <typeparam name="T">The type of item to delete from the Intelligence database.</typeparam>
Public Class DeleteCommand(Of T As {Class, IHasID, New})
    Inherits GenericCommand

    ''' <summary>
    ''' Ask the user for confirmation when deleting the given item and if they accept, delete
    ''' it from the database by removing it from the given <see cref="WrapperList(Of T)"/>
    ''' and letting its <see cref="WrapperList(Of T).deleteAndSave"/> function handle the 
    ''' rest. Checks whether the given list and its selected item are Nothing before
    ''' allowing the command to be executed.
    ''' </summary>
    ''' <param name="list">The <see cref="WrapperList(Of T)"/> to remove the item from.</param>
    Sub New(list As WrapperList(Of T))
		Me.New(Function() list)
	End Sub

	Sub New(list As WrapperList(Of T), canPerformTask As Func(Of Boolean))
		Me.New(Function() list, Function() list IsNot Nothing AndAlso canPerformTask(), Nothing, Nothing)
	End Sub

    ''' <summary>
    ''' Ask the user for confirmation when deleting the given item and if they accept, delete
    ''' it from the database by removing it from the given <see cref="WrapperList(Of T)"/>
    ''' and letting its <see cref="WrapperList(Of T).deleteAndSave"/> function handle the 
    ''' rest. Checks whether the given list and its selected item are Nothing before
    ''' allowing the command to be executed.
    ''' </summary>
    ''' <param name="getList">A function that returns the correct <see cref="WrapperList(Of T)"/> to remove the item from.</param>
    Sub New(getList As System.Func(Of WrapperList(Of T)))
        Me.New(getList, Nothing)
    End Sub

    ''' <summary>
    ''' Ask the user for confirmation when deleting the given item and if
    ''' they accept, delete it from the database by removing it from the
    ''' given <see cref="WrapperList(Of T)"/> and letting its
    ''' <see cref="WrapperList(Of T).deleteAndSave"/> function handle the rest.
    ''' Checks whether the given list and its selected item are Nothing before
    ''' allowing the command to be executed, and allows for 
    ''' a custom action to be performed after deletion happens.
    ''' </summary>
    ''' <param name="getList">A function that returns the correct <see cref="WrapperList(Of T)"/> to remove the item from.</param>
    ''' <param name="onDeleted">
    ''' An action to perform after the item has been deleted (e.g. refresh the UI).
    ''' If this is <c>Nothing</c>, it will be ignored.
    ''' </param>
    Sub New(getList As System.Func(Of WrapperList(Of T)), onDeleted As System.Action)
        Me.New(getList,
               Function() getList() IsNot Nothing AndAlso getList().selectedItem IsNot Nothing,
               Nothing,
               Nothing)
    End Sub

    ''' <summary>
    ''' Ask the user for confirmation when deleting the given item and if
    ''' they accept, delete it from the database by removing it from the
    ''' given <see cref="WrapperList(Of T)"/> and letting its
    ''' <see cref="WrapperList(Of T).deleteAndSave"/> function handle the rest.
    ''' Also has custom checks for whether the given item can be deleted, and
    ''' custom actions that can be performed before and after deletion happens.
    ''' </summary>
    ''' <param name="getList">A function that returns the correct <see cref="WrapperList(Of T)"/> to remove the item from.</param>
    ''' <param name="canPerformTask">
    ''' A function for checking whether the deletion command can be executed.
    ''' If this is <c>Nothing</c>, it will be ignored.
    ''' </param>
    ''' <param name="beforeDelete">
    ''' An action to perform on the wrapper to delete after deletion is
    ''' confirmed but before it actually takes place.
    ''' If this is <c>Nothing</c>, it will be ignored.
    ''' </param>
    ''' <param name="onDeleted">
    ''' An action to perform after the item has been deleted (e.g. refresh the UI).
    ''' If this is <c>Nothing</c>, it will be ignored.
    ''' </param>
    Sub New(getList As System.Func(Of WrapperList(Of T)),
            canPerformTask As System.Func(Of Boolean),
            beforeDelete As System.Action(Of Wrapper(Of T)),
            onDeleted As System.Action)
        MyBase.New(
            Function(o) canPerformTask Is Nothing OrElse canPerformTask(),
            Sub(toDelete As Wrapper(Of T))
                If toDelete IsNot Nothing _
                AndAlso YesNoDialog.display("Are you sure you want to delete " & getName(toDelete) & "?") Then
                    Dim list As WrapperList(Of T) = getList()

                    If beforeDelete IsNot Nothing Then
                        beforeDelete(toDelete)
                    End If

                    If list IsNot Nothing Then
                        If list.deleteAndSave(toDelete) AndAlso onDeleted IsNot Nothing Then
                            onDeleted()
                        End If
                    End If
                End If
            End Sub)
    End Sub

    ''' <summary>
    ''' Get the name of the object to display in the dialog box when
    ''' asking "Are you sure you want to delete "X"?". Will usually
    ''' just use the wrapper's name, but if this is empty, it will
    ''' use the type of database object (i.e. instead
    ''' of "Are you sure you want to delete ""?" it will
    ''' say "Are you sure you want to delete this platform?".
    ''' </summary>
    ''' <param name="toDelete">A wrapper around the database item to be deleted.</param>
    ''' <returns>The string to call the given item by when asking for delete confirmation.</returns>
    Public Shared Function getName(toDelete As Wrapper(Of T))
		Dim name = toDelete.ToString
		If name Is Nothing OrElse name.Equals("") Then
			Dim typeName As String = GetType(T).ToString
			Dim splitName As String() = typeName.Split(".")
			Dim typeNameEnding As String = splitName.Last
			Dim typeNameWithSpaces As String = typeNameEnding.First
			For i = 1 To typeNameEnding.Length - 1
				Dim nextChar As String = typeNameEnding(i)
				If nextChar.ToUpper.Equals(nextChar) Then
					typeNameWithSpaces += " "
				End If
				typeNameWithSpaces += nextChar
			Next
			name = "this " & typeNameWithSpaces.ToLower
		Else
			name = """" & name & """ "
		End If
		Return name
	End Function

End Class
