﻿Imports System.Data.Entity

''' <summary>
''' A command for adding a new object to a <see cref="WrapperList(Of T)"/>
''' and to the Intelligence database before opening a new editor panel
''' to modify the fields of the newly added object.
''' </summary>
''' <typeparam name="T">The type of Intelligece database item to edit.</typeparam>
''' <typeparam name="W">The type of <see cref="Wrapper(Of T)"/> around the database item.</typeparam>
''' <typeparam name="E">The type of <see cref="ICustomEditor(Of W)"/> to edit the newly added wrapper in.</typeparam>
Public NotInheritable Class AddCommand(Of T As {Class, IHasID, New}, W As Wrapper(Of T), E As {New, UserControl, ICustomEditor(Of W)})
    Inherits GenericCommand

    ''' <summary>
    ''' Create a new command for adding a new object to a <see cref="WrapperList(Of T)"/>
    ''' and to the Intelligence database before opening a new editor panel
    ''' to modify the fields of the newly added object.
    ''' </summary>
    ''' <param name="sender">
    ''' The UI element contained within <paramref name="parentPanel"/> that
    ''' the new editor should be opened to the right of.
    ''' </param>
    ''' <param name="parentPanel">
    ''' The panel containing <paramref name="sender"/> where the
    ''' new editor should be opened in.
    ''' </param>
    ''' <param name="list"> The <see cref="WrapperList(Of T)"/> to add the new item to.</param>
    Public Sub New(sender As Object,
                   parentPanel As CustomDockPanel,
                   list As WrapperList(Of T))
        Me.New(sender, parentPanel, Function() list, Nothing)
    End Sub

    ''' <summary>
    ''' Create a new command for adding a new object to a <see cref="WrapperList(Of T)"/>
    ''' and to the Intelligence database before opening a new editor panel
    ''' to modify the fields of the newly added object. Also specifies
    ''' custom a action to be performed when the new item has been added.
    ''' </summary>
    ''' <param name="sender">
    ''' The UI element contained within <paramref name="parentPanel"/> that
    ''' the new editor should be opened to the right of.
    ''' </param>
    ''' <param name="parentPanel">
    ''' The panel containing <paramref name="sender"/> where the
    ''' new editor should be opened in.
    ''' </param>
    ''' <param name="getList">
    ''' A function that will return the correct <see cref="WrapperList(Of T)"/>
    ''' to add the new item to.
    ''' </param>
    ''' <param name="onAdded">An action called after the new item has been added to the database.</param>
    Public Sub New(sender As Object,
                   parentPanel As CustomDockPanel,
                   getList As System.Func(Of WrapperList(Of T)),
                   onAdded As System.Action(Of W))
        Me.New(sender, parentPanel, getList, Nothing, onAdded)
    End Sub

    ''' <summary>
    ''' Create a new command for adding a new object to a <see cref="WrapperList(Of T)"/>
    ''' and to the Intelligence database before opening a new editor panel
    ''' to modify the fields of the newly added object. Also specifies
    ''' custom actions to be performed when the editor is created, and when
    ''' the new item has been added.
    ''' </summary>
    ''' <param name="sender">
    ''' The UI element contained within <paramref name="parentPanel"/> that
    ''' the new editor should be opened to the right of.
    ''' </param>
    ''' <param name="parentPanel">
    ''' The panel containing <paramref name="sender"/> where the
    ''' new editor should be opened in.
    ''' </param>
    ''' <param name="getList">
    ''' A function that will return the correct <see cref="WrapperList(Of T)"/>
    ''' to add the new item to.
    ''' </param>
    ''' <param name="onEditorCreated">An action called when the new editor is created.</param>
    ''' <param name="onAdded">An action called after the new item has been added to the database.</param>
    Public Sub New(sender As Object,
                   parentPanel As CustomDockPanel,
                   getList As System.Func(Of WrapperList(Of T)),
                   onEditorCreated As System.Action(Of E),
                   onAdded As System.Action(Of W))
        MyBase.New(
            Sub()
                Dim list = getList()
                Dim newItem As Wrapper(Of T) = list.addNewAndSave()
                list.selectedItem = newItem

                Dim newEditor As New E
                newEditor.DataContext = newItem
                If onEditorCreated IsNot Nothing Then
                    onEditorCreated(newEditor)
                End If
                parentPanel.showNewPanel(sender, newEditor)

                If onAdded IsNot Nothing Then
                    onAdded(newItem)
                End If
            End Sub)
    End Sub

End Class