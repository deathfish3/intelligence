﻿''' <summary>
''' A set of extensions to arrays that makes them behave more like <see cref="List(Of T)"/>.
''' </summary>
''' <remarks>
''' Usually to get this functionallity, a <see cref="List(Of T)"/> would
''' be better, but the fields in <see cref="MantisIssue"/>s are read in as arrays.
''' When adding or removing new notes or attachments, these extensions are very useful to have.
''' </remarks>
Module ArrayExtension

    ''' <summary>
    ''' An extension method for arrays that
    ''' dynamically resizes them when adding new data.
    ''' </summary>
    ''' <typeparam name="T">The type of items the array contains.</typeparam>
    ''' <param name="arr">The array to add to.</param>
    ''' <param name="item">The item to add. Will be placed at the end of the array.</param>
    ''' <remarks>
    ''' Usually to get this functionallity, a <see cref="List(Of T)"/> would
    ''' be better, but the fields in <see cref="MantisIssue"/>s are read in as arrays.
    ''' When adding new notes or attachments, this extension is very useful to have.
    ''' </remarks>
    <Runtime.CompilerServices.Extension()> _
    Public Sub Add(Of T)(ByRef arr As T(), item As T)
        If arr IsNot Nothing Then
            Array.Resize(arr, arr.Length + 1)
            arr(arr.Length - 1) = item
        Else
            ReDim arr(0)
            arr(0) = item
        End If
    End Sub

    ''' <summary>
    ''' An extension method for arrays that dynamically resizes them when
    ''' removing an object to avoid entries that are <c>Nothing</c>.
    ''' </summary>
    ''' <typeparam name="T">The type of items the array contains.</typeparam>
    ''' <param name="arr">
    ''' The array to remove from. 
    ''' Indices outside the range of the array will not crash, but will be ignored.
    ''' </param>
    ''' <param name="index">The index of the item to remove.</param>
    ''' <remarks>
    ''' Usually to get this functionallity, a <see cref="List(Of T)"/> would
    ''' be better, but the fields in <see cref="MantisIssue"/>s are read in as arrays.
    ''' When removing notes or attachments, this extension is very useful to have.
    ''' </remarks>
    <System.Runtime.CompilerServices.Extension()> _
    Public Sub RemoveAt(Of T)(ByRef arr As T(), ByVal index As Integer)
        Dim uBound = arr.GetUpperBound(0)
        Dim lBound = arr.GetLowerBound(0)
        Dim arrLen = uBound - lBound

        If index < lBound OrElse index > uBound Then
            Throw New ArgumentOutOfRangeException( _
            String.Format("Index must be from {0} to {1}.", lBound, uBound))

        Else
            'create an array 1 element less than the input array
            Dim outArr(arrLen - 1) As T
            'copy the first part of the input array
            Array.Copy(arr, 0, outArr, 0, index)
            'then copy the second part of the input array
            Array.Copy(arr, index + 1, outArr, index, uBound - index)

            arr = outArr
        End If
    End Sub

    ''' <summary>
    ''' An extension method for arrays removes any items matching the given condition,
    ''' and dynamically resizes the array to avoid entries that are <c>Nothing</c>.
    ''' </summary>
    ''' <typeparam name="T">The type of item the array contains.</typeparam>
    ''' <param name="arr">The array to remove from.</param>
    ''' <param name="matching">
    ''' A function that returns True if the item should be removed. Such as:
    ''' <example><code>
    ''' Function(listItem As T) listItem.name.Equals("Jerry")
    ''' </code></example>
    ''' </param>
    ''' <remarks>
    ''' Usually to get this functionallity, a <see cref="List(Of T)"/> would
    ''' be better, but the fields in <see cref="MantisIssue"/>s are read in as arrays.
    ''' When removing multiple notes or attachments, this extension is very useful to have.
    ''' </remarks>
    <System.Runtime.CompilerServices.Extension()> _
    Public Sub RemoveAll(Of T)(ByRef arr As T(), matching As Predicate(Of T))
        If Not IsNothing(arr) Then
            If arr.Count > 0 Then
                Dim ls As List(Of T) = arr.ToList
                ls.RemoveAll(matching)
                arr = ls.ToArray
            End If
        End If
    End Sub
End Module
