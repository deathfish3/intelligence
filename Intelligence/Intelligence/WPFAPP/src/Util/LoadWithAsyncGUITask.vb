﻿''' <summary>
''' Performing tasks in background threads that require a loading indicator, an error dialog
''' box if they fail, and a UI operation to be performed when they complete.
''' </summary>
''' <remarks>
''' All UI operations must happen on the UI thread. For backgound tasks that require a loading
''' dialog to be opened on startup and closed on completion, this complicates things. Error
''' dialogs must also be opened on the UI thread, so errors must be caught and handled on
''' a different thread from the one they occurred on. On top of that, many tasks require UI
''' updates when they finish successfully too, so there needs to be a way of returning to the
''' UI thread when the task completes.
''' 
''' This class has several static methods that work using subclasses of <see cref="Loader"/> 
''' to to perform background tasks, and give contextual progress updates and error messages.
''' </remarks>
Public Class LoadWithAsyncGUITask

    ''' <summary>
    ''' Run the given background function (<paramref name="func"/>) in a separate thread with a loading indicator.
    ''' When the background task completes, the result is passed to <paramref name="finishedFunc"/> which
    ''' is run on the UI thread. Error messages will caught cause an error dialog box to be shown.
    ''' Progress updates and contextual error messages will be provided by <paramref name="loader"/>
    ''' </summary>
    ''' <typeparam name="T">The return type of the background task.</typeparam>
    ''' <param name="loader">Provides contextual error messages and progress updates.</param>
    ''' <param name="func">A function to run in a background thread (usually a member of <paramref name="loader"/></param>
    ''' <param name="finishedFunc">The function to run on the UI thread once <paramref name="func"/> has returned its results.</param>
    Public Shared Sub run(Of T)(loader As Loader, func As System.Func(Of T), finishedFunc As System.Action(Of T))
        Dim loadingIndicator As LoadingDialog = LoadingDialog.getInstance(MainWindow.mainWindow)
        loader.addListener(loadingIndicator)
        run(Of T)(loader, Nothing, Task(Of T).Factory.StartNew(func, TaskCreationOptions.LongRunning), finishedFunc, loadingIndicator)
    End Sub

    ''' <summary>
    ''' Run the given background function (<paramref name="func"/>) in a separate thread with a loading indicator.
    ''' When the background task completes, the result is passed to <paramref name="finishedFunc"/> which
    ''' is run on the UI thread. Error messages will caught cause an error dialog box to be shown.
    ''' Progress updates and contextual error messages will be provided by <paramref name="loader"/>
    ''' </summary>
    ''' <typeparam name="T">The return type of the background task.</typeparam>
    ''' <param name="loader">Provides contextual error messages and progress updates.</param>
    ''' <param name="parent">The UI control to create loading and error dialo boxes over.</param>
    ''' <param name="func">A function to run in a background thread (usually a member of <paramref name="loader"/></param>
    ''' <param name="finishedFunc">The function to run on the UI thread once <paramref name="func"/> has returned its results.</param>
    Public Shared Sub run(Of T)(loader As Loader, parent As UserControl, func As System.Func(Of T), finishedFunc As System.Action(Of T))
        Dim loadingIndicator As LoadingDialog = LoadingDialog.getInstance(parent)
        loader.addListener(loadingIndicator)
        run(Of T)(loader, parent, Task(Of T).Factory.StartNew(func, TaskCreationOptions.LongRunning), finishedFunc, loadingIndicator)
    End Sub

    ''' <summary>
    ''' Run the given background function (<paramref name="func"/>) in a separate thread with a loading indicator.
    ''' When the background task completes, the result is passed to <paramref name="finishedFunc"/> which
    ''' is run on the UI thread. Error messages will caught cause an error dialog box to be shown.
    ''' Progress updates and contextual error messages will be provided by <paramref name="loader"/>
    ''' </summary>
    ''' <typeparam name="T">The return type of the background task.</typeparam>
    ''' <param name="loader">Provides contextual error messages and progress updates.</param>
    ''' <param name="parent">The UI control to create loading and error dialo boxes over.</param>
    ''' <param name="importTask">A function to run in a background thread (usually a member of <paramref name="loader"/></param>
    ''' <param name="finishedFunc">The function to run on the UI thread once <paramref name="func"/> has returned its results.</param>
    ''' <param name="loadingIndicator">
    ''' Only use if a loading dialog box has been created by a previous function, and
    ''' has been set up to listen to the loader.
    ''' </param>
    Public Shared Sub run(Of T)(loader As Loader, parent As UserControl, importTask As Task(Of T), finishedFunc As System.Action(Of T), Optional loadingIndicator As LoadingDialog = Nothing)

        'Initialize the loading indicator if necessary.
        If loadingIndicator Is Nothing Then
            loadingIndicator = LoadingDialog.getInstance(parent)
            loader.addListener(loadingIndicator)
        End If

        loadingIndicator.Show()

        'Handle errors with an error dialog box on the UI thread.
        ErrorDialog.loaderContext = loader
        importTask.ContinueWith(Sub(task)
                                    For Each ex As Exception In task.Exception.InnerExceptions
                                        Dim dlg As ErrorDialog
                                        If parent Is Nothing Then
                                            dlg = New ErrorDialog(MainWindow.mainWindow, ex.Message)
                                        Else
                                            dlg = New ErrorDialog(parent, ex.Message)
                                        End If
                                        dlg.ShowDialog()
                                    Next
                                End Sub,
                                System.Threading.CancellationToken.None,
                                TaskContinuationOptions.OnlyOnFaulted,
                                TaskScheduler.FromCurrentSynchronizationContext())

        'If the background task completes, then run the provides completion code on the UI thread
        'and pass in the result of the background task as an argument.
        importTask.ContinueWith(Sub(res As Task(Of T)) finishedFunc(res.Result),
                                System.Threading.CancellationToken.None,
                                TaskContinuationOptions.OnlyOnRanToCompletion,
                                TaskScheduler.FromCurrentSynchronizationContext())

        'Close the dialog box from the UI thread when the background task finishes (or throws an error).
        importTask.ContinueWith(Sub(ex)
                                    loader.removeListener(loadingIndicator)
                                    loadingIndicator.Close()
                                End Sub,
                                TaskScheduler.FromCurrentSynchronizationContext())
    End Sub

    ''' <summary>
    ''' Run the given background function (<paramref name="func"/>) in a separate thread with a loading indicator.
    ''' When the background task completes, the result is passed to <paramref name="finishedFunc"/> which
    ''' is run on the UI thread. Error messages will caught cause an error dialog box to be shown.
    ''' </summary>
    ''' <typeparam name="T">The return type of the background task.</typeparam>
    ''' <param name="loadingText">Text to display in the loading dialog box to indicate what is happening.</param>
    ''' <param name="func">A function to run in a background thread.</param>
    ''' <param name="finishedFunc">The function to run on the UI thread once <paramref name="func"/> has returned its results.</param>
    Public Shared Sub run(Of T)(loadingText As String, func As System.Func(Of T), finishedFunc As System.Action(Of T))
        run(Of T)(loadingText, Task(Of T).Factory.StartNew(func, TaskCreationOptions.LongRunning), finishedFunc)
    End Sub

    ''' <summary>
    ''' Run the given background function (<paramref name="importTask"/>) in a separate thread with a loading indicator.
    ''' When the background task completes, the result is passed to <paramref name="finishedFunc"/> which
    ''' is run on the UI thread. Error messages will caught cause an error dialog box to be shown.
    ''' </summary>
    ''' <typeparam name="T">The return type of the background task.</typeparam>
    ''' <param name="loadingText">Text to display in the loading dialog box to indicate what is happening.</param>
    ''' <param name="importTask">A function to run in a background thread (usually a member of <paramref name="loader"/></param>
    ''' <param name="finishedFunc">The function to run on the UI thread once <paramref name="func"/> has returned its results.</param>
    Public Shared Sub run(Of T)(loadingText As String, importTask As Task(Of T), finishedFunc As System.Action(Of T))
        Dim loadingIndicator As LoadingDialog = LoadingDialog.getInstance(MainWindow.mainWindow)
        loadingIndicator.loadingText = loadingText
        loadingIndicator.Show()

        'Handle errors with an error dialog box on the UI thread.
        importTask.ContinueWith(Sub(task)
                                    For Each ex As Exception In task.Exception.InnerExceptions
                                        showExceptionErrorDialog(ex, loadingText)
                                    Next
                                End Sub,
                                System.Threading.CancellationToken.None,
                                TaskContinuationOptions.OnlyOnFaulted,
                                TaskScheduler.FromCurrentSynchronizationContext)

        'If the background task completes, then run the provides completion code on the UI thread
        'and pass in the result of the background task as an argument.
        importTask.ContinueWith(Sub(res As Task(Of T)) finishedFunc(res.Result),
                                System.Threading.CancellationToken.None,
                                TaskContinuationOptions.OnlyOnRanToCompletion,
                                TaskScheduler.FromCurrentSynchronizationContext)

        'Close the dialog box from the UI thread when the background task finishes (or throws an error).
        importTask.ContinueWith(Sub(ex)
                                    loadingIndicator.loadingText = ""
                                    loadingIndicator.Close()
                                End Sub,
                                TaskScheduler.FromCurrentSynchronizationContext())
    End Sub

    ''' <summary>
    ''' Show an error dialog box for the given exception 
    ''' (and recurse through any inner exceptions if necessary).
    ''' </summary>
    ''' <param name="ex">The exception whose error message to display.</param>
    ''' <param name="loadingText">A textual description of what was happening when the error occurred.</param>
    Private Shared Sub showExceptionErrorDialog(ex As Exception, loadingText As String)
        Dim dlg = New ErrorDialog(MainWindow.mainWindow, "Error", "Error while " & loadingText, ex.Message)
        dlg.ShowDialog()
        If ex.InnerException IsNot Nothing Then
            showExceptionErrorDialog(ex.InnerException, loadingText)
        End If
    End Sub

End Class
