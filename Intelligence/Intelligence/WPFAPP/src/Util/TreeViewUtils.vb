﻿''' <summary>
''' A group of shared methods for dealing with <see cref="TreeView"/>s
''' including selecting and deselecting items.
''' </summary>
''' <remarks>
''' The <see cref="TreeView.SelectedItem"/> field is <c>ReadOnly</c> so
''' selecting an item in a treeview is not trivial, as it requires recursively
''' descending down all the child nodes to find the <see cref="TreeViewItem"/>
''' with the correct data context, and setting the <see cref="TreeViewItem.IsSelected"/>
''' field. This selection technique is re-used for various different treeviews in the UI.
''' </remarks>
Public Class TreeViewUtils

    ''' <summary>Select the given item in the given TreeView.</summary>
    ''' <param name="treeView">The treeview containing the item.</param>
    ''' <param name="itemToSelect">The item that is the DataContext of one of the tree nodes.</param>
    ''' <returns>True if the item was found in the tree and successfully selected.</returns>
    Public Shared Function setSelectedItem(treeView As TreeView, itemToSelect As Object) As Boolean
        Return setItemSelection(treeView, itemToSelect, True)
    End Function

    ''' <summary>Get a list of all the leaf nodes in a given treeview.</summary>
    ''' <param name="treeView">The treeview whose leaves to return.</param>
    ''' <returns>All the leaf nodes in the given treeview.</returns>
    Public Shared Function getAllLeafNodes(treeView As TreeView) As IEnumerable(Of TreeViewItem)
        If treeView.Items.Count = 0 Then Return {}

        Dim leafNodes As New List(Of TreeViewItem)

        For Each child As Object In treeView.Items
            Dim treeViewChild As TreeViewItem = TryCast(treeView.ItemContainerGenerator.ContainerFromItem(child), TreeViewItem)
            Dim leafNodesFromChildren = getAllLeafNodesRecursive(treeViewChild)
            leafNodes.AddRange(leafNodesFromChildren)
        Next

        Return leafNodes
    End Function

    ''' <summary>Recusrsively get a list of all the leaf nodes that are descendants of the given TreeViewItem.</summary>
    ''' <param name="treeViewItem">The TreeViewItem whose leaves to return.</param>
    ''' <returns>All the leaf nodes descended from the given TreeViewItem.</returns>
    Private Shared Function getAllLeafNodesRecursive(treeViewItem As TreeViewItem) As IEnumerable(Of TreeViewItem)
        If treeViewItem.Items.Count = 0 Then
            Return {treeViewItem}
        Else
            Dim leafNodes As New List(Of TreeViewItem)
            For Each child As Object In treeViewItem.Items

                Dim treeViewChild As TreeViewItem = TryCast(treeViewItem.ItemContainerGenerator.ContainerFromItem(child), TreeViewItem)
                Dim leafNodesFromChildren = getAllLeafNodesRecursive(treeViewChild)
                leafNodes.AddRange(leafNodesFromChildren)
            Next
            Return leafNodes
        End If
    End Function

    ''' <summary>Deselect the given TreeView's currently selected item.</summary>
    ''' <param name="treeView">The treeview to deselect an item in.</param>
    ''' <returns>True if the item was deselected successfully.</returns>
    Public Shared Function deselectItem(treeView As TreeView) As Boolean
        Return treeView.SelectedItem Is Nothing OrElse setItemSelection(treeView, treeView.SelectedItem, False)
    End Function

    ''' <summary>Find the item in the TreeView and set its 
    ''' <see cref="TreeViewItem.IsSelected"/> value to the given one.
    ''' </summary>
    ''' <param name="treeView">The TreeView containing the item.</param>
    ''' <param name="itemToSelect">The item to change the selection value of.</param>
    ''' <param name="selectionValueToSet">True to select the item, False to deselect it.</param>
    ''' <returns>True if the item was found in the tree, and selected or deselected as specified.</returns>
    Private Shared Function setItemSelection(treeView As TreeView, itemToSelect As Object, selectionValueToSet As Boolean) As Boolean
        Debug.Print("Set Item Selection to " & selectionValueToSet.ToString)
        Dim treeViewItemToSelect As TreeViewItem = getTreeViewItemFromDataContext(treeView, itemToSelect)
        If treeViewItemToSelect IsNot Nothing Then
            If treeViewItemToSelect.IsSelected <> selectionValueToSet Then
                treeViewItemToSelect.IsSelected = selectionValueToSet
            End If
            Return True
        End If
        Return False
    End Function

    ''' <summary>Get the TreeViewItem from the given TreeView whose DataContext matches the given object.</summary>
    ''' <param name="treeView">The TreeView whose nodes to search.</param>
    ''' <param name="dataContextToFind">The object that is the DataContext of the desired TreeViewItem.</param>
    ''' <returns>A TreeViewItem whose DataContext matches the given one (or <c>Nothing</c> if none match).</returns>
    Public Shared Function getTreeViewItemFromDataContext(treeView As TreeView, dataContextToFind As Object) As TreeViewItem
        If dataContextToFind Is Nothing Then Return Nothing
        For Each child As Object In treeView.Items
            Dim treeViewChild As TreeViewItem = TryCast(treeView.ItemContainerGenerator.ContainerFromItem(child), TreeViewItem)
            Dim treeViewItemFromDescendent = getTreeViewItemFromDataContext(treeViewChild, dataContextToFind)
            If treeViewItemFromDescendent IsNot Nothing Then
                Return treeViewItemFromDescendent
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>Get the TreeViewItem descended from the given one whose DataContext matches the given object.</summary>
    ''' <param name="treeViewItem">The TreeViewItem whose descendants to search.</param>
    ''' <param name="dataContextToFind">The object that is the DataContext of the desired TreeViewItem.</param>
    ''' <returns>A TreeViewItem whose DataContext matches the given one (or <c>Nothing</c> if none match).</returns>
    Private Shared Function getTreeViewItemFromDataContext(treeViewItem As TreeViewItem, dataContextToFind As Object) As TreeViewItem
        If treeViewItem Is Nothing Then
            Return Nothing
        End If

        If treeViewItem.DataContext.Equals(dataContextToFind) Then
            Return treeViewItem
        End If

        For Each child As Object In treeViewItem.Items
            Dim treeViewChild As TreeViewItem = TryCast(treeViewItem.ItemContainerGenerator.ContainerFromItem(child), TreeViewItem)
            Dim treeViewItemFromDescendent = getTreeViewItemFromDataContext(treeViewChild, dataContextToFind)
            If treeViewItemFromDescendent IsNot Nothing Then
                Return treeViewItemFromDescendent
            End If
        Next
        Return Nothing
    End Function
End Class
