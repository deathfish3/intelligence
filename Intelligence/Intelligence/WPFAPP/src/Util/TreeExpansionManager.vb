﻿''' <summary>
''' A bag of global methods for keeping track of which nodes in the tree
''' of task instances in <see cref="TaskInstancesForVersionView"/> were expanded,
''' and re-expanding them whenever the tree is regenerated. This allows
''' the tree to be refreshed without collapsing all the nodes. It also
''' allows expanded tasks to be remembered whenever switching between different trees.
''' </summary>
Public Class TreeExpansionManager

    ''' <summary>
    ''' Stores the list of tree nodes that are currently expanded (anything
    ''' not in the list is collapsed). The outer dictionary's key is the version id
    ''' of the expanded task node. The inner dictionary's key is the platform id
    ''' of the expanded task node. The inner HashSet stores all the task instance ids
    ''' of task nodes that are expanded (with no duplicates).
    ''' </summary>
    Private Shared tasksSelected As New Dictionary(Of Integer, Dictionary(Of Integer, HashSet(Of Integer)))

    Public Shared Function isExpanded(task As TaskInstanceWrapper) As Boolean
        If task Is Nothing Then Return False
        If task.isRootNode Then Return True
        Dim selectedForVersion As Dictionary(Of Integer, HashSet(Of Integer)) = Nothing
        tasksSelected.TryGetValue(task.version.id, selectedForVersion)
        If selectedForVersion IsNot Nothing Then
            Dim selectedForPlatform As HashSet(Of Integer) = Nothing
            selectedForVersion.TryGetValue(task.platform.id, selectedForPlatform)
            If selectedForPlatform IsNot Nothing Then
                Return selectedForPlatform.Contains(task.id)
            End If
        End If
        Return False
    End Function

    ''' <summary>Record whenever a task is expanded by adding it to <see cref="tasksSelected"/></summary>
    ''' <param name="task">The task whose tree view node was expanded.</param>
    Public Shared Sub onExpanded(task As TaskInstanceWrapper)
        If task IsNot Nothing Then
            Dim selectedForVersion As Dictionary(Of Integer, HashSet(Of Integer)) = Nothing
            tasksSelected.TryGetValue(task.version.id, selectedForVersion)
            If selectedForVersion Is Nothing Then
                selectedForVersion = New Dictionary(Of Integer, HashSet(Of Integer))
                tasksSelected.Add(task.version.id, selectedForVersion)
            End If
            Dim selectedForPlatform As HashSet(Of Integer) = Nothing
            selectedForVersion.TryGetValue(task.platform.id, selectedForPlatform)
            If selectedForPlatform Is Nothing Then
                selectedForPlatform = New HashSet(Of Integer)
                selectedForVersion.Add(task.platform.id, selectedForPlatform)
            End If
            selectedForPlatform.Add(task.id)
        End If
    End Sub

    ''' <summary>Record whenever a task is collapsed by removing it from <see cref="tasksSelected"/></summary>
    ''' <param name="task">The task whose tree view node was collapsed.</param>
    Public Shared Sub onCollapsed(task As TaskInstanceWrapper)
        If task IsNot Nothing Then
            Dim selectedForVersion As Dictionary(Of Integer, HashSet(Of Integer)) = Nothing
            tasksSelected.TryGetValue(task.version.id, selectedForVersion)
            If selectedForVersion IsNot Nothing Then
                Dim selectedForPlatform As HashSet(Of Integer) = Nothing
                selectedForVersion.TryGetValue(task.platform.id, selectedForPlatform)
                If selectedForPlatform IsNot Nothing Then
                    selectedForPlatform.Remove(task.id)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' When the treeview node for the given task is loaded, expand or collapse
    ''' it based on the state recorded in <see cref="tasksSelected"/>.
    ''' </summary>
    ''' <param name="treeviewItem">
    ''' The treeview item to expand or collapse.
    ''' Its <see cref="TreeViewItem.DataContext"/> must be an instance of <see cref="TaskInstanceWrapper"/>
    ''' </param>
    Public Shared Sub setExpandedFromHistory(treeviewItem As TreeViewItem)
        If treeviewItem IsNot Nothing AndAlso treeviewItem.DataContext IsNot Nothing Then
            Dim task As TaskInstanceWrapper = TryCast(treeviewItem.DataContext, TaskInstanceWrapper)
            treeviewItem.IsExpanded = isExpanded(task)
        End If
    End Sub

End Class
