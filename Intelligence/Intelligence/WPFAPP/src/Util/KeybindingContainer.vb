﻿''' <summary>
''' Handles global keyboard shortcuts registered in <see cref="MainWindow"/>
''' and delegates them to the most recently added listener that can handle them.
''' All elements registering for keyboard shortcuts should implement
''' <see cref="IKeyboardShortcutListener"/> and register themselves with this
''' class using <see cref="KeybindingContainer.AddListener"/> to listen in to global
''' shortcut events. They must also call <see cref="KeybindingContainer.RemoveListener"/>
''' before they are closed or destroyed to make sure they do not steal shortcut events of
''' other remaining listeners.
''' </summary>
Public Class KeybindingContainer

    ''' <summary>
    ''' The list of all objects that are listening in to global keypresses.
    ''' Items added most recently (at the end of the list) are offered the keypresses first.
    ''' </summary>
    Private Shared _keybindingList As New List(Of IKeyboardShortcutListener)

    ''' <summary>Register the given object as being able to respond to keyboard shortcuts.</summary>
    ''' <param name="listener">The new listener to register.</param>
    Public Shared Sub AddListener(listener As IKeyboardShortcutListener)
        If Not _keybindingList.Contains(listener) Then
            _keybindingList.Add(listener)
        End If
    End Sub

    ''' <summary>
    ''' When the given listener should no longer respond to shortcuts, this
    ''' must be called to make sure it does not steal shortcut events of
    ''' other remaining listeners.
    ''' </summary>
    ''' <param name="listener">The listener to unregister.</param>
    Public Shared Sub RemoveListener(listener As IKeyboardShortcutListener)
        _keybindingList.Remove(listener)
    End Sub

    ''' <summary>
    ''' Respond to all registered shortcuts by looping through
    ''' <see cref="_keybindingList"/> and allowing the most recently added
    ''' listeners to respond to shortcuts first. If the listener cannot
    ''' respond to the specific shortcut, it is offered to each of the
    ''' previous listeners in turn until one is found that can consume the event.
    ''' </summary>
    ''' <returns>The command for responding to all global shortcuts.</returns>
    Public Shared ReadOnly Property OnKeyPressedCommand As ICommand
        Get
            Return New GenericCommand(
                Sub(key As KeyGesture)
                    For i = _keybindingList.Count - 1 To 0 Step -1
                        If _keybindingList(i).onKeyboardShortcutPressed(key) Then
                            Exit For
                        End If
                    Next
                End Sub)
        End Get
    End Property

End Class
