﻿Imports System.Linq
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Windows
Imports System.Diagnostics

''' <summary>
''' Base class to wrap a specific event with the WeakEventListener.
''' </summary>
''' <typeparam name="TSource">The type of the event source.</typeparam>
Public MustInherit Class WeakEventSourceBase(Of TSource As Class)

    ''' <summary>
    ''' A weak reference to the event source
    ''' </summary>
    Private _weakEventSource As WeakReference

    ''' <summary>
    '''  A weak reference to the WeakEventListener instance.
    ''' </summary>
    Private _weakListener As WeakReference

    ''' <summary>
    ''' Gets the event source instance which this listener is using.
    ''' </summary>
    ''' <remarks>
    ''' The reference to the event source is weak.
    ''' </remarks>
    Public ReadOnly Property EventSource() As Object
        Get
            If _weakEventSource Is Nothing Then
                Return Nothing
            End If

            Return Me._weakEventSource.Target
        End Get
    End Property

    ''' <summary>
    ''' Set the event source for this instance. 
    ''' When passing a new event source it replaces the event source the 
    ''' listener is listen for an event. When passing null/nothing is detaches 
    ''' the previous event source from this event listener. 
    ''' </summary>
    ''' <param name="eventSource">The event source instance.</param>
    Public Sub SetEventSource(eventSource As Object)
        ' the listener can just listen for one event source. 
        ' Detach the previous event source
        Me.Detach()

        ' keep weak-reference to the the event source
        Me._weakEventSource = New WeakReference(eventSource)

        Dim eventObject As TSource = TryCast(eventSource, TSource)
        If eventObject IsNot Nothing Then
            Dim weakListener = CreateWeakEventListenerInternal(eventObject)

            If weakListener Is Nothing Then
                Throw New InvalidOperationException("The method CreateWeakEventListener must return a value.")
            End If

            ' store the weak-listener as weak reference (for Detach method only)
            _weakListener = New WeakReference(weakListener)
        End If
    End Sub

    ''' <summary>
    ''' Does some debug-time checks and creates the weak event listener.
    ''' </summary>
    ''' <param name="eventObject">The event source instance</param>
    ''' <returns>Return the weak event listener instance</returns>
    Private Function CreateWeakEventListenerInternal(eventObject As TSource) As WeakEventListenerBase
        '#Region "Debug time checks"

        ' do some implementation checks when a debugger is attached
        If Debugger.IsAttached Then
            ' search in each type separately unitl we reach the type WeakEventSourceBase 
            '(because Reflection can not return private members in FlattenHierarchy.
            Dim type As Type = Me.[GetType]()
            While (Not type.IsGenericType OrElse type.GetGenericTypeDefinition() <> GetType(WeakEventSourceBase(Of ))) AndAlso type <> GetType(Object)
                Dim bindingFlags__1 As BindingFlags = BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.[Static] Or BindingFlags.DeclaredOnly

                ' get fields expect fields marked with CompilerGeneratedAttribute or derived from Delegate (events are delegate fields)
                Dim queryFields = From f In type.GetFields(bindingFlags__1)
                                  Where f.GetCustomAttributes(GetType(CompilerGeneratedAttribute), True).Count() = 0 _
                                  AndAlso Not f.FieldType.IsSubclassOf(GetType([Delegate]))
                                  Select f.Name

                ' get properties
                Dim queryProperties = From f In type.GetProperties(bindingFlags__1)
                                      Select f.Name

                Dim query = queryFields.Union(queryProperties)

                ' The EventWrapper is intended to be used as a weak-event-wrapper. One should not add additional 
                ' members to this class, because of the possibilty to store the WeakEventListener reference to a member.
                ' Is this the case the memory leak can still occur. 
                ' Therefore, if any field or property is implemented, throw an exception as warning.
                If query.Count() > 0 Then
                    ' note: MessageBox.Show blocks unit tests
                    Throw New InvalidOperationException(String.Format("You should not add any other implementation than overriding methods in the class {0}, because of possible memory you can get within your application.", type.Name))
                End If

                ' continue search in base type
                type = type.BaseType
            End While
        End If

        '#End Region

        ' create weak event listener
        Return CreateWeakEventListener(eventObject)
    End Function

    ''' <summary>
    ''' When overridden in a derived class, it creates the weak event listener for the given event source.
    ''' </summary>
    ''' <param name="eventObject">The event source instance to listen for an event</param>
    ''' <returns>Return the weak event listener instance</returns>
    Protected MustOverride Function CreateWeakEventListener(eventObject As TSource) As WeakEventListenerBase

    ''' <summary>
    ''' Detaches the event from the event source.
    ''' </summary>
    Public Sub Detach()
        If _weakListener IsNot Nothing Then
            ' do it the GC safe way, because an object could potentially be reclaimed 
            ' for garbage collection immediately after the IsAlive property returns true
            Dim target As WeakEventListenerBase = TryCast(_weakListener.Target, WeakEventListenerBase)
            If target IsNot Nothing Then
                target.Detach()
            End If
        End If

        _weakEventSource = Nothing
        _weakListener = Nothing
    End Sub
End Class