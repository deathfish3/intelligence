﻿

'-----------------------------------------------------------------------
'  This source is a slightly modified version from the WeakEventListner
'  in the Silverlight Toolkit Source (www.codeplex.com/Silverlight)
'---------------------------Original Source-----------------------------
' <copyright company="Microsoft">
'      (c) Copyright Microsoft Corporation.
'      This source is subject to the Microsoft Public License (Ms-PL).
'      Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
'      All other rights reserved.
' </copyright>
'-----------------------------------------------------------------------

Imports System.Diagnostics.CodeAnalysis
Imports System.Diagnostics

''' <summary>
''' Implements a weak event listener that allows the owner to be garbage
''' collected if its only remaining link is an event handler.
''' </summary>
''' <typeparam name="TInstance">Type of rootInstance listening for the event.</typeparam>
''' <typeparam name="TSource">Type of source for the event.</typeparam>
''' <typeparam name="TEventArgs">Type of event arguments for the event.</typeparam>
<SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification:="Used as link rootInstance in several projects.")> _
Public NotInheritable Class WeakEventListener(Of TInstance As Class, TSource, TEventArgs)
    Inherits WeakEventListenerBase
#Region "Fields"

    ''' <summary>
    ''' WeakReference to the rootInstance listening for the event.
    ''' </summary>
    Private _weakInstance As WeakReference

    ''' <summary>
    ''' To hold a reference to source object. With this instance the WeakEventListener 
    ''' // can guarantee that the handler get unregistered when listener is released.
    ''' </summary>
    Private _source As TSource

    ''' <summary>
    ''' Delegate to the method to call when the event fires.
    ''' </summary>
    Private _onEventAction As Action(Of TInstance, Object, TEventArgs)

    ''' <summary>
    ''' Delegate to the method to call when detaching from the event.
    ''' </summary>
    Private _onDetachAction As Action(Of WeakEventListener(Of TInstance, TSource, TEventArgs), TSource)

#End Region

#Region "Ctor"

    ''' <summary>
    ''' Initializes a new instances of the WeakEventListener class.
    ''' </summary>
    ''' <param name="instance">Instance subscribing to the event.</param>
    Public Sub New(instance As TInstance, source As TSource)
        If instance Is Nothing Then
            Throw New ArgumentNullException("instance")
        End If

        If source Is Nothing Then
            Throw New ArgumentNullException("source")
        End If

        _weakInstance = New WeakReference(instance)
        Me._source = source
    End Sub

#End Region

#Region "Properties"

    ''' <summary>
    ''' Gets or sets the method to call when the event fires.
    ''' </summary>
    Public Property OnEventAction() As Action(Of TInstance, Object, TEventArgs)
        Get
            Return _onEventAction
        End Get
        Set(value As Action(Of TInstance, Object, TEventArgs))
            ' CHANGED: NEVER REMOVE THIS CHECK. IT CAN CAUSE A MEMORY LEAK.
            If value IsNot Nothing AndAlso Not value.Method.IsStatic Then
                Throw New ArgumentException("OnEventAction method must be static " + "otherwise the event WeakEventListner class does not prevent memory leaks.")
            End If

            _onEventAction = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the method to call when detaching from the event.
    ''' </summary>
    Public Property OnDetachAction() As Action(Of WeakEventListener(Of TInstance, TSource, TEventArgs), TSource)
        Get
            Return _onDetachAction
        End Get
        Set(value As Action(Of WeakEventListener(Of TInstance, TSource, TEventArgs), TSource))

            ' CHANGED: NEVER REMOVE THIS CHECK. IT CAN CAUSE A MEMORY LEAK.
            If value IsNot Nothing AndAlso Not value.Method.IsStatic Then
                Throw New ArgumentException("OnDetachAction method must be static otherwise " + "the event WeakEventListner cannot guarantee to unregister the handler.")
            End If

            _onDetachAction = value
        End Set
    End Property

#End Region

#Region "Public methods"

    ''' <summary>
    ''' Handler for the subscribed event calls OnEventAction to handle it.
    ''' </summary>
    ''' <param name="source">Event source.</param>
    ''' <param name="eventArgs">Event arguments.</param>
    Public Sub OnEvent(source As Object, eventArgs As TEventArgs)
        Dim target As TInstance = DirectCast(_weakInstance.Target, TInstance)
        If target IsNot Nothing Then
            ' Call registered action
            OnEventAction.Invoke(target, source, eventArgs)
        Else
            ' Detach from event
            Detach()
        End If
    End Sub

    ''' <summary>
    ''' Detaches from the subscribed event.
    ''' </summary>
    Public Overrides Sub Detach()
        If OnDetachAction IsNot Nothing Then
            ' CHANGED: Passing the source instance also, because of static event handlers
            OnDetachAction.Invoke(Me, Me._source)
            OnDetachAction = Nothing
        End If
    End Sub

#End Region
End Class

