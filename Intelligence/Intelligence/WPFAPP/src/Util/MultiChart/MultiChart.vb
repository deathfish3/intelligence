﻿Imports System.Net
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Documents
Imports System.Windows.Ink
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Shapes
Imports System.Collections
Imports System.Windows.Controls.DataVisualization.Charting
Imports System.Collections.Specialized

Public Class MultiChart
    Inherits System.Windows.Controls.DataVisualization.Charting.Chart
    Private _sourceChanged As CollectionChangedWeakEventSource

    Public Sub New()
        ' setup wrapper for CollectionChanged
        _sourceChanged = New CollectionChangedWeakEventSource()
        AddHandler _sourceChanged.CollectionChanged, New System.Collections.Specialized.NotifyCollectionChangedEventHandler(AddressOf sourceCollectionChanged_CollectionChanged)
    End Sub

#Region "SeriesSource (DependencyProperty)"

    Public Property SeriesSource() As IEnumerable
        Get
            Return DirectCast(GetValue(SeriesSourceProperty), IEnumerable)
        End Get
        Set(value As IEnumerable)
            SetValue(SeriesSourceProperty, value)
        End Set
    End Property

    Public Shared ReadOnly SeriesSourceProperty As DependencyProperty = DependencyProperty.Register("SeriesSource", GetType(IEnumerable), GetType(MultiChart), New PropertyMetadata(Nothing, New PropertyChangedCallback(AddressOf OnSeriesSourceChanged)))

    Private Shared Sub OnSeriesSourceChanged(d As DependencyObject, e As DependencyPropertyChangedEventArgs)
        Dim oldValue As IEnumerable = DirectCast(e.OldValue, IEnumerable)
        Dim newValue As IEnumerable = DirectCast(e.NewValue, IEnumerable)
        Dim source As MultiChart = DirectCast(d, MultiChart)
        source.OnSeriesSourceChanged(oldValue, newValue)
    End Sub

    Protected Overridable Sub OnSeriesSourceChanged(oldValue As IEnumerable, newValue As IEnumerable)
        '  in order to prevent memory leak, we use a weak-eventing pattern.
        ' for more information about the weak eventing used in this example, please visit the follwoing two urls:
        '    http://blog.thekieners.com/2010/02/11/simple-weak-event-listener-for-silverlight/
        '    http://blog.thekieners.com/2010/02/17/weakeventsource-implementation-2/
        ' ...just set the event source to the wrapper when the source list changes
        _sourceChanged.SetEventSource(newValue)

        Me.Series.Clear()

        If newValue IsNot Nothing Then
            For Each item As Object In newValue
                CreateSeries(item)
            Next
        End If

    End Sub


#End Region

#Region "SeriesTemplate (DependencyProperty)"

    Public Property SeriesTemplate() As DataTemplate
        Get
            Return DirectCast(GetValue(SeriesTemplateProperty), DataTemplate)
        End Get
        Set(value As DataTemplate)
            SetValue(SeriesTemplateProperty, value)
        End Set
    End Property
    'Public Shared ReadOnly SeriesTemplateProperty As DependencyProperty = DependencyProperty.Register("SeriesTemplate", GetType(DataTemplate), GetType(MultiChart), New PropertyMetadata(Nothing))
    Public Shared ReadOnly SeriesTemplateProperty As DependencyProperty =
        DependencyProperty.Register("SeriesTemplate",
                                    GetType(DataTemplate),
                                    GetType(MultiChart),
                                    New FrameworkPropertyMetadata(Nothing,
                                                                  FrameworkPropertyMetadataOptions.AffectsRender,
                                                                  New PropertyChangedCallback(
                                                                        Sub(d, e)
                                                                            'This is necessary, as if the charts are created a within a
                                                                            'data template, the SeriesTemplateProperty is set after the series source.
                                                                            'This causes the graph to be generated empty, so it needs to be
                                                                            'regenreated when this template is set.
                                                                            Dim chart As MultiChart = d
                                                                            If chart.SeriesSource IsNot Nothing Then
                                                                                For Each item As Object In chart.SeriesSource
                                                                                    chart.CreateSeries(item)
                                                                                Next
                                                                            End If
                                                                        End Sub)))


#End Region

#Region "SeriesTemplateSelector (DependencyProperty)"

    Public Property SeriesTemplateSelector() As DataTemplateSelector
        Get
            Return DirectCast(GetValue(SeriesTemplateSelectorProperty), DataTemplateSelector)
        End Get
        Set(value As DataTemplateSelector)
            SetValue(SeriesTemplateSelectorProperty, value)
        End Set
    End Property
    Public Shared ReadOnly SeriesTemplateSelectorProperty As DependencyProperty = DependencyProperty.Register("SeriesTemplateSelector", GetType(DataTemplateSelector), GetType(MultiChart), New PropertyMetadata(Nothing))

#End Region

#Region "Private methods"

    Private Sub CreateSeries(item As Object)
        CreateSeries(item, Me.Series.Count)
    End Sub

    Private Sub CreateSeries(item As Object, index As Integer)
        Dim dataTemplate As DataTemplate = Nothing

        ' get data template
        If Me.SeriesTemplateSelector IsNot Nothing Then
            dataTemplate = Me.SeriesTemplateSelector.SelectTemplate(item, Me)
        End If
        If dataTemplate Is Nothing AndAlso Me.SeriesTemplate IsNot Nothing Then
            dataTemplate = Me.SeriesTemplate
        End If

        ' load data template content
        If dataTemplate IsNot Nothing Then
            Dim series As Series = TryCast(dataTemplate.LoadContent(), Series)

            If series IsNot Nothing Then
                ' set data context
                series.DataContext = item

                Me.Series.Insert(index, series)
            End If
        End If
    End Sub

    Private Sub sourceCollectionChanged_CollectionChanged(sender As Object, e As NotifyCollectionChangedEventArgs)

        Select Case e.Action
            Case NotifyCollectionChangedAction.Add

                If e.NewItems IsNot Nothing Then
                    For i As Integer = 0 To e.NewItems.Count - 1
                        CreateSeries(e.NewItems(0), e.NewStartingIndex + i)
                    Next
                End If

                Exit Select
            Case NotifyCollectionChangedAction.Remove

                If e.OldItems IsNot Nothing Then
                    For i As Integer = e.OldItems.Count To 1 Step -1
                        Me.Series.RemoveAt(i + e.OldStartingIndex - 1)
                    Next
                End If

                Exit Select
            Case NotifyCollectionChangedAction.Replace
                Throw New NotImplementedException("NotifyCollectionChangedAction.Replace is not implemented by MultiChart control.")

            Case NotifyCollectionChangedAction.Reset

                Me.Series.Clear()

                If SeriesSource IsNot Nothing Then
                    For Each item As Object In SeriesSource
                        CreateSeries(item)
                    Next
                End If

                Exit Select
            Case Else
                Exit Select
        End Select

    End Sub

#End Region

End Class

