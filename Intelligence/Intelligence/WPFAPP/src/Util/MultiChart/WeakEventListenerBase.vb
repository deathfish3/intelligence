﻿
Imports System.Net
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Documents
Imports System.Windows.Ink
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Shapes

''' <summary>
''' Provides a non-generic base class for the WeakEventListener implementation.
''' The main reason is to get easier reading code in the WeakEventSourceBase implementation. 
''' </summary>
Public MustInherit Class WeakEventListenerBase
    Public MustOverride Sub Detach()
End Class
