﻿
Imports System.Net
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Documents
Imports System.Windows.Ink
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Shapes
Imports System.Collections.Specialized

''' <summary>
''' Default CollectionChanged weak-event wrapper for INotifyCollectionChanged event source.
''' </summary>
Public Class CollectionChangedWeakEventSource
    Inherits WeakEventSourceBase(Of INotifyCollectionChanged)
    Protected Overrides Function CreateWeakEventListener(eventObject As INotifyCollectionChanged) As WeakEventListenerBase
        Dim weakListener = New WeakEventListener(Of CollectionChangedWeakEventSource, INotifyCollectionChanged, NotifyCollectionChangedEventArgs)(Me, eventObject)
        weakListener.OnDetachAction = Sub(listener, source)
                                          RemoveHandler source.CollectionChanged, AddressOf listener.OnEvent
                                      End Sub
        weakListener.OnEventAction = Sub(instance, source, e)
                                         ' fire event
                                         RaiseEvent CollectionChanged(source, e)
                                     End Sub
        AddHandler eventObject.CollectionChanged, AddressOf weakListener.OnEvent

        Return weakListener
    End Function

    Public Event CollectionChanged As NotifyCollectionChangedEventHandler

End Class