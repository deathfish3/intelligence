﻿Imports System.ComponentModel

''' <summary>
''' What units the time is counted in (e.g. hours, days weeks).
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum TimeUnitType
    <Description("Hours")>
    HOURS
    <Description("Days")>
    DAYS
    <Description("Weeks")>
    WEEKS
    <Description("Months")>
    MONTHS
    <Description("Years")>
    YEARS
End Enum
