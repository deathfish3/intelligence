﻿Imports System.ComponentModel
''' <summary>
''' Where the time constraints apply (i.e. all times before a set point, 
''' after a set point, or between two set times).
''' 
''' The "Description" fields hold human-readable versions of the 
''' enums to be used by <see cref="EnumToStringConverter"/> when
''' binding them to the UI in XAML.
''' </summary>
Public Enum TimeType
    <Description("Before")>
    BEFORE
    <Description("After")>
    AFTER
    <Description("Between")>
    BETWEEN
End Enum
