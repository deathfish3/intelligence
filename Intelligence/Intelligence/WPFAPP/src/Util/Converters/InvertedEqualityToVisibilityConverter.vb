﻿''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' an object, and returns <see cref="Visibility.Collapsed"/> if
''' it matches the object specified via the converter parameter.
''' Is used to conditionally show or hide XAML items.
''' </summary>
Public Class InvertedEqualityToVisibilityConverter
    Implements IValueConverter

    ''' <summary>
    ''' Returns returns <see cref="Visibility.Collapsed"/> if
    ''' it matches the object specified via the converter parameter.
    ''' Returns <see cref="Visibility.Visible"/> otherwise.
    ''' </summary>
    ''' <param name="value">The enum value to test.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">The enum value to compare it to.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' Returns returns <see cref="Visibility.Collapsed"/> if
    ''' it matches the object specified via the converter parameter.
    ''' Returns <see cref="Visibility.Visible"/> otherwise.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Return If(Not value.Equals(parameter), Visibility.Visible, Visibility.Collapsed)
    End Function

    ''' <summary>Not implemented, as you cannot convert back from a visibility to an object.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("This method is not implemented, as you cannot convert back from a visibility to an object.")
    End Function
End Class
