﻿Imports System.Windows.Media

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that converts
''' an <see cref="AccessLevel"/> from the Intelligence Database into a colour.
''' </summary>
Public Class AccessLevelToColourConverter
    Implements IValueConverter

    ''' <summary>
    ''' Convert an <see cref="AccessLevel"/> from the XAML binding into a colour.
    ''' </summary>
    ''' <param name="value">The <see cref="AccessLevel"/> to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' A colour representing the given <see cref="AccessLevel"/>,
    ''' or <c>Nothing</c> by default.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim level As AccessLevel = DirectCast(value, AccessLevel)
        Select Case level.id
            Case AccessLevelUtils.inactiveLevel.id : Return Colors.DarkGray 'Inactive  (Dark Gray)
            Case AccessLevelUtils.developerLevel.id : Return Colors.White   'Developer (White)
            Case AccessLevelUtils.managerLevel.id : Return "#FF03A3EE"      'Manager   (Blue)
            Case AccessLevelUtils.adminLevel.id : Return "#FF00B600"        'Admin     (Green)
        End Select
        Return Nothing
    End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("There should be no need to convert back from a colour to this object.")
    End Function
End Class
