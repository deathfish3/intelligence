﻿Imports System.Windows.Media

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that converts
''' a <see cref="Priority"/> from the Intelligence Database into a colour.
''' </summary>
Public Class PriorityToColourConverter
    Implements IValueConverter

    ''' <summary>
    ''' Convert an <see cref="Priority"/> from the XAML binding into a colour.
    ''' </summary>
    ''' <param name="value">The <see cref="Priority"/> to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' A colour representing the given <see cref="Priority"/>,
    ''' or <c>Nothing</c> by default.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim pri As Priority = DirectCast(value, Priority)
        Select Case pri.id
            Case PriorityUtils.veryLowPriority.id : Return "#FF00FFF3"  'Very Low  (Blue)
            Case PriorityUtils.lowPriority.id : Return "#FF00FF23"      'Low       (Green)
            Case PriorityUtils.mediumPriority.id : Return "#FFFFFF00"   'Medium    (Yellow)
            Case PriorityUtils.highPriority.id : Return "#FFE6830D"     'Hight     (Orange)
            Case PriorityUtils.veryHighPriority.id : Return "#FFDE0000" 'Very High (Red)
        End Select
        Return Nothing
    End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("There should be no need to convert back from a colour to this object.")
    End Function
End Class
