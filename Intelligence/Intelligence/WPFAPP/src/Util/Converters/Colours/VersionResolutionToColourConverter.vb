﻿Imports System.Windows.Media

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that converts
''' a <see cref="VersionResolution"/> from the Intelligence Database into a colour.
''' </summary>
Public Class VersionResolutionToColourConverter
    Implements IValueConverter

    ''' <summary>
    ''' Convert an <see cref="VersionResolution"/> from the XAML binding into a colour.
    ''' </summary>
    ''' <param name="value">The <see cref="VersionResolution"/> to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' A colour representing the given <see cref="VersionResolution"/>,
    ''' or <c>Nothing</c> by default.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim res As VersionResolution = DirectCast(value, VersionResolution)
        Select Case res.id
            Case VersionResolutionUtils.openResolution.id : Return "#FF03A3EE"           'Open     (Blue)
            Case VersionResolutionUtils.releasedResolution.id : Return "#FF00B600"       'Released (Green)
            Case VersionResolutionUtils.replacedWithNewBuildRes.id : Return Colors.Gray  'Replaced (Gray)
            Case VersionResolutionUtils.failedResolution.id : Return Colors.Red          'Fail     (Red)
        End Select
        Return Nothing
    End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("There should be no need to convert back from a colour to this object.")
    End Function
End Class
