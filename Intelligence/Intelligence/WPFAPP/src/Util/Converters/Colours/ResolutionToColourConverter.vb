﻿Imports System.Windows.Media

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that converts
''' a <see cref="Resolution"/> from the Intelligence Database 
''' (for a <see cref="TaskInstance"/>) into a colour.
''' </summary>
Public Class ResolutionToColourConverter
    Implements IValueConverter

    ''' <summary>
    ''' Convert an <see cref="Resolution"/> from the XAML binding into a colour.
    ''' </summary>
    ''' <param name="value">The <see cref="Resolution"/> to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' A colour representing the given <see cref="Resolution"/>,
    ''' or <c>Nothing</c> by default.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim res As Resolution = DirectCast(value, Resolution)
        Select Case res.id
            Case ResolutionUtils.openResolution.id : Return "#FF03A3EE"               'Open             (Blue)
            Case ResolutionUtils.passResolution.id : Return "#FF00B600"               'Pass             (Green)
            Case ResolutionUtils.failResolution.id : Return Colors.Red                'Fail             (Red)
            Case ResolutionUtils.naResolution.id : Return Colors.Black                'N/A              (Black)
            Case ResolutionUtils.finSubtasksResolution.id : Return Colors.Gray        'Finished Subasks (Gray)
            Case ResolutionUtils.notCompleteResolution.id : Return Colors.DarkGray    'Not Completed    (DarkGray)
            Case ResolutionUtils.inProgressResolution.id : Return "#FFCDAD00"         'In Progress      (Dark Yellow)
        End Select
        Return Nothing
    End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("There should be no need to convert back from a colour to this object.")
    End Function
End Class
