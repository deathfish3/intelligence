﻿''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' an object, and returns <see cref="Visibility.Collapsed"/> if
''' it is <c>Nothing</c>, or its string value is an empty string or 0.
''' Is used to conditionally show or hide XAML items.
''' </summary>
Public Class HideIfNothingConverter
    Implements IValueConverter

    ''' <summary>
    ''' Returns <see cref="Visibility.Collapsed"/> if <paramref name="value"/>
    ''' is <c>Nothing</c>, or its string value is an empty string or <c>"0"</c>.
    ''' Returns <see cref="Visibility.Visible"/> otherwise.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' Returns <see cref="Visibility.Collapsed"/> if <paramref name="value"/>
    ''' is <c>Nothing</c>, or its string value is an empty string or <c>"0"</c>.
    ''' Returns <see cref="Visibility.Visible"/> otherwise.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Return If(value Is Nothing OrElse value.ToString.Equals("") OrElse value.ToString.Equals("0"), Visibility.Collapsed, Visibility.Visible)
    End Function

    ''' <summary>Not implemented, as you cannot convert back from a visibility to an object.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("This method is not implemented, as you cannot convert back from a visibility to an object.")
    End Function
End Class
