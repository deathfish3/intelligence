﻿''' <summary>
''' A <see cref="IValueConverter"/> for XAML that takes 
''' a boolean value, and returns configurable values based
''' on whether it is <c>True</c> or <c>False</c>.
''' </summary>
''' <typeparam name="T">The type of value this converter returns.</typeparam>
''' <remarks>
''' These return values can be set in a subclass 
''' (e.g. <see cref="InvertedBooleanToVisibiliyConverter"/>), or can be 
''' set in XAML like:
''' 
''' <example><code>
''' &lt;Application.Resources&gt;
'''    &lt;app:BooleanToVisibilityConverter 
'''        x:Key="BooleanToVisibilityConverter" 
'''        True="Collapsed" 
'''        False="Visible" /&gt;
''' &lt;/Application.Resources&gt;
''' </code></example>
''' 
''' Based on this stack overflow article:
''' http://stackoverflow.com/questions/534575/how-do-i-invert-booleantovisibilityconverter
''' </remarks>
Public Class BooleanConverter(Of T)
    Implements IValueConverter

    ''' <summary>
    ''' Create a new converter with the given return values for <c>True</c> and <c>False</c>.
    ''' </summary>
    ''' <param name="trueValue">What to return when converting <c>True</c> from the XAML.</param>
    ''' <param name="falseValue">What to return when converting <c>False</c> from the XAML.</param>
    Public Sub New(trueValue As T, falseValue As T)
        [True] = trueValue
        [False] = falseValue
    End Sub

    ''' <summary>What to return when converting <c>True</c> from the XAML.</summary>
    Public Property [True] As T

    ''' <summary>What to return when converting <c>False</c> from the XAML.</summary>
    Public Property [False] As T

    ''' <summary>
    ''' Return the correct object depending on whether <paramref name="value"/> is 
    ''' <c>True</c> or <c>False</c>.
    ''' </summary>
    ''' <param name="value">The boolean value to test from the XAML.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' The correct object depending on whether <paramref name="value"/> is 
    ''' <c>True</c> or <c>False</c>
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Return If(TypeOf value Is Boolean AndAlso DirectCast(value, Boolean), [True], [False])
    End Function

    ''' <summary>
    ''' Returns <c>True</c> if <paramref name="value"/> is equal to the
    ''' this converter's <c>True</c> value.
    ''' </summary>
    ''' <param name="value">An object from XAML to convert back into a boolean.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="value"/> is equal to the
    ''' this converter's <c>True</c> value.
    ''' </returns>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Return TypeOf value Is T AndAlso EqualityComparer(Of T).Default.Equals(DirectCast(value, T), [True])
    End Function
End Class
