﻿''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' a boolean value, and returns <see cref="Visibility.Collapsed"/> if
''' it is True, and <see cref="Visibility.Visible"/> if False.
''' Is used to conditionally show or hide XAML items.
''' </summary>
Public Class InvertedBooleanToVisibiliyConverter
    Inherits BooleanConverter(Of Visibility)

    ''' <summary>
    ''' A new boolean converter that returns <see cref="Visibility.Collapsed"/> 
    ''' if it is True, and <see cref="Visibility.Visible"/> if False.
    ''' </summary>
    Sub New()
        MyBase.New(Visibility.Collapsed, Visibility.Visible)
    End Sub

End Class
