﻿Imports System.ComponentModel

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' a TimeSpan value, and returns the more nicely formatted string
''' version of it for displaying on the UI e.g. "X minutes", or "2 days".
''' </summary>
Public Class TimeSpanToStringConverter
	Implements IValueConverter

    ''' <summary>Get a nicely formatted string for displaying an datetime value on the UI.</summary>
    ''' <param name="value">TimeSpan value to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' A string like "X days" or "2 minutes"
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
		If value Is Nothing Then Return Nothing
		Dim duration As TimeSpan = CType(value, TimeSpan)

		Dim timeType As String = "seconds"
		Dim timeValue As Integer = 0

		If duration.Days > 0 Then
			timeType = "day"
			timeValue = duration.Days
		ElseIf duration.Hours > 0 Then
			timeType = "hour"
			timeValue = duration.Hours
		ElseIf duration.Minutes > 0 Then
			timeType = "minute"
			timeValue = duration.Minutes
		Else
			timeType = "second"
			timeValue = duration.Seconds
		End If

		Dim units As String = If(1 = timeValue, timeType, timeType & "s")

		Return timeValue & " " & units
	End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
		Throw New NotImplementedException("This method is not implemented.")
	End Function
End Class
