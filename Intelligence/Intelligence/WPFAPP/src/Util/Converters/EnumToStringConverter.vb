﻿Imports System.ComponentModel

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' an enum value, and returns the more nicely formatted string
''' version of it for displaying on the UI. This value
''' is retreived from a "Description" tag above each enum element,
''' such as seen in <see cref="FieldType"/> and similar enums.
''' </summary>
''' <remarks><example><code>
''' Public Enum FieldType
'''     &gt;Description("Product")&lt;
'''     PRODUCT
''' End Enum
''' </code></example></remarks>
Public Class EnumToStringConverter
    Implements IValueConverter

    ''' <summary>Get a nicely formatted string for displaying an enum value on the UI.</summary>
    ''' <param name="value">Enum value to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' The value from the "Description" tag above the enum value (or its
    ''' usual ALL_CAPS string if no "Description" exists.
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim type As Type = value.GetType()
        If TypeOf value Is [Enum] Then
            Dim memberInfo As System.Reflection.MemberInfo() = type.GetMember(value.ToString())
            If memberInfo IsNot Nothing AndAlso memberInfo.Length > 0 Then
                Dim attrs As Object() = memberInfo(0).GetCustomAttributes(GetType(DescriptionAttribute), False)
                If attrs IsNot Nothing AndAlso attrs.Length > 0 Then
                    'Pull out the description value
                    Return DirectCast(attrs(0), DescriptionAttribute).Description
                End If
            End If
        End If
        Return value.ToString
    End Function

    ''' <summary>Not implemented, as you cannot convert back from a formatted string to an enum value.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException("This method is not implemented, as you cannot convert back from a visibility to an object.")
    End Function

    ''' <summary>
    ''' Get a nicely formatted string for displaying an enum value on the UI.
    ''' This method provides an easier method for converting in code rather than using
    ''' a XAML binding with a converter.
    ''' </summary>
    ''' <param name="enumValue">The enum value to get the string version of.</param>
    ''' <returns>
    ''' The value from the "Description" tag above the enum value (or its
    ''' usual ALL_CAPS string if no "Description" exists.
    ''' </returns>
    Public Shared Function enumToString(enumValue As Object) As String
        Dim enumConverter = New EnumToStringConverter
        Return enumConverter.Convert(enumValue, Nothing, Nothing, Nothing)
    End Function
End Class
