﻿''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' a two values and adds them together. Can also subtract
''' if negative values are passed in.
''' </summary>
Public Class AdditionConverter
    Implements IValueConverter

    ''' <summary>
    ''' Add two values from the XAML together.
    ''' </summary>
    ''' <param name="value">The first XAML value to add (as a <c>Double</c>).</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">
    ''' The second XAML value to add (as a <c>Double</c>).
    ''' Passed in via the XAML's <c>CommandParameter</c> binding.
    ''' </param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' The sum of <paramref name="value"/> and <paramref name="parameter"/> 
    ''' (as a <c>Double</c>).
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim valueIn As Double = System.Convert.ToDouble(value)
        Dim parameterIn As Double = System.Convert.ToDouble(parameter)
        Return valueIn + parameterIn
    End Function

    ''' <summary>
    ''' Get the original XAML binding's value before it was added to.
    ''' </summary>
    ''' <param name="value">The sum of the binding value and the command parameter (as a <c>Double</c>).</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">
    ''' A value that was added to the binding.
    ''' Passed in via the XAML's <c>CommandParameter</c> binding.
    ''' </param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' Returns <paramref name="value"/> - <paramref name="parameter"/> 
    ''' (as a <c>Double</c>).
    ''' </returns>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Dim valueIn As Double = System.Convert.ToDouble(value)
        Dim parameterIn As Double = System.Convert.ToDouble(parameter)
        Return valueIn - parameterIn
    End Function
End Class
