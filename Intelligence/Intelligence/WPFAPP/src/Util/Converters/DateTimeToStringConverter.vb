﻿Imports System.ComponentModel

''' <summary>
''' An <see cref="IValueConverter"/> for XAML that takes 
''' a DateTime value, and returns the more nicely formatted string
''' version of it for displaying on the UI (dd/MM/yyyy HH:mm).
''' </summary>
Public Class DateTimeToStringConverter
	Implements IValueConverter

    ''' <summary>Get a nicely formatted string for displaying an datetime value on the UI (dd/MM/yyyy HH:mm).</summary>
    ''' <param name="value">DateTime value to convert.</param>
    ''' <param name="targetType">Not used.</param>
    ''' <param name="parameter">Not used.</param>
    ''' <param name="culture">Not used.</param>
    ''' <returns>
    ''' The DateTime in (dd/MM/yyyy HH:mm) format
    ''' </returns>
    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
		If value Is Nothing Then Return Nothing
		Dim dt As DateTime = CType(value, DateTime)
		Return dt.ToString("dd/MM/yyyy HH:mm")
	End Function

    ''' <summary>Not implemented.</summary>
    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
		Throw New NotImplementedException("This method is not implemented.")
	End Function
End Class
