﻿''' <summary>
''' An item from the Intelligence Database, all of which have an <c>id</c> field for the primary key.
''' </summary>
Public Interface IHasID

    ''' <summary>The <c>id</c> field of a database item that is its primary key.</summary>
    Property id As Integer

End Interface
