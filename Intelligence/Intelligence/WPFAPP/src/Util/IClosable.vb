﻿''' <summary>
''' Anything with a <c>close</c> method.
''' Used by <see cref="TaskAdminPanel"/> to perform any teardown tasks when closing panels.
''' </summary>
Public Interface IClosable

    ''' <summary>
    ''' Called by <see cref="TaskAdminPanel"/> to perform any teardown tasks when closing a panel.
    ''' </summary>
    Sub close()

End Interface
