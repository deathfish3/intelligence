﻿Public Interface IKeyboardShortcutListener

    ''' <summary>
    ''' Respond to a keyboard shortcut. 
    ''' Returns true if it was handled, and false if it needs to be handled elsewhere.
    ''' </summary>
    Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean

End Interface
