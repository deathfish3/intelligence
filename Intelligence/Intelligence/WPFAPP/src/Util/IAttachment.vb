﻿Public Interface IAttachment
	Property fileName As String
	Property fileUrl As String
	Property dateSubmitted As DateTime
	Property submitter As IntelligenceUser
End Interface
