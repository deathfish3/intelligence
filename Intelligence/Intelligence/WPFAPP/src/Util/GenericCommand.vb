﻿''' <summary>
''' A basic implementation of <see cref="ICommand"/> to make binding commands to the UI less verbose.
''' </summary>
Public Class GenericCommand
    Implements ICommand

    ''' <summary>
    ''' Run a <c>Sub</c> with no arguments that can always be executed. Called like:
    ''' <example><code>
    ''' New GenericCommand(Sub() Debug.Print("Hello World"))
    ''' </code></example>
    ''' </summary>
    ''' <param name="action"> A <c>Sub</c> with no arguments that will be executed for this command.</param>
    Public Sub New(action As System.Action)
        Me.New(Nothing, action)
    End Sub

    ''' <summary>
    ''' Run a <c>Sub</c> with a single arguments that can always be executed.
    ''' The argument will be supplied automatically via a <c>CommandParameter</c> binding in the XAML.
    ''' Called like:
    ''' <example><code>
    ''' New GenericCommand(Sub(parameter) Debug.Print(parameter.ToString()))
    ''' </code></example>
    ''' </summary>
    ''' <param name="action">
    ''' A <c>Sub</c> with a single argument that will be executed for this command.
    ''' The argument will be supplied automatically via a <c>CommandParameter</c> binding in the XAML.
    ''' </param>
    Public Sub New(action As System.Action(Of Object))
        Me.New(Nothing, action)
    End Sub

    ''' <summary>
    ''' Run a <c>Sub</c> with no arguments if the given predicate is true. Called like:
    ''' <example><code>
    ''' New GenericCommand(
    '''      Function(o) x > 4,
    '''      Sub() Debug.Print("Hello World!")
    ''' )
    ''' </code></example>
    ''' </summary>
    ''' <param name="condition">A function that indicates whether the action can be performed.</param>
    ''' <param name="action"> A <c>Sub</c> with no arguments that will be executed for this command.</param>
    Public Sub New(condition As System.Predicate(Of Object), action As System.Action)
        Me.condition = condition
        Me.action = action
    End Sub

    ''' <summary>
    ''' Run a <c>Sub</c> with no arguments if the given predicate is true. Called like:
    ''' <example><code>
    ''' New GenericCommand(
    '''      Function(o) x > 4,
    '''      Sub(parameter) Debug.Print(parameter.ToString())
    ''' )
    ''' </code></example>
    ''' </summary>
    ''' <param name="condition">A function that indicates whether the action can be performed.</param>
    ''' <param name="action">
    ''' A <c>Sub</c> with a single argument that will be executed for this command.
    ''' The argument will be supplied automatically via a <c>CommandParameter</c> binding in the XAML.
    ''' </param>
    Public Sub New(condition As System.Predicate(Of Object), action As System.Action(Of Object))
        Me.condition = condition
        Me.actionWithArg = action
    End Sub

    ''' <summary>
    ''' An action that accepts an arguemnt that will be run when the command is executed.
    ''' Either <see cref="actionWithArg"/> or <see cref="action"/> will be supplied via a constructor,
    ''' so both will never be present at once.
    ''' </summary>
    Private actionWithArg As System.Action(Of Object)

    ''' <summary>
    ''' An action that accepts no arguemnts that will be run when the command is executed.
    ''' Either <see cref="actionWithArg"/> or <see cref="action"/> will be supplied via a constructor,
    ''' so both will never be present at once.
    ''' </summary>
    Private action As System.Action

    ''' <summary>
    ''' A condition used to determine whether the command can be executed.
    ''' Will be ignored unless set via the constructor.
    ''' </summary>
    Private condition As System.Predicate(Of Object)

    ''' <summary>Whether the command can be run or not.</summary>
    ''' <param name="parameter">The parameter passed in via a <c>CommandParameter</c> binding in the XAML.</param>
    ''' <returns>If <see cref="condition"/> is supplied, it will be used. Otherwise returns True.</returns>
    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return If(condition Is Nothing, True, condition(parameter))
    End Function

    ''' <summary>
    ''' Run the action supplied via the constructor. 
    ''' If the action accepts a parameter via a <c>CommandParameter</c> binding in the XAML,
    ''' it will be passed in when called.
    ''' </summary>
    ''' <param name="parameter">A parameter from the <c>CommandParameter</c> binding in XAML</param>
    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If Not action Is Nothing Then
            action.Invoke()
        ElseIf Not actionWithArg Is Nothing Then
            actionWithArg.Invoke(parameter)
        End If
    End Sub

    ''' <summary>Not used. Only present to fully implement <see cref="ICommand"/>.</summary>
    Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged
End Class
