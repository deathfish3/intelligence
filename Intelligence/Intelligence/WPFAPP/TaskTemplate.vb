'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class TaskTemplate
Implements IHasID
Implements IRanked

    Public Property id As Integer Implements IHasID.id
    Public Property name As String
    Public Property description As String
    Public Property active As Boolean
    Public Property rank As Integer = 0 Implements IRanked.rank

    Public Overridable Property product As Product
    Public Overridable Property taskInstances As ICollection(Of TaskInstance) = New HashSet(Of TaskInstance)
    Public Overridable Property subTasks As ICollection(Of TaskTemplate) = New HashSet(Of TaskTemplate)
    Public Overridable Property parentTask As TaskTemplate
    Public Overridable Property platforms As ICollection(Of Platform) = New HashSet(Of Platform)
    Public Overridable Property attachments As ICollection(Of TemplateAttachment) = New HashSet(Of TemplateAttachment)

    Public Overrides Function GetHashCode() As Integer
        Dim multiplier As Integer = 31
        Dim hash = Me.GetType.GetHashCode()

        hash = hash * multiplier + id.GetHashCode()

        Return hash
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetHashCode.Equals(obj.GetHashCode)
    End Function

End Class
