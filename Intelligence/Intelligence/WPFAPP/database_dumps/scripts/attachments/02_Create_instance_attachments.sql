CREATE TABLE `intel`.`instance_attachments` (
  `id` INT(11) NOT NULL,
  `instance_id` INT(11) NOT NULL,
  `file_name` VARCHAR(256) NOT NULL,
  `file_url` VARCHAR(2048) NOT NULL,
  `date_submitted` DATETIME NOT NULL,
  `submitter_id` INT(11) NOT NULL,
  `rank` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `FK_instance_id_idx` (`instance_id` ASC),
  INDEX `FK_submitter_id_idx` (`submitter_id` ASC),
  CONSTRAINT `FK_instance_attachments_taskinstances_instance_id`
    FOREIGN KEY (`instance_id`)
    REFERENCES `intel`.`taskinstances` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_instance_attachments_intelligenceusers_submitter_id`
    FOREIGN KEY (`submitter_id`)
    REFERENCES `intel`.`intelligenceusers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `intel`.`instance_attachments` 
RENAME TO  `intel`.`instanceattachments` ;
