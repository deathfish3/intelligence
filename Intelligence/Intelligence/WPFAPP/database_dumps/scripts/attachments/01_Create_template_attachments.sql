CREATE TABLE `intel`.`template_attachments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `template_id` INT(11) NOT NULL,
  `file_name` VARCHAR(256) NOT NULL,
  `file_url` VARCHAR(2048) NOT NULL,
  `date_submitted` DATETIME NOT NULL,
  `submitter_id` INT(11) NOT NULL,
  `should_be_inherited` TINYINT(1) NOT NULL DEFAULT 1,
  `rank` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `FK_task_template_id_idx` (`template_id` ASC),
  INDEX `FK_submitter_id_idx` (`submitter_id` ASC),
  CONSTRAINT `FK_task_template_id`
    FOREIGN KEY (`template_id`)
    REFERENCES `intel`.`tasktemplates` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_submitter_id`
    FOREIGN KEY (`submitter_id`)
    REFERENCES `intel`.`intelligenceusers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
ALTER TABLE `intel`.`template_attachments` 
DROP FOREIGN KEY `FK_submitter_id`,
DROP FOREIGN KEY `FK_task_template_id`;
ALTER TABLE `intel`.`template_attachments` 
ADD CONSTRAINT `FK_template_attachments_intelligence_users_submitter_id`
  FOREIGN KEY (`submitter_id`)
  REFERENCES `intel`.`intelligenceusers` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_template_attachments_tasktemplates_template_id`
  FOREIGN KEY (`template_id`)
  REFERENCES `intel`.`tasktemplates` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `intel`.`template_attachments` 
RENAME TO  `intel`.`templateattachments` ;

