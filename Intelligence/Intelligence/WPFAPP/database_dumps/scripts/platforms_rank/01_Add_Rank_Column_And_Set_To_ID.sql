ALTER TABLE `intel`.`platforms` 
ADD COLUMN `rank` INT(11) NOT NULL DEFAULT 0 AFTER `active`;
SET SQL_SAFE_UPDATES=0;
UPDATE intel.platforms SET rank=id;
SET SQL_SAFE_UPDATES=1;