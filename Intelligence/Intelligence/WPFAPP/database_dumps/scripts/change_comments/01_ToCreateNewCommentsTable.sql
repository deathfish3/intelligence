CREATE TABLE `intel`.`comments` (
  `id` INT(11) NOT NULL UNIQUE AUTO_INCREMENT,
  `task_instance_id` INT(11) NOT NULL,
  `commenter_id` INT(11) NOT NULL,
  `text` TEXT NOT NULL,
  `date_submitted` DATETIME NOT NULL,
  `date_edited` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_task_instance_id_idx` (`task_instance_id` ASC),
  INDEX `FK_commenter_id_idx` (`commenter_id` ASC),
  CONSTRAINT `FK_task_instance_id`
    FOREIGN KEY (`task_instance_id`)
    REFERENCES `intel`.`taskinstances` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_commenter_id`
    FOREIGN KEY (`commenter_id`)
    REFERENCES `intel`.`intelligenceusers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

