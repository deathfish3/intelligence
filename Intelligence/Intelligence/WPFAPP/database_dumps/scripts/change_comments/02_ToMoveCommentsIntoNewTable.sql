INSERT INTO intel.comments (task_instance_id, commenter_id, text, date_submitted)
SELECT 
	id, 
    IFNULL(IFNULL(assignee_id, assigner_id), 3), #Default to Dan=3 if there was no assignee or assigner that could have commented
    comments,
    IFNULL(IFNULL(IFNULL(timeFinished, timeStarted), timeAssigned), timeCreated) #Try to guess the time the comment came from - latest first
FROM intel.taskinstances
WHERE comments <> ''
;
