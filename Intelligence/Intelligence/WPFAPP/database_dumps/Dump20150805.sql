CREATE DATABASE  IF NOT EXISTS `intel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `intel`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: YOYO-BUGTEST01    Database: intel
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesslevels`
--

DROP TABLE IF EXISTS `accesslevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesslevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesslevels`
--

LOCK TABLES `accesslevels` WRITE;
/*!40000 ALTER TABLE `accesslevels` DISABLE KEYS */;
INSERT INTO `accesslevels` VALUES (0,'Inactive','No Permissions',1),(1,'Developer','View Only',1),(2,'Manager','Dev and Admin Panel',1),(3,'Admin','Man and Create Users',1);
/*!40000 ALTER TABLE `accesslevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mantisID` int(11) NOT NULL,
  `taskInstance_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_FK_BugTaskInstance` (`taskInstance_id`),
  CONSTRAINT `FK_BugTaskInstance` FOREIGN KEY (`taskInstance_id`) REFERENCES `taskinstances` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intelligenceusers`
--

DROP TABLE IF EXISTS `intelligenceusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intelligenceusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `accessLevel_id` int(11) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `defaultzenlogin` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_FK_AccessLevelIntelligenceUser` (`accessLevel_id`),
  CONSTRAINT `FK_AccessLevelIntelligenceUser` FOREIGN KEY (`accessLevel_id`) REFERENCES `accesslevels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intelligenceusers`
--

LOCK TABLES `intelligenceusers` WRITE;
/*!40000 ALTER TABLE `intelligenceusers` DISABLE KEYS */;
INSERT INTO `intelligenceusers` VALUES (1,'SamCumming','sam.cumming@yoyogames.com',3,'1000:AV52d/0dWy09NOEJ0xMMvXdRn6UChf4DVA==:ImneNyQyd5kPwxbhflbKqfiAJ3RS2to8',1),(2,'l','lewis.crawford@yoyogames.com',3,'1000:h7+zGkUvrf7NvOCA+ro6YXOIQIduhbYP9A==:xKjermWBiFUQDratlJHors1UcqUqR/l/',1),(3,'DanCleaton','dan@yoyogames.com',3,'1000:96DHj1ZTX4J6yhyxaz0Wc+WhJJ5fpB1Ngg==:RXCmHwR1cZE0C0RP2HdQKkVry9CDp3P3',1);
/*!40000 ALTER TABLE `intelligenceusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platforms`
--

DROP TABLE IF EXISTS `platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platforms`
--

LOCK TABLES `platforms` WRITE;
/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
INSERT INTO `platforms` VALUES (42,'Test Platform',1);
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priorities`
--

DROP TABLE IF EXISTS `priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priorities`
--

LOCK TABLES `priorities` WRITE;
/*!40000 ALTER TABLE `priorities` DISABLE KEYS */;
INSERT INTO `priorities` VALUES (1,'none','None',1),(2,'Very Low','Not important',1),(3,'Low','Slightly important',1),(4,'Medium','Why isnt this fixed yet?',1),(5,'High','Ok, now it\'s important',1),(6,'Very High','Seriously, this need fixed',1),(7,'Catastrophic','Fix it fix it fix it!',0),(8,'World Endangering','MAKE IT WORK, PLEASE!',0);
/*!40000 ALTER TABLE `priorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (137,'Test Product',1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolutions`
--

DROP TABLE IF EXISTS `resolutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolutions`
--

LOCK TABLES `resolutions` WRITE;
/*!40000 ALTER TABLE `resolutions` DISABLE KEYS */;
INSERT INTO `resolutions` VALUES (1,'Open','Task is Open',1),(2,'Pass','Passed test condition',1),(3,'Fail','Failed test condition',1),(4,'N/A','Not Applicable',1),(5,'Finished Subtasks','All sub-tasks complete',1),(6,'Not Complete','Closed before all tasks complete',1),(7,'In Progress','Task is In Progress',1);
/*!40000 ALTER TABLE `resolutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statsqueries`
--

DROP TABLE IF EXISTS `statsqueries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statsqueries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `type_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `timeCreated` datetime NOT NULL,
  `is_visible_to_others` tinyint(1) NOT NULL,
  `queryString` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_stats_queries_stats_query_types_type_id_idx` (`type_id`),
  KEY `fk_stats_queries_intelligenceusers_creator_id_idx` (`creator_id`),
  CONSTRAINT `fk_stats_queries_intelligenceusers_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stats_queries_stats_query_types_type_id` FOREIGN KEY (`type_id`) REFERENCES `statsquerytypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statsqueries`
--

LOCK TABLES `statsqueries` WRITE;
/*!40000 ALTER TABLE `statsqueries` DISABLE KEYS */;
/*!40000 ALTER TABLE `statsqueries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statsquerytypes`
--

DROP TABLE IF EXISTS `statsquerytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statsquerytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statsquerytypes`
--

LOCK TABLES `statsquerytypes` WRITE;
/*!40000 ALTER TABLE `statsquerytypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `statsquerytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_templates_platforms`
--

DROP TABLE IF EXISTS `task_templates_platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_templates_platforms` (
  `taskTemplates_id` int(11) NOT NULL,
  `platforms_id` int(11) NOT NULL,
  PRIMARY KEY (`taskTemplates_id`,`platforms_id`),
  KEY `IX_FK_task_templates_platforms_platform` (`platforms_id`),
  CONSTRAINT `FK_task_templates_platforms_platform` FOREIGN KEY (`platforms_id`) REFERENCES `platforms` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_task_templates_platforms_task_templates` FOREIGN KEY (`taskTemplates_id`) REFERENCES `tasktemplates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_templates_platforms`
--

LOCK TABLES `task_templates_platforms` WRITE;
/*!40000 ALTER TABLE `task_templates_platforms` DISABLE KEYS */;
INSERT INTO `task_templates_platforms` VALUES (1706,42),(1708,42),(1709,42),(1710,42),(1711,42);
/*!40000 ALTER TABLE `task_templates_platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taskinstances`
--

DROP TABLE IF EXISTS `taskinstances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskinstances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeCreated` datetime NOT NULL,
  `timeAssigned` datetime DEFAULT NULL,
  `timeStarted` datetime DEFAULT NULL,
  `timeFinished` datetime DEFAULT NULL,
  `timeDeadline` datetime DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `platform_id` int(11) NOT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `assigner_id` int(11) DEFAULT NULL,
  `version_id` int(11) NOT NULL,
  `taskTemplate_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_instances_platforms_platformid` (`platform_id`),
  KEY `IX_fk_instances_assigneeid_users_userid` (`assignee_id`),
  KEY `IX_fk_instances_assignerid_users_userid` (`assigner_id`),
  KEY `IX_fk_instances_versions_versionid` (`version_id`),
  KEY `IX_fk_instances_templates_templateid` (`taskTemplate_id`),
  KEY `IX_FK_ResolutionTaskInstance` (`resolution_id`),
  KEY `IX_FK_PriorityTaskInstance` (`priority_id`),
  CONSTRAINT `FK_PriorityTaskInstance` FOREIGN KEY (`priority_id`) REFERENCES `priorities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResolutionTaskInstance` FOREIGN KEY (`resolution_id`) REFERENCES `resolutions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_assigneeid_users_userid` FOREIGN KEY (`assignee_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_assignerid_users_userid` FOREIGN KEY (`assigner_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_platforms_platformid` FOREIGN KEY (`platform_id`) REFERENCES `platforms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_templates_templateid` FOREIGN KEY (`taskTemplate_id`) REFERENCES `tasktemplates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_versions_versionid` FOREIGN KEY (`version_id`) REFERENCES `versions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4599 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taskinstances`
--

LOCK TABLES `taskinstances` WRITE;
/*!40000 ALTER TABLE `taskinstances` DISABLE KEYS */;
INSERT INTO `taskinstances` VALUES (4576,'2015-07-31 14:32:53',NULL,NULL,'2015-08-03 09:05:36',NULL,'',42,NULL,NULL,148,1706,5,4),(4577,'2015-07-31 14:32:53','2015-07-31 14:33:00','2015-07-31 14:33:21','2015-07-31 15:03:22',NULL,'',42,2,2,148,1708,3,4),(4578,'2015-07-31 14:32:53','2015-07-31 14:33:02','2015-07-31 15:07:38','2015-08-03 09:05:36',NULL,'ChildB works now.',42,1,2,148,1709,2,4),(4579,'2015-07-31 14:58:44',NULL,NULL,NULL,NULL,'',42,NULL,NULL,149,1706,7,4),(4580,'2015-07-31 14:58:44','2015-07-31 14:59:07','2015-07-31 14:58:47',NULL,NULL,'',42,2,2,149,1708,7,4),(4581,'2015-07-31 14:58:44',NULL,NULL,NULL,NULL,'',42,NULL,NULL,149,1709,1,4),(4582,'2015-07-31 14:59:59',NULL,NULL,NULL,NULL,'',42,NULL,NULL,150,1706,7,4),(4583,'2015-07-31 14:59:59','2015-07-31 15:00:04','2015-07-31 15:00:12','2015-07-31 15:00:52',NULL,'',42,2,2,150,1708,2,4),(4584,'2015-07-31 14:59:59',NULL,NULL,NULL,NULL,'',42,NULL,NULL,150,1709,1,4),(4585,'2015-07-31 15:07:22',NULL,NULL,NULL,NULL,'',42,NULL,NULL,151,1706,7,4),(4586,'2015-07-31 15:07:22',NULL,'2015-07-31 15:07:43',NULL,NULL,'',42,NULL,NULL,151,1708,1,4),(4587,'2015-07-31 15:07:22','2015-07-31 15:07:56','2015-07-31 15:08:03','2015-07-31 15:08:19',NULL,'',42,2,2,151,1709,7,4),(4588,'2015-07-31 15:09:08',NULL,'2015-07-31 15:11:44',NULL,NULL,'',42,NULL,NULL,152,1706,7,4),(4589,'2015-07-31 15:09:08','2015-07-31 15:09:16','2015-07-31 15:11:44','2015-07-31 15:11:45',NULL,'',42,2,2,152,1708,3,4),(4590,'2015-07-31 15:09:08',NULL,NULL,NULL,NULL,'',42,NULL,NULL,152,1709,1,4),(4591,'2015-07-31 15:11:52',NULL,'2015-07-31 15:12:07',NULL,'2015-08-12 00:00:00','',42,NULL,NULL,153,1706,7,4),(4592,'2015-07-31 15:11:52','2015-07-31 15:11:56','2015-07-31 15:12:07','2015-07-31 15:12:10','2015-08-12 00:00:00','',42,2,2,153,1708,2,4),(4593,'2015-07-31 15:11:52',NULL,'2015-08-03 10:40:42',NULL,NULL,'',42,NULL,NULL,153,1709,7,4),(4594,'2015-07-31 15:16:42',NULL,'2015-07-31 15:17:01',NULL,NULL,'',42,NULL,NULL,154,1706,1,4),(4595,'2015-07-31 15:16:42',NULL,NULL,NULL,NULL,'',42,NULL,NULL,154,1708,1,4),(4596,'2015-07-31 15:16:42',NULL,NULL,NULL,NULL,'',42,NULL,NULL,154,1709,1,4),(4597,'2015-07-31 15:16:42','2015-07-31 15:16:52','2015-07-31 15:17:01',NULL,NULL,'',42,2,2,154,1710,7,4),(4598,'2015-07-31 15:16:42','2015-07-31 15:16:52','2015-07-31 15:17:01',NULL,NULL,'',42,2,2,154,1711,7,4);
/*!40000 ALTER TABLE `taskinstances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasktemplates`
--

DROP TABLE IF EXISTS `tasktemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasktemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `parentTask_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_templates_products_productid` (`product_id`),
  KEY `IX_fk_templates_templates_templateid` (`parentTask_id`),
  CONSTRAINT `fk_templates_products_productid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_templates_templates_templateid` FOREIGN KEY (`parentTask_id`) REFERENCES `tasktemplates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1712 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasktemplates`
--

LOCK TABLES `tasktemplates` WRITE;
/*!40000 ALTER TABLE `tasktemplates` DISABLE KEYS */;
INSERT INTO `tasktemplates` VALUES (1706,'Parent','',1,137,NULL),(1708,'ChildA','',1,137,1706),(1709,'ChildB','',1,137,1706),(1710,'ChildC','',1,137,1706),(1711,'ChildD','',1,137,1710);
/*!40000 ALTER TABLE `tasktemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versionresolutions`
--

DROP TABLE IF EXISTS `versionresolutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versionresolutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versionresolutions`
--

LOCK TABLES `versionresolutions` WRITE;
/*!40000 ALTER TABLE `versionresolutions` DISABLE KEYS */;
INSERT INTO `versionresolutions` VALUES (1,'Open','Version is Open',1),(2,'Released','Version is Released',1),(3,'Replaced with New Build','Version Closed: New Version available',1),(4,'Failed','Version has Failed',1);
/*!40000 ALTER TABLE `versionresolutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `timeCreated` datetime NOT NULL,
  `timeClosed` datetime DEFAULT NULL,
  `comments` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_versions_products_productid` (`product_id`),
  KEY `IX_fk_versions_user_userid` (`creator_id`),
  KEY `IX_FK_VersionResolutionVersion` (`resolution_id`),
  CONSTRAINT `FK_VersionResolutionVersion` FOREIGN KEY (`resolution_id`) REFERENCES `versionresolutions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_versions_products_productid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_versions_user_userid` FOREIGN KEY (`creator_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES (148,'1','2015-07-31 14:31:36',NULL,'',137,2,1),(149,'2','2015-07-31 14:58:40',NULL,'',137,2,1),(150,'3','2015-07-31 14:59:54',NULL,'',137,2,1),(151,'4','2015-07-31 15:07:17',NULL,'',137,2,1),(152,'5','2015-07-31 15:09:01',NULL,'',137,2,1),(153,'6','2015-07-31 15:11:40',NULL,'',137,2,1),(154,'7','2015-07-31 15:16:35',NULL,'',137,2,1);
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-05 17:58:30
