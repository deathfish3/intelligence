CREATE DATABASE  IF NOT EXISTS `intel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `intel`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: YOYO-BUGTEST01    Database: intel
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesslevels`
--

DROP TABLE IF EXISTS `accesslevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesslevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesslevels`
--

LOCK TABLES `accesslevels` WRITE;
/*!40000 ALTER TABLE `accesslevels` DISABLE KEYS */;
INSERT INTO `accesslevels` VALUES (0,'Inactive','No Permissions',1),(1,'Developer','View Only',1),(2,'Manager','Dev and Admin Panel',1),(3,'Admin','Man and Create Users',1);
/*!40000 ALTER TABLE `accesslevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mantisID` int(11) NOT NULL,
  `taskInstance_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_FK_BugTaskInstance` (`taskInstance_id`),
  CONSTRAINT `FK_BugTaskInstance` FOREIGN KEY (`taskInstance_id`) REFERENCES `taskinstances` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
INSERT INTO `bugs` VALUES (112,18160,4587),(113,18161,4587),(114,18160,4593);
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intelligenceusers`
--

DROP TABLE IF EXISTS `intelligenceusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intelligenceusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `accessLevel_id` int(11) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `defaultzenlogin` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_FK_AccessLevelIntelligenceUser` (`accessLevel_id`),
  CONSTRAINT `FK_AccessLevelIntelligenceUser` FOREIGN KEY (`accessLevel_id`) REFERENCES `accesslevels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intelligenceusers`
--

LOCK TABLES `intelligenceusers` WRITE;
/*!40000 ALTER TABLE `intelligenceusers` DISABLE KEYS */;
INSERT INTO `intelligenceusers` VALUES (1,'s','sam.cumming@yoyogames.com',3,'1000:NFooFrORdgQDmFTc/q9A11albR3Hb06Tuw==:IgzNEppM0ggJY1bKzTHXz+Lt1NejSEbV',1),(2,'l','lewis.crawford@yoyogames.com',3,'1000:h7+zGkUvrf7NvOCA+ro6YXOIQIduhbYP9A==:xKjermWBiFUQDratlJHors1UcqUqR/l/',1),(3,'Dan','dan@yoyogames.com',3,'1000:i14RPy0ONPSrXSsKxYO7rGzxhhMN1s5Y+Q==:RmCgr73GEq6ao/aEbZJYY4metKZoJXNi',1);
/*!40000 ALTER TABLE `intelligenceusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platforms`
--

DROP TABLE IF EXISTS `platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platforms`
--

LOCK TABLES `platforms` WRITE;
/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
INSERT INTO `platforms` VALUES (42,'PlayStation 4 (YYC)',1),(43,'Xbox One (YYC)',1),(44,'Windows (YYC)',1);
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priorities`
--

DROP TABLE IF EXISTS `priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priorities`
--

LOCK TABLES `priorities` WRITE;
/*!40000 ALTER TABLE `priorities` DISABLE KEYS */;
INSERT INTO `priorities` VALUES (1,'none','None',1),(2,'Very Low','Not important',1),(3,'Low','Slightly important',1),(4,'Medium','Why isnt this fixed yet?',1),(5,'High','Ok, now it\'s important',1),(6,'Very High','Seriously, this need fixed',1),(7,'Catastrophic','Fix it fix it fix it!',0),(8,'World Endangering','MAKE IT WORK, PLEASE!',0);
/*!40000 ALTER TABLE `priorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (137,'Test Product',1),(138,'Leeperz',1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolutions`
--

DROP TABLE IF EXISTS `resolutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolutions`
--

LOCK TABLES `resolutions` WRITE;
/*!40000 ALTER TABLE `resolutions` DISABLE KEYS */;
INSERT INTO `resolutions` VALUES (1,'Open','Task is Open',1),(2,'Pass','Passed test condition',1),(3,'Fail','Failed test condition',1),(4,'N/A','Not Applicable',1),(5,'Finished Subtasks','All sub-tasks complete',1),(6,'Not Complete','Closed before all tasks complete',1),(7,'In Progress','Task is In Progress',1);
/*!40000 ALTER TABLE `resolutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statsqueries`
--

DROP TABLE IF EXISTS `statsqueries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statsqueries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `type_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `timeCreated` datetime NOT NULL,
  `is_visible_to_others` tinyint(1) NOT NULL,
  `queryString` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_stats_queries_stats_query_types_type_id_idx` (`type_id`),
  KEY `fk_stats_queries_intelligenceusers_creator_id_idx` (`creator_id`),
  CONSTRAINT `fk_stats_queries_intelligenceusers_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stats_queries_stats_query_types_type_id` FOREIGN KEY (`type_id`) REFERENCES `statsquerytypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statsqueries`
--

LOCK TABLES `statsqueries` WRITE;
/*!40000 ALTER TABLE `statsqueries` DISABLE KEYS */;
INSERT INTO `statsqueries` VALUES (8,'All tasks by resolution','',1,2,'2015-08-06 17:23:47',1,'COUNT//aggregator//7//chart////query//1//type////field//PRODUCT//field//NONE//field//COUNT//field//True//query//1//type////field//RESOLUTION//field//NONE//field//COUNT//field//True'),(9,'Amazing Filter','It\'s amazing',1,1,'2015-08-07 10:03:43',0,'COUNT//aggregator//0//chart//'),(10,'Task duration (hours) by assignee','',1,2,'2015-08-07 11:56:07',1,'COUNT//aggregator//0//chart////query//1//type////field//ASSIGNEE//field//NONE//field//COUNT//field//True//query//3//type////field//CREATED_TIME//field//WEEKS//field//1'),(11,'Version duration','',1,2,'2015-08-07 13:15:25',1,'AVERAGE_VERSION_DURATION//aggregator//0//chart////query//1//type////field//RESOLUTION//field//NONE//field//COUNT//field//True'),(12,'Relative time','',1,2,'2015-08-07 13:43:58',1,'COUNT//aggregator//0//chart////query//3//type////field//CREATED_TIME//field//WEEKS//field//1'),(13,'All','',1,2,'2015-08-07 14:01:29',0,'COUNT//aggregator//0//chart////query//1//type////field//ASSIGNER//field//NONE//field//COUNT//field//True'),(14,'Timegroup','',1,2,'2015-08-07 14:25:16',0,'COUNT//aggregator//0//chart////query//1//type////field//CREATED_TIME//field//NONE//field//COUNT//field//True');
/*!40000 ALTER TABLE `statsqueries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statsquerytypes`
--

DROP TABLE IF EXISTS `statsquerytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statsquerytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statsquerytypes`
--

LOCK TABLES `statsquerytypes` WRITE;
/*!40000 ALTER TABLE `statsquerytypes` DISABLE KEYS */;
INSERT INTO `statsquerytypes` VALUES (1,'Intelligence Task Stats','Statsistics about task instances in the Intelligence database. ',1);
/*!40000 ALTER TABLE `statsquerytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_templates_platforms`
--

DROP TABLE IF EXISTS `task_templates_platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_templates_platforms` (
  `taskTemplates_id` int(11) NOT NULL,
  `platforms_id` int(11) NOT NULL,
  PRIMARY KEY (`taskTemplates_id`,`platforms_id`),
  KEY `IX_FK_task_templates_platforms_platform` (`platforms_id`),
  CONSTRAINT `FK_task_templates_platforms_platform` FOREIGN KEY (`platforms_id`) REFERENCES `platforms` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_task_templates_platforms_task_templates` FOREIGN KEY (`taskTemplates_id`) REFERENCES `tasktemplates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_templates_platforms`
--

LOCK TABLES `task_templates_platforms` WRITE;
/*!40000 ALTER TABLE `task_templates_platforms` DISABLE KEYS */;
INSERT INTO `task_templates_platforms` VALUES (1706,42),(1708,42),(1709,42),(1710,42),(1711,42),(1712,42),(1713,42),(1714,42),(1717,42),(1718,42),(1720,42),(1722,42),(1723,42),(1724,42),(1725,42),(1726,42),(1727,42),(1728,42),(1729,42),(1730,42),(1731,42),(1732,42),(1735,42),(1736,42),(1737,42),(1738,42),(1739,42),(1740,42),(1741,42),(1742,42),(1743,42),(1744,42),(1745,42),(1746,42),(1747,42),(1748,42),(1749,42),(1755,42),(1756,42),(1757,42),(1758,42),(1760,42),(1761,42),(1762,42),(1763,42),(1764,42),(1765,42),(1766,42),(1768,42),(1769,42),(1771,42),(1772,42),(1773,42),(1774,42),(1775,42),(1776,42),(1777,42),(1778,42),(1779,42),(1780,42),(1781,42),(1782,42),(1783,42),(1784,42),(1785,42),(1786,42),(1787,42),(1788,42),(1789,42),(1791,42),(1792,42),(1793,42),(1864,42),(1865,42),(1866,42),(1867,42),(1868,42),(1869,42),(1870,42),(1871,42),(1872,42),(1873,42),(1874,42),(1875,42),(1876,42),(1877,42),(1878,42),(1879,42),(1880,42),(1881,42),(1882,42),(1883,42),(1884,42),(1885,42),(1886,42),(1887,42),(1888,42),(1889,42),(1890,42),(1891,42),(1712,43),(1713,43),(1714,43),(1717,43),(1718,43),(1720,43),(1722,43),(1723,43),(1724,43),(1725,43),(1726,43),(1727,43),(1728,43),(1729,43),(1730,43),(1731,43),(1732,43),(1735,43),(1736,43),(1737,43),(1738,43),(1739,43),(1740,43),(1741,43),(1742,43),(1743,43),(1744,43),(1745,43),(1746,43),(1747,43),(1748,43),(1749,43),(1755,43),(1756,43),(1757,43),(1758,43),(1760,43),(1761,43),(1762,43),(1763,43),(1764,43),(1765,43),(1766,43),(1768,43),(1769,43),(1771,43),(1772,43),(1773,43),(1774,43),(1775,43),(1776,43),(1777,43),(1778,43),(1779,43),(1780,43),(1781,43),(1782,43),(1783,43),(1784,43),(1785,43),(1786,43),(1787,43),(1788,43),(1789,43),(1870,43),(1871,43),(1872,43),(1873,43),(1874,43),(1875,43),(1876,43),(1877,43),(1878,43),(1879,43),(1880,43),(1881,43),(1882,43),(1883,43),(1884,43),(1885,43),(1886,43),(1887,43),(1890,43),(1712,44),(1713,44),(1714,44),(1717,44),(1718,44),(1720,44),(1722,44),(1723,44),(1724,44),(1725,44),(1726,44),(1727,44),(1728,44),(1729,44),(1730,44),(1731,44),(1732,44),(1735,44),(1736,44),(1737,44),(1738,44),(1739,44),(1740,44),(1741,44),(1742,44),(1743,44),(1744,44),(1745,44),(1746,44),(1747,44),(1748,44),(1749,44),(1755,44),(1756,44),(1757,44),(1758,44),(1760,44),(1761,44),(1762,44),(1763,44),(1764,44),(1765,44),(1766,44),(1768,44),(1769,44),(1771,44),(1772,44),(1773,44),(1774,44),(1775,44),(1776,44),(1777,44),(1778,44),(1779,44),(1780,44),(1781,44),(1782,44),(1870,44),(1871,44),(1872,44),(1873,44),(1874,44),(1875,44),(1876,44),(1877,44),(1878,44),(1879,44),(1880,44),(1881,44),(1882,44),(1883,44),(1884,44),(1885,44),(1886,44),(1887,44),(1890,44);
/*!40000 ALTER TABLE `task_templates_platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taskinstances`
--

DROP TABLE IF EXISTS `taskinstances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskinstances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeCreated` datetime NOT NULL,
  `timeAssigned` datetime DEFAULT NULL,
  `timeStarted` datetime DEFAULT NULL,
  `timeFinished` datetime DEFAULT NULL,
  `timeDeadline` datetime DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `platform_id` int(11) NOT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `assigner_id` int(11) DEFAULT NULL,
  `version_id` int(11) NOT NULL,
  `taskTemplate_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_instances_platforms_platformid` (`platform_id`),
  KEY `IX_fk_instances_assigneeid_users_userid` (`assignee_id`),
  KEY `IX_fk_instances_assignerid_users_userid` (`assigner_id`),
  KEY `IX_fk_instances_versions_versionid` (`version_id`),
  KEY `IX_fk_instances_templates_templateid` (`taskTemplate_id`),
  KEY `IX_FK_ResolutionTaskInstance` (`resolution_id`),
  KEY `IX_FK_PriorityTaskInstance` (`priority_id`),
  CONSTRAINT `FK_PriorityTaskInstance` FOREIGN KEY (`priority_id`) REFERENCES `priorities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResolutionTaskInstance` FOREIGN KEY (`resolution_id`) REFERENCES `resolutions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_assigneeid_users_userid` FOREIGN KEY (`assignee_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_assignerid_users_userid` FOREIGN KEY (`assigner_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_platforms_platformid` FOREIGN KEY (`platform_id`) REFERENCES `platforms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_templates_templateid` FOREIGN KEY (`taskTemplate_id`) REFERENCES `tasktemplates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instances_versions_versionid` FOREIGN KEY (`version_id`) REFERENCES `versions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4601 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taskinstances`
--

LOCK TABLES `taskinstances` WRITE;
/*!40000 ALTER TABLE `taskinstances` DISABLE KEYS */;
INSERT INTO `taskinstances` VALUES (4576,'2015-07-31 14:32:53',NULL,NULL,'2015-08-03 09:05:36',NULL,'',42,NULL,NULL,148,1706,5,4),(4577,'2015-07-31 14:32:53','2015-07-31 14:33:00','2015-07-31 14:33:21','2015-07-31 15:03:22',NULL,'',42,2,2,148,1708,3,4),(4578,'2015-07-31 14:32:53','2015-07-31 14:33:02','2015-07-31 15:07:38','2015-08-03 09:05:36',NULL,'ChildB works now.',42,1,2,148,1709,2,4),(4579,'2015-07-31 14:58:44',NULL,NULL,'2015-08-07 13:17:13',NULL,'',42,NULL,NULL,149,1706,6,4),(4580,'2015-07-31 14:58:44','2015-07-31 14:59:07','2015-07-31 14:58:47','2015-08-07 13:17:13',NULL,'',42,2,2,149,1708,6,4),(4581,'2015-07-31 14:58:44',NULL,NULL,'2015-08-07 13:17:13',NULL,'',42,NULL,NULL,149,1709,6,4),(4582,'2015-07-31 14:59:59',NULL,NULL,'2015-08-07 13:17:14',NULL,'',42,NULL,NULL,150,1706,6,4),(4583,'2015-07-31 14:59:59','2015-07-31 15:00:04','2015-07-31 15:00:12','2015-07-31 15:00:52',NULL,'',42,2,2,150,1708,2,4),(4584,'2015-07-31 14:59:59',NULL,NULL,'2015-08-07 13:17:14',NULL,'',42,NULL,NULL,150,1709,6,4),(4585,'2015-07-31 15:07:22',NULL,NULL,'2015-08-07 13:17:16',NULL,'',42,NULL,NULL,151,1706,6,4),(4586,'2015-07-31 15:07:22',NULL,'2015-07-31 15:07:43','2015-08-07 13:17:16',NULL,'',42,NULL,NULL,151,1708,6,4),(4587,'2015-07-31 15:07:22','2015-07-31 15:07:56','2015-07-31 15:08:03','2015-08-07 13:17:16',NULL,'This one is fine, but we need a bug for stats, so let\'s use #18160 #18161',42,2,2,151,1709,6,4),(4588,'2015-07-31 15:09:08',NULL,'2015-07-31 15:11:44','2015-08-07 13:17:19',NULL,'',42,NULL,NULL,152,1706,6,6),(4589,'2015-07-31 15:09:08','2015-07-31 15:09:16','2015-07-31 15:11:44','2015-07-31 15:11:45',NULL,'',42,2,2,152,1708,3,4),(4590,'2015-07-31 15:09:08',NULL,NULL,'2015-08-07 13:17:19',NULL,'',42,NULL,NULL,152,1709,6,6),(4591,'2015-07-31 15:11:52',NULL,'2015-07-31 15:12:07','2015-08-07 13:17:05','2015-08-12 00:00:00','',42,NULL,NULL,153,1706,6,5),(4592,'2015-07-31 15:11:52','2015-07-31 15:11:56','2015-07-31 15:12:07','2015-07-31 15:12:10','2015-08-12 00:00:00','',42,2,2,153,1708,2,3),(4593,'2015-07-31 15:11:52',NULL,'2015-08-03 10:40:42','2015-08-07 13:17:05',NULL,'Bug #18160',42,NULL,NULL,153,1709,6,5),(4594,'2015-07-31 15:16:42',NULL,'2015-07-31 15:17:01','2015-08-07 16:23:08',NULL,'',42,NULL,NULL,154,1706,6,4),(4595,'2015-07-31 15:16:42',NULL,NULL,'2015-08-07 16:23:08',NULL,'',42,NULL,NULL,154,1708,6,4),(4596,'2015-07-31 15:16:42',NULL,NULL,'2015-08-07 16:23:08',NULL,'',42,NULL,NULL,154,1709,6,4),(4597,'2015-07-31 15:16:42','2015-07-31 15:16:52','2015-07-31 15:17:01','2015-08-06 17:10:30',NULL,'',42,2,2,154,1710,5,4),(4598,'2015-07-31 15:16:42','2015-07-31 15:16:52','2015-07-31 15:17:01','2015-08-06 17:10:30',NULL,'',42,2,2,154,1711,2,4),(4599,'2015-08-07 16:23:14','2015-08-07 16:23:24','2015-08-07 16:23:18','2015-08-07 16:23:19',NULL,'',42,2,2,155,1706,5,4),(4600,'2015-08-07 16:23:14','2015-08-07 16:23:24','2015-08-07 16:23:18','2015-08-07 16:23:21',NULL,'',42,2,2,155,1709,2,4);
/*!40000 ALTER TABLE `taskinstances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasktemplates`
--

DROP TABLE IF EXISTS `tasktemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasktemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `parentTask_id` int(11) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_templates_products_productid` (`product_id`),
  KEY `IX_fk_templates_templates_templateid` (`parentTask_id`),
  CONSTRAINT `fk_templates_products_productid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_templates_templates_templateid` FOREIGN KEY (`parentTask_id`) REFERENCES `tasktemplates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1892 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasktemplates`
--

LOCK TABLES `tasktemplates` WRITE;
/*!40000 ALTER TABLE `tasktemplates` DISABLE KEYS */;
INSERT INTO `tasktemplates` VALUES (1706,'Parent','',1,137,NULL,0),(1708,'ChildA','',1,137,1706,0),(1709,'ChildB','',0,137,1706,0),(1710,'ChildC','CDEsc',1,137,1706,0),(1711,'ChildD','',0,137,1710,0),(1712,'Main Menu','Testing the main menu within the game',1,138,NULL,0),(1713,'Options Menu','Testing the Options menu within the game',1,138,NULL,0),(1714,'Shown After Title Screen','The main menu should be shown after the player progresses past the Title Screen',1,138,1712,0),(1717,'Startup','Everything up to and including the title screen',1,138,NULL,0),(1718,'Start the Game','The game should start without any problems',1,138,1717,0),(1720,'YoYo Splash','The YoYo Games Splash screen should be shown for 3 seconds',1,138,1717,0),(1722,'Made with GameMaker: Studio Splash','The Made with GameMaker: Studio splash screen should be shown for 3 seconds',1,138,1717,0),(1723,'Title Screen','The Leeperz title screen is shown and awaits user input.',1,138,1717,0),(1724,'Press [button] to continue','\"Press [X, A] to continue\" is shown (as appropriate to the platform tested)',1,138,1723,0),(1725,'Can be advanced','The title screen progresses when pressing relevant button as defined in \"Press [button] to continue\"',1,138,1723,0),(1726,'Title Shown Correctly','The main menu should be titled, which should look correctly scaled and positioned',1,138,1712,0),(1727,'Menu Items Shown Correctly','Menu options should be in the order:\r\n\nSingle Player\nMultiplayer\nTutorial\nStats\nOptions\nCredits',1,138,1712,0),(1728,'Appropriate Button Prompts Shown Correctly','Main menu should show the appropriate prompts:\r\n\n[X,A] Select\n[O,B] Back',1,138,1712,0),(1729,'Navigates Correctly','The main menu should be able to be navigated in the correct order that options are displayed in. Using the [Ldpad,Rdpad,Lstick] will adjust the selected option.',1,138,1712,0),(1730,'Able To Select Submenus Correctly','Using the [select] button will advance to the highlighted submenu.',1,138,1712,0),(1731,'Return To Title Screen','Using the [back] button will return to the title screen.',1,138,1712,0),(1732,'Menu Items Shown Correctly','The following options are shown in the options menu:\r\n\nMusic\nSFX',1,138,1713,0),(1735,'Shown When Selected','Selecting the options menu actually brings the player to the options menu.',1,138,1713,0),(1736,'Title Shown Correctly','The options menu should be titled, which should look correctly scaled and positioned',1,138,1713,0),(1737,'Navigates Correctly','The main menu should be able to be navigated in the correct order that options are displayed in.\r\n',1,138,1713,0),(1738,'Able to Adjust Items Correctly','Using the [Ldpad,Rdpad,Lstick] will adjust the selected option.',1,138,1713,0),(1739,'Credits Menu','Credits screen shows staff details and any licensing details relevant to the game',1,138,NULL,0),(1740,'Shown When Selected','The credits screen actually opens when requested by the player in the menu screens',1,138,1739,0),(1741,'Title Shown Correctly','',1,138,1739,0),(1742,'Can Return To Main Menu','Can you navigate back up using the [back] button?',1,138,1713,0),(1743,'Can Return To Main Menu','Can you navigate back up using the [back] button?',1,138,1739,0),(1744,'Credits Shown Correctly','All expected staff credits and licensee are shown, test is correctly scaled and positioned, etc.',1,138,1739,0),(1745,'Tutorial Menu','The tutorial gives information on how to play the game. It\'s a sequene of fullscreen images the player must progress forward/backward.',1,138,NULL,0),(1746,'Shown When Selected','Does the tutorial system actuall begin when requested?',1,138,1745,0),(1747,'Title Shown Correctly','The menu should be titled correctly, text should be scaled and positioned nicely. A game logo/motif might be used also, and this should also be scaled and positioned nicely.',1,138,1745,0),(1748,'Stats Menu','Can you view details player stats by opening the Stats Menu?',1,138,NULL,0),(1749,'Can Navigate Forward Through Tutorials','Can you press the [next] button to step through the tutorial screens and exit back to the Main Menu at the far end?',1,138,1745,0),(1755,'Can Navigate Backwards Through Tutorials','Can you press the [previous] button to step backwards through the tutorial screens and exit back to the Main Menu by going backwards whilst on slide 1?',1,138,1745,0),(1756,'Order Is Correct','Tutorials should be displayed in the order:\r\n\nJumping\nAttacking\nRace\nInfinite\nTraps\nPickups',1,138,1745,0),(1757,'Content Is Correct','Is all text accurate in describing the game mechanics/controls and also free from errors/typos?',1,138,1745,0),(1758,'Title Shown Correctly','The menu should be titled correctly, text should be scaled and positioned nicely. A game logo/motif might be used also, and this should also be scaled and positioned nicely.',1,138,1748,0),(1760,'Order Is Correct','Stats should be displayed in the order:\r\n\nDistance\nAttacks\nTotal Height\nHighest Height\nSingle Player Wins\nLocal MP Wins\nOnline MP Wins',1,138,1748,0),(1761,'Content Is Correct','Are all stats you\'re being presented accurate to what was expected? Is all text also free from errors/typos?',1,138,1748,0),(1762,'Can Return To Main Menu','Can you navigate back up using the [back] button?',1,138,1748,0),(1763,'Single Player Menu','The menu options for single-player games',1,138,NULL,0),(1764,'Shown When Selected','Can you open the single-player menu when requested by the player on the main menu?',1,138,1763,0),(1765,'Shown When Selected','Can you open the Stats Menu correctly when requested on the Main Menu?',1,138,1748,0),(1766,'Title Shown Correctly','The menu should be titled correctly, text should be scaled and positioned nicely. ',1,138,1763,0),(1768,'Order Is Correct','The following options are shown in the single player menu:\r\n\nWorld\nGame Type\nGame Mode\nBots\nPickups\nTimer\nStart',1,138,1763,0),(1769,'Content Is Correct','All text is accurate, properly scaled/positioned, and free from typos, etc.',1,138,1763,0),(1771,'Navigates Correctly','The menu should be able to be navigated in the correct order that options are displayed in. Using the [Ldpad,Rdpad,Lstick] will adjust the selected option.',1,138,1763,0),(1772,'World','Can you control the world (level theme) your game will take place in?\r\n\r\nThe following options should be available:\r\n\nJungle\nVolcano\nGlacier\nRandom',1,138,1763,0),(1773,'Game Type','Can you control the style of game you will play?\r\n\r\nThe following options should be available:\r\n\nRace\nInfinite\nNo Going Back',1,138,1763,0),(1774,'Game Mode','Can you control the mode (overall win condition) for the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nSingle Game\nFirst to 4\nFirst to 7\nFirst to 10',1,138,1763,0),(1775,'Bots','The following options should be available:\r\n\nNone\nOne\nTwo\nThree',1,138,1763,0),(1776,'Pickups','Can you control whether pickups will be spawned in the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nOn\nOff',1,138,1763,0),(1777,'Timer','Can you control the time limit set for each level in the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nOff\n0:30\n1:00\n1:30\n2:00',1,138,1763,0),(1778,'\"Infinite Mode\" Overrides Correctly','When infinite mode is selected the following options should be set and not editable:\r\n\nGame Mode - Single Game\nTimer - Off',1,138,1763,0),(1779,'\"No Going Back\" Overrides Correctly','When \"No Going Back\" mode is selected the following options should be set and not editable:\r\n\nTimer - Off',1,138,1763,0),(1780,'Start Button Immediately Accepts Current Values','Using the start button should take the player into a game with all the current settings shown on the Menu. This can be pressed when any option in this menu is highlighted.',1,138,1763,0),(1781,'Appropriate Button Prompts Shown','Appropriate button prompts for the platform currently running-on are shown to the player:\r\n\r\n[X, A] Select\n[O, B] Back',1,138,1763,0),(1782,'Can Return To The Main Menu','Can you navigate back up using the [back] button?',1,138,1763,0),(1783,'Multiplayer Menu','The menu options for multi-player games',1,138,NULL,0),(1784,'Shown When Selected','Can you open the multi-player menu when requested by the player on the main menu?',1,138,1783,0),(1785,'Title Shown Correctly','The menu should be titled correctly, text should be scaled and positioned nicely. ',1,138,1783,0),(1786,'Navigates Correctly','The menu should be able to be navigated in the correct order that options are displayed in. Using the [Ldpad,Rdpad,Lstick] will adjust the selected option.',1,138,1783,0),(1787,'Order Is Correct','Options should be displayed in the order:\r\n\nLocal Game\nFind Online Game\nCreate Online Game',1,138,1783,0),(1788,'Appropriate Button Prompts Shown','Appropriate button prompts for the platform currently running-on are shown to the player:\r\n\r\n[X, A] Select\n[O, B] Back',1,138,1783,0),(1789,'Can Return To The Main Menu','Can you navigate back up using the [back] button?',1,138,1783,0),(1791,'ChildB','',1,137,1706,0),(1792,'ChildD','',1,137,1706,0),(1793,'ChildD','',1,137,1706,0),(1864,'Parent','',1,137,NULL,0),(1865,'ChildA','',1,137,1864,0),(1866,'ChildC','CDEsc',1,137,1864,0),(1867,'ChildB','',1,137,1864,0),(1868,'ChildD','',1,137,1864,0),(1869,'ChildD','',1,137,1864,0),(1870,'Local Play Menu','The menu options for same-console multi-player games',1,138,NULL,0),(1871,'Shown When Selected','Can you open the multi-player menu when requested by the player on the main menu?',1,138,1870,0),(1872,'Title Shown Correctly','The menu should be titled correctly, text should be scaled and positioned nicely. ',1,138,1870,0),(1873,'Order Is Correct','The following options are shown in the multi-player menu:\r\n\nWorld\nGame Type\nGame Mode\nBots\nPickups\nTimer\nStart',1,138,1870,0),(1874,'Content Is Correct','All text is accurate, properly scaled/positioned, and free from typos, etc.',1,138,1870,0),(1875,'Navigates Correctly','The menu should be able to be navigated in the correct order that options are displayed in. Using the [Ldpad,Rdpad,Lstick] will adjust the selected option.',1,138,1870,0),(1876,'World','Can you control the world (level theme) your game will take place in?\r\n\r\nThe following options should be available:\r\n\nJungle\nVolcano\nGlacier\nRandom',1,138,1870,0),(1877,'Game Type','Can you control the style of game you will play?\r\n\r\nThe following options should be available:\r\n\nRace\nInfinite\nNo Going Back',1,138,1870,0),(1878,'Game Mode','Can you control the mode (overall win condition) for the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nSingle Game\nFirst to 4\nFirst to 7\nFirst to 10',1,138,1870,0),(1879,'Bots','The following options should be available:\r\n\nNone\nOne\nTwo\nThree',1,138,1870,0),(1880,'Pickups','Can you control whether pickups will be spawned in the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nOn\nOff',1,138,1870,0),(1881,'Timer','Can you control the time limit set for each level in the game you\'re about to play?\r\n\r\nThe following options should be available:\r\n\nOff\n0:30\n1:00\n1:30\n2:00',1,138,1870,0),(1882,'\"Infinite Mode\" Overrides Correctly','When infinite mode is selected the following options should be set and not editable:\r\n\nGame Mode - Single Game\nTimer - Off',1,138,1870,0),(1883,'\"No Going Back\" Overrides Correctly','When \"No Going Back\" mode is selected the following options should be set and not editable:\r\n\nTimer - Off',1,138,1870,0),(1884,'Start Button Immediately Accepts Current Values','Using the start button should take the player into a game with all the current settings shown on the Menu. This can be pressed when any option in this menu is highlighted.',1,138,1870,0),(1885,'Appropriate Button Prompts Shown','Appropriate button prompts for the platform currently running-on are shown to the player:\r\n\r\n[X, A] Select\n[O, B] Back\n[SQU, X] Join\n[TRI, Y] Leave',1,138,1870,0),(1886,'Can Return To The Main Menu','Can you navigate back up using the [back] button?',1,138,1870,0),(1887,'Empty Slots Shown Silouetted','',1,138,1870,0),(1888,'','',1,137,1865,0),(1889,'','',1,137,1710,0),(1890,'Slots Filled In Order As Players Join','',1,138,1870,0),(1891,'','',1,137,1792,0);
/*!40000 ALTER TABLE `tasktemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versionresolutions`
--

DROP TABLE IF EXISTS `versionresolutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versionresolutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versionresolutions`
--

LOCK TABLES `versionresolutions` WRITE;
/*!40000 ALTER TABLE `versionresolutions` DISABLE KEYS */;
INSERT INTO `versionresolutions` VALUES (1,'Open','Version is Open',1),(2,'Released','Version is Released',1),(3,'Replaced with New Build','Version Closed: New Version available',1),(4,'Failed','Version has Failed',1);
/*!40000 ALTER TABLE `versionresolutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `timeCreated` datetime NOT NULL,
  `timeClosed` datetime DEFAULT NULL,
  `comments` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `IX_fk_versions_products_productid` (`product_id`),
  KEY `IX_fk_versions_user_userid` (`creator_id`),
  KEY `IX_FK_VersionResolutionVersion` (`resolution_id`),
  CONSTRAINT `FK_VersionResolutionVersion` FOREIGN KEY (`resolution_id`) REFERENCES `versionresolutions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_versions_products_productid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_versions_user_userid` FOREIGN KEY (`creator_id`) REFERENCES `intelligenceusers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES (148,'1','2015-07-31 14:31:36','2015-08-07 13:17:11','',137,2,2),(149,'2','2015-07-31 14:58:40','2015-08-07 13:17:13','',137,2,3),(150,'3','2015-07-31 14:59:54','2015-08-07 13:17:14','',137,2,4),(151,'4','2015-07-31 15:07:17','2015-08-07 13:17:16','',137,2,3),(152,'5','2015-07-31 15:09:01','2015-08-07 13:17:19','',137,2,2),(153,'6','2015-07-31 15:11:40','2015-08-07 13:17:05','',137,2,2),(154,'7','2015-07-31 15:16:35','2015-08-07 16:23:08','',137,2,3),(155,'8','2015-08-07 16:23:01',NULL,'',137,2,1);
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-13  9:57:49
