﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Windows.Forms.Form
Imports System.ServiceModel.Channels
Imports System.Windows.Shell
Imports System.Threading
Imports WPFAPP.com.yoyogames.bugs
Imports System.Deployment.Application

''' <summary>
''' The MainWindow and origin of Intelligence.
''' </summary>
''' <remarks></remarks>
Public Class MainWindow
    Inherits Window

    Public panelReportIssue As ControlPanelReportIssue
    Dim panelMyWork As PanelMyWorkControl
	Dim panelTaskAdmin As TaskAdminPanel
	Public panelCurrentTask As CurrentTaskPanel
	Public panelPreferences As PreferencesPanel
    Public statsPanel As StatsPanel
    Public panelGenReport As ReportGenPanel
	Private notificationThread As Thread

	Dim storageString As String
    Dim senddate As DateTime
    Dim senduser As String

    Public Shared masterNotification As TextBox
    Public Shared mainWindow As MainWindow

    ''' <summary>
    ''' Clears up temp folder and initializes each panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onLoad(sender As Object, e As RoutedEventArgs)
        mainWindow = Me

        If My.Settings.MainWindowMaximized = True Then
            SystemCommands.MaximizeWindow(Me)
        End If

        'Clear up temp folder
        If My.Computer.FileSystem.DirectoryExists(System.IO.Path.GetTempPath & "intelligence") Then
            My.Computer.FileSystem.DeleteDirectory(System.IO.Path.GetTempPath & "intelligence", FileIO.DeleteDirectoryOption.DeleteAllContents)
        End If

        TextBoxNotification.Text = Nothing
        panelReportIssue = New ControlPanelReportIssue()
		panelTaskAdmin = New TaskAdminPanel()
		panelCurrentTask = New CurrentTaskPanel()
		panelPreferences = New PreferencesPanel()
        statsPanel = New StatsPanel()
        panelGenReport = New ReportGenPanel()


		Dim version As String = Nothing
		Try
			version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
		Catch ex As InvalidDeploymentException
			version = "not installed"
		End Try
		'MessageDialog.display("Version", "Using version: " & version)
		LogoGrid.ToolTip = version





		masterNotification = TextBoxNotification

        ' Dim loginResult As Boolean = showLoginDialog()
        ' If loginResult = False Then
        'SystemCommands.CloseWindow(Me)
        '  End If

        panelMyWork = New PanelMyWorkControl(UserInfo.getCurrentIntelligenceUser())
        setMainPanel(panelMyWork)
        setSelectedTab(GridMyWork)
        panelMyWork.refreshTasks()

        GridTaskAdmin.Visibility = Windows.Visibility.Collapsed
        If AccessLevelUtils.CanAccessDeveloperContent() Then
            GridTaskAdmin.Visibility = Windows.Visibility.Visible
        End If

        If AccessLevelUtils.CanAccessAdminContent() Then
            AdminNotificationSendButton.Visibility = Windows.Visibility.Visible
            TextBoxNotification.IsReadOnly = False
        Else
            AdminNotificationSendButton.Visibility = Windows.Visibility.Hidden
            TextBoxNotification.IsReadOnly = True
        End If

        'Check for Mantis Login Details here. If false, execute showLoginDialogForMantis. Else, use login details from that file (needs function to read it)

        Try
            'Try to open file
            Dim contents As Byte() = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
            'If successful, decrypt contents
            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
            'Convert to string
            Dim humanReadableUserDetails As String = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)


            'Separate userDetail blocks from each other with /u/ 

            Dim splitUsers As String() = Split(humanReadableUserDetails, "/@'")
            'For each user in details
            For Each userString In splitUsers
                If userString <> "" Then
                    'Split individual details with /p/ delimiter
                    Dim splitUserDetails As String() = Split(userString, "/p'")

                    'If the associatedUsername is the same as the current user of intelligence
                    Dim associatedUsername As String = splitUserDetails(4)
                    If associatedUsername = UserInfo.getCurrentIntelligenceUser.name Then
                        'Assign saved user details
                        UserInfo.user.mantisUsername = splitUserDetails(0)
                        UserInfo.user.mantisPassword = splitUserDetails(1)
                        UserInfo.user.zendeskUsername = splitUserDetails(2)
                        UserInfo.user.zendeskPassword = splitUserDetails(3)
                        Exit For
                    End If
                End If
            Next
            'If no details gained
            If UserInfo.user.mantisUsername Is Nothing Then
                'Prompt user to log in.
                showLoginDialogForMantis()
            End If

            If UserInfo.getCurrentIntelligenceUser.defaultzenlogin = False AndAlso UserInfo.user.zendeskUsername Is Nothing Then
                showLoginDialogForZendesk()
            End If

        Catch ex As Exception
            showLoginDialogForMantis()
            If UserInfo.getCurrentIntelligenceUser.defaultzenlogin = False AndAlso UserInfo.user.zendeskUsername Is Nothing Then
                showLoginDialogForZendesk()
            End If
        End Try

    End Sub

    Private Sub OnMouseLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Private Sub onCloseButtonClicked(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)


        My.Settings.GridInprogressHeight = panelMyWork.GridInprogress.Height.Value

        My.Settings.GridMyTasksHeight = panelMyWork.GridTasks.Height.Value

        My.Settings.GridClosedHeight = panelMyWork.GridClosed.Height.Value

        My.Settings.StatsGraphRow = statsPanel.GridGraph.Height.Value

        My.Settings.StatsQueryRow = statsPanel.GridQuery.Height.Value

        My.Settings.Save()

        Environment.Exit(0)
    End Sub

    ''' <summary>
    ''' Fires functions specific to loading the MyWork panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onMyWorkClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        setMainPanel(panelMyWork)
        setSelectedTab(GridMyWork)
        panelMyWork.refreshTasks()
    End Sub

    ''' <summary>
    ''' Fires functions specific to loading the ReportIssue panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onReportIssueClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        setMainPanel(panelReportIssue)
        setSelectedTab(GridReportIssue)
    End Sub

    ''' <summary>
    ''' Fires functions specific to loading the TaskAdmin panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onTaskAdminClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles GridTaskAdmin.MouseLeftButtonDown
		setMainPanel(panelTaskAdmin)
		setSelectedTab(GridTaskAdmin)
	End Sub

	Private Sub onCurrentTaskClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles GridCurrentTask.MouseLeftButtonDown
		setMainPanel(panelCurrentTask)
		setSelectedTab(GridCurrentTask)
	End Sub

    ''' <summary>
    ''' Fires functions specific to loading the Stats panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onStatsTaskClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles GridStats.MouseLeftButtonDown
        setMainPanel(statsPanel)
        setSelectedTab(GridStats)
    End Sub

    ''' <summary>
    ''' Fires functions specific to loading the Preferences panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onPreferencesClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles GridPreferences.MouseLeftButtonDown
        setMainPanel(panelPreferences)
        setSelectedTab(GridPreferences)
        panelPreferences.refreshAll()
    End Sub

    ''' <summary>
    ''' Fires functions specific to loading the GenReport panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onGenReportClicked(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs) Handles GridGenReport.MouseLeftButtonDown
        setMainPanel(panelGenReport)
        setSelectedTab(GridGenReport)
        panelGenReport.refreshAll()
    End Sub

    ''' <summary>
    ''' Changes the colour of a target tab when fired.
    ''' </summary>
    ''' <param name="tabToSelect">The tab to modify as Grid</param>
    ''' <remarks></remarks>
    Public Sub setSelectedTab(tabToSelect As Grid)
        For Each tab As Grid In Me.SidepanelStackPanel.Children
            tab.Background = If(tab.Equals(tabToSelect), Brushes.LightGray, Brushes.Gray)
        Next
    End Sub

    ''' <summary>
    ''' Clears the main viewing window and displays the new content.
    ''' </summary>
    ''' <param name="content">The content to display as UserControl.</param>
    ''' <remarks></remarks>
    Public Sub setMainPanel(content As UserControl)
        mainPanel.Children.Clear()
        mainPanel.Children.Add(content)
    End Sub

    ''' <summary>
    ''' Creates a new window and initializes the listener thread for Admin notifications.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        InitializeComponent()

        Me.Left = My.Settings.MainWindowPosX
        Me.Top = My.Settings.MainWindowPosY
        Me.Width = My.Settings.MainWindowWidth
        Me.Height = My.Settings.MainWindowHeight



        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.MaximizeWindowCommand, Sub(t, e) Me.OnMaximizeWindow(t, e), Sub(t, e) Me.OnCanResizeWindow(t, e)))
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.MinimizeWindowCommand, Sub(t, e) Me.OnMinimizeWindow(t, e), Sub(t, e) Me.OnCanMinimizeWindow(t, e)))
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.RestoreWindowCommand, Sub(t, e) Me.OnRestoreWindow(t, e), Sub(t, e) Me.OnCanResizeWindow(t, e)))

        Using db As New IntelligenceDB

            storageString = (From notification In db.Notifications
                                          Where notification.id = 1
                                          Select notification.notificationstring).FirstOrDefault

        End Using


        notificationThread = New Thread(AddressOf RefreshNotificationThreadTask)
        notificationThread.IsBackground = True
        notificationThread.Start()







    End Sub

    Private Sub OnCanResizeWindow(sender As Object, e As CanExecuteRoutedEventArgs)
        e.CanExecute = (ResizeMode.CanResize = Me.ResizeMode) Or (ResizeMode.CanResizeWithGrip = Me.ResizeMode)
    End Sub

    Private Sub OnCanMinimizeWindow(sender As Object, e As CanExecuteRoutedEventArgs)
        e.CanExecute = Me.ResizeMode <> ResizeMode.NoResize
    End Sub

    ''' <summary>
    ''' Automatically closed on safe shutdown, saves user preferences.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OnCloseWindow(target As Object, e As ExecutedRoutedEventArgs)

        My.Settings.GridInprogressHeight = panelMyWork.GridInprogress.Height.Value

        My.Settings.GridMyTasksHeight = panelMyWork.GridTasks.Height.Value

        My.Settings.GridClosedHeight = panelMyWork.GridClosed.Height.Value

		My.Settings.CurrentTaskLHSWidth = panelCurrentTask.LHSGrid.Width.Value

		My.Settings.CurrentTaskRHSWidth = panelCurrentTask.RHSGrid.Width.Value

		My.Settings.StatsGraphRow = statsPanel.GridGraph.Height.Value

        My.Settings.StatsQueryRow = statsPanel.GridQuery.Height.Value

        My.Settings.MainWindowPosX = Me.Left
        My.Settings.MainWindowPosY = Me.Top
        My.Settings.MainWindowWidth = Me.Width
        My.Settings.MainWindowHeight = Me.Height

        If Me.WindowState = Windows.WindowState.Maximized Then
            My.Settings.MainWindowMaximized = True
        Else
            My.Settings.MainWindowMaximized = False
        End If
        'SystemCommands.
        My.Settings.Save()
        SystemCommands.CloseWindow(Me)
    End Sub

    Private Sub OnMaximizeWindow(target As Object, e As ExecutedRoutedEventArgs)

        SystemCommands.MaximizeWindow(Me)
    End Sub

    Private Sub OnMinimizeWindow(target As Object, e As ExecutedRoutedEventArgs)

        SystemCommands.MinimizeWindow(Me)
    End Sub

    Private Sub OnRestoreWindow(target As Object, e As ExecutedRoutedEventArgs)

        SystemCommands.RestoreWindow(Me)

    End Sub

    ''' <summary>
    ''' Pops up the login dialog.
    ''' </summary>
    ''' <returns>True if login successful, else false.</returns>
    ''' <remarks></remarks>
    Private Function showLoginDialog() As Boolean
        Dim dlg As New LoginDialog("Please Log In, bro", "Shouldnt show, bro")
        dlg.ShowDialog()
        Return dlg.DialogResult
    End Function

    ''' <summary>
    ''' Shows the login window for Mantis, and checks to see if the entered details are valid.
    ''' </summary>
    ''' <returns>True if successful login, else false.</returns>
    ''' <remarks></remarks>
    Public Shared Function showLoginDialogForMantis() As Boolean
        Dim dlg As New LoginDialog("Enter Mantis Login", "Log in below...", True, False)
        dlg.ShowDialog()
        Dim contents As Byte()
        Dim humanReadableUserDetails As String = ""
        Try
            'Try to open file. If fail, skip this section
            contents = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
            'Convert to string
            humanReadableUserDetails = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)
        Catch ex As Exception
        End Try

        Dim userDetails As String = "/@'" & UserInfo.user.mantisUsername & "/p'" & UserInfo.user.mantisPassword & "/p'" & UserInfo.user.zendeskUsername & "/p'" & UserInfo.user.zendeskPassword & "/p'" & UserInfo.getCurrentIntelligenceUser.name & "/p'"
        'Append to rest of details
        humanReadableUserDetails = humanReadableUserDetails & userDetails
        'Encrypt entire user details file
        Dim encryptedDetails As Byte() = ProtectData.ProtectData.Protect(System.Text.Encoding.Unicode.GetBytes(humanReadableUserDetails))
        Dim file As System.IO.FileStream
        file = New System.IO.FileStream("mantisUserAuth.txt", System.IO.FileMode.Create)
        'Overwrite existing file.
        file.Write(encryptedDetails, 0, encryptedDetails.Count)

        Return dlg.DialogResult
    End Function

    ''' <summary>
    ''' Shows the login window for Zendesk, and checks to see if the entered details are valid.
    ''' </summary>
    ''' <returns>True if successful login, else false.</returns>
    ''' <remarks></remarks>
    Public Shared Function showLoginDialogForZendesk() As Boolean
        Dim dlg As New LoginDialog("Enter Zendesk Login", "Log in below...", False, True)
        dlg.ShowDialog()
        Dim contents As Byte()
        Dim humanReadableUserDetails As String = ""
        Try
            contents = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
            humanReadableUserDetails = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)
        Catch ex As Exception
        End Try

        Dim userDetails As String = "/@'" & UserInfo.user.mantisUsername & "/p'" & UserInfo.user.mantisPassword & "/p'" & UserInfo.user.zendeskUsername & "/p'" & UserInfo.user.zendeskPassword & "/p'" & UserInfo.getCurrentIntelligenceUser.name & "/p'"
        'Append to rest of details
        humanReadableUserDetails = humanReadableUserDetails & userDetails
        'Encrypt entire user details file
        Dim encryptedDetails As Byte() = ProtectData.ProtectData.Protect(System.Text.Encoding.Unicode.GetBytes(humanReadableUserDetails))
        Dim file As System.IO.FileStream
        file = New System.IO.FileStream("mantisUserAuth.txt", System.IO.FileMode.Create)
        'Overwrite existing file.
        file.Write(encryptedDetails, 0, encryptedDetails.Count)


        Return dlg.DialogResult

    End Function



    Private Sub WindowMain_SizeChanged(sender As Object, e As SizeChangedEventArgs)


    End Sub

    ''' <summary>
    ''' Refreshes the notifcation thread in prep for a new notification.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RefreshNotificationThreadTask()

        Do
            Using db As New IntelligenceDB
                Try
                    Dim notificationIn As Notification
                    notificationIn = (From notification In db.Notifications
                                      Where notification.id = 1).FirstOrDefault
                    storageString = notificationIn.notificationstring
                    senddate = notificationIn.senddate
                    senduser = notificationIn.sender.username
                Catch e As Exception
                    Debug.Print("error: " & e.ToString)
                End Try

                If storageString IsNot Nothing Then
                    TextBoxNotification.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, New Action(Of String, String, DateTime)(AddressOf InvokeAction), storageString, senduser, senddate)
                End If
                'Debug.Print(storageString)
            End Using
            Thread.Sleep(10000)

        Loop
    End Sub

    ''' <summary>
    ''' Executes sending a new notification when button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub AdminNotificationSendButton_Click(sender As Object, e As RoutedEventArgs)
        Using db As New IntelligenceDB
            Dim notification As Notification = db.Notifications.Find(1)
            notification.notificationstring = TextBoxNotification.Text
            notification.senddate = DateTime.Now
            notification.sender = db.IntelligenceUsers.Find(UserInfo.getCurrentIntelligenceUser.id)
            db.SaveChanges()
        End Using
    End Sub

    ''' <summary>
    ''' Pops up a new toast notification and sets the notification textbox to the notification string.
    ''' </summary>
    ''' <param name="storageString">The target message to be displayed.</param>
    ''' <param name="sendUser">The user that sent the target message.</param>
    ''' <param name="sendDate">The date that the target message was sent.</param>
    ''' <remarks></remarks>
    Public Sub InvokeAction(storageString As String, sendUser As String, sendDate As DateTime)
        If TextBoxNotification.IsFocused = False AndAlso storageString <> TextBoxNotification.Text Then

            If TextBoxNotification.Text IsNot Nothing AndAlso Not TextBoxNotification.Text.Equals("") Then

                Me.BeginStoryboard(Me.FindResource("NotificationHighlight"))
                Dim notificationWindow As New ToastNotification(storageString, sendUser, sendDate)
                notificationWindow.Show()
                notificationWindow.beginDisplay()
            End If
            TextBoxNotification.Text = storageString
            TextBoxNotification.ToolTip = sendUser & " - " & sendDate
        End If
    End Sub

End Class
