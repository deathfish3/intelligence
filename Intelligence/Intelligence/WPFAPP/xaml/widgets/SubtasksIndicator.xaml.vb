﻿Public Class SubtasksIndicator
    Inherits UserControl

    Sub New()
        InitializeComponent()
    End Sub

    Public Shared ReadOnly isNumberVisibleProperty As DependencyProperty = DependencyProperty.Register(
                 "isNumberVisible",
                 GetType(Boolean),
                 GetType(SubtasksIndicator),
                 New PropertyMetadata(True)
            )

    Public Property isNumberVisible As Boolean
        Get
            Return GetValue(isNumberVisibleProperty)
        End Get
        Set(value As Boolean)
            SetValue(isNumberVisibleProperty, value)
        End Set
    End Property

End Class
