﻿Public Class CustomDockPanel
    Inherits UserControl

    Sub New()
        InitializeComponent()
    End Sub


    'Issue with how panel deletion works. If you expand all the way to task editor and then select a new platform, duplicates are formed
    'It seems that on selecteditemchanged in the task treeview is called when removing happens. Find a way to stop this from happening.
    'If you click on an element that has another element between it when you delete, there'll be duplication issues.
    'Find a way to catch selectionchanged IN ANY ELEMENT. They all seem to be an issue
    Public Function showNewPanel(sender As Object, target As Object) As Boolean
        If closeAllToRightOf(sender) AndAlso target IsNot Nothing Then
            Add(target)
            Return True
        End If
        Return False
    End Function

    Private Sub Add(target As Object)
        DockPanel.SetDock(target, Dock.Left)

        'Dim colIndexToAddTo = ContentPanel.Children.Count
        'Grid.SetColumn(target, colIndexToAddTo)

        ''If colIndexToAddTo > Then
        ''    Dim prevCol As ColumnDefinition = ContentPanel.ColumnDefinitions.Item(colIndexToAddTo - 1)
        ''    prevCol.Width = Grid
        ''End If

        'Dim col As New ColumnDefinition()
        'col.Width = GridLength.Auto
        'ContentPanel.ColumnDefinitions.Add(col)


        'Dim uiElement As UserControl

        'Dim expander = TryCast(target, CustomExpander)
        'If expander IsNot Nothing Then
        '    uiElement = TryCast(expander.innerObject, UserControl)
        'Else
        '    uiElement = TryCast(target, UserControl)
        'End If

        'If uiElement IsNot Nothing Then
        '    AddHandler uiElement.Loaded, AddressOf onLoaded
        'End If

        ContentPanel.Children.Add(target)

    End Sub

    'Public Sub onLoaded(sender As Object, e As RoutedEventArgs)
    '    Dim uiElement As UserControl = TryCast(sender, UserControl)
    '    If uiElement IsNot Nothing AndAlso uiElement.Content IsNot Nothing Then
    '        'If uiElement.Content.children.count > 0 Then
    '        '    Dim first = uiElement.Content.children.item(0)
    '        '    first.focus()
    '        'End If
    '        'MoveFocus(New TraversalRequest(FocusNavigationDirection.Next))
    '    End If
    'End Sub

    Private Sub Remove(index As Integer)
        ContentPanel.Children.RemoveAt(index)
        'ContentPanel.ColumnDefinitions.RemoveAt(index)
    End Sub

    Public Function showNewPanel(sender As Object, target As Object, title As String) As Boolean
        Return showNewPanel(sender, New CustomExpander(title, target))
    End Function

    Public Function closeAllToRightOf(sender As Object) As Boolean
        Dim senderIndexPos = getStackIndexOf(sender)
        If (sender Is Nothing OrElse senderIndexPos > -1) Then
            While ContentPanel.Children.Count > (senderIndexPos + 1)
                Dim index = ContentPanel.Children.Count - 1
                If Not close(index) Then
                    Return False
                End If
            End While
            Return True
        End If
        Return False
    End Function

    Private Function close(index As Integer) As Boolean
        Dim child As UIElement = ContentPanel.Children.Item(index)

        Dim expander As CustomExpander = TryCast(child, CustomExpander)
        If expander IsNot Nothing Then
            child = expander.innerObject
        End If

        Dim editor As ICustomEditor = TryCast(child, ICustomEditor)
        If editor IsNot Nothing Then
            Dim canClose As Boolean = editor.submitChanges()
            If Not canClose Then
                Return False
            End If
        End If

        Remove(index)

        Dim disposable As IClosable = TryCast(child, IClosable)
        If disposable IsNot Nothing Then
            disposable.close()
        End If

        'Dim uiElement As UserControl = TryCast(child, UserControl)
        'If uiElement IsNot Nothing Then
        '    RemoveHandler uiElement.Loaded, AddressOf Me.onLoaded
        'End If

        Return True
    End Function

    Private Function getStackIndexOf(sender As Object) As Integer
        If sender Is Nothing Then
            Return -1
        End If

        Dim senderIndexPos As Integer = -1
        Dim loopIndex As Integer = 0
        For Each child In ContentPanel.Children
            If child.GetType.Equals(sender.GetType) Then
                senderIndexPos = loopIndex
                Exit For
            ElseIf child.GetType.Equals(GetType(CustomExpander)) Then
                Dim expander As CustomExpander = DirectCast(child, CustomExpander)
                If expander.innerObject.GetType.Equals(sender.GetType) Then
                    senderIndexPos = loopIndex
                    Exit For
                End If
            End If
            loopIndex += 1
        Next
        Return senderIndexPos
    End Function
End Class
