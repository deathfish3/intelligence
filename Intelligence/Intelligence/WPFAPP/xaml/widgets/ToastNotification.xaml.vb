﻿Imports System
Imports System.Windows
Imports System.Windows.Threading




Partial Public Class ToastNotification

    Dim _notificationText As String
    Public Property NotificationBody
        Get
            Return _notificationText
        End Get
        Set(value)
            _notificationText = value
        End Set
    End Property

    Dim _sendUser As String
    Public Property SendUserTitle
        Get
            Return _sendUser
        End Get
        Set(value)
            _sendUser = value
        End Set
    End Property

    Dim _sendDate As DateTime
    Public Property SendDateTime
        Get
            Return _sendDate.ToShortDateString
        End Get
        Set(value)
            _sendDate = value
        End Set
    End Property


    Public Sub New(noteText As String, sendUser As String, sendDate As DateTime)


        ' This call is required by the designer.
        InitializeComponent()
        Me.DataContext = Me
        ' Add any initialization after the InitializeComponent() call.
        NotificationBody = noteText
        SendUserTitle = sendUser
        SendDateTime = sendDate

        Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, New Action(AddressOf InvokeAction))

    End Sub

    Public Sub beginDisplay()
    End Sub

    Private Sub InvokeAction()
        Dim taskBarHeight = My.Computer.Screen.Bounds.Height - My.Computer.Screen.WorkingArea.Height
        Dim workingArea = System.Windows.SystemParameters.WorkArea
        Dim transform = PresentationSource.FromVisual(Me).CompositionTarget.TransformFromDevice '.TransformFromDevice
        Dim corner = transform.Transform(New Point(workingArea.Right, workingArea.Bottom))

        Me.Left = corner.X - Me.ActualWidth
        Me.Top = corner.Y - Me.ActualHeight

    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        'Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, New Action(AddressOf InvokeAction))
        'Dim taskBarHeight = My.Computer.Screen.Bounds.Height - My.Computer.Screen.WorkingArea.Height
        'Dim workingArea = My.Computer.Screen.WorkingArea
        'Dim transform = PresentationSource.FromVisual(Me).CompositionTarget.TransformFromDevice '.TransformFromDevice
        'Dim corner = transform.Transform(New Point(workingArea.Right, workingArea.Bottom))

        '        Me.Left = corner.X - Me.ActualWidth
        '       Me.Top = (corner.Y - Me.Height) ' - Me.Height 'ActualHeight
    End Sub



    Private Sub Storyboard_Completed(sender As Object, e As EventArgs)
        Me.Close()
    End Sub
End Class
