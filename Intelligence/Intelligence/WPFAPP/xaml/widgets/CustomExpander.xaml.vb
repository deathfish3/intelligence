﻿Public Class CustomExpander

    Private title As String

    Private _innerObject As Object
    Public ReadOnly Property innerObject As Object
        Get
            Return _innerObject
        End Get
    End Property

    Public Sub New(title As String, content As Object)
        InitializeComponent()

        Me.title = title
        nameTextBox.Text = title

        oldHeader = MyExpander.Header
        MyExpander.Header = "Versions"
        MyExpander.ExpandDirection = ExpandDirection.Down
        MyExpander.IsExpanded = True

        Me._innerObject = content
        If content IsNot Nothing Then
            innerGrid.Children.Add(content)
        End If
    End Sub

    Private oldHeader As Object
    Private Sub Expander_Expanded(sender As Object, e As RoutedEventArgs)
        MyExpander.Header = title
        MyExpander.ExpandDirection = ExpandDirection.Down
    End Sub

    Private Sub Expander_Collapsed(sender As Object, e As RoutedEventArgs)
        MyExpander.Header = oldHeader
        MyExpander.ExpandDirection = ExpandDirection.Right
    End Sub
End Class
