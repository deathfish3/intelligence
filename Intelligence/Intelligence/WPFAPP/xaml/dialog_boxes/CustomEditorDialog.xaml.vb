﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation

Partial Public Class CustomEditorDialog
    Inherits Window

    Private editableObject As Object
    Private customEditor As ICustomEditor

    Private Sub New(editableObject As Object, userControl As UserControl, customEditor As ICustomEditor)
        Me.New(MainWindow.mainWindow, editableObject, userControl, customEditor)
    End Sub

    Private Sub New(window As Window, editableObject As Object, userControl As UserControl, customEditor As ICustomEditor)
        InitializeComponent()
        Owner = window
        Me.editableObject = editableObject
        Me.customEditor = customEditor
        userControl.DataContext = editableObject
        customEditorGrid.Children.Add(userControl)
    End Sub

    Protected Overrides Sub OnContentRendered(e As EventArgs)
        MyBase.OnContentRendered(e)
        Me.MaxWidth = Double.PositiveInfinity
        Me.MaxHeight = Double.PositiveInfinity
        Me.SizeToContent = Windows.SizeToContent.Manual
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
    End Sub

    Private Sub YesClicked(sender As Object, e As RoutedEventArgs) Handles OkButton.Click
        If customEditor.submitChanges() Then
            DialogResult = True
            Close()
        End If
    End Sub

    Private Sub NoClicked(sender As Object, e As RoutedEventArgs) Handles CancelButton.Click
        DialogResult = False
        Close()
    End Sub

    Public Shared Function showEditorDialog(Of TOut, TEditor As {UserControl, ICustomEditor(Of TOut)}) _
                                                (editableObject As TOut, customEditor As TEditor) As TOut
        Return showEditorDialog(Of TOut, TEditor)(MainWindow.mainWindow, editableObject, customEditor)
    End Function

    'Returns Nothing if cancelled, or the edited version of the object if OK was clicked
    'Takes in an object to edit (TOut), and an editor for that type of object (TEditor)
    Public Shared Function showEditorDialog(Of TOut, TEditor As {UserControl, ICustomEditor(Of TOut)}) _
                                (window As Window, editableObject As TOut, customEditor As TEditor) As TOut
        Dim dlg As New CustomEditorDialog(window, editableObject, customEditor, customEditor)
        dlg.ShowDialog()
        If dlg.DialogResult = True Then
            Return dlg.editableObject
        Else
            Return Nothing
        End If
    End Function

End Class