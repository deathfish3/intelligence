﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation

Partial Public Class YesNoDialog
    Inherits Window

    Public Sub New(message As String)
        Me.New(MainWindow.mainWindow, message)
    End Sub

    Public Sub New(control As UserControl, message As String)
        Me.New(Window.GetWindow(control), message)
    End Sub

    Public Sub New(window As Window, message As String)
        InitializeComponent()
        Owner = window
        messageBox.Text = message
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
    End Sub

    Private Sub YesClicked(sender As Object, e As RoutedEventArgs) Handles YesButton.Click
        DialogResult = True
        Close()
    End Sub

    Private Sub NoClicked(sender As Object, e As RoutedEventArgs) Handles NoButton.Click
        DialogResult = False
        Close()
    End Sub

    Public Shared Function display(message As String) As Boolean
        Dim dlg As New YesNoDialog(message)
        dlg.ShowDialog()
        Return dlg.DialogResult
    End Function

End Class