﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports WPFAPP.MantisIssue
Imports WPFAPP.com.yoyogames.bugs
Imports System.Net.Http
Imports System.Net.Http.Headers

Partial Public Class LoginDialog
    Inherits Window

    Private _enteredThroughReportIssue As Boolean = False
    Private _enteredDetailsForZendesk As Boolean = False

    Public Sub New()
        InitializeComponent()
        UsernameTextBox.Focus()
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
    End Sub

    Public Sub New(header As String, message As String)
        Me.New(MainWindow.mainWindow, header, message)
        UsernameTextBox.Focus()
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
    End Sub

    Public Sub New(header As String, message As String, calledFromReportIssue As Boolean, enteredDetailsForZendesk As Boolean)
        Me.New(MainWindow.mainWindow, header, message)
        UsernameTextBox.Focus()
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
        _enteredThroughReportIssue = calledFromReportIssue
        _enteredDetailsForZendesk = enteredDetailsForZendesk
    End Sub

    Public Sub New(control As UserControl, header As String, message As String)
        Me.New(Window.GetWindow(control), header, message)
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
    End Sub

    Public Sub New(window As Window, header As String, message As String)
        InitializeComponent()
        Owner = window
        headerBox.Text = header
        Me.CommandBindings.Add(New CommandBinding(SystemCommands.CloseWindowCommand, Sub(t, e) OnCloseWindow(t, e)))
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
    End Sub

    Private Sub OKButton_Click() Handles OKButton.Click

        If (_enteredThroughReportIssue = False AndAlso _enteredDetailsForZendesk = False) Then

            Dim user As IntelligenceUserWrapper = Nothing
            Dim couldConnect As Boolean = True
            Using db As New IntelligenceDB
				Try
					Dim nameFromTextBox = UsernameTextBox.Text
					Dim u As IntelligenceUser = (From DBuser In db.IntelligenceUsers
												 Where DBuser IsNot Nothing AndAlso DBuser.username IsNot Nothing AndAlso DBuser.username = nameFromTextBox
												 Select DBuser).FirstOrDefault
					If u IsNot Nothing Then
						user = New IntelligenceUserWrapper(u)
					End If
				Catch ex As System.Data.Entity.Core.EntityException
					couldConnect = False
                    showErrorDialog("Database Connection Error", "Database Connection Error", "Could not connect to the Intelligence database. Please check your connection settings and try again.")
                End Try
            End Using

            If couldConnect Then
                If user Is Nothing Then
                    showErrorDialog("Login Error", "Login Error", "Username/Password Incorrect - Try Again")

                ElseIf (PassHasher.PassHasher.ValidatePassword(PasswordTextBox.Password, user.hash)) Then
                    If user.accessLevel.id = 0 Then
                        showErrorDialog("Access Level Error", "Access Level Error", "Access Level too low. This account may be inactive. Please use an active account.")
                    Else
                        UserInfo.user.intelligenceEmail = user.email
                        'DialogResult = True
                        Dim mainwindow As New MainWindow
                        mainwindow.Show()
                        Close()
                    End If
                Else
                    showErrorDialog("Login Error", "Login Error", "Username/Password Incorrect - Try Again")
                End If
            End If
        ElseIf (_enteredDetailsForZendesk) Then
            UserInfo.user.zendeskUsername = UsernameTextBox.Text
            UserInfo.user.zendeskPassword = PasswordTextBox.Password
            Dim zenLoader As New ZendeskLoader

            LoadWithAsyncGUITask.run(Of ZendeskUser) _
                        (zenLoader, MainWindow.mainWindow.panelPreferences,
                        zenLoader.getCurrentZendeskUser(UserInfo.user),
                        Sub(m)
                                Close()
                            End Sub)



        Else
            UserInfo.user.mantisUsername = UsernameTextBox.Text
            UserInfo.user.mantisPassword = PasswordTextBox.Password

            Try
                Dim mantisClient As New MantisConnect
                mantisClient.mc_enum_priorities(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)
                Close()
            Catch ex As Exception
                Dim dlg As New ErrorDialog("Validation Error",
                   "Error Modifying Mantis Details: " & "Mantis Username/Password Error",
                   "The Username or Password entered is invalid. Please enter a valid Username or Password")
                dlg.ShowDialog()
            End Try

        End If

    End Sub

    Private Function showErrorDialog(title As String, errorHeader As String, errorMessage As String) As Boolean
        Dim dlg As New ErrorDialog(title, errorHeader, errorMessage)
        dlg.ShowDialog()
        Return dlg.DialogResult
    End Function

    Private Sub PasswordTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles PasswordTextBox.KeyDown
        If e.Key.Equals(Key.Enter) AndAlso Not e.IsRepeat Then
            OKButton_Click()
        End If
    End Sub

    Private Sub OnCloseWindow(target As Object, e As ExecutedRoutedEventArgs)
        SystemCommands.CloseWindow(Me)
    End Sub
End Class