﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation

Partial Public Class LoadingDialog
    Inherits Window
    Implements Loader.LoadingProgressListener

    Private Sub New()
        InitializeComponent()
        loadingDescriptionBox.DataContext = Me
    End Sub

    Private text As String

    Public Property loadingText As String
        Get
            Return text
        End Get
        Set(value As String)
            text = value
            loadingDescriptionBox.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, Sub() loadingDescriptionBox.Text = value)
        End Set
    End Property

    Private Sub New(control As UserControl)
        Me.New(Window.GetWindow(control))
    End Sub

    Private Sub New(window As Window)
        Me.New()
        Owner = window
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
        loadingDialog = Nothing
    End Sub

    Sub onProgressUpdated(desc As String) Implements MantisLoader.LoadingProgressListener.onProgressUpdated
        loadingText = desc
    End Sub

    Private Shared lock As New Object
    Private Shared loadingDialog As LoadingDialog
    Public Shared Function getInstance(parent As UserControl) As LoadingDialog
        If parent Is Nothing Then
            Return getInstance(MainWindow.mainWindow)
        Else
            Return getInstance(Window.GetWindow(parent))
        End If
    End Function

    Public Shared Function getInstance(window As Window) As LoadingDialog
        SyncLock lock
            If loadingDialog Is Nothing Then
                If Not window Is Nothing Then
                    loadingDialog = New LoadingDialog(window)
                Else
                    loadingDialog = New LoadingDialog()
                End If
            End If
        End SyncLock
        Return loadingDialog
    End Function
End Class