﻿Imports System.ComponentModel

Public Class TaskInstanceEditor
    Inherits UserControl
    Implements ICustomEditor(Of TaskInstanceWrapper)

	Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
		Return True
	End Function

	Private Sub openBug()
        Dim selectedBug As BugWrapper = BugList.SelectedItem
        MainWindow.mainWindow.setMainPanel(MainWindow.mainWindow.panelReportIssue)
        MainWindow.mainWindow.setSelectedTab(MainWindow.mainWindow.GridReportIssue)
        MainWindow.mainWindow.panelReportIssue.ImportBug(selectedBug.mantisID)
    End Sub

    Private Sub startNowButton_Click(sender As Object, e As RoutedEventArgs) Handles startNowButton.Click
        If DataContext IsNot Nothing Then
            Dim task As TaskInstanceWrapper = TryCast(DataContext, TaskInstanceWrapper)
            If task IsNot Nothing Then
                task.startTask()
            End If
        End If
    End Sub

    Private Sub disableMouseWheel(sender As Object, e As MouseWheelEventArgs)
        e.Handled = True
    End Sub

    Private Sub Bug_MouseDown(sender As Object, e As MouseButtonEventArgs)
        If e.ClickCount = 2 Then
            openBug()
        End If
    End Sub
End Class
