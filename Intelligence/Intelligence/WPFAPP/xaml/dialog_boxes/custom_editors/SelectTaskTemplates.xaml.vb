﻿Imports System.ComponentModel
Imports WPFAPP.PlatformSelectionEditor

Public Class SelectTaskTemplates
    Inherits UserControl
    Implements ICustomEditor(Of IEnumerable(Of TaskTemplateSelection))

    Public Sub New()
        InitializeComponent()
        Debug.Print("In Select Task Templates")
    End Sub

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
        Return True
    End Function

    Public Shared Function wrapAsTemplateSelection(taskTemplates As IEnumerable(Of TaskTemplate),
                                           existingInstances As IEnumerable(Of TaskInstance)) _
                                           As IEnumerable(Of TaskTemplateSelection)

        Dim selection As New List(Of TaskTemplateSelection)

        For Each template As TaskTemplate In taskTemplates
            If template.active Then

                Dim activeSubTasks As IEnumerable(Of TaskTemplate) = template.subTasks.Where(Function(subTask) subTask.active).OrderBy(Function(t) t.rank).ThenBy(Function(t) t.id)
                Dim subTasks As IEnumerable(Of TaskTemplateSelection) = wrapAsTemplateSelection(activeSubTasks, existingInstances)

                Dim platformsSelected As IEnumerable(Of PlatformSelection) = PlatformSelectionEditor.wrapAsPlatformSelection(template.platforms, False)

                Dim hasInstanceForAllPlatforms As Boolean = True
                For Each platformSelection As PlatformSelection In platformsSelected
                    Dim hasInstance As Boolean = hasInstanceForPlatform(template, platformSelection.platform, existingInstances)
                    platformSelection.isSelected = hasInstance
                    platformSelection.isEnabled = Not hasInstance
                    hasInstanceForAllPlatforms = hasInstance And hasInstanceForAllPlatforms
                Next

                Dim selected As Boolean = hasInstanceForAllPlatforms AndAlso platformsSelected.Count > 0
                Dim enabled As Boolean = Not hasInstanceForAllPlatforms AndAlso platformsSelected.Count > 0

                selection.Add(New TaskTemplateSelection(template, platformsSelected, subTasks, selected, enabled))
            End If
        Next

        Return selection
    End Function

    Public Shared Function hasInstanceForPlatform(template As TaskTemplate, platform As Platform, existingInstances As IEnumerable(Of TaskInstance))


        Return (From instance As TaskInstance In existingInstances.ToList()
                    Where instance.taskTemplate.id = template.id _
                    And platform.id = instance.platform.id
                    Select instance).Any()
    End Function


    Public Class TaskTemplateSelection
        Implements INotifyPropertyChanged, PlatformSelectionListener

        Public Property taskTemplate As TaskTemplate
        Public Property parent As TaskTemplateSelection

        Public ReadOnly Property isRootNode As Boolean
            Get
                Return parent Is Nothing
            End Get
        End Property

        Private _realIsSelected As Boolean
        Private Property _isSelected As Boolean
            Get
                Return _realIsSelected
            End Get
            Set(value As Boolean)
                If Me.enabled Then
                    _realIsSelected = value
                End If
            End Set
        End Property
        Public Property isSelected As Boolean
            Get
                Return _isSelected

            End Get
            Set(value As Boolean)
                If enabled Then
                    _isSelected = value
                    RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("isSelected"))
                End If
                For Each child In subTasks
                    child.isSelected = value
                Next
                refreshFromChildren(parent, value)

                If _isSelected AndAlso (hasNoPlatformsSelected()) Then
                    For Each p In platformsSelected
                        p.isSelected = True
                    Next
                ElseIf Not _isSelected Then
                    For Each p In platformsSelected
                        p.isSelected = False
                    Next
                End If
            End Set
        End Property

        Private Function hasNoPlatformsSelected() As Boolean
            Return platformsSelected.All(Function(p) p.isEnabled AndAlso Not p.isSelected)
        End Function

        Private Sub refreshFromChildren(taskTemplateSelection As TaskTemplateSelection, isSelected As Boolean)
            If taskTemplateSelection IsNot Nothing AndAlso isSelected <> taskTemplateSelection.isSelected Then
                If isSelected Then
                    taskTemplateSelection._isSelected = True
                    refreshFromChildren(taskTemplateSelection.parent, True)
                    taskTemplateSelection.refreshSelectionGUI()
                ElseIf taskTemplateSelection.subTasks.All(Function(child) Not child.isSelected) Then
                    taskTemplateSelection._isSelected = False
                    refreshFromChildren(taskTemplateSelection.parent, False)
                    taskTemplateSelection.refreshSelectionGUI()
                End If
            End If
        End Sub

        Private Sub refreshSelectionGUI()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("isSelected"))
        End Sub

        Public Property platformsSelected As IEnumerable(Of PlatformSelection)
        Public Property subTasks As IEnumerable(Of TaskTemplateSelection)
        Public Property enabled As Boolean = True

        Public Sub New(taskTemplate As TaskTemplate, platformsSelected As IEnumerable(Of PlatformSelection), subTasks As IEnumerable(Of TaskTemplateSelection), selected As Boolean, enabled As Boolean)
            Me.taskTemplate = taskTemplate
            Me.platformsSelected = platformsSelected

            For Each p As PlatformSelection In platformsSelected
                p.listener = Me
            Next

            Me.subTasks = subTasks
            Me._isSelected = selected
            Me.enabled = enabled

            For Each child As TaskTemplateSelection In Me.subTasks
                child.parent = Me
            Next
        End Sub

        Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

        Private Sub setPlatformSelection(platform As Platform, value As Boolean)
            Dim platformSelection As PlatformSelection = getPlatformSelection(platform)
            If platformSelection IsNot Nothing AndAlso platformSelection.isEnabled Then
                platformSelection._isSelectedWithoutTriggeringListeners = value
            End If
            For Each child As TaskTemplateSelection In subTasks
                child.setPlatformSelection(platform, value)
            Next
        End Sub

        Private Sub refreshPlatformSelectionFromChildren(taskSelection As TaskTemplateSelection, platform As Platform, isSelected As Boolean)
            If taskSelection IsNot Nothing Then
                Dim platformSelection As PlatformSelection = taskSelection.getPlatformSelection(platform)
                If platformSelection.isSelected <> isSelected AndAlso platformSelection.isEnabled Then
                    If isSelected Then
                        platformSelection._isSelectedWithoutTriggeringListeners = True
                        refreshPlatformSelectionFromChildren(taskSelection.parent, platform, True)
                    ElseIf Not hasChildForPlatform(taskSelection, platform) Then
                        platformSelection._isSelectedWithoutTriggeringListeners = False
                        refreshPlatformSelectionFromChildren(taskSelection.parent, platform, False)
                    End If
                End If
            End If
        End Sub

        Private Function hasChildForPlatform(taskSelection As TaskTemplateSelection, platform As Platform) As Boolean
            Return (From child As TaskTemplateSelection In taskSelection.subTasks
                    Where child.hasPlatformSelected(platform) _
                    Select child).Any()
        End Function

        Private Function getPlatformSelection(platform As Platform) As PlatformSelection
            Return platformsSelected.Where(Function(p) p.platform.id.Equals(platform.id)).FirstOrDefault()
        End Function

        Private Function hasPlatformSelected(platform As Platform) As Boolean
            Dim platformSelection As PlatformSelection = getPlatformSelection(platform)
            Return platformSelection IsNot Nothing AndAlso platformSelection.isSelected
        End Function

        Public Sub onPlatformSelectionChanged(platform As Platform, isSelected As Boolean) Implements PlatformSelectionListener.onPlatformSelectionChanged
            For Each child As TaskTemplateSelection In subTasks
                child.setPlatformSelection(platform, isSelected)
            Next
            refreshPlatformSelectionFromChildren(parent, platform, isSelected)
            If isSelected AndAlso Not Me.isSelected Then
                Me.isSelected = True
            ElseIf hasNoPlatformsSelected() Then
                Me.isSelected = False
            End If
        End Sub
    End Class

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        TreeViewUtils.deselectItem(Me.taskTreeView)
    End Sub

    Private Sub taskTreeView_PreviewMouseRightButtonDown(sender As Object, e As MouseButtonEventArgs) Handles taskTreeView.PreviewMouseRightButtonDown
        Dim treeViewItem As TreeViewItem = VisualUpwardSearch(e.OriginalSource)

        If (treeViewItem IsNot Nothing) Then
            treeViewItem.Focus()
            e.Handled = True
        End If

    End Sub

    Shared Function VisualUpwardSearch(source As DependencyObject) As TreeViewItem
        While (source IsNot Nothing AndAlso Not (source Is GetType(TreeViewItem)))
            source = VisualTreeHelper.GetParent(source)
        End While
        Return source
    End Function
End Class
