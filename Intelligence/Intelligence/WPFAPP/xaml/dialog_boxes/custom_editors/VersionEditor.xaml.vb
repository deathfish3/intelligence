﻿Public Class VersionEditor
    Inherits UserControl
    Implements ICustomEditor(Of VersionWrapper)

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
        Return True
    End Function

    Private Sub resolutionListView_PreviewMouseWheel(sender As Object, e As MouseWheelEventArgs)
        e.Handled = True
    End Sub

    Private Sub UserControl_Loaded(sender As Object, e As RoutedEventArgs)
        Me.versionNameField.Focus()
    End Sub
End Class
