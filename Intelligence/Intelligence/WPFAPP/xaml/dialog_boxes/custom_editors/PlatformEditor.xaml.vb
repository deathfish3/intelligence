﻿Public Class PlatformEditor
    Inherits UserControl
    Implements ICustomEditor(Of PlatformWrapper)

    Public addCommand As ICommand

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
        Return True
    End Function

    Private Sub UserControl_Loaded(sender As Object, e As RoutedEventArgs)
        Me.NameTextBox.Focus()
    End Sub

    Private Sub NameTextBox_KeyUp(sender As Object, e As KeyEventArgs)
        If addCommand IsNot Nothing AndAlso e.Key = Key.Enter Then
            addCommand.Execute(Nothing)
        End If
    End Sub
End Class