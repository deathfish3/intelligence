﻿Public Class ProductEditor
    Inherits UserControl
    Implements ICustomEditor(Of ProductWrapper)

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
        Return True
    End Function

    Private Sub UserControl_Loaded(sender As Object, e As RoutedEventArgs)
        Me.NameTextBox.Focus()
    End Sub
End Class
