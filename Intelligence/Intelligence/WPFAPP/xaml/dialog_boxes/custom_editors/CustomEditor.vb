﻿Public Interface ICustomEditor
    Function submitChanges() As Boolean
End Interface

Public Interface ICustomEditor(Of T)
    Inherits ICustomEditor
End Interface
