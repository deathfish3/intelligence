﻿Public Class PlatformSelectionEditor
    Inherits UserControl
    Implements ICustomEditor(Of IEnumerable(Of PlatformSelection))

    Public Interface PlatformSelectionListener
        Sub onPlatformSelectionChanged(platform As Platform, isSelected As Boolean)
    End Interface

    Public Class PlatformSelection
        Inherits NotifyPropertyChangedBase

        Public dataCtx As Object

        Public listener As PlatformSelectionListener

        Public Property platform As Platform

        Private _isSelected As Boolean = True
        Public Property _isSelectedWithoutTriggeringListeners As Boolean
            Get
                Return _isSelected
            End Get
            Set(value As Boolean)
                If isEnabled Then
                    setField(value, _isSelected, Sub(v) _isSelected = v)
                    onPropertyChanged(Function() Me.isSelected)
                End If
            End Set
        End Property

        Public Property isSelected As Boolean
            Get
                Return _isSelectedWithoutTriggeringListeners
            End Get
            Set(value As Boolean)
                If isEnabled Then
                    If setField(value, _isSelectedWithoutTriggeringListeners, Sub(v) _isSelectedWithoutTriggeringListeners = v) _
                    AndAlso listener IsNot Nothing Then
                        listener.onPlatformSelectionChanged(platform, value)
                    End If
                End If
            End Set
        End Property
        Public Property isEnabled As Boolean = True
        Public Property task As TaskTemplate

        Public Sub New(platform As Platform, isSelected As Boolean, isEnabled As Boolean)
            Me.platform = platform
            Me.isSelected = isSelected
            Me.isEnabled = isEnabled

        End Sub

        Public Sub New(platform As Platform, selected As Boolean)
            Me.New(platform, selected, True)
        End Sub

        Public Sub New(platform As Platform)
            Me.New(platform, True)
        End Sub

        Public Sub New(platform As Platform, context As Object)
            Me.New(platform, True, True)
            dataCtx = context
            If dataCtx.isSelected = False Then
                Me.isEnabled = False
            End If
        End Sub
    End Class

    Public Shared Function wrapAsPlatformSelection(platforms As IEnumerable(Of Platform), selected As Boolean) As IEnumerable(Of PlatformSelection)
		Return (From platform In platforms
				Order By platform.rank Ascending, platform.id Ascending
				Select New PlatformSelection(platform, selected)).ToList()
	End Function

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
        Return True
    End Function
End Class
