﻿
Imports WPFAPP.com.yoyogames.bugs

Public Class SelectMantisProject
    Inherits UserControl
    Implements ICustomEditor(Of ProjectSelection)

    Public Sub New()
        InitializeComponent()

        Dim mantisClient As New MantisConnect
        Dim projectsDown = mantisClient.mc_projects_get_user_accessible(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)

        For Each project In projectsDown
            _mantisprojects.Add(ObjectRefFactory.Create(project.id, project.name))
            If project.subprojects IsNot Nothing Then
                For Each subproject In project.subprojects
                    subproject.name = "  >>  " & subproject.name
                    _mantisprojects.Add(ObjectRefFactory.Create(subproject.id, subproject.name))
                Next
            End If
        Next

        Dim defaultProject = _mantisprojects.Where(Function(p) p.id = 1).FirstOrDefault
        If defaultProject Is Nothing Then defaultProject = _mantisprojects.FirstOrDefault
        ProjectsComboBox.SelectedItem = defaultProject
    End Sub

    Dim _mantisprojects As New List(Of ObjectRef)
    Public ReadOnly Property mantisprojects As IList(Of ObjectRef)
        Get
            Return _mantisprojects
        End Get
    End Property


    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges

        Dim myProject = DirectCast(DataContext, ProjectSelection)
        Dim selectedProject As ObjectRef = ProjectsComboBox.SelectedItem
        myProject.project = selectedProject
        '= ProjectsComboBox.SelectedItem
        Return True

    End Function

    Public Sub setSelectedProject(project As ObjectRef)
        ProjectsComboBox.SelectedItem = project
    End Sub


    Public Class ProjectSelection
        Public project As ObjectRef
    End Class

End Class
