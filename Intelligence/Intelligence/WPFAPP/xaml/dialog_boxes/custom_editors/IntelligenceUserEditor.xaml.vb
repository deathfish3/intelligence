﻿Class IntelligenceUserEditor
    Inherits UserControl
    Implements ICustomEditor(Of IntelligenceUserWrapper)
    Public Sub New()
        InitializeComponent()
    End Sub

    Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges

        Dim shouldEditUser = YesNoDialog.display("Are you sure you want to edit this user?")
        If shouldEditUser Then
            Dim user As IntelligenceUserWrapper = DirectCast(DataContext, IntelligenceUserWrapper)

            If isEmpty(UsernameTextBox.Text) Then
                MessageDialog.display("Warning", "Please enter a username.")
                Return False
            ElseIf isEmpty(EmailTextBox.Text) Then
                MessageDialog.display("Warning", "Please enter an email.")
                Return False
            ElseIf AccessComboBox.SelectedItem Is Nothing Then
                MessageDialog.display("Warning", "Please select an access level for this user.")
                Return False
            ElseIf user.hash Is Nothing AndAlso (isEmpty(PasswordTextBox.Password)) Then
                MessageDialog.display("Warning", "Please enter a password for this user.")
                Return False
            Else
                user.name = UsernameTextBox.Text
                user.email = EmailTextBox.Text
                If Not isEmpty(PasswordTextBox.Password) Then
                    user.hash = PassHasher.PassHasher.CreateHash(PasswordTextBox.Password)
                End If
                user.accessLevel = AccessComboBox.SelectedItem
                user.defaultzenlogin = DefaultZenCheckbox.IsChecked
                Return True
            End If
        End If
        Return True
    End Function

    Private Function isEmpty(s As String) As Boolean
        Return s Is Nothing OrElse s.Equals("")
    End Function

    Private Sub AccessComboBox_PreviewMouseWheel(sender As Object, e As MouseWheelEventArgs)
        e.Handled = True
    End Sub

    Private Sub UserControl_Loaded(sender As Object, e As RoutedEventArgs)
        Me.UsernameTextBox.Focus()
    End Sub
End Class
