﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation

Partial Public Class ErrorDialog
    Inherits Window

    Public Shared loaderContext As Loader

    Public Sub New(errorMessage As String)
        Me.New(loaderContext.currentErrorTitle, loaderContext.currentErrorHeader, errorMessage, Nothing)
    End Sub

    Public Sub New(title As String, errorHeader As String, errorMessage As String)
        Me.New(title, errorHeader, errorMessage, Nothing)
    End Sub

    Public Sub New(window As Window, errorMessage As String)
        Me.New(window, loaderContext.currentErrorTitle, loaderContext.currentErrorHeader, errorMessage)
    End Sub

    Public Sub New(parent As UserControl, errorMessage As String)
        Me.New(parent, loaderContext.currentErrorTitle, loaderContext.currentErrorHeader, errorMessage)
    End Sub

    Public Sub New(window As Window, title As String, errorHeader As String, errorMessage As String)
        Me.New(title, errorHeader, errorMessage, window)
    End Sub

    Public Sub New(control As UserControl, title As String, errorHeader As String, errorMessage As String)
        Me.New(title, errorHeader, errorMessage, Window.GetWindow(control))
    End Sub

    Public Sub New(title As String, errorHeader As String, errorMessage As String, window As Window)
        InitializeComponent()
        If Not window Is Nothing Then
            Owner = window
        End If
        Me.Title = title
        titleBox.Text = title
        errorHeaderBox.Text = errorHeader
        errorDescriptionBox.Text = errorMessage
    End Sub

    Private Sub okButton_Click(sender As Object, e As RoutedEventArgs) Handles okButton.Click
        Close()
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub
End Class