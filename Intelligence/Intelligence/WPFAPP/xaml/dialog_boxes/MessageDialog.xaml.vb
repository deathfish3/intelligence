﻿Imports System
Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Media
Imports System.Windows.Media.Animation

Partial Public Class MessageDialog
    Inherits Window

    Public Sub New(header As String, message As String)
        Me.New(MainWindow.mainWindow, header, message)
    End Sub

    Public Sub New(control As UserControl, header As String, message As String)
        Me.New(Window.GetWindow(control), header, message)
    End Sub

    Public Sub New(window As Window, header As String, message As String)
        InitializeComponent()
        Owner = window
        headerBox.Text = header
        messageBox.Text = message
        OKButton.Focus()
    End Sub

    Private Sub onLeftMouseDown(ByVal sender As Object, ByVal e As System.Windows.Input.MouseButtonEventArgs)
        If Mouse.LeftButton = MouseButtonState.Pressed Then Me.DragMove()
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
        If Not Owner Is Nothing Then
            Owner.Focus()
        End If
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub

    Public Shared Sub display(header As String, message As String)
        Dim dlg As New MessageDialog(header, message)
        dlg.ShowDialog()
    End Sub
End Class