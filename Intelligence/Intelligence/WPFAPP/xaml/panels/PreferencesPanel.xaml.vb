﻿Imports System
Imports System.Collections.Generic
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes
Imports WPFAPP.MantisIssue
Imports WPFAPP.com.yoyogames.bugs
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.ComponentModel



Partial Public Class PreferencesPanel
    Implements INotifyPropertyChanged


    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.DataContext = Me

    End Sub

    Public ReadOnly Property MantisUsername
        Get
            Return UserInfo.user.mantisUsername
        End Get
    End Property

    Public ReadOnly Property ZendeskUsername
        Get
            Return UserInfo.user.zendeskUsername
        End Get
    End Property

    Public ReadOnly Property IntelUsername
        Get
            Return UserInfo.getCurrentIntelligenceUser.name
        End Get
    End Property

    Public ReadOnly Property IntelEmail
        Get
            Return UserInfo.getCurrentIntelligenceUser.email
        End Get
    End Property

    Public ReadOnly Property zenVisibility As Visibility
        Get
            If UserInfo.getCurrentIntelligenceUser.defaultzenlogin = False Then
                Return System.Windows.Visibility.Visible
            Else
                Return System.Windows.Visibility.Collapsed
            End If
        End Get
    End Property

    Public ReadOnly Property UpdateMantisDetails As ICommand
        Get
            Dim command As New GenericCommand(
                Sub()

                    If (TextboxMantisUsername.Text <> "" AndAlso TextboxMantisPassword.Password <> "") Then
                        Dim contents As Byte()
                        Dim humanReadableUserDetails As String = ""
                        UserInfo.user.mantisUsername = TextboxMantisUsername.Text
                        UserInfo.user.mantisPassword = TextboxMantisPassword.Password

                        Try
                            'Dim mantisClient As New MantisConnect
                            'mantisClient.mc_enum_priorities(UserInfo.user.mantisUsername, UserInfo.user.mantisPassword)


                            contents = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
                            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
                            humanReadableUserDetails = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)
                            Dim userDetails As String = "/@'" & UserInfo.user.mantisUsername & "/p'" & UserInfo.user.mantisPassword & "/p'" & UserInfo.user.zendeskUsername & "/p'" & UserInfo.user.zendeskPassword & "/p'" & UserInfo.getCurrentIntelligenceUser.name & "/p'"

                            Dim splitUsers As String() = Split(humanReadableUserDetails, "/@'")
                            'For each user in details
                            Dim splitUsersIndex As Integer = -1
                            For Each userString In splitUsers
                                splitUsersIndex = splitUsersIndex + 1
                                If userString <> "" Then
                                    'Split individual details with /p/ delimiter
                                    Dim splitUserDetails As String() = Split(userString, "/p'")

                                    'If the associatedUsername is the same as the current user of intelligence
                                    Dim associatedUsername As String = splitUserDetails(4)
                                    If associatedUsername = UserInfo.getCurrentIntelligenceUser.name Then
                                        'Assign saved user details
                                        splitUsers(splitUsersIndex) = userDetails
                                        Exit For
                                    End If
                                End If
                            Next
                            Dim reformedUsers As String = ""
                            For Each userString In splitUsers
                                If userString <> "" Then
                                    reformedUsers = reformedUsers & "/@'" & userString
                                End If
                            Next
                            Dim encryptedDetails As Byte() = ProtectData.ProtectData.Protect(System.Text.Encoding.Unicode.GetBytes(reformedUsers))
                            Dim file As System.IO.FileStream
                            file = New System.IO.FileStream("mantisUserAuth.txt", System.IO.FileMode.Create)
                            'Overwrite existing file.
                            file.Write(encryptedDetails, 0, encryptedDetails.Count)
                            Dim dlg As New MessageDialog("Stored Mantis Account Details Update Successful", "Your stored Mantis Account Details were updated successfully")
                            dlg.Show()
                        Catch ex As Exception
                            Dim dlg As New ErrorDialog("Validation Error",
                                               "Error Modifying Mantis Details: " & "Mantis Username/Password Error",
                                               "The Username or Password entered is invalid. Please enter a valid Username or Password")
                            dlg.ShowDialog()
                        End Try
                    Else
                        Dim dlg As New ErrorDialog("Validation Error",
                                               "Error Modifying Mantis Details: " & "Mantis Username/Password Error",
                                               "The Username or Password entered is invalid. Please enter a valid Username or Password")
                        dlg.ShowDialog()

                    End If

                End Sub)

            Return command
        End Get
    End Property

    Public ReadOnly Property UpdateZendeskDetails As ICommand
        Get
            Dim command As New GenericCommand(
                Sub()

                    If (TextboxZendeskUsername.Text <> "" AndAlso TextboxZendeskPassword.Password <> "") Then
                        Dim contents As Byte()
                        Dim humanReadableUserDetails As String = ""
                        UserInfo.user.zendeskUsername = TextboxZendeskUsername.Text
                        UserInfo.user.zendeskPassword = TextboxZendeskPassword.Password

                        Dim zen As New ZendeskTicket
                        Dim zenLoader As New ZendeskLoader

                        LoadWithAsyncGUITask.run(Of ZendeskUser) _
                        (zenLoader, Me,
                        zenLoader.getCurrentZendeskUser(UserInfo.user),
                        Sub(m)

                            'Dim isSuccessfulLogin = user.loadZendeskTicketFromId(0, UserInfo.user)

                            contents = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
                            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
                            humanReadableUserDetails = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)
                            Dim userDetails As String = "/@'" & UserInfo.user.mantisUsername & "/p'" & UserInfo.user.mantisPassword & "/p'" & UserInfo.user.zendeskUsername & "/p'" & UserInfo.user.zendeskPassword & "/p'" & UserInfo.getCurrentIntelligenceUser.name & "/p'"

                            Dim splitUsers As String() = Split(humanReadableUserDetails, "/@'")
                            'For each user in details
                            Dim splitUsersIndex As Integer = -1
                            For Each userString In splitUsers
                                splitUsersIndex = splitUsersIndex + 1
                                If userString <> "" Then
                                    'Split individual details with /p/ delimiter
                                    Dim splitUserDetails As String() = Split(userString, "/p'")

                                    'If the associatedUsername is the same as the current user of intelligence
                                    Dim associatedUsername As String = splitUserDetails(4)
                                    If associatedUsername = UserInfo.getCurrentIntelligenceUser.name Then
                                        'Assign saved user details
                                        splitUsers(splitUsersIndex) = userDetails
                                        Exit For
                                    End If
                                End If
                            Next
                            Dim reformedUsers As String = ""
                            For Each userString In splitUsers
                                If userString <> "" Then
                                    reformedUsers = reformedUsers & "/@'" & userString
                                End If
                            Next
                            Dim encryptedDetails As Byte() = ProtectData.ProtectData.Protect(System.Text.Encoding.Unicode.GetBytes(reformedUsers))
                            Dim file As System.IO.FileStream
                            file = New System.IO.FileStream("mantisUserAuth.txt", System.IO.FileMode.Create)
                            'Overwrite existing file.
                            file.Write(encryptedDetails, 0, encryptedDetails.Count)
                            Dim dlg As New MessageDialog("Stored Zendesk Account Details Update Successful", "Your stored Zendesk Account Details were updated successfully")
                            dlg.Show()
                        End Sub)
                    End If

                End Sub)
            Return command
        End Get
    End Property

    Public ReadOnly Property UpdateIntelDetails As ICommand
        Get
            Dim command As New GenericCommand(
                Sub()
                    UserInfo.getCurrentIntelligenceUser.name = TextboxIntelUsername.Text
                    If TextboxIntelPassword.Password <> "" Then
                        UserInfo.getCurrentIntelligenceUser.hash = PassHasher.PassHasher.CreateHash(TextboxIntelPassword.Password)
                    End If
                    UserInfo.getCurrentIntelligenceUser.email = TextboxIntelEmail.Text
                    Dim dlg As New MessageDialog("Intelligence Account Details Update Successful", "Your Intelligence Account Details were updated successfully")
                    dlg.Show()
                End Sub)
            Return command
        End Get
    End Property


    Public Shared Function showLoginDialogForMantis() As Boolean
        Dim dlg As New LoginDialog("Enter Mantis Login", "Log in below...", True, False)
        dlg.ShowDialog()
        Dim contents As Byte()
        Dim humanReadableUserDetails As String = ""
        Try
            'Try to open file. If fail, skip this section
            contents = My.Computer.FileSystem.ReadAllBytes("mantisUserAuth.txt")
            Dim decryptedUserDetails As Byte() = ProtectData.ProtectData.Unprotect(contents)
            'Convert to string
            humanReadableUserDetails = System.Text.Encoding.Unicode.GetString(decryptedUserDetails)
        Catch ex As Exception
        End Try
        'Construct user string, delimiting properties from each other
        Dim userDetails As String = "/@'" & UserInfo.user.mantisUsername & "/p'" & UserInfo.user.mantisPassword & "/p'" & UserInfo.getCurrentIntelligenceUser.name & "/p'"
        'Append to rest of details
        humanReadableUserDetails = humanReadableUserDetails & userDetails
        'Encrypt entire user details file
        Dim encryptedDetails As Byte() = ProtectData.ProtectData.Protect(System.Text.Encoding.Unicode.GetBytes(humanReadableUserDetails))
        Dim file As System.IO.FileStream
        file = New System.IO.FileStream("mantisUserAuth.txt", System.IO.FileMode.Create)
        'Overwrite existing file.
        file.Write(encryptedDetails, 0, encryptedDetails.Count)


        Return dlg.DialogResult
    End Function

    Public Sub refreshAll()
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(""))
    End Sub


    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged
End Class
