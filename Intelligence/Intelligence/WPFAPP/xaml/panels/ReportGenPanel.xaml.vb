﻿Imports System
Imports System.Collections.Generic
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes
Imports System.ComponentModel


Public Class ReportGenPanel
    Inherits UserControl
    Implements INotifyPropertyChanged



    Private viewModel As DocGenViewmodel

    Public Sub New()


        ' This call is required by the designer.
        InitializeComponent()

        viewModel = New DocGenViewmodel


        Me.DataContext = viewModel
        ' Add any initialization after the InitializeComponent() call.



    End Sub

    Public Sub UserControl_Loaded()
        viewModel.initMantisProjects()
    End Sub

    Public Sub UserControl_Unloaded()

    End Sub

    Private Sub AvailableProjectList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles AvailableProjectList.SelectionChanged

    End Sub

    Public Sub refreshAll()
        viewModel.timeControl.startTime = DateTime.Now
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(""))
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged
End Class
