﻿Public Class CurrentTaskPanel
	Inherits UserControl
	Implements ICustomEditor(Of TaskInstanceWrapper())

	Public Sub New()
		MyBase.New()
		InitializeComponent()
		RHS.Visibility = Windows.Visibility.Collapsed
		RHS.adminGrid.Visibility = Windows.Visibility.Collapsed

		LHSGrid.Width = New GridLength(My.Settings.CurrentTaskLHSWidth, GridUnitType.Star)
		RHSGrid.Width = New GridLength(My.Settings.CurrentTaskRHSWidth, GridUnitType.Star)


	End Sub

	Public Function submitChanges() As Boolean Implements ICustomEditor.submitChanges
		Return RHS.submitChanges()
	End Function

	Private Sub TreeView_SelectedItemChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Object))
		Dim selectedTask As TaskInstanceWrapper = taskHierarchy.SelectedItem
		If selectedTask IsNot Nothing Then
			RHS.DataContext = selectedTask
			If RHS.Visibility.Equals(Windows.Visibility.Collapsed) Then
				RHS.Visibility = Windows.Visibility.Visible
			End If
		Else
			RHS.Visibility = Windows.Visibility.Collapsed
		End If
	End Sub

	Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
		TreeViewUtils.deselectItem(Me.taskHierarchy)
	End Sub
End Class
