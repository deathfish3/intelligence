﻿Imports System
Imports System.Collections.Generic
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes
Imports System.ComponentModel

Partial Public Class PanelMyWorkControl
    Dim user As IntelligenceUserWrapper

    Public Sub New(user As IntelligenceUserWrapper)
        MyBase.New()
        Me.InitializeComponent()
        Me.user = user
        Me.DataContext = user
        Me.RefreshButton.DataContext = Me
        GridInprogress.Height = New GridLength(My.Settings.GridInprogressHeight, GridUnitType.Star)
        GridTasks.Height = New GridLength(My.Settings.GridMyTasksHeight, GridUnitType.Star)
        GridClosed.Height = New GridLength(My.Settings.GridClosedHeight, GridUnitType.Star)

        'LHSGrid.Width = New GridLength(My.Settings.CurrentTaskLHSWidth, GridUnitType.Star)
        'RHSGrid.Width = New GridLength(My.Settings.CurrentTaskRHSWidth, GridUnitType.Star)

        'setCurrentTask(Nothing)
        'RHS.Visibility = Windows.Visibility.Collapsed
        'RHS.adminGrid.Visibility = Windows.Visibility.Hidden
    End Sub

	'Private lastSelectedItem As TaskInstanceWrapper = Nothing

	Private respondToSelectionChanging As Boolean = True
	'Public Sub onSelectedItemChanged(sender As Object, e As SelectionChangedEventArgs)
	'    If respondToSelectionChanging Then
	'        Dim listThatChanged As ListView = DirectCast(sender, ListView)
	'        Dim selectedItem As TaskInstanceWrapper = listThatChanged.SelectedItem

	'        respondToSelectionChanging = False
	'        Me.inProgressList.setSelectedItem(Nothing)
	'        Me.myTasksList.setSelectedItem(Nothing)
	'        Me.closedList.setSelectedItem(Nothing)
	'        listThatChanged.SelectedItem = selectedItem
	'        respondToSelectionChanging = True

	'        If selectedItem Is Nothing Then
	'            RHS.Visibility = Windows.Visibility.Collapsed
	'        Else
	'            If RHS.Visibility.Equals(Windows.Visibility.Collapsed) Then
	'                RHS.Visibility = Windows.Visibility.Visible
	'            End If
	'        End If
	'        setCurrentTask(selectedItem)
	'        lastSelectedItem = selectedItem
	'    End If
	'End Sub

	Public Sub HandleDoubleClick(sender As Object, e As MouseEventArgs)
		Dim l As ListViewItem = DirectCast(sender, ListViewItem)
		Debug.Print(l.DataContext.ToString)
		MainWindow.mainWindow.panelCurrentTask.DataContext = {l.DataContext}
		MainWindow.mainWindow.setMainPanel(MainWindow.mainWindow.panelCurrentTask)
		MainWindow.mainWindow.setSelectedTab(MainWindow.mainWindow.GridCurrentTask)
	End Sub


	Public Sub HandleSplitterDoubleClick(sender As GridSplitter, e As MouseEventArgs)
        If sender.Name = "SplitterMyTasks" Then
            GridInprogress.Height = New GridLength(1, GridUnitType.Star)
            My.Settings.GridInprogressHeight = GridInprogress.Height.Value
        ElseIf sender.Name = "SplitterClosed" Then
            GridTasks.Height = New GridLength(1, GridUnitType.Star)
            My.Settings.GridMyTasksHeight = GridTasks.Height.Value
        End If


        My.Settings.Save()
    End Sub

    Public ReadOnly Property refresh As ICommand
        Get
            Return New GenericCommand(Sub() refreshTasks())
        End Get
    End Property

    Public Sub refreshTasks()
        respondToSelectionChanging = False
        user.refreshTasks()

        'Task.Delay(100).ContinueWith(Sub() setSelectedTask(lastSelectedItem), TaskScheduler.FromCurrentSynchronizationContext)
    End Sub

    'Private Sub setCurrentTask(task As TaskInstanceWrapper)
    '    Dim oldTask As TaskInstanceWrapper = TryCast(RHS.DataContext, TaskInstanceWrapper)
    '    If oldTask IsNot Nothing Then
    '        RemoveHandler oldTask.PropertyChanged, AddressOf Me.onTaskPropertyChanged
    '    End If

    '    RHS.DataContext = task

    '    If task IsNot Nothing Then
    '        AddHandler task.PropertyChanged, AddressOf Me.onTaskPropertyChanged
    '    End If
    'End Sub

    'Private Sub onTaskPropertyChanged(sender As Object, e As PropertyChangedEventArgs)
    '    Dim taskWrapper As TaskInstanceWrapper = TryCast(sender, TaskInstanceWrapper)
    '    If taskWrapper IsNot Nothing Then
    '        If NotifyPropertyChangedBase.isEventForProperty(e, Function() taskWrapper.resolution) Then
    '            'Me.refreshTasks()
    '            Me.user.onTaskResolutionChanged(taskWrapper)

    '            lastSelectedItem = taskWrapper
    '            Task.Delay(100).ContinueWith(Sub() setSelectedTask(taskWrapper), TaskScheduler.FromCurrentSynchronizationContext)
    '        End If
    '    End If
    'End Sub

    'Private Sub setSelectedTask(task As TaskInstanceWrapper)
    '    respondToSelectionChanging = False
    '    Me.inProgressList.setSelectedItem(Nothing)
    '    Me.myTasksList.setSelectedItem(Nothing)
    '    Me.closedList.setSelectedItem(Nothing)
    '    respondToSelectionChanging = True

    '    setCurrentTask(task)
    '    RHS.Visibility = Windows.Visibility.Collapsed

    '    If lastSelectedItem IsNot Nothing Then
    '        If Not Me.inProgressList.setSelectedItem(task) Then
    '            If Not Me.myTasksList.setSelectedItem(task) Then
    '                Me.closedList.setSelectedItem(task)
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub UserControl_Loaded(sender As Object, e As RoutedEventArgs)
        Debug.Print("loaded")
        respondToSelectionChanging = True
    End Sub

    Private Sub UserControl_Unloaded(sender As Object, e As RoutedEventArgs)
        Debug.Print("unloaded")
        respondToSelectionChanging = False
    End Sub
End Class
