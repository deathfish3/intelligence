﻿Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf

Public Class StatsPanel
    Inherits UserControl
    Implements StatsQueryList.ISelectionChanedListener(Of StatsQuery)

    Private viewModel As StatsViewModel
	
    Private premadeStatsList As StatsQueryListView


    Sub New()
        InitializeComponent()
        Me.viewModel = New StatsViewModel(Function() If(Me.viewModel.shouldDisplaySeparateGraphs, Me.SingleGraphTreeView, Me.MultiGraphTreeView))
        Me.DataContext = viewModel
        Me.premadeStatsList = New StatsQueryListView()
        Dim expander = New CustomExpander("Stats Queries", premadeStatsList)
        Me.GridToPutSatsQueriesIn.Children.Add(expander)

        GridQuery.Height = New GridLength(My.Settings.StatsQueryRow, GridUnitType.Star)
        GridGraph.Height = New GridLength(My.Settings.StatsGraphRow, GridUnitType.Star)


        AddHandler MainWindow.mainWindow.Closed, AddressOf Me.onClosed

        'Dim test As System.Windows.Controls.DataVisualization.Charting.BarSeries


        'Dim test As System.Windows.Controls.DataVisualization.Charting.BarSeries
        'Dim TaskKeyValues As IList(Of KeyValuePair(Of String, Integer))
        ''init ilist
        'TaskKeyValues = New List(Of KeyValuePair(Of String, Integer))

        'Using db As New IntelligenceDB
        '    Dim instances = (From value In db.TaskInstances
        '                     Order By value.taskTemplate.name
        '                     Group By value.resolution
        '                     Into grp = Group, Count())

        '    Dim iter As Integer = 0
        '    For Each entry In instances
        '        Dim name As String = entry.resolution.name
        '        Dim count As Integer = entry.Count
        '        Dim tempKeyValues As KeyValuePair(Of String, Integer) = New KeyValuePair(Of String, Integer)(name, count)

        '        TaskKeyValues.Add(tempKeyValues)
        '    Next

        '    Dim series = DirectCast(TestChart.Series(0), System.Windows.Controls.DataVisualization.Charting.PieSeries)

        '    series.ItemsSource = TaskKeyValues
        '    TestChart.Series(0) = series

        'End Using

        ' Create a new PDF document
        Dim document As PdfDocument = New PdfDocument
        document.Info.Title = "Created with PDFsharp"

        ' Create an empty page
        Dim page As PdfPage = document.AddPage

        ' Get an XGraphics object for drawing
        Dim gfx As XGraphics = XGraphics.FromPdfPage(page)

        ' Draw crossing lines
        Dim pen As XPen = New XPen(XColor.FromArgb(255, 0, 0))
        gfx.DrawLine(pen, New XPoint(0, 0), New XPoint(page.Width.Point, page.Height.Point))
        gfx.DrawLine(pen, New XPoint(page.Width.Point, 0), New XPoint(0, page.Height.Point))

        ' Draw an ellipse
        gfx.DrawEllipse(pen, 3 * page.Width.Point / 10, 3 * page.Height.Point / 10, 2 * page.Width.Point / 5, 2 * page.Height.Point / 5)

        ' Create a font
        Dim font As XFont = New XFont("Verdana", 20, XFontStyle.Bold)

        ' Draw the text
        gfx.DrawString("Hello, World!", font, XBrushes.Black, _
        New XRect(0, 0, page.Width.Point, page.Height.Point), XStringFormats.Center)

        ' Save the document...
        Dim filename As String = "HelloWorld.pdf"
        'document.Save(filename)

        ' ...and start a viewer.
        'Process.Start(filename)





    End Sub


    Private Sub StatsPanel_Loaded(sender As Object, e As RoutedEventArgs)
        Dim statsQueryList As StatsQueryList = PremadeStatsList.StatsQueriesList.DataContext
        statsQueryList.setSelectionChangedListener(Me)
        Dim firstItem = statsQueryList.FirstOrDefault()
        If firstItem IsNot Nothing Then
            statsQueryList.selectedItem = firstItem
        Else
            Me.QueryDetailsView.Visibility = System.Windows.Visibility.Collapsed
        End If
    End Sub

    Public Sub onSelectionChanged(newlySelectedItem As Wrapper(Of StatsQuery)) Implements WrapperList(Of StatsQuery).ISelectionChanedListener(Of StatsQuery).onSelectionChanged
        If newlySelectedItem IsNot Nothing Then
            Me.QueryEditor.DataContext = newlySelectedItem
            Me.viewModel.setStatsQuery(newlySelectedItem)
            Me.QueryDetailsView.Visibility = System.Windows.Visibility.Visible
        Else
            Me.QueryDetailsView.Visibility = System.Windows.Visibility.Collapsed
        End If
    End Sub


    Private Sub onClosed(sender As Object, e As System.EventArgs)
        Me.viewModel.saveQuery()
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

    End Sub
End Class
