﻿Public Class TaskAdminPanel

    Public Sub New()
        InitializeComponent()
        ContentPanel.showNewPanel(Nothing, New ProductListView(ContentPanel, New ProductList()), "Products")
    End Sub

    


    'Code for exporting as a component as a PNG. Will be useful for exporting charts later.

    'Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
    '    Dim thingToExport = Me.ContentPanel
    '    Dim bounds As Rect = VisualTreeHelper.GetDescendantBounds(thingToExport)

    '    Dim renderBitmap As New RenderTargetBitmap(CInt(bounds.Width), CInt(bounds.Height), 96, 96, PixelFormats.Pbgra32)

    '    Dim isolatedVisual As New DrawingVisual()
    '    Using drawing As DrawingContext = isolatedVisual.RenderOpen()
    '        ' Optional Background
    '        'drawing.DrawRectangle(Brushes.White, Nothing, New Rect(New Point(), bounds.Size))

    '        'Actual image.
    '        drawing.DrawRectangle(New VisualBrush(thingToExport), Nothing, New Rect(New Point(), bounds.Size))
    '    End Using

    '    renderBitmap.Render(isolatedVisual)

    '    Dim uloz_obr As New Microsoft.Win32.SaveFileDialog()
    '    uloz_obr.FileName = "Graf"
    '    uloz_obr.DefaultExt = "png"

    '    Dim result As Nullable(Of Boolean) = uloz_obr.ShowDialog()
    '    If result = True Then
    '        Dim obr_cesta As String = uloz_obr.FileName

    '        Using outStream As New System.IO.FileStream(obr_cesta, System.IO.FileMode.Create)
    '            Dim encoder As New PngBitmapEncoder()
    '            encoder.Frames.Add(BitmapFrame.Create(renderBitmap))
    '            encoder.Save(outStream)
    '        End Using
    '    End If
    'End Sub
End Class
