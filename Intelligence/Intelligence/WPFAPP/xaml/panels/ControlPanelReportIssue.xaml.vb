﻿Imports System
Imports System.Collections.Generic
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Animation
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes
Imports WPFAPP.com.yoyogames.bugs
Imports System.ServiceModel.Channels
Imports System.Web.Services.Protocols
Imports WPFAPP.MantisIssue

Partial Public Class ControlPanelReportIssue
    Inherits UserControl

    Public Shared fileDir As String()

    Dim mantisIssue As MantisIssue
    Dim testZendeskTicketId As String = "85560"
    'Dim testProjectId As String = "45"
    Dim testIssueId As String = "0018161"

    Dim tempAttachment As AttachmentData()
    Dim tempPath As String()
    Public Shared firstEntry As Boolean = True

    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        GridNotes.Visibility = Windows.Visibility.Collapsed
        GridAttachments.Visibility = Windows.Visibility.Collapsed
        GridRelationships.Visibility = Windows.Visibility.Collapsed

    End Sub

    Private Sub button_MantisImportClick(sender As Object, e As RoutedEventArgs) Handles ButtonImportMantis.Click
        Dim mantisId As String = issueSourceField.Text
        Dim mantisLoader As New MantisLoader

        LoadWithAsyncGUITask.run(Of MantisIssue) _
            (mantisLoader, Me,
             Function() mantisLoader.issueFromId(mantisId, UserInfo.user),
             Sub(m) resetMantisIssue(m))


    End Sub

    Public Sub ImportBug(idIn As String)
        Dim mantisID As String = idIn
        Dim mantisLoader As New MantisLoader

        LoadWithAsyncGUITask.run(Of MantisIssue) _
            (mantisLoader, Me,
             Function() mantisLoader.issueFromId(mantisID, UserInfo.user),
             Sub(m) resetMantisIssue(m))
    End Sub

    Private Sub button_ZendeskImportClick(sender As Object, e As RoutedEventArgs) Handles ButtonImportZendesk.Click
        Dim zenLoader As New ZendeskLoader
        Dim mantisLoader As New MantisLoader
        Dim zenId As String = issueSourceField.Text

        mantisLoader.addListener(LoadingDialog.getInstance(Me))
        zenLoader.addListener(LoadingDialog.getInstance(Me))

        If Not zenId Is Nothing AndAlso zenId.Length > 0 Then
            LoadWithAsyncGUITask.run(Of MantisIssue) _
                (zenLoader, Me,
                 importZendeskTicket(zenLoader, mantisLoader, zenId),
                 Sub(m)
                     resetMantisIssue(m)
                     GridNotes.Visibility = Windows.Visibility.Collapsed
                     GridAttachments.Visibility = Windows.Visibility.Collapsed
                     GridRelationships.Visibility = Windows.Visibility.Collapsed
                 End Sub)

        End If
    End Sub

    Private Async Function importZendeskTicket(zenLoader As ZendeskLoader, mantisLoader As MantisLoader, zenId As Integer) As Task(Of MantisIssue)
        Dim zenTicket As ZendeskTicket = Await zenLoader.loadZendeskTicketFromId(zenId, UserInfo.user).ConfigureAwait(False)
        Dim zenUser As ZendeskUser = Await zenLoader.loadZendeskUserFromId(zenTicket.reporterId, UserInfo.user).ConfigureAwait(False)
        ErrorDialog.loaderContext = mantisLoader

        Return mantisLoader.mantisIssueFromZendesk(zenTicket, zenUser, UserInfo.user)
    End Function

    Private Sub resetMantisIssue(issue As MantisIssue)
        mantisIssue = issue

        If issue IsNot Nothing Then
            If issue.source = IssueSource.Mantis Then
                GridNotes.Visibility = Windows.Visibility.Visible
                GridAttachments.Visibility = Windows.Visibility.Visible
                GridRelationships.Visibility = Windows.Visibility.Visible
            Else
                GridNotes.Visibility = Windows.Visibility.Collapsed
                GridAttachments.Visibility = Windows.Visibility.Collapsed
                GridRelationships.Visibility = Windows.Visibility.Collapsed

                If issue.source = IssueSource.Zendesk Then
                    Dim projectSelection As New SelectMantisProject.ProjectSelection With {.project = issue.internalIssueData.project}
                    Dim projectSelectionEditor As New SelectMantisProject
                    projectSelectionEditor.setSelectedProject(issue.internalIssueData.project)
                    projectSelection = CustomEditorDialog.showEditorDialog(projectSelection, projectSelectionEditor)

                    If projectSelection IsNot Nothing Then
                        Dim project As ObjectRef = projectSelection.project
                        issue.internalIssueData.project = project
                        issue.CategoryCollection = MantisCacheManager.MantisCategoriesInProject(project.id)
                        If project.id <> 1 Then
                            issue.CustomFields = {}
                        End If
                    End If
                End If
            End If

            LayoutRoot.DataContext = mantisIssue
            issue.onAllPropertiesChanged()
        End If

    End Sub

    Private Sub button_SubmitClick(sender As Object, e As RoutedEventArgs) Handles ButtonSubmit.Click
        If Not mantisIssue Is Nothing Then
            Dim mantisLoader As New MantisLoader
            Dim zenLoader As New ZendeskLoader
            mantisLoader.addListener(LoadingDialog.getInstance(Me))
            zenLoader.addListener(LoadingDialog.getInstance(Me))

            If (ComboCategory.SelectedItem IsNot Nothing AndAlso _
                TextBoxSummary.Text <> "" AndAlso _
                TextBoxDescription.Text <> "") Then
                LoadWithAsyncGUITask.run(Of MantisIssue) _
                    (mantisLoader, Me,
                     Function() submitAndClose(mantisLoader, zenLoader, mantisIssue, UserInfo.user),
                     Sub(m)
                         Dim message As String = "Submitted issue """ & m.internalIssueData.id & """ to Mantis"
                         If mantisIssue.source.Equals(IssueSource.Zendesk) Then
                             message = message & " and solved Zendesk ticket """ & mantisIssue.zendeskId & """"
                         End If
                         Dim dlg As New MessageDialog("Success", message & ".")
                         dlg.Show()
                         resetMantisIssue(m)
                     End Sub)
            Else
                Dim dlg As New ErrorDialog("Form Completion Error",
                   "Error Submitting form: " & "Mandatory field not completed",
                   "A mandatory field is not complete. Please fill in this field...")
                dlg.ShowDialog()
            End If

        End If
    End Sub

    Private Function submitAndClose(mantisLoader As MantisLoader, zenLoader As ZendeskLoader, issue As MantisIssue, user As UserInfo) As MantisIssue
        If Not issue Is Nothing Then
            Dim mantisId As Integer = mantisLoader.submitToMantis(issue, user)
            If mantisId <> -1 Then
                If IssueSource.Zendesk = mantisIssue.source Then
                    ErrorDialog.loaderContext = zenLoader
                    zenLoader.closeTicket(mantisIssue.zendeskId, mantisIssue.zendeskUser, mantisId, user)
                End If

                ErrorDialog.loaderContext = mantisLoader
                Return mantisLoader.issueFromId(mantisId, user)
            End If
        End If
        Return Nothing
    End Function

    Private Sub onCancelButtonClicked(sender As Object, e As RoutedEventArgs)
        resetMantisIssue(Nothing)

    End Sub

    Private Sub issueSourceField_KeyUp(sender As Object, e As KeyEventArgs) Handles issueSourceField.KeyUp
        Debug.Print("Key pressed " & e.Key)
        If e.Key.Equals(Key.Enter) Then
            Debug.Print("Enter pressed " & e.Key)
            button_MantisImportClick(sender, e)
        End If
    End Sub

    Private Sub disableMouseWheel(sender As Object, e As MouseWheelEventArgs)
        e.Handled = True
    End Sub

    Private Sub ButtonNewIssue_Click(sender As Object, e As RoutedEventArgs) Handles ButtonNewIssue.Click
        Dim mantisLoader As New MantisLoader
        Dim project As New SelectMantisProject.ProjectSelection
        project = CustomEditorDialog.showEditorDialog(project, New SelectMantisProject)

        GridNotes.Visibility = Windows.Visibility.Collapsed
        GridAttachments.Visibility = Windows.Visibility.Collapsed
        GridRelationships.Visibility = Windows.Visibility.Collapsed

        If project IsNot Nothing Then
            LoadWithAsyncGUITask.run(Of MantisIssue) _
                    (mantisLoader, Me,
                     Function() mantisLoader.newIssue(UserInfo.user, project.project),
                     Sub(m) resetMantisIssue(m))
        End If
    End Sub

End Class
