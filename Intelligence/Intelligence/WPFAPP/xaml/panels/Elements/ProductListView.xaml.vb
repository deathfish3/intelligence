﻿Imports System.ComponentModel

Public Class ProductListView
    Inherits UserControl
    Implements INotifyPropertyChanged
    Implements IKeyboardShortcutListener
    Implements IClosable



    Dim parentPanel As CustomDockPanel
    Dim products As ProductList

    Public ReadOnly Property editProduct As ICommand
        Get
            Return EditCommand.create(Of Product, ProductWrapper, ProductEditor)(Me, parentPanel, products)
        End Get
    End Property

    Public ReadOnly Property deleteProduct As ICommand
        Get
            Return New DeleteCommand(Of Product)(products)
        End Get
    End Property

    Public ReadOnly Property addProduct As ICommand
        Get
            Return New AddCommand(Of Product, ProductWrapper, ProductEditor)(Me, parentPanel, products)
        End Get
    End Property

    Public ReadOnly Property dupProduct As ICommand
        Get
            Return New GenericCommand(
                Function(o) ProductsList.SelectedItem IsNot Nothing,
                Sub(oldProductWrapper As ProductWrapper)

                    LoadWithAsyncGUITask.run(Of ProductWrapper) _
                        ("Duplicating product.",
                            Function()
                                Dim newProduct As ProductWrapper = Nothing
                                Using db As New IntelligenceDB
                                    Dim oldProduct = db.Products.Find(oldProductWrapper.id)
                                    If oldProduct IsNot Nothing Then
                                        newProduct = products.addNew(db, False)
                                        newProduct.getWrappedItem.active = oldProduct.active
                                        newProduct.getWrappedItem.name = oldProduct.name

                                        For Each templ In oldProduct.taskTemplates.Where(Function(t) t.active AndAlso t.parentTask Is Nothing)
                                            copyExistingTemplate(templ, newProduct, db, Nothing)
                                        Next

                                        db.SaveChanges()
                                    End If
                                End Using
                                Return newProduct
                            End Function,
                            Sub(newProduct As ProductWrapper)
                                If newProduct IsNot Nothing Then
                                    products.Add(newProduct)
                                End If
                            End Sub)
                End Sub)
        End Get
    End Property

    Public Sub New(parentPanel As CustomDockPanel, products As ProductList)
        InitializeComponent()
        Me.parentPanel = parentPanel
        Me.products = products

        Me.DataContext = Me
        Me.rightClickOnListMenu.DataContext = Me

        EditTaskTemplatesButton.IsEnabled = False
        ProductsList.DataContext = products

        KeybindingContainer.AddListener(Me)
    End Sub

    Public Sub copyExistingTemplate(toCopy As TaskTemplate, newProduct As ProductWrapper, db As IntelligenceDB, parent As TaskTemplate)
        Dim newCopy As New TaskTemplate
        db.TaskTemplates.Add(newCopy)
        newCopy.active = toCopy.active
        newCopy.description = toCopy.description
        newCopy.name = toCopy.name
        newCopy.parentTask = parent
        copyPlatforms(newCopy, toCopy, db)
        newCopy.product = newProduct.getWrappedItem
        db.SaveChanges()
        For Each copyChild As TaskTemplate In toCopy.subTasks
            copyExistingTemplate(copyChild, newProduct, db, newCopy)
        Next
    End Sub

    Private Sub ProductsList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles ProductsList.SelectionChanged
        Dim product As ProductWrapper = ProductsList.SelectedItem
        Dim newPanel = If(product Is Nothing, Nothing, New VersionListView(parentPanel, product.getVersions()))
        parentPanel.showNewPanel(Me, newPanel, "Versions")
        EditTaskTemplatesButton.IsEnabled = product IsNot Nothing
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("dupProduct"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("editProduct"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("deleteProduct"))
    End Sub

    Public Shared Sub copyTemplate(copyFrom As TaskTemplate, copyTo As TaskTemplate, parentTask As TaskTemplate)
        copyTo.active = True
        copyTo.description = copyFrom.description
        copyTo.name = copyFrom.name
        copyTo.product = copyFrom.product
        copyTo.parentTask = parentTask
    End Sub

    Public Sub copyPlatforms(copyTo As TaskTemplate, copyFrom As TaskTemplate, db As IntelligenceDB)
        Dim meFromDB As TaskTemplate = db.TaskTemplates.Find(copyFrom.id)
        For Each p As Platform In meFromDB.platforms
            copyTo.platforms.Add(p)
        Next
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.ProductsList.SelectedItem = Nothing
        parentPanel.closeAllToRightOf(Me)
    End Sub

    Private Sub EditTaskTemplatesButton_Click(sender As Object, e As RoutedEventArgs)
        Dim product As ProductWrapper = products.selectedItem
        If product IsNot Nothing Then
            Dim templates As New TaskTemplateList(product)
            Dim templatesPanel As New TaskTemplateTreeEditor(parentPanel, templates)
            parentPanel.showNewPanel(Me, templatesPanel, "Task Templates")
        End If
    End Sub

    Private Sub onEditPlatformsButtonClicked(sender As Object, e As RoutedEventArgs)
        Dim newPanel As New PlatformListView(parentPanel, New PlatformList())
        parentPanel.showNewPanel(Me, newPanel, "Platforms")
    End Sub

    Private Sub ViewUsersButton_Click(sender As Object, e As RoutedEventArgs)
        Dim newPanel As New IntelligenceUserListView(parentPanel, New IntelligenceUserList)
        parentPanel.showNewPanel(Me, newPanel, "Users")
    End Sub

    Private Sub ViewUsersButton_Loaded(sender As Object, e As RoutedEventArgs) Handles ViewUsersButton.Loaded
        If Not AccessLevelUtils.CanAccessAdminContent() Then
            ViewUsersButton.Visibility = Windows.Visibility.Collapsed
        End If
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs)
        products.reloadContents()
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Public Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean Implements IKeyboardShortcutListener.onKeyboardShortcutPressed
        If key.Key = Input.Key.N AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addProduct.Execute(Nothing)
            Return True
        End If
        If key.Key = Input.Key.D AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.dupProduct.Execute(ProductsList.SelectedItem)
            Return True
        End If
        If key.Key = Input.Key.Delete Then
            Me.deleteProduct.Execute(ProductsList.SelectedItem)
            Return True
        End If
        Return False
    End Function

	Private Sub ListViewItem_PreviewMouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
		Dim item As ListViewItem = TryCast(sender, ListViewItem)
		If item IsNot Nothing AndAlso item.IsSelected Then
			ProductsList_SelectionChanged(Nothing, Nothing)
		End If
	End Sub

	Private Sub ListViewItem_PreviewMouseMove(sender As Object, e As MouseEventArgs)
		Dim item As ListViewItem = TryCast(sender, ListViewItem)
		If item IsNot Nothing AndAlso e.LeftButton.Equals(MouseButtonState.Pressed) Then
			DragDrop.DoDragDrop(item, item.DataContext, DragDropEffects.Move)
			item.IsSelected = True
			ProductsList_SelectionChanged(Nothing, Nothing)
		End If
	End Sub


	Private Sub ListViewItem_Drop(sender As Object, e As DragEventArgs)
		'This is based on the C# code from here: http://stackoverflow.com/questions/3350187/wpf-c-rearrange-items-in-listbox-via-drag-and-drop

		Dim droppedData As ProductWrapper = e.Data.GetData(GetType(ProductWrapper))
		Dim target As ProductWrapper = (DirectCast(sender, ListBoxItem)).DataContext

		Dim removedIndex = ProductsList.Items.IndexOf(droppedData)
		Dim targetIndex = ProductsList.Items.IndexOf(target)

		If removedIndex < targetIndex Then
			Using db As New IntelligenceDB()
				products.moveWrapperBeside(droppedData, target, False, db)
			End Using
		Else
			Dim remIdx = removedIndex + 1
			If products.Count + 1 > remIdx Then
				Using db As New IntelligenceDB()
					products.moveWrapperBeside(droppedData, target, True, db)
				End Using
			End If
		End If
	End Sub

	Public Sub close() Implements IClosable.close
		KeybindingContainer.RemoveListener(Me)
	End Sub
End Class
