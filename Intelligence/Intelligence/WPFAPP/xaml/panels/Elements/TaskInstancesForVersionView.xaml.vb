﻿Public Class TaskInstancesForVersionView
    Implements IClosable
    Implements WrapperList(Of TaskInstance).IReloadContentsListener

    Private parentPanel As CustomDockPanel

    Dim topLevelTasks As TaskInstanceList

    Public Sub New(parentPanel As CustomDockPanel, topLevelTasks As TaskInstanceList)
        InitializeComponent()
        Me.parentPanel = parentPanel
        Me.topLevelTasks = topLevelTasks
        topLevelTasks.setReloadListener(Me)
        Me.DataContext = topLevelTasks
    End Sub


    Private lastSelectedItem As Wrapper(Of TaskInstance)
    Private Sub TaskInstancesTreeView_SelectedItemChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Object))
        If TaskInstancesTreeView.SelectedItem IsNot Nothing AndAlso (e.OldValue IsNot e.NewValue OrElse e.NewValue.instance.id <> e.OldValue.instance.id) Then
            lastSelectedItem = TaskInstancesTreeView.SelectedItem
            Dim newPanel = New TaskInstanceEditor()
            newPanel.DataContext = TaskInstancesTreeView.SelectedItem
            parentPanel.showNewPanel(Me, newPanel)
        End If
    End Sub

    Private Function getContianer(task As TaskInstanceWrapper) As TaskInstanceList
        If task.parent Is Nothing Then Return topLevelTasks
        Return task.parent.subTasks
    End Function


    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs) Handles RefreshButton.Click
        topLevelTasks.version.reloadWrappedItem()
    End Sub

    Public Sub close() Implements IClosable.close
        topLevelTasks.removeReloadListener(Me)
        topLevelTasks.stopListeningToVersion()
    End Sub

    Public Sub onContentsReloaded() Implements WrapperList(Of TaskInstance).IReloadContentsListener.onContentsReloaded
        If lastSelectedItem IsNot Nothing Then
            selectLastTreeViewItemDelayed(0)
        End If
    End Sub

    Private Sub selectLastTreeViewItemDelayed(numRetries As Integer)
        Task.Delay(50).ContinueWith(Sub()
                                        If Not selectLastTreeViewItem(numRetries) Then
                                            selectLastTreeViewItemDelayed(numRetries + 1)
                                        End If
                                    End Sub, TaskScheduler.FromCurrentSynchronizationContext)
    End Sub

    Private Function selectLastTreeViewItem(numRetries As Integer) As Boolean
        If lastSelectedItem Is Nothing Then Return True
		If numRetries > 300 Then Return True
		Return TreeViewUtils.setSelectedItem(TaskInstancesTreeView, lastSelectedItem)
    End Function

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        TreeViewUtils.deselectItem(Me.TaskInstancesTreeView)
        parentPanel.closeAllToRightOf(Me)
    End Sub

    Private Sub TaskGrid_Loaded(sender As Object, e As RoutedEventArgs)
        TreeExpansionManager.setExpandedFromHistory(sender)
    End Sub

    Private Sub TaskGrid_Expanded(sender As Object, e As RoutedEventArgs)
        TreeExpansionManager.onExpanded(sender.DataContext)
    End Sub

    Private Sub TaskGrid_Collapsed(sender As Object, e As RoutedEventArgs)
        TreeExpansionManager.onCollapsed(sender.DataContext)
    End Sub
End Class
