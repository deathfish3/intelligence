﻿Public Class PlatformsForTasksListView
    Inherits UserControl

    Private parentPanel As CustomDockPanel

    Private version As VersionWrapper

    Public Sub New(parentPanel As CustomDockPanel, version As VersionWrapper)
        InitializeComponent()
        Me.parentPanel = parentPanel
        Me.version = version
        Me.DataContext = version


        If version.alltaskscount = 0 Then
            PlatformsList.Visibility = Windows.Visibility.Hidden
        End If

        AddHandler version.PropertyChanged, AddressOf onVersionPropertyChanged

    End Sub

    Public Shared shouldRespondToSelectionChanging As Boolean = True

    Private Sub PlatformList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles PlatformsList.SelectionChanged

        If version.alltaskscount = 0 Then
            PlatformsList.Visibility = Windows.Visibility.Hidden
        End If

        If shouldRespondToSelectionChanging Then
            Dim platform As VersionWrapper.PlatformTasks = PlatformsList.SelectedItem

            parentPanel.closeAllToRightOf(Me)

            If platform IsNot Nothing Then
                shouldRespondToSelectionChanging = False
                TaskInstanceList.allowReload = False

                version.reloadWrappedItem()

                Dim allTopLevelInstances = New TaskInstanceList(Me.version, platform.platform)
                Dim newPanel = New TaskInstancesForVersionView(parentPanel, allTopLevelInstances)
                parentPanel.showNewPanel(Me, newPanel, "Tasks")

                shouldRespondToSelectionChanging = True
                TaskInstanceList.allowReload = True
            End If
        End If
    End Sub

    Private Sub onVersionPropertyChanged()
        If version.alltaskscount = 0 Then
            PlatformsList.Visibility = Windows.Visibility.Hidden
        Else
            PlatformsList.Visibility = Windows.Visibility.Visible
        End If
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.PlatformsList.SelectedItem = Nothing
    End Sub
End Class
