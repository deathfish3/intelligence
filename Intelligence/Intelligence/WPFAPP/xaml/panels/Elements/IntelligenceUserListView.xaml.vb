﻿Imports System.ComponentModel

Public Class IntelligenceUserListView
    Implements INotifyPropertyChanged
    Implements IKeyboardShortcutListener
    Implements IClosable


    Dim parentPanel As CustomDockPanel
    Dim users As IntelligenceUserList


    Public ReadOnly Property addUser As ICommand
        Get
            Return users.getAddNewUserCommand()
        End Get
    End Property

    Public ReadOnly Property editUser As ICommand
        Get
            Return EditCommand.create(Of IntelligenceUser, IntelligenceUserWrapper, IntelligenceUserEditor)(Me, parentPanel, users)
        End Get
    End Property

    Public Sub New(parentPanel As CustomDockPanel, users As IntelligenceUserList)
        InitializeComponent()
        Me.parentPanel = parentPanel

        Me.users = users

        Me.DataContext = users
        Me.rightClickOnListMenu.DataContext = Me

        KeybindingContainer.AddListener(Me)

    End Sub

    Dim lastItemSelected As IntelligenceUserWrapper = Nothing
    Private Sub UsersList_Selected(sender As Object, e As RoutedEventArgs)
        Dim selectedUser As IntelligenceUserWrapper = UsersList.SelectedItem
        If lastItemSelected IsNot selectedUser OrElse (lastItemSelected IsNot Nothing AndAlso Not lastItemSelected.Equals(selectedUser)) Then

            Debug.Print("Selected: " + If(selectedUser Is Nothing, Nothing, selectedUser.ToString))

            Dim selectionSuccessFul As Boolean
            If selectedUser Is Nothing Then
                selectionSuccessFul = parentPanel.closeAllToRightOf(Me)
            Else
                Dim newPanel As New PanelMyWorkControl(selectedUser)
                selectionSuccessFul = parentPanel.showNewPanel(Me, newPanel)
            End If

            If Not selectionSuccessFul Then
                UsersList.SelectedItem = lastItemSelected
            Else
                lastItemSelected = selectedUser
            End If
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("editUser"))
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.UsersList.SelectedItem = Nothing
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Public Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean Implements IKeyboardShortcutListener.onKeyboardShortcutPressed
        If key.Key = Input.Key.N AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addUser.Execute(Nothing)
            Return True
        End If
        Return False
    End Function

    Public Sub close() Implements IClosable.close
        KeybindingContainer.RemoveListener(Me)
    End Sub
End Class
