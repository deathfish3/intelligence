﻿Imports System.Reflection
Imports System.ComponentModel
Imports WPFAPP.TaskTemplateWrapper

Public Class TaskTemplateTreeEditor
    Implements INotifyPropertyChanged
    Implements IKeyboardShortcutListener
    Implements IClosable



    Dim parentPanel As CustomDockPanel
    Dim startPoint As Point
    Dim isDragging As Boolean
    Dim lastMouseDown As Point
    Dim target As TreeViewItem
    Dim draggedItem As TreeViewItem
    Dim rightMouseLastClicked As Boolean = False
    Dim rightClickTreeMenuOpen As Boolean = False
    Dim lastSelectedTreeViewItem As TreeViewItem = Nothing
    Dim templates As TaskTemplateList

    Public Property addTaskTemplate As ICommand
    Public Property addRootTaskTemplate As ICommand

    Public ReadOnly Property duplicateTaskTemplate As ICommand
        Get
            Return New GenericCommand(
                Function(o) Me.taskTemplateTreeView.SelectedItem IsNot Nothing,
                Sub(toDuplicate As TaskTemplateWrapper)
					If toDuplicate IsNot Nothing Then
						Using db As New IntelligenceDB
							Dim copiedVersion = toDuplicate.container.copyExistingTemplate(toDuplicate, db, False, True)
							setSelectedItem(copiedVersion)
						End Using
					End If
				End Sub)
        End Get
    End Property

    Public ReadOnly Property deleteTaskTemplate As ICommand
        Get
            Return New DeleteCommand(Of TaskTemplate)(AddressOf getTemplateListToDeleteFrom,
                                                      Function() Me.taskTemplateTreeView.SelectedItem IsNot Nothing,
                                                      Sub() TreeViewUtils.deselectItem(Me.taskTemplateTreeView),
                                                      Sub()
                                                          parentPanel.closeAllToRightOf(Me)
                                                          RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
                                                          RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
                                                      End Sub)
        End Get
    End Property

    Public Sub New(parentPanel As CustomDockPanel, templates As TaskTemplateList)
        InitializeComponent()
        Me.parentPanel = parentPanel
        Me.templates = templates
        Me.DataContext = Me 'templates

        Me.taskTemplateTreeView.DataContext = templates

        Me.rightClickTreeMenu.DataContext = Me
        Me.DescLabel.DataContext = templates

        Me.addTaskTemplate = New AddCommand(Of TaskTemplate, TaskTemplateWrapper, TaskTemplateEditor) _
                                            (Me, parentPanel,
                                             AddressOf getTemplateListToAddTo,
                                             Sub(newItem As TaskTemplateWrapper)
                                                 Dim itemParent As TaskTemplateWrapper = newItem.parent

                                                 If itemParent IsNot Nothing Then
                                                     Dim item As TreeViewItem = TreeViewUtils.getTreeViewItemFromDataContext(Me.taskTemplateTreeView, itemParent)

                                                     If item IsNot Nothing Then

                                                         item.IsExpanded = True
                                                     End If
                                                 End If
                                                 Task.Delay(10).ContinueWith(Sub() setSelectedItem(newItem), TaskScheduler.FromCurrentSynchronizationContext)
                                             End Sub)
        Me.addRootTaskTemplate = New AddCommand(Of TaskTemplate, TaskTemplateWrapper, TaskTemplateEditor) _
                                               (Me, parentPanel, Function() templates, AddressOf setSelectedItem)

        If templates.Count = 0 Then
            addRootTaskTemplate.Execute(Nothing)
            templates.First.name = templates.First.getWrappedItem.product.name
        End If


        KeybindingContainer.AddListener(Me)

    End Sub

    Private Function getTemplateListToAddTo() As TaskTemplateList
        Dim selectedTemplate As TaskTemplateWrapper = taskTemplateTreeView.SelectedItem
        If selectedTemplate Is Nothing Then
            Return templates
        Else

            Return selectedTemplate.subTasks
        End If
    End Function

    Private Function getTemplateListToDeleteFrom() As TaskTemplateList
        If taskTemplateTreeView.SelectedItem IsNot Nothing Then
            Dim selectedTemplate As TaskTemplateWrapper = taskTemplateTreeView.SelectedItem
            Return selectedTemplate.container
        Else
            Return Nothing
        End If
    End Function

    Public Sub onTaskTemplateSelected(sender As Object, e As RoutedEventArgs)
        taskTemplateTreeView.Tag = e.OriginalSource
        Dim selectedTaskTemplate As TaskTemplateWrapper = taskTemplateTreeView.SelectedItem
        If selectedTaskTemplate Is Nothing Then
            parentPanel.closeAllToRightOf(Me)
        Else
            Dim templateEditor As New TaskTemplateEditor
            templateEditor.DataContext = selectedTaskTemplate
            parentPanel.showNewPanel(Me, templateEditor)
        End If
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
    End Sub

    ' Function to set item selection in treeview
    Public Sub setSelectedItem(templateToSelect As TaskTemplateWrapper)
        If templateToSelect Is Nothing Then
            parentPanel.closeAllToRightOf(Me)
        End If

        TreeViewUtils.setSelectedItem(taskTemplateTreeView, templateToSelect)

        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
    End Sub

    Public Sub TreeView_MouseDown(sender As Object, e As MouseButtonEventArgs)
        If e.ChangedButton = MouseButton.Left Then
            lastMouseDown = e.GetPosition(taskTemplateTreeView)

            Dim treeView As TreeView = TryCast(sender, TreeView)
            If treeView IsNot Nothing Then
                If lastSelectedTreeViewItem IsNot Nothing Then
                    lastSelectedTreeViewItem.IsSelected = False
                    'context.selectedTaskTemplate = Nothing
                    lastSelectedTreeViewItem = Nothing
                    treeView.Focus()
                End If
            End If
        End If
    End Sub

    Private isScrolling As Boolean

    Public Sub TreeView_MouseMove(sender As Object, e As MouseEventArgs)
        Dim currentPosition As Point = e.GetPosition(taskTemplateTreeView)
        If e.LeftButton = MouseButtonState.Pressed Then
            If currentPosition.X > (taskTemplateTreeView.ActualWidth - 15) Then
                isScrolling = True
            End If
        Else
            isScrolling = False
        End If

        If taskTemplateTreeView.SelectedItem IsNot Nothing Then

            If e.LeftButton = MouseButtonState.Pressed AndAlso Not rightClickTreeMenuOpen Then

                'Make sure the mouse was not over the scrollbar, and was moved a non-negligable ammount
                If Not isScrolling _
                AndAlso ((Math.Abs(currentPosition.X - lastMouseDown.X) > 10.0) OrElse (Math.Abs(currentPosition.Y - lastMouseDown.Y) > 10.0)) Then
                    draggedItem = taskTemplateTreeView.Tag
                    If Not draggedItem Is Nothing Then
                        Dim finalDropEffect As DragDropEffects = DragDrop.DoDragDrop(taskTemplateTreeView, taskTemplateTreeView.SelectedItem, DragDropEffects.Move)
                        If ((finalDropEffect = DragDropEffects.Move) AndAlso target IsNot Nothing) Then
                            If Not draggedItem.Equals(target) Then
                                'Change above check to check dragging/targetitem TaskTemplate IDS instead. Safer
                                Dim draggingItem As TaskTemplateWrapper = TryCast(draggedItem.DataContext, TaskTemplateWrapper)
                                Dim targetItem As TaskTemplateWrapper = TryCast(target.DataContext, TaskTemplateWrapper)

                                Dim newSelection As Wrapper(Of TaskTemplate)
                                Dim draggedItemWasExpanded As Boolean = draggedItem.IsExpanded
                                Dim success As Boolean = False

                                Using db As New IntelligenceDB
                                    If targetItem.dragDropMoveType = MoveType.CHILD_OF Then
                                        Dim targetContainer As TaskTemplateList = If(targetItem Is Nothing, templates, targetItem.subTasks)
                                        newSelection = targetContainer.copyExistingTemplate(draggingItem, db, True, True)
                                    Else
                                        Dim moveAbove As Boolean = targetItem.dragDropMoveType = MoveType.ABOVE
                                        newSelection = targetItem.container.moveWrapperBeside(draggingItem, targetItem, moveAbove, db)
                                    End If
                                    success = newSelection IsNot Nothing AndAlso DBManager.saveChanges(db)
                                End Using

                                If success Then
                                    TreeViewUtils.setSelectedItem(Me.taskTemplateTreeView, newSelection)
                                    Dim newTreeViewItem As TreeViewItem = TreeViewUtils.getTreeViewItemFromDataContext(Me.taskTemplateTreeView, newSelection)
                                    If newTreeViewItem IsNot Nothing Then
                                        newTreeViewItem.IsExpanded = draggedItemWasExpanded
                                    End If
                                    RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
                                    RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
                                    Task.Delay(10).ContinueWith(
                                        Sub()
                                            TreeViewUtils.setSelectedItem(Me.taskTemplateTreeView, newSelection)
                                            newTreeViewItem = TreeViewUtils.getTreeViewItemFromDataContext(Me.taskTemplateTreeView, newSelection)
                                            If newTreeViewItem IsNot Nothing Then
                                                newTreeViewItem.IsExpanded = draggedItemWasExpanded
                                            End If
                                            RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
                                            RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
                                        End Sub, TaskScheduler.FromCurrentSynchronizationContext())
                                End If

                                targetItem.dragDropMoveType = MoveType.NONE
                                draggingItem.dragDropMoveType = MoveType.NONE
                                target = Nothing
                                draggedItem = Nothing
                            End If
                        End If
                    End If
                End If
            End If
            rightMouseLastClicked = False
        End If
    End Sub



    Public Sub TreeView_Drop(sender As Object, e As DragEventArgs)
        If Not rightClickTreeMenuOpen Then
            e.Effects = DragDropEffects.None
            e.Handled = True

            If target IsNot Nothing Then
                Dim oldTargetWrapper As TaskTemplateWrapper = target.DataContext
                oldTargetWrapper.dragDropMoveType = MoveType.NONE
            End If

            Dim TargetItem As TreeViewItem = GetNearestContainer(GetObjectOfTypeInVisualTree(Of TreeViewItem)(e.OriginalSource))
            If TargetItem Is Nothing AndAlso draggedItem IsNot Nothing Then
                Dim draggedWrapper As TaskTemplateWrapper = draggedItem.DataContext
                Dim parentWrapper As TaskTemplateWrapper = draggedWrapper.parent
                If parentWrapper IsNot Nothing Then
                    parentWrapper.dragDropMoveType = MoveType.BELOW
                    target = TreeViewUtils.getTreeViewItemFromDataContext(Me.taskTemplateTreeView, parentWrapper)
                    e.Effects = DragDropEffects.Move
                End If
            ElseIf TargetItem IsNot Nothing AndAlso draggedItem IsNot Nothing AndAlso Not TargetItem.DataContext.Equals(draggedItem.DataContext) Then
                target = TargetItem

                Dim positionOnTarget As Point = e.GetPosition(target)

                'These should ideally be generated dynamically, but using ActualHeight gets messed up when the
                'TreeViewItem has children, so the hardcoded versions are used here.
                Dim elementHeight As Double = 36
                Dim elementWidth As Double = 362

                Dim percentageHeight As Double = positionOnTarget.Y / elementHeight
                Dim percentageWidth As Double = positionOnTarget.X / elementWidth

                Dim targetWrapper As TaskTemplateWrapper = target.DataContext
                If percentageWidth > 0.75 Then
                    targetWrapper.dragDropMoveType = MoveType.CHILD_OF
                    target.IsExpanded = True
                ElseIf percentageHeight > 0.5 Then
                    targetWrapper.dragDropMoveType = MoveType.BELOW
                Else
                    targetWrapper.dragDropMoveType = MoveType.ABOVE
                End If
                e.Effects = DragDropEffects.Move
                Debug.Print("Percentage Width = " & percentageWidth.ToString & "PercentageHeight = " & percentageHeight.ToString & "  pos = " & positionOnTarget.ToString & "  height " & elementHeight.ToString & " move type " & targetWrapper.dragDropMoveType.ToString)
            End If
        End If
    End Sub

    Private Function GetNearestContainer(element As UIElement) As TreeViewItem
        Dim container As TreeViewItem = element
        While ((container Is Nothing) And Not (element Is Nothing)) 'Modify this function to find a type that matches type fed in
            element = VisualTreeHelper.GetParent(element)
            container = element
        End While
        Return container
    End Function

    Private Function GetObjectOfTypeInVisualTree(Of T As DependencyObject)(dpob As DependencyObject) As T
        Dim parent As DependencyObject = dpob
        Do While (parent IsNot Nothing)
            parent = VisualTreeHelper.GetParent(parent)
            Dim parentAsT As T = TryCast(parent, T)
            If parentAsT IsNot Nothing Then
                Return parentAsT
            End If
        Loop
        Return Nothing
    End Function

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        TreeViewUtils.deselectItem(Me.taskTemplateTreeView)
        parentPanel.closeAllToRightOf(Me)
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteTaskTemplate))
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.duplicateTaskTemplate))
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Private Sub taskTemplateTreeView_PreviewMouseRightButtonDown(sender As Object, e As MouseButtonEventArgs) Handles taskTemplateTreeView.PreviewMouseRightButtonDown
        Dim treeViewItem As TreeViewItem = VisualUpwardSearch(DirectCast(e.OriginalSource, DependencyObject))
        lastMouseDown = e.GetPosition(taskTemplateTreeView)
        rightMouseLastClicked = True
        If (treeViewItem IsNot Nothing) Then
            treeViewItem.Focus()
            e.Handled = True
        End If

    End Sub

    Shared Function VisualUpwardSearch(source As DependencyObject) As TreeViewItem
        While (source IsNot Nothing AndAlso Not (TypeOf source Is TreeViewItem))
            source = VisualTreeHelper.GetParent(source)
        End While
        Return DirectCast(source, TreeViewItem)
    End Function

    Private Sub rightClickTreeMenu_Closed(sender As Object, e As RoutedEventArgs)
        rightClickTreeMenuOpen = False
    End Sub

    Private Sub rightClickTreeMenu_Opened(sender As Object, e As RoutedEventArgs)
        rightClickTreeMenuOpen = True
    End Sub

    Private Sub taskTemplateTreeView_DragOver(sender As Object, e As DragEventArgs)
        If target IsNot Nothing Then
            Dim oldTargetWrapper As TaskTemplateWrapper = target.DataContext
            oldTargetWrapper.dragDropMoveType = MoveType.NONE
        End If

        Dim TargetItem As TreeViewItem = GetNearestContainer(GetObjectOfTypeInVisualTree(Of TreeViewItem)(e.OriginalSource))
        If TargetItem Is Nothing AndAlso draggedItem IsNot Nothing Then
            Dim draggedWrapper As TaskTemplateWrapper = draggedItem.DataContext
            Dim parentWrapper As TaskTemplateWrapper = draggedWrapper.parent
            If parentWrapper IsNot Nothing Then
                parentWrapper.dragDropMoveType = MoveType.BELOW
                target = TreeViewUtils.getTreeViewItemFromDataContext(Me.taskTemplateTreeView, parentWrapper)
            End If
        ElseIf TargetItem IsNot Nothing AndAlso draggedItem IsNot Nothing AndAlso Not TargetItem.DataContext.Equals(draggedItem.DataContext) Then
            target = TargetItem

            Dim positionOnTarget As Point = e.GetPosition(target)

            'These should ideally be generated dynamically, but using ActualHeight gets messed up when the
            'TreeViewItem has children, so the hardcoded versions are used here.
            Dim elementHeight As Double = 36
            Dim elementWidth As Double = 362

            Dim percentageHeight As Double = positionOnTarget.Y / elementHeight
            Dim percentageWidth As Double = positionOnTarget.X / elementWidth

            Dim targetWrapper As TaskTemplateWrapper = target.DataContext
            If percentageWidth > 0.75 Then
                targetWrapper.dragDropMoveType = MoveType.CHILD_OF
            ElseIf percentageHeight > 0.5 Then
                targetWrapper.dragDropMoveType = MoveType.BELOW
            Else
                targetWrapper.dragDropMoveType = MoveType.ABOVE
            End If
        End If
    End Sub

    Private Sub taskTemplateTreeView_IsVisibleChanged(sender As Object, e As DependencyPropertyChangedEventArgs)
        If Me.IsVisible = True Then
            Me.Focusable = True
            Keyboard.Focus(Me)
        ElseIf Me.IsVisible = False Then
            Me.Focusable = False
            Keyboard.Focus(Nothing)
        End If
    End Sub

    Public Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean Implements IKeyboardShortcutListener.onKeyboardShortcutPressed
        If key.Key = Input.Key.R AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addRootTaskTemplate.Execute(Nothing)
            Return True
        ElseIf key.Key = Input.Key.N AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addTaskTemplate.Execute(Nothing)
            Return True
        ElseIf key.Key = Input.Key.D AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.duplicateTaskTemplate.Execute(taskTemplateTreeView.SelectedItem)
            Return True
        ElseIf key.Key = Input.Key.Delete Then
            Me.deleteTaskTemplate.Execute(taskTemplateTreeView.SelectedItem)
            Return True
        End If
        Return False
    End Function

    Public Sub close() Implements IClosable.close
        KeybindingContainer.RemoveListener(Me)
    End Sub
End Class
