﻿Imports System.ComponentModel

Public Class PlatformListView
    Inherits UserControl
    Implements INotifyPropertyChanged
    Implements IKeyboardShortcutListener
    Implements IClosable


    Dim parentPanel As CustomDockPanel
    Dim platforms As PlatformList

    Public ReadOnly Property addPlatform As ICommand
        Get
            Return New AddCommand(Of Platform, PlatformWrapper, PlatformEditor)(Me, parentPanel, Function() platforms, Sub(e) e.addCommand = Me.addPlatform, Nothing)
        End Get
    End Property

    Public ReadOnly Property deletePlatform As ICommand
        Get
            Return New DeleteCommand(Of Platform)(platforms)
        End Get
    End Property

    Public ReadOnly Property dupPlatform As ICommand
        Get
            Return New GenericCommand(
                Function(o) platforms.selectedItem IsNot Nothing,
                Sub(oldPlatform As PlatformWrapper)
                    Using db As New IntelligenceDB
                        Dim newPlatform As PlatformWrapper = platforms.addNew(db)
                        newPlatform.getWrappedItem.active = oldPlatform.getWrappedItem.active
                        newPlatform.getWrappedItem.name = oldPlatform.getWrappedItem.name

                        

                        DBManager.saveChanges(db)
                    End Using
                End Sub)
        End Get
    End Property

    Public Sub New(parentPanel As CustomDockPanel, platforms As PlatformList)
        InitializeComponent()

        Me.parentPanel = parentPanel
        Me.platforms = platforms

        Me.DataContext = Me
        Me.rightClickOnListMenu.DataContext = Me

        PlatformsList.DataContext = platforms

        KeybindingContainer.AddListener(Me)

    End Sub

    Private Sub PlatformsList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles PlatformsList.SelectionChanged
        Dim platform As PlatformWrapper = PlatformsList.SelectedItem
        If platform Is Nothing Then
            parentPanel.closeAllToRightOf(Me)
        Else
            Dim platformEditor As New PlatformEditor
            platformEditor.DataContext = platform
            platformEditor.addCommand = Me.addPlatform
            parentPanel.showNewPanel(Me, platformEditor)
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("deletePlatform"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("dupPlatform"))
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.PlatformsList.SelectedItem = Nothing
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Public Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean Implements IKeyboardShortcutListener.onKeyboardShortcutPressed
        If key.Key = Input.Key.N AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addPlatform.Execute(Nothing)
            Return True
        End If
        If key.Key = Input.Key.Delete Then
            Me.deletePlatform.Execute(platforms.selectedItem)
            Return True
        End If
        If key.Key = Input.Key.D AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.dupPlatform.Execute(platforms.selectedItem)
            Return True
        End If
        Return False
    End Function

	Public Sub close() Implements IClosable.close
		KeybindingContainer.RemoveListener(Me)
	End Sub

	Private Sub ListViewItem_PreviewMouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
		Dim item As ListViewItem = TryCast(sender, ListViewItem)
		If item IsNot Nothing AndAlso item.IsSelected Then
			PlatformsList_SelectionChanged(Nothing, Nothing)
		End If
	End Sub

	Private Sub ListViewItem_PreviewMouseMove(sender As Object, e As MouseEventArgs)
		Dim item As ListViewItem = TryCast(sender, ListViewItem)
		If item IsNot Nothing AndAlso e.LeftButton.Equals(MouseButtonState.Pressed) Then
			DragDrop.DoDragDrop(item, item.DataContext, DragDropEffects.Move)
			item.IsSelected = True
			PlatformsList_SelectionChanged(Nothing, Nothing)
		End If
	End Sub


	Private Sub ListViewItem_Drop(sender As Object, e As DragEventArgs)
		'This is based on the C# code from here: http://stackoverflow.com/questions/3350187/wpf-c-rearrange-items-in-listbox-via-drag-and-drop

		Dim droppedData As PlatformWrapper = e.Data.GetData(GetType(PlatformWrapper))
		Dim target As PlatformWrapper = (DirectCast(sender, ListBoxItem)).DataContext

		Dim removedIndex = PlatformsList.Items.IndexOf(droppedData)
		Dim targetIndex = PlatformsList.Items.IndexOf(target)

		If removedIndex < targetIndex Then
			Using db As New IntelligenceDB()
				platforms.moveWrapperBeside(droppedData, target, False, db)
			End Using
		Else
			Dim remIdx = removedIndex + 1
			If platforms.Count + 1 > remIdx Then
				Using db As New IntelligenceDB()
					platforms.moveWrapperBeside(droppedData, target, True, db)
				End Using
			End If
		End If
	End Sub
End Class
