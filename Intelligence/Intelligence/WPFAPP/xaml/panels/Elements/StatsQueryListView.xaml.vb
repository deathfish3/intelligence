﻿Imports System.ComponentModel

Public Class StatsQueryListView
    Inherits UserControl
    Implements INotifyPropertyChanged

    Dim statsQueries As StatsQueryList

    Public ReadOnly Property addStatsQuery As ICommand
        Get
            Return New GenericCommand(
                Sub()
                    Dim newItem As Wrapper(Of StatsQuery) = statsQueries.addNewAndSave()
                    statsQueries.selectedItem = newItem
                End Sub)
        End Get
    End Property

    Public ReadOnly Property deleteStatsQuery As ICommand
        Get
            Return New DeleteCommand(Of StatsQuery)(statsQueries)
        End Get
    End Property

    Public Sub New()
        InitializeComponent()

        Me.statsQueries = New StatsQueryList(UserInfo.getCurrentIntelligenceUser().getWrappedItem)

        Me.DataContext = Me
        Me.rightClickOnListMenu.DataContext = Me

        StatsQueriesList.DataContext = statsQueries
    End Sub

    Private Sub StatsQueriesList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles StatsQueriesList.SelectionChanged
        RaiseEvent PropertyChanged(Me, NotifyPropertyChangedBase.getPropertyChangedArgs(Function() Me.deleteStatsQuery))
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.StatsQueriesList.SelectedItem = Nothing
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Private Sub Refresh_Click(sender As Object, e As RoutedEventArgs)
        statsQueries.reloadContents()
    End Sub
End Class
