﻿Imports WPFAPP.SelectTaskTemplates
Imports WPFAPP.PlatformSelectionEditor
Imports System.ComponentModel

Public Class VersionListView
    Inherits UserControl
    Implements INotifyPropertyChanged
    Implements IKeyboardShortcutListener
    Implements IClosable


    Dim parentPanel As CustomDockPanel
    Dim versions As VersionList

    Public Property addVersion As ICommand

    Public Sub New(parentPanel As CustomDockPanel, versions As VersionList)
        InitializeComponent()

        Me.parentPanel = parentPanel
        Me.versions = versions

        Me.DataContext = Me
        Me.rightClickOnListMenu.DataContext = Me

        VersionsList.DataContext = versions

        addVersion = New AddCommand(Of Version, VersionWrapper, VersionEditor)(Me, parentPanel, Function() versions, Sub(newVersion) Me.VersionsList_SelectionChanged(Nothing, Nothing))

        KeybindingContainer.AddListener(Me)

    End Sub

    Private Sub VersionsList_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles VersionsList.SelectionChanged
        Dim version As VersionWrapper = VersionsList.SelectedItem
        If version Is Nothing Then
            parentPanel.closeAllToRightOf(Me)
        Else
            Dim newEditPanel = New VersionEditor
            version.shouldLoadLeafNodeIndicators = True
            version.reloadWrappedItem()
            newEditPanel.DataContext = version
            Dim newPlatformPanel = If(version Is Nothing, Nothing, New PlatformsForTasksListView(parentPanel, version))
            parentPanel.showNewPanel(Me, newEditPanel, "Version Info")
            parentPanel.showNewPanel(newEditPanel, newPlatformPanel, "Platforms")
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("addTasks"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("deleteVersion"))
    End Sub

    Private Sub onDeselectPanelClicked(sender As Object, e As MouseButtonEventArgs)
        Me.VersionsList.SelectedItem = Nothing
    End Sub

    ReadOnly Property deleteVersion As ICommand
        Get
            Return New GenericCommand(
                Function(o) VersionsList.SelectedItem IsNot Nothing,
                Sub()
                    Using db As New IntelligenceDB
                        Dim selectedVersion As VersionWrapper = VersionsList.SelectedItem
                        If selectedVersion IsNot Nothing Then
                            Dim versionToDelete = db.Versions.Find(selectedVersion.id)
                            If versionToDelete IsNot Nothing Then
                                Dim dlg As New YesNoDialog("Are you sure you want to permanently delete this Version and all of it's Task Instances?" & vbNewLine & "Warning: There are " & db.TaskInstances.Where(Function(t) t.version.id = selectedVersion.id AndAlso t.assignee IsNot Nothing).Count & " assigned tasks.")
                                dlg.ShowDialog()
                                If dlg.DialogResult = True Then
                                    Dim instancesToDel = db.TaskInstances.Where(Function(t) t.version.id = selectedVersion.id).ToList
                                    Dim success As Boolean = True
                                    If instancesToDel IsNot Nothing Then
                                        db.TaskInstances.RemoveRange(instancesToDel)
                                        success = DBManager.saveChanges(db)
                                    End If

                                    If success Then
                                        db.Versions.Remove(versionToDelete)
                                        success = DBManager.saveChanges(db)
                                        If success Then
                                            versions.Remove(selectedVersion)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End Using
                End Sub)
        End Get
    End Property

    ReadOnly Property addTasks As ICommand
        Get
            Return New GenericCommand(
                Function(o) VersionsList.SelectedItem IsNot Nothing,
                Sub()
                    Dim selectedTasks As IEnumerable(Of TaskTemplateSelection) = Nothing
                    Dim selectedVersion As VersionWrapper = VersionsList.SelectedItem
                    Using db As New IntelligenceDB
                        If selectedVersion IsNot Nothing Then
                            Dim selectedProduct As ProductWrapper = selectedVersion.product
                            Dim tempSelProduct = db.Products.Find(selectedProduct.id)
                            Dim rootTasks As IEnumerable(Of TaskTemplate) =
                                From task As TaskTemplate In tempSelProduct.taskTemplates
                                Where task.parentTask Is Nothing
                                Order By task.rank Ascending, task.id Ascending
                                Select task

                            Dim instances As IEnumerable(Of TaskInstance) = db.TaskInstances.Where(Function(instance) selectedVersion.id = instance.version.id)
                            selectedTasks = SelectTaskTemplates.wrapAsTemplateSelection(rootTasks, instances)
                        End If
                    End Using

                    selectedTasks = CustomEditorDialog.showEditorDialog(selectedTasks, New SelectTaskTemplates)

                    'Add a button in VersionsPanel to call this in a separate command referencing the datacontext
                    If selectedTasks IsNot Nothing Then
                        LoadWithAsyncGUITask.run(Of Boolean)("Adding tasks.",
                                                             Function()
                                                                 Using db As New IntelligenceDB
                                                                     addSelectedTasks(selectedTasks, selectedVersion, db)
                                                                     Return True
                                                                 End Using
                                                             End Function,
                                                             Sub(success)
                                                                 If success Then selectedVersion.onTasksUpdated()
                                                             End Sub)
                    End If
                End Sub)
        End Get
    End Property

    Private Sub addSelectedTasks(selectedTasks As IEnumerable(Of TaskTemplateSelection), selectedVersion As VersionWrapper, db As IntelligenceDB)
        Dim startTime As DateTime = DateTime.Now
        Dim version As Version = db.Versions.Find(selectedVersion.id)
        Dim medium As Priority = db.Priorities.Find(TaskInstanceUtils.getDefaultPriorityForNewTaskInstance().id)
        Dim open As Resolution = db.Resolutions.Find(ResolutionUtils.openResolution.id)

        For Each task As TaskTemplateSelection In selectedTasks
            For Each platformSelection As PlatformSelection In task.platformsSelected
                Dim platform As Platform = db.Platforms.Find(platformSelection.platform.id)
                If platform IsNot Nothing Then
                    addSelectedTasks(task, startTime, version, platform, medium, open, db, False)
                End If
            Next
        Next
        Dim after As DateTime = DateTime.Now
        Dim duration = after - startTime
        Debug.Print("")
        Debug.Print("")
        Debug.Print("Took " & duration.TotalSeconds & " seconds to load (" & duration.TotalMilliseconds & ")ms")
        Debug.Print("")
        Debug.Print("")
    End Sub

    Private Sub addSelectedTasks(task As TaskTemplateSelection, now As DateTime, version As Version, platform As Platform, medium As Priority, open As Resolution, db As IntelligenceDB, parentWasGeneratedInThisPass As Boolean)
        'Debug.Print("Task " & task.taskTemplate.name & " isSelected = " & task.isSelected & " enabled = " & task.enabled)
        Dim generatedATaskThisIteration As Boolean = False
        If task.isSelected AndAlso task.enabled Then

            Dim templateFromDB As TaskTemplate = db.TaskTemplates.Find(task.taskTemplate.id)

            Dim platformSelection As PlatformSelection = task.platformsSelected.Where(Function(p) p.platform.Equals(platform)).FirstOrDefault

            If platformSelection IsNot Nothing Then
                'Debug.Print("Platform " & platform.name & " isSelected = " & task.isSelected)
                If platformSelection.isSelected AndAlso platformSelection.isEnabled Then

                    Dim newTaskInstance As New TaskInstance
                    db.TaskInstances.Add(newTaskInstance)

                    newTaskInstance.taskTemplate = templateFromDB
                    newTaskInstance.resolution = open
                    newTaskInstance.platform = platform
                    newTaskInstance.version = version
                    newTaskInstance.timeCreated = now
                    newTaskInstance.priority = medium

                    'db.TaskInstances.Add(newTaskInstance)
                    generatedATaskThisIteration = True
                    db.SaveChanges()

                    If Not parentWasGeneratedInThisPass Then
                        Dim parentTask As TaskInstance = TaskInstanceUtils.getParentInstance(newTaskInstance)
                        If parentTask IsNot Nothing Then
                            TaskInstanceUtils.setResolutionForInstance(newTaskInstance, open, Nothing, db)
                            TaskInstanceUtils.assignInstance(newTaskInstance, Nothing, Nothing, Nothing, db)
                            TaskInstanceUtils.setPriorityForInstance(newTaskInstance, medium, db)
                            db.SaveChanges()
                        End If
                    End If
                End If
            End If
        End If
        For Each subTask As TaskTemplateSelection In task.subTasks
            addSelectedTasks(subTask, now, version, platform, medium, open, db, generatedATaskThisIteration)
        Next
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Public Function onKeyboardShortcutPressed(key As KeyGesture) As Boolean Implements IKeyboardShortcutListener.onKeyboardShortcutPressed
        If key.Key = Input.Key.N AndAlso key.Modifiers = ModifierKeys.Control Then
            Me.addVersion.Execute(Nothing)
            Return True
        End If
        Return False
    End Function

    Public Sub close() Implements IClosable.close
        KeybindingContainer.RemoveListener(Me)
    End Sub
End Class
