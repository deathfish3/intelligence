﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure


<DbConfigurationType(GetType(MySql.Data.Entity.MySqlEFConfiguration))> _
Partial Public Class IntelligenceDB
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=IntelligenceDB")
    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
        Throw New UnintentionalCodeFirstException()
    End Sub

    Public Overridable Property Platforms() As DbSet(Of Platform)
    Public Overridable Property Products() As DbSet(Of Product)
    Public Overridable Property TaskInstances() As DbSet(Of TaskInstance)
    Public Overridable Property IntelligenceUsers() As DbSet(Of IntelligenceUser)
    Public Overridable Property Versions() As DbSet(Of Version)
    Public Overridable Property TaskTemplates() As DbSet(Of TaskTemplate)
    Public Overridable Property Bugs() As DbSet(Of Bug)
    Public Overridable Property Comments() As DbSet(Of Comment)
    Public Overridable Property TemplateAttachments() As DbSet(Of TemplateAttachment)
    Public Overridable Property InstanceAttachments() As DbSet(Of InstanceAttachment)
    Public Overridable Property Notifications() As DbSet(Of Notification)
    Public Overridable Property StatsQueries() As DbSet(Of StatsQuery)
    Public Overridable Property AccessLevels() As DbSet(Of AccessLevel)
    Public Overridable Property StatsQueryTypes() As DbSet(Of StatsQueryType)
    Public Overridable Property Resolutions() As DbSet(Of Resolution)
    Public Overridable Property Priorities() As DbSet(Of Priority)
    Public Overridable Property VersionResolutions() As DbSet(Of VersionResolution)

End Class
